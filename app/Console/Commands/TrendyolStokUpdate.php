<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\TrendyolJopController;

class TrendyolStokUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trendyol:stokupdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stok Güncelleme İşlemi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $controller = new TrendyolJopController();  
       $controller->stockUpdate();

       echo "Success";
   }
}
