<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\EmekController;

class emekupdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emek:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cat opt saved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new EmekController(); // make sure to import the controller
        $controller->updateBook();

        echo "Success";
    }
}
