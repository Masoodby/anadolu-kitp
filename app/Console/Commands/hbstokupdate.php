<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\HbController;

class hbstokupdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hb:stokupdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'hb orders saved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new HbController(); // make sure to import the controller
        $controller->stokUpdate();

        echo "Success";
    }
}
