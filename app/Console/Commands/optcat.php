<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\CategoryController;

class optcat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optcat:islem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cat opt saved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new CategoryController(); // make sure to import the controller
        $controller->optcat();

        echo "Success";
    }
}
