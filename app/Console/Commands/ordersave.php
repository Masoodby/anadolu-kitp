<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\TrendyolController;

class ordersave extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trendyol:ordersave';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'trendyol orders saved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new TrendyolController(); // make sure to import the controller
        $controller->saveOrders();

        echo "Success";
    }
}
