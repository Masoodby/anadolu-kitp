<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\XmlFeedController;

class sitemapcreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cat opt saved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new XmlFeedController(); // make sure to import the controller
        $controller->siteMapsCreate();

        echo "Success";
    }
}
