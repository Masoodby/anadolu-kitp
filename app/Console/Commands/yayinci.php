<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\BookController;

class yayinci extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yayinci:islem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Yayınevleri düzenledi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new BookController(); // make sure to import the controller
        $controller->yayinevi_islem();

        echo "Success";
    }
}
