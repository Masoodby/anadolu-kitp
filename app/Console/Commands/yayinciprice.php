<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\YayinciController;

class yayinciprice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yayinciprice:islem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Yayınevleri fiyat uygulandı';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new YayinciController(); // make sure to import the controller
        $controller->prices();

        echo "Success";
    }
}
