<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\BookController;

class yazar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yazar:islem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Yazar düzenledi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new BookController(); // make sure to import the controller
        $controller->yazar_islem();

        echo "Success";
    }
}
