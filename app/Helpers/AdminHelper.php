<?php



function sideNav()
{
 return [
  ['name'=>"Panel","icon"=>"ri-home-4-line","link"=>"crudv4.home","sub"=>false],


  ['name'=>"Sipariş Yönetimi","icon"=>"ri-shopping-cart-line","link"=>"#","sub"=>[
   ['name'=>"Site Siparişleri","icon"=>"ri-shopping-cart-2-line","link"=>'crudv4.order',"sub"=>false],
   ['name'=>"TrendYol Siparişleri","icon"=>"ri-shopping-cart-2-line","link"=>'crudv4.trendyol.orders',"sub"=>false],
   ['name'=>"HB Siparişleri","icon"=>"ri-shopping-cart-2-line","link"=>'crudv4.hb.orders',"sub"=>false],
   ['name'=>"N11 Siparişleri","icon"=>"ri-shopping-cart-2-line","link"=>'crudv4.n11.orders',"sub"=>false],

 ]
],


['name'=>"Katalog Yönetimi","icon"=>"ri-profile-line","link"=>"#","sub"=>[
 ['name'=>"Kategori Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.dnr.addYayinci',"sub"=>false],
 ['name'=>"Kategori Düzenleme","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.category.oldnew',"sub"=>false],
 ['name'=>"Ürün Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.product',"sub"=>false],
 ['name'=>"Ürün seti Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.combo',"sub"=>false],
 ['name'=>"Varyant Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.product',"sub"=>false],
 ['name'=>"Yayınevi Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.yayinevi',"sub"=>false],
 ['name'=>"Yazar Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.yazar',"sub"=>false],

]],
['name'=>"dnr resim ekel","icon"=>"ri-profile-line","link"=>"#","sub"=>[
 ['name'=>"resimsiz kitaplar","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.resimara',"sub"=>false],
 ['name'=>"Kategori Düzenleme","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.category.oldnew',"sub"=>false],
 ['name'=>"Ürün Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.product',"sub"=>false],
 ['name'=>"Varyant Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.product',"sub"=>false],
 ['name'=>"Yayınevi Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.yayinevi',"sub"=>false],
 ['name'=>"Yazar Yönetimi","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.yazar',"sub"=>false],

]],

['name'=>"Üye Yönetimi","icon"=>"ri-user-line","link"=>"#","sub"=>[
 ['name'=>"Yöneticiler","icon"=>"ri-file-list-line","link"=>'crudv4.user.admin',"sub"=>false],
 ['name'=>"Kullanıcılar","icon"=>"ri-file-list-line","link"=>'crudv4.user.member',"sub"=>false],


]],


['name'=>"İçerik Yönetimi","icon"=>"ri-todo-line","link"=>"#","sub"=>[
 ['name'=>"Slider Yönetimi","icon"=>"ri-gallery-line","link"=>'crudv4.slider',"sub"=>false],
 ['name'=>"Günlük fırsatlar Yönetimi","icon"=>"ri-gallery-line","link"=>'crudv4.tslider',"sub"=>false],
 ['name'=>"banner Yönetimi","icon"=>"ri-gallery-line","link"=>'crudv4.banner',"sub"=>false],
 ['name'=>"Ek Sayfalar","icon"=>"ri-file-list-line","link"=>'crudv4.pages',"sub"=>false],
 ['name'=>"Blog Sayfaları","icon"=>"ri-file-list-line","link"=>'crudv4.blogs',"sub"=>false],


]],


['name'=>"Kitap Entegrasyonu","icon"=>"ri-book-3-fill","link"=>"#","sub"=>[
 ['name'=>"Yeni Kitap Kontrol","icon"=>"ri-gallery-line","link"=>'crudv4.home',"sub"=>false],
 ['name'=>"Fiyat Güncellemeleri","icon"=>"ri-file-list-line","link"=>'crudv4.home',"sub"=>false],
 ['name'=>"Resim işlemleri","icon"=>"ri-file-list-line","link"=>'crudv4.home',"sub"=>false],


]],
['name'=>"Depo İşlemleri","icon"=>"ri-hard-drive-2-line","link"=>"#","sub"=>[
 ['name'=>"Depo Çıkış","icon"=>"ri-file-list-line","link"=>'crudv4.depolog.depoexit',"sub"=>false],
 ['name'=>"Tüm Depo Kitapları","icon"=>"ri-book-mark-line","link"=>'crudv4.depolog.depolist',"sub"=>false],
 ['name'=>"Depolar","icon"=>"ri-gallery-line","link"=>'crudv4.depo.add',"sub"=>false],
 ['name'=>"Tüm Odalar","icon"=>"ri-file-list-line","link"=>'crudv4.oda.add',"sub"=>false],
 ['name'=>"Depo Analitiği","icon"=>"ri-pie-chart-fill","link"=>'crudv4.depolog.analitik',"sub"=>false],
 ['name'=>"Depo Logları","icon"=>"ri-pie-chart-fill","link"=>'crudv4.depolog.depoLogs',"sub"=>false],


]],

['name'=>"Pazar Yerleri","icon"=>"ri-store-2-line","link"=>"#","sub"=>[


 ['name'=>"Pazar Yerleri Tanımları","icon"=>"ri-file-list-line","link"=>'crudv4.marketplaces.add',"sub"=>false],

 ['name'=>"TrendYol","icon"=>"ri-store-3-fill","link"=>'#',"sub"=>[
  ['name'=>"Dashboard","icon"=>"ri-refresh-fill","link"=>'crudv4.trendyol',"sub"=>false],
  ['name'=>"Kategori Eşleştir","icon"=>"ri-database-line","link"=>'crudv4.trendyol.catmatch',"sub"=>false],
  ['name'=>"Marka Eşleştir","icon"=>"ri-award-fill","link"=>'crudv4.trendyol.brand',"sub"=>false],
  ['name'=>"Onaylanmış Ürünler","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.trendyol.products',"sub"=>false],
]],

['name'=>"HB","icon"=>"ri-store-3-fill","link"=>'#',"sub"=>[
  ['name'=>"Dashboard","icon"=>"ri-refresh-fill","link"=>'crudv4.hb',"sub"=>false],
  ['name'=>"Kategori Eşleştir","icon"=>"ri-database-line","link"=>'crudv4.hb.catmatch',"sub"=>false],
  ['name'=>"Marka Eşleştir","icon"=>"ri-award-fill","link"=>'crudv4.hb.brand',"sub"=>false],
  ['name'=>"Onaylanmış Ürünler","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.hb.products',"sub"=>false],
]],

['name'=>"N11","icon"=>"ri-store-3-fill","link"=>'#',"sub"=>[
  ['name'=>"Dashboard","icon"=>"ri-refresh-fill","link"=>'crudv4.n11',"sub"=>false],
  ['name'=>"Kategori Eşleştir","icon"=>"ri-database-line","link"=>'crudv4.n11.catmatch',"sub"=>false],
  ['name'=>"Marka Eşleştir","icon"=>"ri-award-fill","link"=>'crudv4.n11.brand',"sub"=>false],
  ['name'=>"Onaylanmış Ürünler","icon"=>"ri-folder-shield-2-line","link"=>'crudv4.n11.products',"sub"=>false],
]],



]

],



['name'=>"Genel Ayarlar","icon"=>"ri-list-settings-line","link"=>"crudv4.settings.index","sub"=>false],
];
}


function formElement()
{

  return ['input_text','input_group_text','input_file','textarea','tags','selectbox','radio','checkbox'];
}

function konum()
{

  return ['product_konum_home'=>"AnaSayfa",'product_konum_new'=>"Yeni",'product_konum_today'=>"Günün Ürünü",'product_konum_cat'=>"Kategori Side",'product_konum_cok'=>"Çok Satan",'product_konum_ind'=>"İndirimliler",'prduct_konum_firsat'=>'Firsetler'];
}

function addSelectone($txt='',$title = "")
{
  $append = '<option value="0">'.$title.'</option>';

  $parts =  explode('>', $txt);

  $parts[1] = $append.$parts[1];

  return implode(">", $parts);


}


function seoLimit($key = "page")
{
  $limits = [
    "title"=>160,
    "desc"=>260,
    "keyw"=>360,
  ];

  return $limits[$key];
}


function prep($val)
{
  echo "<pre>";
  print_r($val);
  echo "</pre>";
  die();
}





function bildirim()
{
 return false;
}


function mesajlar()
{
 return false;
}


function loginUser()
{
  return \Auth::guard('yonetim')->user();
}


function dizi_creat_sayi($number = 50,$start = 1)
{
  $arrayName = array();
  for ($i=$start; $i < $number+1; $i++) {
    $arrayName[$i] = $i;
  }
  return $arrayName;
}

function dizi_creat_harf($number = 50)
{
  $hafr = 'x A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
  $harfs = explode(' ', $hafr);
  $arrayName = array();
  for ($i=1; $i < $number+1; $i++) {
    $arrayName[$i] = $harfs[$i];
  }
  return $arrayName;
}

function rafrenk($adet)
{
  switch ($adet) {
    case $adet <20:
    $renk = "bg-light";
    break;

    case $adet > 21 && $adet <= 40:
    $renk = "bg-warning";
    break;

    case $adet >40:
    $renk = "bg-success";
    break;

    default:
    $renk = "";
    break;
  }

  echo $renk;
}


function barkod($q = 0)
{
 $q = str_replace('-', '', $q);
 $q = str_replace('_', '', $q);
 $q = str_replace(' ', '', $q);

 return $q;
}

function depoIslem($adet)
{

  if ($adet <0) {
   $return =  "Depo Çıkışı";
 }elseif($adet >0){
  $return = "Depo Girişi";
}else{
  $return = "Depodan Kaldırma";
}


return $return;


}


function trendYolOrderStatus($i = "")
{
  $dizi = [

    "Awaiting"=>"Ödeme Bekliyor",
    "Created"=>"Yeni",
    "Picking"=>"Hazırlanıyor",
    "Invoiced"=>"Fatura Kesildi",
    "Shipped"=>"Kargoya Verildi",
    "Cancelled"=>"İptal",
    "Delivered"=>"Teslim",
    "UnDelivered"=>"Teslim Edilemedi",
    "Returned"=>"Geri İade",
    "Repack"=>"Değişim",
    "UnSupplied"=>"Tedarik Edilemedi"

  ];

  if ($i == "") {
    return $dizi;
  }else{
    return $dizi[$i];
  }



}

function n11OrderStatus($i = "")
{
  $dizi = [

    "New"=>"Yeni siparişler",
    "Approved"=>"Onaylanmış Siparişler",
    "Rejected"=>"İptal Edilmiş Siparişler",

    "Shipped"=>"Kargolanmış Siparişler",

    "Delivered"=>"Teslim  Edilen Siparişler",
    "Completed"=>"Tamamlanmış Siparişler",
    "“Claimed”"=>"İptal-İade-Değişim Durumundaki Siparişler",
    "LATE_SHIPMENT"=>"Kargolanması Geciken Siparişler",


  ];

  if ($i == "") {
    return $dizi;
  }else{
    return $dizi[$i];
  }



}



function n11SaleStatus($key=0)
{
  $dizi = [1=>  "Satış Öncesi",2=>"Satışta",3=>"Stok yok",4=>"Satışa kapalı"];

  if ($key>0) {
   return isset($dizi[$key])?$dizi[$key]:"";
 }else{
  return $dizi;
}

}


function n11ApprovalStatus($key=0)
{
  $dizi = [ 1 =>  "Aktif",2=>"Beklemede",3=>"Yasaklı"];


  if ($key>0) {
    return isset($dizi[$key])?$dizi[$key]:"";
  }else{
    return $dizi;
  }

}

function eraseIn($a,$b)
{
  $a = str_replace($b, '', $a);
  return $a;
}

function kisalt($a, $i,$dots = true) {
  $b = mb_substr($a, 0, $i, "UTF-8");
  if (strlen($a) > $i && $dots == true) {
    $b = $b . "...";
  }
  return $b;
}


function lokasyon($barkod='')
{
 $nerde =  App\Models\DepoBook::with('getBook','getRaf.getOda')->orderBy('adet','asc')->where('isbn',$barkod)->where('adet','>',0)->get();

 $ret = [];
 if ($nerde) {



   foreach ($nerde as $key => $value) {

    $indis = $value->id;

    $ret[$indis] = [
      "oda"=>$value->getRaf->getOda->oda_name,
      "raf"=>$value->getRaf->raf_name,
      "adet" =>$value->adet,
    ];

  }

}
return $ret;



}


function nerde($barkod='',$adet = false)
{
  $lokasyon = lokasyon($barkod);

  if (count($lokasyon) > 0) {


    $ret = [];
    foreach ($lokasyon as $key => $var) {

      $adets = $adet?" (".$var["adet"]." adet)":"";

      $ret[$key] = $var["oda"].' - '.$var['raf'].$adets;
    }

    return implode(" | ", $ret);

  }else{
    return "";
  }

}


function barkod_rep($barkod)
{
  if (preg_match('/-/i', $barkod)) {
   $barkods = explode('-', $barkod);
   return $barkods[0];
 }
 return $barkod;
}


