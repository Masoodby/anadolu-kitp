<?php


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

function theme()
{
	return "gurgen";
}

if (!function_exists('set')) {
	
	function set($key='')
	{
		try {

			$settings = App\Models\Setting::where('settings_key',$key)->firstOrFail();

			return $settings->settings_val;

		} catch (ModelNotFoundException $ex) {

			return ""; 
		}
		
	}
}
if (!function_exists('setAll')) {
	
	function setAll($key='')
	{
		try {

			return  App\Models\Setting::where('settings_group',$key)->get();
			
		} catch (ModelNotFoundException $ex) {

			return []; 
		}
		
	}
}


if (!function_exists('setpar')) {
	
	function setpar($key='')
	{
		
		$val = set($key);

		if (!empty($val)) {
			return explode(',', $val);
		}else{
			return [];
		}
	}
}

if (!function_exists('nav')) {
	
	function nav()
	{
		return App\Models\Category::firstUlAttr(['class'=> 'sm pixelstrap sm-vertical','id'=>"sub-menu"])->renderAsHtml();;
		
		
	}
}



if (!function_exists('getProduct')) {
	
	function getProduct($konum="product_konum_home",$limit=10)
	{
		return  DB::table('products')
		->leftJoin('categories', 'categories.id', '=', 'products.product_cat')
		->leftJoin('product_details', 'product_details.product_id', '=', 'products.id')
		->select('products.*', 'categories.categories_name', 'categories.categories_url', 'categories.id as cid', 'product_details.*')
		->where($konum, 1)
		
		->where('products.is_active', 1)
		->limit($limit)
		->orderBy('products.id', 'desc')
		->get();

	}
}

if (!function_exists('groupcats')) {
	
	function groupcats($dizi)
	{
		$cats = [];

		foreach ($dizi as $key => $var) {
			$key = $var->cid;
			$cats[$key] = $var->categories_name;
		}

		return $cats;

	}
}


if (!function_exists('yuzde')) {
	function yuzde($a,$b)
	{

		if ($b>0 && $a>0) {			

			$oran = $b*100/$a;
			$oranx = 100-$oran;
			return "%".ceil($oranx);

		}else{
			return "%25";
		}
	}

}

if (!function_exists('yuzdex')) {
	function yuzdex($price='',$oran)
	{
		return round($price-($price*$oran)/100,2);
	}

}
if (!function_exists('yuzdePlus')) {
	function yuzdePlus($price='',$oran)
	{
		return round($price+($price*$oran)/100,2);
	}

}




if (!function_exists('get_tree')) {


	function get_tree($id)
	{

		global $tree;

		$cat = DB::table('categories')->where('id',$id)->where('is_active',1)->whereNull('deleted_at')->first();

		if ($cat->categories_main == 0) {
			$tree[$id] = ['categories_url'=>$cat->categories_url,'id'=>$cat->id,'categories_name'=>$cat->categories_name];

		}else{		
			$tree[$id] = ['categories_url'=>$cat->categories_url,'id'=>$cat->id,'categories_name'=>$cat->categories_name];
			get_tree($cat->categories_main);

		}

		return array_reverse($tree);
	}

}


if (!function_exists('get_main')) {


	function get_main($id)
	{
		

		$cat = DB::table('categories')->where('id',$id)->where('is_active',1)->whereNull('deleted_at')->first();

		if ($cat->categories_main == 0) {
			return $cat;

		}else{		

			return get_main($cat->categories_main);

		}

		
	}

}


if (!function_exists('nav_cat')) {


	function nav_cat()
	{


		return App\Models\Category::nested()->get();

		
	}

}


if (!function_exists('staticPages')) {


	function staticPages($type=1,$limit=10)
	{


		return DB::table('pages')->where('page_type',$type)->where('is_active',1)->whereNull('deleted_at')->limit($limit)->get();

		
	}

}


if (!function_exists('staticPage')) {


	function staticPage($id = 0)
	{


		return DB::table('pages')->where('id',$id)->where('is_active',1)->whereNull('deleted_at')->first();

		
	}

}


if (!function_exists('satisSozlesme')) {


	function satisSozlesme($text = 0)
	{

		$text =  str_replace('##!UNVAN!##', set('unvan'), $text);

		return $text;

		
	}

}



if (!function_exists('get_child')) {


	function get_child($id)
	{

		global $idler;

		$cat = DB::table('categories')->where('categories_main',$id)->where('is_active',1)->get();

		$idler[$id] = $id;

		foreach ($cat as $var) {

			$ids = $var->id;
			$idler[$ids] = $var->id;
			get_child($ids);

		}

		return $idler;
		
	}

}






if (!function_exists('getProductAll')) {


	function getProductAll($cid,$limit=10)
	{

		$idler = get_child($cid);

		return  DB::table('products')
		->leftJoin('categories', 'categories.id', '=', 'products.product_cat')
		->leftJoin('product_details', 'product_details.product_id', '=', 'products.id')
		->select('products.*', 'categories.categories_name', 'categories.categories_url', 'product_details.*')
		->whereIn('product_cat', $idler)
		->where('products.is_active', 1)
		->limit($limit)
		->orderBy('products.id', 'desc')
		->get();
		
	}

}


if (!function_exists('ucwords_tr')) {
	function ucwords_tr($gelen){

		$sonuc='';
		$kelimeler=explode(" ", $gelen);

		foreach ($kelimeler as $kelime_duz){

			$kelime_uzunluk=strlen($kelime_duz);
			$ilk_karakter=mb_substr($kelime_duz,0,1,'UTF-8');

			if($ilk_karakter=='Ç' or $ilk_karakter=='ç'){
				$ilk_karakter='Ç';
			}elseif ($ilk_karakter=='Ğ' or $ilk_karakter=='ğ') {
				$ilk_karakter='Ğ';
			}elseif($ilk_karakter=='I' or $ilk_karakter=='ı'){
				$ilk_karakter='I';
			}elseif ($ilk_karakter=='İ' or $ilk_karakter=='i'){
				$ilk_karakter='İ';
			}elseif ($ilk_karakter=='Ö' or $ilk_karakter=='ö'){
				$ilk_karakter='Ö';
			}elseif ($ilk_karakter=='Ş' or $ilk_karakter=='ş'){
				$ilk_karakter='Ş';
			}elseif ($ilk_karakter=='Ü' or $ilk_karakter=='ü'){
				$ilk_karakter='Ü';
			}else{
				$ilk_karakter=strtoupper($ilk_karakter);
			}

			$digerleri=mb_substr($kelime_duz,1,$kelime_uzunluk,'UTF-8');
			$sonuc.=$ilk_karakter.kucuk_yap($digerleri).' ';

		}

		$son=trim(str_replace('  ', ' ', $sonuc));
		return $son;

	}
}

if (!function_exists('kucuk_yap')) {
	function kucuk_yap($gelen){

		$gelen=str_replace('Ç', 'ç', $gelen);
		$gelen=str_replace('Ğ', 'ğ', $gelen);
		$gelen=str_replace('I', 'ı', $gelen);
		$gelen=str_replace('İ', 'i', $gelen);
		$gelen=str_replace('Ö', 'ö', $gelen);
		$gelen=str_replace('Ş', 'ş', $gelen);
		$gelen=str_replace('Ü', 'ü', $gelen);
		$gelen=strtolower($gelen);

		return $gelen;
	}
}


if (!function_exists('price')) {
	function price($para = 0)
	{
		$paraDizi = explode('.', $para);
		if(count($paraDizi)>1){
			if (strlen($paraDizi[1]) < 2) {
				$kurus = $paraDizi[1].'0';
			}else{
				$kurus = substr($paraDizi[1],0,2);
			}

			$para = $paraDizi[0].'.'.$kurus;
		}else{
			$para = $para.'.00';
		}

	//money_format($para,2).' TL';


		return number_format($para,2).' TL';
	}
}

if (!function_exists('countcart')) {
	function countcart()
	{

		return Cart::count();
	}
}


if (!function_exists('filterparameters')) {
	function filterparameters($data)
	{
		$ret = [];



		foreach ($data as $key => $var) {

			$yazar_key = $var->extend->getYazar->yazar_url;
			$yayinci_key = $var->extend->getYayinci->yayinci_url;


			if (isset($ret['yazar_adet'][$yazar_key])) {

				$ret['yazar_adet'][$yazar_key]++;
			}else{

				$ret['yazar_adet'][$yazar_key] = 1;
			}


			if (isset($ret['yayinci_adet'][$yayinci_key])) {

				$ret['yayinci_adet'][$yayinci_key]++;
			}else{

				$ret['yayinci_adet'][$yayinci_key] = 1;
			}



			$ret['yazar'][$yazar_key] =  ["name" => $var->extend->getYazar->yazar_name,"slug"=>$var->extend->getYazar->yazar_url];
			$ret['yayinci'][$yayinci_key] =  ["name" => $var->extend->getYayinci->yayinci_name,"slug"=>$var->extend->getYazar->yayinci_url];
			$ret['price'][$var->product_price] = $var->product_price;
			if ($var->detail->product_konum_ind == 1) {
				$ret['discount'][] = $var;
			}
			
		}

		return $ret;
	}
}

if (!function_exists('in_array_string')) {
	function in_array_string($string,$key)
	{

		$dizi = explode('-', $string);

		if (in_array($key, $dizi)) {
			return TRUE;
		}else{
			return FALSE;
		}

	}
}

if (!function_exists('pay_metod')) {
	function pay_metod($id=0)
	{
		$dizi = [1=>'Kredi Kartı/Banka Kartı',2=>'Banka Havalesi',3=>"Kapıda Ödeme"];
		unset($dizi[3]);
		if ($id == 0) {
			return $dizi;
		}else{
			return $dizi[$id];
		}
	}
}


if (!function_exists('pay_detail')) {
	function pay_detail($id=0)
	{
		$dizi = [
			1=>'Bir sonraki aşamada 3D Secure Yöntemi ile Ödeme Yapabilirsiniz',
			2=>'Hesap numaralarımıza Banka Havalesi ile transfer yaparak siparişinizi tamamlayabilirsiniz. Para Transfer açıklamasına E-Posta Adresinize iletilecek sipariş kodunu girmeyi unutmayınız',
			3=>"Kapıda Ödeme Hizmet bedeli dahil edilerek siparişinizi tamamlayabilirsiniz"
		];
		unset($dizi[3]);
		if ($id == 0) {
			return $dizi;
		}else{
			return $dizi[$id];
		}
	}
}



if (!function_exists('order_durum')) {

	function order_durum($no= -1)
	{


		$dizi = [
			0=>"Onay Bekliyor",
			1=>"Kargo için Hazırlanıyor",
			2=>"Kargoya verildi",
			3=>"Teslim Edildi",
			4=>"Teslim Edilemedi",
			5=>"İptal edildi",
			6=>"İade edildi",
		];

		if ($no >=0) {
			return $dizi[$no];
		}else{
			return $dizi;
		}

	}
}

function SMSXMLPOST($PostAddress,$xmlData)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$PostAddress);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,2);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
	$result = curl_exec($ch);
	return $result;
}

function smsSend($to,$mesaj)
{
	$xml='<?xml version="1.0" encoding="UTF-8"?>
	<mainbody>
	<header>
	<company dil="TR">Netgsm</company>        
	<usercode>2129094730</usercode>
	<password>swd9914713</password>
	<type>1:n</type>
	<msgheader>ANADOLUSRGI</msgheader>
	</header>
	<body>
	<msg>
	<![CDATA['.$mesaj.']]>
	</msg>
	<no>'.telno($to).'</no>

	</body>
	</mainbody>';

	$gelen=SMSXMLPOST('https://api.netgsm.com.tr/sms/send/xml',$xml);
	return $gelen;
}

function telno($telno = ''){
	$telno = str_replace(' ','',$telno);
	$telno = str_replace('-','',$telno);
	$telno = str_replace('(','',$telno);
	$telno = str_replace(')','',$telno);
	return trim($telno);
}


function olt($key ="",$label ="hizliform") 
{
	if (session()->has($label)) {
		$data = session($label);

		return isset($data[$key])?$data[$key]:"";
	}

	return "";
}

function hbSku($sku)
{
	$urun =  DB::table("product_market_place")->where('market_product_code',$sku)->first();
	return $urun->spec_barcode;
}

function hbSkuPro($sku)
{
	$urun =  DB::table("product_market_place")->leftJoin('products', 'products.id', '=', 'product_market_place.product_id')->where('market_product_code',$sku)->first();
	return $urun;
}

function kalanSure($tarih)
{
	$simdiki_tarih = \Carbon\Carbon ::now();
	$ileriki_tarih = \Carbon\Carbon ::parse($tarih);

	$saniye_farki = $simdiki_tarih -> diffInSeconds($ileriki_tarih, false);
	$dakika_farki = $simdiki_tarih -> diffInMinutes($ileriki_tarih, false);
	$saat_farki   = $simdiki_tarih -> diffInHours($ileriki_tarih, false);
	$gun_farki    = $simdiki_tarih -> diffInDays($ileriki_tarih, false);
	$ay_farki     = $simdiki_tarih -> diffInMonths($ileriki_tarih, false);
	$yil_farki    = $simdiki_tarih -> diffInYears($ileriki_tarih, false);

	$gun = $gun_farki;
	$saat = $saat_farki%24;
	$ddk = $dakika_farki%60;
	return  $gun_farki. ' gün, '.$saat.' saat, '.$ddk.' dk';
}




