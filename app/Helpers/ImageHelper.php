<?php

use Intervention\Image\ImageManager;



function r_size($key = "page")
{

	$sizes = [
		"cat"=>["160x160"],
		"product"=>[  "250x250", "350x350", "500x500"],
		"combo"=>[  "250x250", "350x350", "500x500"],
		"page"=>["160x160"],
		"blog"=>["240x160","730x490"],
		"slider"=>["510x395","840x395"],
		"banner"=>["375x250"],
		"yayinci_logo"=>["185x160"]
	];

	return $sizes[$key];
}



function jpegCreate($src = "",$name = "", $folder='')
{

	$upload_page = public_path('uploads/'.$folder.'/jpg/');

	makeDir($upload_page);

	try {
		Image::make($src)->encode("jpg", 75)->save($upload_page.$name, 80,"jpg");
	} catch (Exception $e) {

	}




}


function my_upload_files($src,$isbn = "", $folder='')
{

	$upload_page = public_path('files/'.$folder.'/jpg/');

	$jpg = public_path('files/'.$folder.'/'.$isbn.'.jpg');
	$wm = public_path('logo.png');


	Image::make($src)->encode("jpg" ,75)->save($jpg);

	$name = $isbn.'.jpg';

	makeDir($upload_page);
	makeDir($upload_page.'thumb');
	try {


		Image::make($src)->insert($wm, 'bottom-right', 10, 10)->encode("jpg", 75)->save($upload_page.$name, 80,'jpg');


	//Image::make($src)->fit(100, 100)->save($upload_page.'thumb/100x100_'.$name);

		foreach (["250x250", "350x350", "500x500"] as $key => $sizeItem) {

			$size = explode('x', $sizeItem);


			Image::make($src)->insert($wm, 'bottom-right', 10, 10)->resize($size[0], $size[1],
				function ($constraint) {
					$constraint->aspectRatio();
				})
			->resizeCanvas($size[0], $size[1])
			->encode("jpg", 75)
			->save($upload_page.'thumb/'.$sizeItem.'_'.$name);

		}

	} catch (Exception $e) {

	}
}




function my_upload($src = "",$name = "", $folder='',$sizes="",$encode = "jpg")
{

	$upload_page = public_path('uploads/'.$folder.'/');

	makeDir($upload_page);
	makeDir($upload_page.'thumb');
	Image::make($src)->encode($encode, 75)->save($upload_page.$name, 80,$encode);



	Image::make($src)->resize(100, 100,
		function ($constraint) {
			$constraint->aspectRatio();
		})
	->resizeCanvas(100, 100)
	->encode($encode, 75)
	->save($upload_page.'thumb/100x100_'.$name);





	//Image::make($src)->fit(100, 100)->save($upload_page.'thumb/100x100_'.$name);

	foreach ($sizes as $key => $sizeItem) {

		$size = explode('x', $sizeItem);


		Image::make($src)->resize($size[0], $size[1],
			function ($constraint) {
				$constraint->aspectRatio();
			})
		->resizeCanvas($size[0], $size[1])
		->encode($encode, 75)
		->save($upload_page.'thumb/'.$sizeItem.'_'.$name);

	}
}

function res($folder='',$name="",$boyut = false)
{
	$upload_page = public_path('uploads/'.$folder.'/');


	if ($boyut == true) {

		if (file_exists($upload_page.'/thumb/'.$boyut.'_'.$name)) {
			return asset('uploads/'.$folder.'/thumb/'.$boyut.'_'.$name);
		}else{

			return asset('uploads/noimage.jpg');
		}

	}else{
		return asset('uploads/'.$folder.'/'.$name);
	}


}

function res2($folder='',$name="",$boyut = false)
{
	$upload_page = public_path('files/'.$folder.'/jpg');


	if ($boyut == true) {

		if (file_exists($upload_page.'/thumb/'.$boyut.'_'.$name)) {
			return asset('files/'.$folder.'/jpg/thumb/'.$boyut.'_'.$name);
		}else{

			return asset('uploads/noimage.jpg');
		}

	}else{
		return asset('files/'.$folder.'/'.$name);
	}


}

function ress($isbn,$folder,$size = false)
{


	$upload_page = public_path('files/'.$folder.'/jpg');


	if ($size == true) {

		if (file_exists($upload_page.'/thumb/'.$size.'_'.$isbn.'.jpg')) {
			return asset('files/'.$folder.'/jpg/thumb/'.$size.'_'.$isbn.'.jpg');
		}else{

			return asset('uploads/noimage.jpg');
		}

	}else{
		if (file_exists($upload_page.'/'.$isbn.'.jpg')) {
			return asset('files/'.$folder.'/jpg/'.$isbn.'.jpg');
		}else{
			return asset('uploads/noimage.jpg');
		}

	}





}

function jres($val='',$key=0)
{
	if (strlen($val) > 5) {
		$json = json_decode($val,TRUE);
	}else{

		$json[$key] = "";
	}


	return $json[$key];
}

function makeDir($path='')
{
	if(!is_dir($path))
	{
		mkdir($path,0755,TRUE);
	}

}

function userImage($size = [64,64],$name = "A",$sname="B")
{
	$img = Image::canvas(intval($size[0]),intval($size[1]), '#f4f4f4');

	$img->text(ucwords_tr($name[0]).ucwords_tr($sname[0]), intval($size[0]/2),intval($size[1]/2), function($font) use ($size) {
		$font->file('font/Roboto-Medium.ttf');
		$font->size($size[0]/2);
		$font->color('#333333');
		$font->align('center');
		$font->valign('middle');
		$font->angle(0);
	})->stream('data-url');

	echo  $img;


}



function removeRes($folder,$res,$size = ["250x250", "350x350", "500x500"])
{
	$upload_page = public_path('files/'.$folder.'/');
	@unlink($upload_page.$res.'.jpg');
	@unlink($upload_page.'jpg/'.$res.'.jpg');
	@unlink($upload_page.'jpg/thumb/100x100_'.$res);

	foreach (r_size($size) as $key => $item) {
		@unlink($upload_page.'jpg/thumb/'.$item.'_'.$res);
	}
}
