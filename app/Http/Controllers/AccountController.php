<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserAdres;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Str;

use App\Models\Basket;
use App\Models\BasketProduct;
use Cart;

use App\Models\Support;
use App\Models\SubMessage;

class AccountController extends Controller
{
	function index()
	{
		$active = "index";		

		$adres = UserAdres::where(['user_id' => auth()->id(), 'adres_current' => 1,'adres_tur' => 0])->first();
		$user = auth()->user();
		$lastorders = Order::with('baskets.basket_products')->where(['user_id' => auth()->id()])->orderByDesc('id')->limit(5)->get();

		return view(theme().'.panel.index',compact( 'active',"user","adres","lastorders"));
	}

	function orders()
	{
		$active = "orders";
		$user = auth()->user();
		$orders = Order::with('baskets.basket_products','address','fatadres')->where(['user_id' => auth()->id()])->orderByDesc('id')->get();
		return view(theme().'.panel.orders',compact( 'active','orders','user'));
	}

	function order($id = 0)
	{

		$user = auth()->user();
		$order = Order::with('baskets.basket_products.product','address','fatadres')->where(['user_id' => auth()->id(),'id'=>$id])->first();
		return view(theme().'.panel.order-detay',compact( 'order','user'));
	}

	function adres()
	{
		$active = "adres";

		$adresler = UserAdres::where(['user_id' => auth()->id()])->get();

		return view(theme().'.panel.adres',compact( 'active',"adresler"));
	}

	function support()
	{
		$active = "info";
		$title = "Destek Talepleri";
		$supports = Support::where(['user_id' => auth()->id()])->get();

		return view(theme().'.panel.support',compact( 'active',"title", "supports"));
	}


	function editProfil()
	{
		$active = "hesap";
		$title = "Hesap Bilgilerim";
		$user_detail = auth()->user()->getDetail;

		return view(theme().'.panel.hesap',compact( 'active',"title", "user_detail"));
	}



	function editProfilSave(Request $request)
	{

		$user = auth()->user();



		$this->validate($request, [
			'name' => 'required',
			'sname' => 'required',


		]);

		if ($request->has(['pass', 'oldpass'])) {

			if (Hash::check($request->oldpass, $user->pass)) { 
				$user->fill([
					'pass' => Hash::make($request->pass),
					'name' =>  $request->name,
					'sname' =>  $request->sname,
					
				])->save();


			}else{

				$user->fill([					
					'name' =>  $request->name,
					'sname' =>  $request->sname,
					
				])->save();
			}

			return response()->json(['statu'=>'success','message'=>"Bilgiler Güncellendi"]);




		} else {


			return response()->json(['statu'=>'error','message'=>"Eski Şifre Eşleşmedi"]);

		}


	}





	function adresForm()
	{

		$cities = DB::table('city_counties')->groupBy('city')->get();
		$user = auth()->user();



		if (request()->has('id')) {
			$id = request("id");
			$adres = UserAdres::where(['user_id' => auth()->id(),'id' => $id])->first();
			$ilceler = DB::table('city_counties')->where('city',$adres->adres_il)->get();

			return view(theme().'.panel.edit_adress',compact( 'user','cities','adres','ilceler'));

		}else{
			return view(theme().'.panel.add_adress',compact( 'user','cities'));
		}

	}
	

	function fatadresForm()
	{

		$cities = DB::table('city_counties')->groupBy('city')->get();
		$user = auth()->user();



		if (request()->has('id')) {
			$id = request("id");
			$adres = UserAdres::where(['user_id' => auth()->id(),'id' => $id])->first();
			$ilceler = DB::table('city_counties')->where('city',$adres->adres_il)->get();

			return view(theme().'.panel.edit_fat_adress',compact( 'user','cities','adres',"ilceler"));

		}else{
			return view(theme().'.panel.add_fat_adress',compact( 'user','cities'));

		}
	}


	function addresAdd()
	{


		UserAdres::where(['user_id' => auth()->id(),'adres_current' => 1,'adres_tur' => 0])->update(['adres_current' => 0]);


		$adresData = [
			'adres_title' => request("adres_title"), 
			'adres_name' => request("adres_name"),		
			'adres_sname' =>  request("adres_sname"),		
			'adres_il' => request("adres_il"),		
			'adres_ilce' => request("adres_ilce"),		
			'adres_detay' => request("adres_detay"),		
			'adres_gsm' => request("adres_gsm"),		
			'adres_current' => 1,		
			'adres_tur' => 0,	
			'user_id' => auth()->id(),	

		];



		UserAdres::create($adresData);


		if (request()->has('fatura-info-onay')) {


			$fatData = [
				'adres_title' => request("adres_title"), 
				'adres_name' => request("adres_name"),		
				'adres_sname' =>  request("adres_sname"),		
				'adres_il' => request("adres_il"),		
				'adres_ilce' => request("adres_ilce"),		
				'adres_detay' => request("adres_detay"),		
				'adres_gsm' => request("adres_gsm"),		
				'adres_current' => 1,		
				'adres_tur' => 1,		
				'fatura_unvan' => request("fatura_unvan"),		
				'fatura_vd' => request("fatura_vd"),		
				'fatura_vno' => request("fatura_vno"),		
				'fatura_tck' => request("fatura_tck"),		
				'fatura_tur' => request("fatura_tur"),	
				'user_id' => auth()->id(),		
			];

			if (request("equalask") == 1 ) {
				$fatData["adres_il"] = request("fat_adres_il");
				$fatData["adres_ilce"] = request("fat_adres_ilce");
				$fatData["adres_detay"] = request("fat_adres_detay");
			}

			UserAdres::where(['user_id' => auth()->id(),'adres_current' => 1,'adres_tur' => 1])->update(['adres_current' => 0]);
			UserAdres::create($fatData);


		}

		echo 1;

	}

	
	function fataddresAdd()
	{


		UserAdres::where(['user_id' => auth()->id(),'adres_current' => 1,'adres_tur' => 1])->update(['adres_current' => 0]);

		


		$fatData = [
			'adres_title' => request("adres_title"),
			'adres_name' => request("adres_name"),		
			'adres_sname' =>  request("adres_sname"),		
			'adres_il' => request("adres_il"),		
			'adres_ilce' => request("adres_ilce"),		
			'adres_detay' => request("adres_detay"),		
			'adres_gsm' => request("adres_gsm"),		
			'adres_current' => 1,		
			'adres_tur' => 1,		
			'efatura' => request()->has('efatura')?1:0,		
			'fatura_unvan' => request("fatura_unvan"),		
			'fatura_vd' => request("fatura_vd"),		
			'fatura_vno' => request("fatura_vno"),		
			'fatura_tck' => request("fatura_tck"),		
			'fatura_tur' => request("fatura_tur"),	
			'user_id' => auth()->id(),		
		];


		UserAdres::create($fatData);



		echo 1;

	}




	function adresEdit($id = 0)
	{
		$adres_current = 0;
		if (request()->has('fatura-info-onay')) {

			UserAdres::where(['user_id' => auth()->id(),'adres_current' => 1,'adres_tur' => 0])->update(['adres_current' => 0]);
			$adres_current = 1;
		}

		$adresData = [
			'adres_title' => request("adres_title"), 
			'adres_name' => request("adres_name"),		
			'adres_sname' =>  request("adres_sname"),		
			'adres_il' => request("adres_il"),		
			'adres_ilce' => request("adres_ilce"),		
			'adres_detay' => request("adres_detay"),		
			'adres_gsm' => request("adres_gsm"),		
			'adres_current' => $adres_current,		

		];

		UserAdres::where(['user_id' => auth()->id(), 'id' => request("id")])->update($adresData);

	}




	function fatadresEdit($id = 0)
	{
		$adres_current = 0;
		if (request()->has('fatura-info-onay')) {

			UserAdres::where(['user_id' => auth()->id(),'adres_current' => 1,'adres_tur' => 1])->update(['adres_current' => 0]);
			$adres_current = 1;
		}

		$fatData = [
			'adres_title' => request("adres_title"),
			'adres_name' => request("adres_name"),		
			'adres_sname' =>  request("adres_sname"),		
			'adres_il' => request("adres_il"),		
			'adres_ilce' => request("adres_ilce"),		
			'adres_detay' => request("adres_detay"),		
			'adres_gsm' => request("adres_gsm"),		
			'adres_current' => $adres_current,		

			'efatura' => request()->has('efatura')?1:0,		
			'fatura_unvan' => request("fatura_unvan"),		
			'fatura_vd' => request("fatura_vd"),		
			'fatura_vno' => request("fatura_vno"),		
			'fatura_tck' => request("fatura_tck"),		
			'fatura_tur' => request("fatura_tur"),	

		];

		UserAdres::where(['user_id' => auth()->id(), 'id' => request("id")])->update($fatData);

	}






	function changeAdress()
	{
		UserAdres::where(['user_id' => auth()->id(), 'adres_current' => 1,'adres_tur' => request("tur")])->update(['adres_current' => 0]);
		UserAdres::where(['user_id' => auth()->id(), 'id' => request("id"),'adres_tur' => request("tur")])->update(['adres_current' => 1]);
	}


	function delAdres($id = 0)
	{ 
		$addrr = UserAdres::where(['user_id' => auth()->id(),'id' => $id])->first();

		$addrr->delete();

		return "1";


	}

	function talepForm()
	{
		$user = auth()->user();
		$lastorders = Order::with('baskets.basket_products')->where(['user_id' => auth()->id()])->orderByDesc('id')->limit(5)->get();
		return view(theme().'.panel.add_talep',compact( 'user','lastorders'));

	}

	function talepAdd()
	{

		$talep = [
			'user_id' => auth()->id(),
			'message' => request("message"),
			'is_read' =>0,
			'subject' =>request('subject')
		];


		Support::create($talep);



		echo 1;
	}

	function talepDetay($id='')
	{
		$active = "info";


		$title = Support::where('id',$id)->first();

		
		return view(theme().'.panel.talep_detay', compact('active', 'title'));

	}



}
