<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Banner::orderby('id','desc')->get();
        $title = "Banner Yönetimi";
        // dd($list);
        return view('admin.banner.list',compact('list' ,'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form($id = 0)
    {
        $size=Setting::orderBy('id')->where('settings_name','banner position or size')->get();
        $title ="Banner Ekleme Formu";
        $page = "add";
        $banner = new Banner;

        if ($id>0) {
            $banner = Banner::find($id);

            $title ="Banner Güncelleme Formu";
            $page = "edit";
        }


        return view('admin.banner.'.$page,compact('banner',  'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function save($id=0)
    {


        $data = request()->only('title', 'link','image','text','button','position');

        $this->validate(request(),[
            'image' => 'required',
            'position' => 'required',
            'title' => 'required'

        ]);

         $data["banner_desc"] = json_encode(request('banner_desc'));


        if ($id>0) {
            $ban = Banner::where('id',$id)->firstOrFail();
            $ban->update($data);
            $page = "edit";

        }else{
            $ban = Banner::create($data);
            $page = "add";
        }



        if(request()->hasFile('image')){




            $this->validate(request(),[
                'image' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
            ]);

            $resim = request()->file('image');

            $namesi = str_slug(request('title')).time().  ".jpg";

            if ($resim->isValid()) {
                // ,'webp'
                my_upload($resim->getRealPath(),$namesi,"banner",r_size("banner"));
                Banner::updateOrCreate(
                    ['id' => $ban->id],
                    ['image' => $namesi]
                );
            }
        }

        return redirect()
        ->route('crudv4.banner')
        ->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
        ->with('mesaj_tur','success');
    }


    function update($id)
    {


        $data = [request('status')=>request('val')];

        $entry = Banner::where('id',$id)->firstOrFail();
        $entry->update($data);

    }

    function del($id)
    {

        $sld = Banner::find($id);
        removeRes("slider",$sld->slider_image,"slider");
        $sld->delete();


        return "1";
    }
}
