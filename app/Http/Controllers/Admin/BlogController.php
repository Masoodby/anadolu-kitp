<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;


class BlogController extends Controller
{
	public function index()
	{
		$list = Blog::orderby('id','desc')->get();
		$title = "Blog Sayfaları Yönetimi";
		return view('admin.blog.list',compact('list' ,'title'));
	}

	
	public function form($id = 0)
	{

		$title ="Blog Sayfaları Ekleme Formu";
		$page = "add";
		$pages = new Blog;

		if ($id>0) {
			$pages = Blog::find($id);

			$title ="Blog Sayfaları Güncelleme Formu";
			$page = "edit";
		}


		return view('admin.blog.'.$page,compact('pages',  'title'));
	}



	function save($id=0)
	{


		$data = request()->only('page_name', 'page_url','page_title', 'page_desc','page_keyw','page_content');

		$this->validate(request(),[
			'page_name' => 'required',
			'page_url' => 'required',	
			'page_content' => 'required',
			

		]);

		$data["is_active"] = 1;


		if ($id>0) {
			$sld = Blog::where('id',$id)->firstOrFail();
			$sld->update($data);
			$page = "edit";

		}else{
			$sld = Blog::create($data);
			$page = "add";
		}

		if(request()->hasFile('resim')){


			$this->validate(request(),[
				'resim' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
			]);

			$resim = request()->file('resim');

			$namesi = str_slug(request('page_name')).  ".webp";

			if ($resim->isValid()) {

				my_upload($resim->getRealPath(),$namesi,"blog",r_size("blog"),'webp');
				Blog::updateOrCreate(
					['id' => $sld->id],
					['page_img' => $namesi]
				);
			}
		}




		return redirect()
		->route('crudv4.blogs.'.$page,$sld->id)
		->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
		->with('mesaj_tur','success');
	}



	function update($id)
	{


		$data = [request('label')=>request('val')];

		$entry = Blog::where('id',$id)->firstOrFail();
		$entry->update($data);

	}

	function del($id)
	{

		$sld = Blog::find($id);

		$sld->delete();


		return "1";
	}
}
