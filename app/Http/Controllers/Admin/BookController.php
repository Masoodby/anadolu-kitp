<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Depolog;
use App\Models\DepoBook;
use App\Models\Product;
use App\Models\ProductExtend;

use Illuminate\Support\Facades\DB;

class BookController extends Controller
{

	protected $filter = ['isbn','adet'];
	function index()
	{
		$depo_actions = Depolog::with('getRaf')->limit(200)->orderby('id','desc')->get();

		$title = "Depo Analitiği";



	//	$alert_raf = DepoBook::with('getRaf','getBook')->where('adet','<=',set('ikaz'))->get();
		$alert_raf = DepoBook::with('getBook','getRaf.getOda')->where('adet','<=',set('ikaz'))->get();




		$maliyet = DB::table('depo_book')
		->leftJoin('products', 'products.id', '=', 'depo_book.book_id')
		->leftJoin('product_details','product_details.product_id' , '=', 'products.id')
		->select(DB::raw('sum(product_details.product_old_price*depo_book.adet) as satis,sum(product_details.product_get_price*depo_book.adet) as alis,sum(products.product_price*depo_book.adet) as price'))
		->first();


		$toplam_kitap = DB::table('depo_book')		
		->select(DB::raw('sum(adet) as tadet'))
		->first();


		$kitap_cesit = DB::table('depo_book')
		->select( DB::raw('COUNT(id) as count'))
		->groupBy('isbn')
		->get();

		$kitap_cesit = count($kitap_cesit);



		return view('admin.depo.depolog',compact('depo_actions','title','maliyet','toplam_kitap','alert_raf','kitap_cesit'));
	}


	function depoLogsss()
	{
		$list = Depolog::with('getRaf','getBook')->where('adet','<',0)->orderBy('id','desc')->limit(120)->get();

		return view('admin.depo.table',compact('list'));
	}

	function depoLogs()
	{
		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 25;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>101?100:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}


		$query = Depolog::query();

		$query->when(request()->has('notes'), function ($q) {
			$val = request('notes');
			return $q->where('notes', 'like',  "%$val%");
		});


		$query->when(request()->has('isbn'), function ($q) {
			$val = request('isbn');
			return $q->where('isbn', 'like',  "%$val%");
		});




		$query->when(request()->has('durum'), function ($q) {
			$val = request('durum')==1?'>':'<';
			return $q->where('adet', $val, 0);
		});

		$list = $query->with('getRaf','getBook')->orderBy($orderby[0],$orderby[1] )->paginate($limit);


		$title ="Depo Loglaro Yönetimi";

		return view('admin.depo.logs',compact('list','title'));
	}


	function yayinevi_islem()
	{


		$list =  DB::table('yayinevi')->get();


		foreach ($list as $key => $item) {

			$yid =  $item->id;

			$varmi =  ProductExtend::with('product')->whereHas('product', function ($q){
				return $q->where('is_active', 1);
			})->where('yayinci_id',$yid)->count();

			if ($varmi > 0) {
				$aktif = 1;
			}else{
				$aktif = 0;
			}

			DB::table('yayinevi')->where('id',$yid)->update(['is_active'=>$aktif]);

		}
	}

	
	function yazar_islem()
	{

		$list =  DB::table('yazar')->get();

		foreach ($list as $key => $item) {

			$yid =  $item->id;

			$varmi =  ProductExtend::with('product')->whereHas('product', function ($q){
				return $q->where('is_active', 1);
			})->where('yazar_id',$yid)->count();

			if ($varmi > 0) {
				$aktif = 1;
			}else{
				$aktif = 0;
			}

			DB::table('yazar')->where('id',$yid)->update(['is_active'=>$aktif]);

		}



	}


	function depolist()
	{
		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 25;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>101?100:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}


		$query = DepoBook::query();

		
		$query->when(request()->has('isbn'), function ($q) {
			$val = request('isbn');
			return $q->where('isbn',  intval($val));
		});


		$list = $query->with('getBook','getRaf.getOda')->orderBy($orderby[0],$orderby[1] )->paginate($limit);





		$title ="Depo Kitapları";


		return view('admin.depo.list',compact('list','title'));
	}



	function stokguncelle()
	{


		DB::raw('UPDATE depo_book AS b INNER JOIN products AS g ON b.isbn = g.product_stock_code SET b.book_id = g.id WHERE    b.book_id = 0');
	//	DB::raw('UPDATE products AS b INNER JOIN depo_book AS g ON b.product_stock_code = g.isbn SET b.is_depo = 1 WHERE  b.is_depo = 0');
		DB::raw('UPDATE products SET  is_depo =  0 WHERE   is_depo = 1');

		$maliyet = DB::table('depo_book')
		->groupBy('isbn')		 
		->select(DB::raw('sum(adet) as t_adet,isbn,book_id'))
		->where("book_id",">",0)
		->get();


		DB::table('products')->where('product_stok', 0)->where('is_depo',1)->update([ 'is_depo'=>0]);
		DB::table('products')->where('product_stok','>',0)->where('is_depo',1)->update(['product_stok'=>0,'is_active'=>0]);
		foreach ($maliyet as $key => $var) {

			DB::table('products')->where('id',$var->book_id)->update(['product_stok'=>$var->t_adet,'is_active'=>1,'is_depo'=>1]);
			
		}



	}


	function depoTosite()
	{


		$q_str = 'UPDATE depo_book AS b
		INNER JOIN products AS g ON b.isbn = g.product_stock_code
		SET b.book_id = g.id  
		WHERE    b.book_id = 0';


		DB::statement($q_str);

		$books = DB::table('depo_book')
		->leftJoin('raf','raf.id' , '=', 'depo_book.raf_id')
		->leftJoin('oda', 'oda.id', '=', 'raf.oda_id')
		->leftJoin('depo','depo.id' , '=', 'oda.depo_id')	
		->where('depo_book.book_id',0)
		->groupBy('depo_book.isbn')	
		->get();

		echo "<h3>Depoda Var Sitede Yok</h3>";
		//echo $books;
	//	die();



		$i = 1;

		foreach ($books as $key => $item) {

		//	echo $item->depo_name.' -> '.$item->oda_name.' -> '.$item->raf_name.' -------------> ISBN    :  '.$item->isbn."<br>";


			$varmi = DB::table('products')->where('product_stock_code',$item->isbn);

			if ($varmi->count() > 0) {
				$product = $varmi->first();

			//	DB::table('depo_book')->where('id',$item->id)->update(['book_id'=>$product->id]);
			}else{
			//	DB::table('depo_book')->where('id',$item->id)->update(['book_id'=>0]);


				echo $i.' ------> ' .$item->depo_name.' -> '.$item->oda_name.' -> '.$item->raf_name.' -------------> ISBN    :  '.$item->isbn."<br>";
			}

			$i++;


		}




	}

	function depoexit()
	{
		$title ="Depo Çıkış İşlemleri";


		return view('admin.depo.exit',compact('title'));
	}

	function exitdepo()
	{
		$isbn = request('isbn');

		$list = DepoBook::with('getBook','getRaf.getOda')->where('isbn',$isbn)->get();

		return view('admin.depo.exitlist',compact('list'));
	}


	function exitdeporun($id)
	{

		$list = DepoBook::where('id',$id)->first();


		$adet = $list->adet-1;

		$list->update(['adet'=>$adet]);

		Depolog::create(['raf_id'=>$list->raf_id,'isbn'=>$list->isbn,'adet'=>-1,'book_id'=>$list->book_id,'notes'=>"Manuel çıkış yapıldı"]);

		echo 1;
	}


}
