<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Catnew;
use App\Models\Product;
use App\Library\NestedSortable;



class CategoryController extends Controller
{

	protected $filter = ['categories_name','categories_main','id','is_active'];

	function index()
	{


		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 25;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>101?100:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}




		$query = Category::query();

		$query->when(request()->has('categories_name'), function ($q) {
			$val = request('categories_name');
			return $q->where('categories_name', 'like',  "%$val%");
		});
		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});	
		$query->when(request()->has('categories_main'), function ($q) {
			$val = request('categories_main');
			return $q->where('categories_main',  intval($val));
		});

		$query->when(request()->has('is_active'), function ($q) {
			$val = request('is_active');
			return $q->where('is_active',  intval($val));
		});

		$query->when(request()->has('is_top'), function ($q) {
			$val = request('is_top');
			return $q->where('is_top',  intval($val));
		});

		$list = $query->with('mainCategory')->orderBy($orderby[0],$orderby[1] )->paginate($limit);







		$selectbox = Category::attr(['name' => 'categories_main',"class"=>"form-control selectfilter ml-3 sel2","data-filter"=>"categories_main"]);

		if (request()->has('categories_main')) {
			$main_cats =  $selectbox->selected(request('categories_main'))->renderAsDropdown();
		}else{
			$main_cats =  $selectbox->renderAsDropdown();
		}
		



		$title ="Kategori Yönetimi";


		return view('admin.category.index',compact('list', 'main_cats','title'));
	}




	function form($id=0)
	{



		$selectbox = Category::attr(['name' => 'categories_main',"class"=>"form-control ml-3 sel2"]);
		$main_cats =  $selectbox->renderAsDropdown();
		$title ="Kategori Ekleme Formu";
		$page = "addform";
		$cat = new Category;

		if ($id>0) {
			$cat = Category::find($id);
			$main_cats =  $selectbox->selected($cat->categories_main)->renderAsDropdown();
			$title ="Kategori Güncelleme Formu";
			$page = "editform";
		}

		
		return view('admin.category.'.$page,compact('cat', 'main_cats','title'));
	}


	

	function save($id=0)
	{
		$data = request()->only('categories_name', 'categories_url', 'categories_main','categories_title', 'categories_desc', 'categories_keyw','categories_content');

		if (!request()->filled('categories_url')) {
			$data['categories_url'] = str_slug(request('categories_name'));
			request()->merge(['categories_url' => $data['categories_url']]);
		}


		$this->validate(request(),[
			'categories_name' => 'required',
			'categories_url' => (request('old_categories_url') != request('categories_url') ? 'unique:categories,categories_url' : '')
		]);




		if ($id>0) {
			$cat = Category::where('id',$id)->firstOrFail();
			$cat->update($data);
			$page = "edit";

		}else{
			$cat = Category::create($data);
			$page = "add";
		}



		if(request()->hasFile('resim')){




			$this->validate(request(),[
				'resim' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
			]);

			$resim = request()->file('resim');

			$namesi = str_slug(request('categories_name')).  ".webp";

			if ($resim->isValid()) {

				my_upload($resim->getRealPath(),$namesi,"category",r_size("cat"));
				Category::updateOrCreate(
					['id' => $cat->id],
					['categories_image' => $namesi]
				);
			}
		}



		return redirect()
		->route('crudv4.category.'.$page,$cat->id)
		->with('mesaj',($id>0 ? 'Güncellendi ': 'Kaydedildi'))
		->with('mesaj_tur','success');
	}


	function update($id)
	{


		$data = [request('label')=>request('val')];

		$entry = Category::where('id',$id)->firstOrFail();
		$entry->update($data);

	}

	function del($id)
	{
		//attach/detach
		//kategorideki ürünlerin silinmesi için
		$cat = Category::find($id);
		$cat->getProduct()->detach();
		$cat->delete();

		return "1";
	}


	function sortable()
	{
		$categories = Category::all();
		$catsx = new NestedSortable();
		$cats = $catsx->sort($categories);
		$title = "Kategori Yönetimi";


		return view('admin.category.sortable',compact('cats','title'));
	}



	function save_menu()
	{


		if( request('id') > 0){

			$db_data = [
				"categories_name"=>request('categories_name'),
				"categories_title"=>request('categories_name'),
				"categories_desc"=>request('categories_name'),
				"categories_keyw"=>request('categories_name'),
				"categories_url"=>request('categories_url'),				
			];

			Category::where('id',request('id'))->update($db_data);

			$arr['type']  = 'edit';
			$arr['label'] = $postData['db_data']['categories_name'];
			$arr['link']  = $postData['db_data']['categories_url'];
			$arr['id']    = $postData['db_data']['categories_id'];
		} else {


			$db_data = [
				"categories_name"=>request('categories_name'),
				"categories_title"=>request('categories_name'),
				"categories_desc"=>request('categories_name'),
				"categories_keyw"=>request('categories_name'),
				"categories_url"=>request('categories_url'),
				"is_active"=>1,
				"rank"=>0,
				"categories_main"=>0,
			];


			$lastID = Category::create($db_data)->id;
			
			$arr['menu'] = '<li class="dd-item dd3-item" data-id="'.$lastID .'" >
			<div class="dd-handle dd3-handle">Drag</div>
			<div class="dd3-content"><span id="label_show'.$lastID .'">'.$db_data['categories_name'].'</span>
			<span class="span-right">/<span id="link_show'.$lastID .'">'.$db_data['categories_url'].'</span> &nbsp;&nbsp; 
			<a class="edit-button" id="'.$lastID .'" label="'.$db_data['categories_name'].'" link="'.$db_data['categories_url'].'" ><i class="fa fa-pencil"></i></a>
			<a class="del-button" id="'.$lastID .'"><i class="fa fa-trash"></i></a>
			</span> 
			</div>';
			$arr['type'] = 'add';
		}
		
		return response()->json($arr);
	}


	function savesort()
	{


		$data = json_decode(request('data'));


		//$json2array = new NestedSortable();
		$readbleArray = $this->parseJsonArray($data);


		$i=0;
		foreach($readbleArray as $row){
			$i++;

			$params = array('categories_main'=>$row['parentID'],'rank'=>$i);

			Category::where('id',$row['id'])->update($params);


		}

	}

	function parseJsonArray($jsonArray, $parentID = 0) {
		$return = array();
		foreach ($jsonArray as $subArray) {
			$returnSubSubArray = array();
			if (isset($subArray->children)) {
				$returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
			}
			$return[] = array('id' => $subArray->id, 'parentID' => $parentID);
			$return = array_merge($return, $returnSubSubArray);
		}
		return $return;
	}

	function optcat()
	{

		Category::where('categories_main','>',0)->update(['is_active'=>0]);
		$cats = Category::with("mainCategory")->where('categories_main','>',0)->get();

		foreach ($cats as $key => $var) {

			$varmi = Product::where(["product_cat"=>$var->id,"is_active"=>1])->count();

			if ($varmi > 0) {
				$url = str_slug($var->mainCategory->categories_name.'-'.$var->categories_name);


				Category::where('id',$var->id)->update(['categories_url'=>$url,'countproduct'=>$varmi,'is_active'=>1]);

			}
		}



		Category::where('categories_main',0)->update(['is_active'=>0]);
		$cats = Category::where('categories_main',0)->get();

		foreach ($cats as $key => $var) {

			$olan1 = Product::where(["product_cat"=>$var->id,"is_active"=>1])->count();
			$olan = Category::where(["categories_main"=>$var->id,"is_active"=>1])->sum("countproduct");

			$varmi = $olan1+$olan;

			if ($varmi > 0) {

				Category::where('id',$var->id)->update(['countproduct'=>$varmi,'is_active'=>1]);

			}
		}



		
	}


	function oldnew()
	{
		$selectbox_old = Category::attr(['name' => 'old','id' => 'oldcat',"class"=>"form-control ml-3 sel2"]);
		$main_cats_old =  $selectbox_old->renderAsDropdown();

		$selectbox_new = Catnew::attr(['name' => 'new','id' => 'newcat',"class"=>"form-control ml-3 sel2"]);
		$main_cats_new =  $selectbox_new->renderAsDropdown();


		$news = Catnew::with('oldCat.mainCategory','mainCategory')->get();

		$title ="Kategori Eşleştime Formu";

		return view('admin.category.eslestir',compact('main_cats_old','main_cats_new','news','title'));
	}


	function esles()
	{
		$oldcat =  request('oldcat');
		$newcat =  request('newcat');

		Catnew::where('id',$newcat)->update(['old_id'=>$oldcat]);
		Category::where('id',$oldcat)->update(['new_id'=>$newcat]);
	}

	function unesles($id,$nid)
	{

		Category::where('id',$id)->update(['new_id'=>0]);
        Catnew::where('id',$nid)->update(['old_id'=>0]);


        return redirect()
            ->route('crudv4.category.oldnew')
            ->with('mesaj_tur','success');
	}


}
