<?php
namespace App\Http\Controllers\Admin;
use App\Models\Product;
use App\Models\Combo;
use App\Models\ComboProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SebastianBergmann\Environment\Console;

class ComboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Combo::orderby('id','DESC')->with('products')->get();
        $title = "kitap seti Yönetimi";
        // dd($list);
        return view('admin.combo.list',compact('list' ,'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form($id = 0)
    {

        $title ="Ürün seti Ekleme Formu";
        $page = "add";
        $combos = new ComboProduct;

        if ($id>0) {
            $slider = ComboProduct::find($id);

            $title ="urun seti Güncelleme Formu";
            $page = "edit";
        }


        return view('admin.combo.'.$page,compact('combos',  'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function save($id=0)
    {

        // dd(request()->all());

        $data = request()->only('combo_name', 'combo_price','combo_price','combo_desc','combo_url');
        $pid=request()->only('product_id');
        // dd($pid);
        $this->validate(request(),[
            'combo_name' => 'required',
            'combo_price' => 'required',
            'combo_desc' => 'required',
            'combo_price' => 'required',
            'product_id' => 'required',

        ]);




        if ($id>0) {
            $combo = Combo::where('id',$id)->firstOrFail();
            $combo->update($data);
            $page = "edit";

        }else{

            $combo = Combo::create([
                'combo_name' => request()->combo_name,
                'combo_price' => request()->combo_price,
                'combo_desc' => request()->combo_desc,

                'combo_price' => request()->combo_price,
                ]);
                $combo->update([
                    'combo_url' => 'set-urun-'.$combo->id,
                ]);
                 $combo->products()->sync(request()->product_id);

            $page = "add";
        }



        if(request()->hasFile('combo_image')){




            $this->validate(request(),[
                'combo_image' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
            ]);

            $resim = request()->file('combo_image');

            $namesi = str_slug(request('combo_name'));//////.  ".webp"

            if ($resim->isValid()) {

                my_upload($resim->getRealPath(),$namesi,"combo",r_size("combo"));
                Combo::updateOrCreate(
                    ['id' => $combo->id],
                    ['combo_image' => $namesi]
                );
            }
        }






        return redirect()
        ->route('crudv4.combo.'.$page,$combo->id)
        ->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
        ->with('mesaj_tur','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Combo  $combo
     * @return \Illuminate\Http\Response
     */
    public function show(Combo $combo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Combo  $combo
     * @return \Illuminate\Http\Response
     */
    public function edit(Combo $combo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Combo  $combo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Combo $combo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Combo  $combo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Combo $combo)
    {
        //
    }


	public function urunara(){

        $search = request("search");


        if($search == ''){
            $employees = Product::orderby('product_name','DESC')->select('id','product_name')->limit(5)->get();
        }else{
            $employees = Product::orderby('product_name','asc')->where('product_stok', '>', 0)->select('id','product_name','product_price')
            ->where('product_name', 'like', '%' .$search . '%')->orwhere('product_stock_code', 'like', '%' .$search . '%')->limit(20)->get();
        }

        $response = array();
        foreach($employees as $employee){
            $response[] = array(
                "id"=>$employee->id,
                "text"=>$employee->product_name,
                "price"=>$employee->product_price

            );
        }

        return json_encode($response);
        exit;
    }

}
