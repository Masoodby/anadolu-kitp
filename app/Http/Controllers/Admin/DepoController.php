<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Depo;
use Illuminate\Support\Facades\DB;


class DepoController extends Controller
{
	public function form($id = 0)
	{

		$depos = Depo::with('getRoom')->get();

		$depoAdetler = DB::table('depo')
		->leftJoin('oda', 'oda.depo_id', '=', 'depo.id')
		->leftJoin('raf','raf.oda_id' , '=', 'oda.id')
		->leftJoin('depo_book','depo_book.raf_id' , '=', 'raf.id')
		->select(DB::raw('sum(depo_book.adet) as adet,depo.depo_name as name'))
		->groupBy('depo.id')
		->get();


		$title ="Depo İşlemleri";
		$page = "add";
		$depo = new Depo;

		if ($id>0) {
			$depo = Depo::find($id);		 
			$page = "edit";
		}


		return view('admin.depo.'.$page,compact('depo',  'title','depos','depoAdetler'));
	}




	function save($id = 0)
	{
		

		$data = request()->only('depo_name');


		if ($id>0) {
			$depo = Depo::where('id',$id)->firstOrFail();
			$depo->update($data);
			$page = "edit";

		}else{
			$depo = Depo::create($data);
			$page = "add";
		}


		return redirect()
		->route('crudv4.depo.'.$page,$depo->id)
		->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
		->with('mesaj_tur','success');
	}

	function del($id)
	{

		$del = Depo::find($id);

		$del->delete();

		return "1";
	}


}
