<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DnrYayinci;
use App\Models\Product;



class DnrBotController extends Controller
{
	public function harfler($key = 0)
	{
		$harfler =  ['A','B','C','Ç','D','E','F','G','H','İ','I','J','K','L','M','N','O','Ö','P','Q','R','S','Ş','T','U','Ü','V','W','X','Y','Z'];

		if (isset($harfler[$key])) {
			return $harfler[$key];
		}else{
			return false;
		}

	}

	function addYayinci($hkey = 0)
	{
		$harf =  $this->harfler($hkey);

		echo $harf;

		if ($harf) {

			$content = $this->get_web_page("https://www.dr.com.tr//Catalog/PublisherList?pref=".$harf);

			if (preg_match('/class="authors-list"/i', $content['content'])) {

				$liler = explode('<li>', $content['content']);
				unset($liler[0]);
				foreach ($liler as $key => $li) {

					$name = strip_tags('<li>'.$li);

					$name = trim($name);

					$link = $this->arasiniAl($li,'href="','"');

					$id = $this->arasiniAl($li,'id="','"');

					$ekleme = [
						"name"=>$name,
						"dnr_id"=>trim($id),
						"links"=>trim($link),
					];

					DnrYayinci::updateOrCreate(
						["dnr_id"=>trim($id)],
						$ekleme
					);


				}

			}
		}

		$hkey++;
	//	echo "<script> setTimeout(function(){location.href='/crudv4/dnr/addYayinci/".$hkey."'},2000)</script>";


	}






	function getProduct()
	{

		//DnrYayinci::update(['durum'=>0]);
		$yyz = DnrYayinci::where('durum',0);

		if ($yyz->count() < 1) {
			die('bitti');
		}

		$yy = $yyz->first();

		$yid = $yy->dnr_id;

		

		$content = $this->get_web_page("https://www.dr.com.tr/Catalog/PublisherProducts?manufacturerId=".$yid."&size=36&page=1&HideNotForSale=false");
		//$content = get_web_page("https://www.dr.com.tr/Catalog/PublisherProducts?manufacturerId=9738&size=36&page=1&HideNotForSale=false");

		if (preg_match('/id="hdnHitCount"/i', $content['content'])) {

			$cell = explode('class="cell"', $content['content']);


			$toplam = $this->arasiniAl($content['content'],'id="hdnHitCount"','/>');

			$toplam =  $this->arasiniAl($toplam,'value="','"');

			$toplam = trim($toplam);

			$page_adet =  ceil($toplam/36);

			unset($cell[0]);

			foreach ($cell as $key => $item) {

				$u_link = $this->arasiniAl($item,'href="/Kitap','"');

				$link = "https://www.dr.com.tr/Kitap".$u_link;



				$skod = $this->get_skod($link);



				$img = $this->arasiniAl($item,'cache/154x170-0','"');




				$img = "https://i.dr.com.tr/".$img;
				$yol = public_path('dnr_img/'.$skod.'.jpg');


				$filename= $img;
				$file_headers = @get_headers($filename);

				if($file_headers[0] == 'HTTP/1.0 404 Not Found'){

				} else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found'){

				} else {
					@copy($img,$yol);
				}






			}

			echo "<script> setTimeout(function(){location.href='/crudv4/dnr/getProduct_page/".$yid."/2/".$page_adet."'},2000)</script>";


		}else{
			DnrYayinci::where('dnr_id',$yid)->update(['durum'=>1]);
			
			echo "<script> setTimeout(function(){location.href='/crudv4/dnr/getProduct'},2000)</script>";

		}





	}


	function get_skod($url)
	{
		$content = $this->get_web_page($url);

		if (preg_match('/"isbn":/i', $content['content'])) {

			$skod =   $this->arasiniAl($content['content'],'"isbn":','",');

			$skod =  str_replace('"', '', $skod );

			return trim($skod);


		}
	}


	function getProduct_page($yid,$page,$lastNum)
	{

//die('beklemede');

		if ($page > $lastNum) {

			DnrYayinci::where('dnr_id',$yid)->update(['durum'=>1]);
			
			echo "<script> setTimeout(function(){location.href='/crudv4/dnr/getProduct'},2000)</script>";

		}else{



			$content = $this->get_web_page("https://www.dr.com.tr/Catalog/PublisherProducts?manufacturerId=".$yid."&size=36&page=1&HideNotForSale=false");


			if (preg_match('/id="hdnHitCount"/i', $content['content'])) {

				$cell = explode('class="cell"', $content['content']);




				unset($cell[0]);

				foreach ($cell as $key => $item) {

					$u_link = $this->arasiniAl($item,'href="/Kitap','"');

					$link = "https://www.dr.com.tr/Kitap".$u_link;



					$skod = $this->get_skod($link);



					$img = $this->arasiniAl($item,'cache/154x170-0','"');




					$img = "https://i.dr.com.tr/".$img;
					$yol = public_path('dnr_img/'.$skod.'.jpg');


					if (file_exists($yol)) {

					}else{

						@copy($img,$yol);

					}


				}




				$nexts = intval($page)+1;


				echo "<script> setTimeout(function(){location.href='/crudv4/dnr/getProduct_page/".$yid."/".$nexts."/".$lastNum."'},2000)</script>";
			}else{
				die('hata');
			}
		}

	}


	function kcenter($isbn)
	{
		$url = "https://www.kitapcenter.com/arama?q=".$isbn;

		$content = $this->get_web_page($url);


		$page = $this->arasiniAl($content['content'],'PRODUCTS_TOTAL',';');
		$adet = str_replace('=', "", $page);

		$adet =  trim($adet);

		if ($adet == 1) {

			$image = $this->arasiniAl($content['content'],'class="imgInner"','</span>');

			$src = $this->arasiniAl($image,'src="','"');

			$src = str_replace('K.jpg', "B.jpg", $src);

			$new_path =  public_path('kcenter/'.$isbn.'.jpg');

			@copy($src,$new_path );

			return 4;
		}else{

			return 5;
		}

	}


	function resim()
	{
		$all = Product::where('resim_durum',2)->limit(200)->get();

		ini_set('max_execution_time', '0');

		$i = 0;


		foreach ($all as $key => $item) {


			$res = $this->kcenter($item->product_stock_code);

			Product::where('id',$item->id)->update(["resim_durum"=>$res]);
			$i++;
			
		}

		if ($i>0) {
			echo "<script> setTimeout(function(){location.href='/crudv4/dnr/resim'},2000)</script>";
		}else{
			die('bitti');
		}

		

	}

	


	function get_web_page( $url )
	{
		$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
		$options = array(
	        CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER      => false,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYHOST      => 0,       // stop after 10 redirects
        );
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}


	function arasiniAl($txt='',$start,$end)
	{

		$test = str_replace('/', '\/', $start);


		if (preg_match('/'.$test.'/i', $txt)) {


			$txt1 =  explode($start, $txt);
			$txt2 =  explode($end, $txt1[1]);
			return trim($txt2[0]);

		}else{
			return "-99";
		}
	}

}
