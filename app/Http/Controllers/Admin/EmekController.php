<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Category;
use App\Models\Emek;
use DB;
use Illuminate\Support\Facades\Artisan;


class EmekController extends Controller
{
	function index($value='')
	{

	}


	function addBook($id = 0)
	{

		set_time_limit (0);
		ini_set('memory_limit', '-1');
		ini_set('default_socket_timeout', 36000000);
		ini_set('display_errors', 'on');

		if ($id <351 ) {


			$books = json_decode(file_get_contents(public_path('/dist/json/book_'.$id.'.json')));

			$i=0;
			foreach ($books as $key => $book) {

				$cid = Emek::catIslem($book->p26);
				$yayinci_id = Emek::addYayinci($book->p15,$book->p14);
				$yazar_id = Emek::addYazar($book->p24[0]->a2,$book->p24[0]->a1);



				$product = Emek::product2Array($book);

				$product_extent['extend_json'] = json_encode(Emek::bookData($book));


				$product_details = [
					'product_old_price' => floatval(str_replace(",",".",$book->p8)),

				];




				$product_extent['yazar_id']=$yazar_id;
				$product_extent['yayinci_id']=$yayinci_id;
				$product['product_cat']=$cid;
				$product_details['product_image']='["'.$book->p7.'.jpg"]';



				$pid = Emek::addBook($product,$product_extent,$product_details);


				$i++;



			}

			$id++;

			echo "<script> setTimeout(function(){location.href='/crudv4/emek/addBook/".$id."'},2000)</script>";


		//	return redirect()->route('crudv4.emek.addBook',$id);

		}else{
			return "Bitti";
		}


	}


	function updateBook()
	{
		$list =  json_decode(file_get_contents( 'https://www.berkanka.ga/berkan9090/updateProduct/links.json'));

		foreach ($list as $key => $item) {

			$books = json_decode(file_get_contents('https://www.berkanka.ga/berkan9090/'.$item));

			foreach ($books as $key => $book) {

				$cid = Emek::catIslem($book->p26);
				$yayinci_id = Emek::addYayinci($book->p15,$book->p14);
				$yazar_id = Emek::addYazar($book->p24[0]->a2,$book->p24[0]->a1);



				$product = Emek::product2Array($book);

				$product_extent['extend_json'] = json_encode(Emek::bookData($book));


				$product_details = [
					'product_old_price' => floatval(str_replace(",",".",$book->p8)),

				];


				$product_extent['yazar_id']=$yazar_id;
				$product_extent['yayinci_id']=$yayinci_id;
				$product['product_cat']=$cid;
				$product_details['product_image']='["'.$book->p7.'.jpg"]';



				$pid = Emek::addBook($product,$product_extent,$product_details);




			}



		}
	}


	function oranUpdate()
	{
		$xml =  simplexml_load_file('emek.xml');



		foreach ($xml as $key => $cell) {


			$var = $cell->Cell;




			$oran = $var[1];
			$soran = $var[1] -15;

			if ($oran < 15) {
				$soran = 0;
			}
			$name =  trim($var[0]);
			$yayinci =  DB::table("yayinevi")->where('yayinci_name',$name)->first();

			if ($yayinci) {

				DB::table("yayinevi")->where('id',$yayinci->id)->update(['emek_alis'=>$oran,'emek_satis'=>$soran]);
			}
		}
	}


	function fileFolder()
	{
		$all = Product::where('resim_durum',0)->limit(5000)->get();
		$i=0;
		foreach ($all as $key => $item) {

			$folder =  DB::table('files')->where('file_id',$item->product_stock_code)->first();
			if ($folder) {
				$folder_name = $folder->folder;

				$item->update(['resim_durum'=>1,"product_group_code"=>$folder_name]);

			}else{
				$folder_name = "";
				$item->update(['resim_durum'=>2]);
			}
			$i++;

		}
		if ($i==0) {
			die('bitti');
		}else{
			Artisan::call('emek:fileFolder');
		}


	}


}
