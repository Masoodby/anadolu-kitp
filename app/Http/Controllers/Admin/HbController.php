<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MarketPlaces;

use App\Library\Hb;



use App\Library\PDF_Code128;

use App\Models\Category;
use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\MarketWriterMatch;
use App\Models\Yayinevi;
use App\Models\Yazar;
use App\Models\Depolog;

use App\Models\Product;
use App\Models\ProductMarketPlace;
use Illuminate\Support\Facades\DB;





class HbController extends Controller
{
	public $api_key;
	public $api_secret;


	public $pid = 3;
	public $hb;
	public $hb_config;

	public function __construct()
	{
		$this->hb_config = MarketPlaces::find($this->pid);
		$config = ['seller_id'=>$this->hb_config->user_no,"api_user"=>$this->hb_config->api_id,"api_pass"=>$this->hb_config->api_key];
		$this->hb =  new Hb($config);

	}


	function index()
	{
        ////index market yerler

		$title = "Hb İşlem Logları";


		$last_queries = DB::table('hb_results')->orderByDesc('id')->limit(500)->get();



		return view('admin.hb.index',compact('title','last_queries'));
	}




	function catmatch()
	{

		$title ="HB Kategori Eşleştirme";
		$page = "cat_match";
		$hbcat = file_get_contents(public_path('hbcat.json'));



		$cats =  Category::nested()->get();
		$matched =  MarketCatMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();

		return view('admin.hb.'.$page,compact('title','hbcat','cats','matched'));
	}




	function saveMatchCat()
	{


		foreach (request('cats') as $key => $item) {

			//$cat = Category::where('id',$key)->firstOrFail();

			$r_name = explode('->', request('remote_cat_name'));

			MarketCatMatch::updateOrCreate([

				'local_id'   => $key,
				'remote_id'   => request('remote_cat_id'),
				'pazaryeri_id'   => $this->pid,
			],[
				'local_id'   => $key,
				'remote_id'   => request('remote_cat_id'),
				'pazaryeri_id'   => $this->pid,
				'local_name'   => $item,
				'remote_name'   => request('remote_cat_name'),
			]);


		}



		$matched =  MarketCatMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();

		$typeval = "cat";

		return view('admin.hb.matched',compact('matched','typeval'));
	}


	function catRequired($id)
	{

		header('Content-Type: application/json');
		return $this->hb->getCatAttr($id);
	}


	function del($id)
	{
		$matched = MarketCatMatch::find($id);
		$matched->delete();
		return "1";
	}


	function delbrand($id)
	{
		$matched = MarketBrandMatch::find($id);
		$matched->delete();
		return "1";
	}


	function products($page = 0)
	{

		$title ="HepsiBurada Ürünleri";
		$products = $this->hb->getProducts($page);

		return view('admin.hb.products',compact('title','products'));
	}


	function saveProductLocal()
	{
		$products = $this->hb->getProducts();

		$products = json_decode($products);
		$urunler = $products->listings;

		foreach ($urunler as $key => $item) {

			$product = Product::with(['get_category','extend'])->where('product_stock_code', barkod_rep($item->merchantSku))->first();


			$cat_remote = MarketCatMatch::where('local_id',$product->get_category->id)->where('pazaryeri_id',$this->pid)->first();


			if (isset($cat_remote)) {


				$cat_remote_id = $cat_remote->remote_id;
				$cat_remote_name = $cat_remote->remote_name;

			}else{
				$cat_remote_id = 0;
				$cat_remote_name = "";
			}

			if ($product) {

				$idsi = $product->id;
				$json = [
					"cat_local_id" =>$product->product_cat,
					"cat_local_name" =>$product->get_category->categories_name,
					"cat_remote_id" =>$cat_remote_id,
					"cat_remote_name" =>$cat_remote_name,
					"brand_local_id" =>$product->extend->yayinci_id,
					"brand_local_name" =>$product->extend->getYayinci->yayinci_name,
					"brand_remote_id" =>0,
					"brand_remote_name" =>$product->extend->getYayinci->yayinci_name,
					"attr"=>  "",
				];
			}else{
				$json = [];
				$idsi = 0;
			}


			$price = yuzdePlus($product->product_price,$this->hb_config->percent_unit);
			$price = floatval($price) + floatval($this->hb_config->price_unit);

			$list_price =  yuzdePlus($price,25);
			$data = [
				"market_id" =>$this->pid,
				"product_id" =>$idsi,

				"spec_barcode"=>$item->merchantSku,
				"market_product_code"=>$item->hepsiburadaSku,
				"price"=>$price,
				"list_price"=>$list_price,
				"jsons"=>json_encode($json),
				"fixed"=>0,
				"approved"=>1,
				"on_sale"=>1,
				"locked"=>0,
			];

			ProductMarketPlace::updateOrCreate(
				["market_id" =>$this->pid,"spec_barcode" => $item->merchantSku],
				$data
			);



		}
	}


	function createExcel()
	{
		$urunler = Product::where('is_depo',1)->where('product_stok' ,'>', 3)->get();

		$ret = [];
		foreach ($urunler as $key => $var) {

			$ret[] = [$var->product_name,$var->product_stock_code,$var->product_stock_code];
		}


		$columns = ['Ürün Adı','Satıcı Stok Kodu','Barkod'];

		header('Content-Encoding: UTF-8');
		header('Content-Type: text/plain; charset=utf-8');
		header("Content-disposition: attachment; filename=hb.xls");

		echo "\xEF\xBB\xBF";

		$say = count($columns);

		echo '<table border="1">';

		echo '<tr>';
		foreach($columns as $v){
			echo '<th style="background-color:#FFA500">'.trim($v).'</th>';
		}
		echo '</tr>';

		foreach($ret as $val){

			echo '<tr>';

			for($i=0; $i < $say; $i++){
				echo '<td>'.$val[$i].'</td>';
			}

			echo '</tr>';
		}

		echo '</table>';


	}


	/*

 {
    "categoryId": 18021982,
    "merchant": "6fc6d90d-ee1d-4372-b3a6-264b1275e9ff",
    "attributes": {
      "merchantSku": "SAMPLE-SKU-INT-0",
       "VaryantGroupID": "Hepsiburada0",
      "Barcode": "1234567891234",
      "UrunAdi": "Roth Tyler",
      "UrunAciklamasi": "Duis enim duis magna ex veniam elit id Lorem cillum minim nisi id aliquip. Laboris magna id est et deserunt adipisicing tempor eu ea officia ipsum deserunt. Irure occaecat sit aliquip elit ipsum sint dolore quis est amet aute pariatur cupidatat fugiat. Cillum pariatur pariatur occaecat sint. Aliqua qui in exercitation nulla aliquip id ipsum aliquip ad ut excepteur culpa consequat aliquip. Nisi ut ex tempor enim adipisicing anim irure pariatur.\r\n",
      "Marka": "Nike",
      "GarantiSuresi": 24,
      "kg": "1",
      "tax_vat_rate" : "5",
      "Image1": "https://productimages.hepsiburada.net/s/27/552/10194862145586.jpg",
      "Image2": "https://productimages.hepsiburada.net/s/27/552/10194862145586.jpg",
      "Image3": "https://productimages.hepsiburada.net/s/27/552/10194862145586.jpg",
      "Image4": "https://productimages.hepsiburada.net/s/27/552/10194862145586.jpg",
      "Image5": "https://productimages.hepsiburada.net/s/27/552/10194862145586.jpg",
      "renk_variant_property": "Siyah",
      "ebatlar_variant_property": "Büyük Ebat"
    }
  },

	*/


  function addProduct()
  {
  	$mevcut = ProductMarketPlace::where('market_id',$this->pid)->get()->pluck('product_id');
  	$urunler = Product::with('detail',"extend")->where('is_active',1)->where('is_depo',1)->where('resim_durum',1)->where('product_stok','>',3)->limit(10)->whereNotIn("id",$mevcut)->get();

  	foreach ($urunler as $key => $row) {


  		if ( $row->product_cat > 0) {

  			$cid = $row->product_cat;
  			$isbn = $row->product_stock_code;
  			$folder = $row->product_group_code;
  			$yid = $row->extend->yayinci_id;
  			$yazarid = $row->extend->yazar_id;

  			$src = public_path('files/'.$folder.'/'.$isbn.'.jpg');

  			if (file_exists($src)) {

  				$url = "https://www.anadolukitap.com/files/".$folder."/".$isbn.".jpg";


  				$cat_remote = MarketCatMatch::where('local_id',$cid)->where('pazaryeri_id',$this->pid)->first();



  				if ($cat_remote) {
  					$cat_remote_id = $cat_remote->remote_id;



  					if ($row->product_price == 0) {

  						$oran_price = yuzdex($row->detail->product_old_price,20);
  						$price = yuzdePlus($oran_price,$this->hb_config->percent_unit);
  						$price = floatval($price) + floatval($this->hb_config->price_unit);

  					}else{

  						$price = yuzdePlus($row->product_price,$this->hb_config->percent_unit);
  						$price = floatval($price) + floatval($this->hb_config->price_unit);

  					}

  					$price = round($price,2);
  					$price = str_replace('.', ',', $price);

  					$data[] = [
  						"categoryId" =>$cat_remote_id,
  						"merchant" => $this->hb_config->user_no,

  						"attributes" => [
  							"merchantSku" => $isbn,
  							"VaryantGroupID" => $isbn.'-aktp',
  							"Barcode" => $isbn,
  							"UrunAdi" => $row->product_name,
  							"UrunAciklamasi" => $row->product_content,
  							"Marka" => $row->extend->getYayinci->yayinci_name,
  							"GarantiSuresi" => 24,
  							"kg" => 1,
  							"tax_vat_rate" => 0,
  							"price"=>$price,
  							"stock"=>$row->product_stok,
  							"Image1" => $url,
  							"Image2" => "",
  							"Image3" => "",
  							"Image4" => "",
  							"Image5" => "",
  							"renk_variant_property" => "",
  							"ebatlar_variant_property" => "",
  						],
  					];

  			}// end cat_remote
  		} //if (file_exists($src))

  		} // end $row->product_cat > 0)



  	} // end foreach ($urunler as $key => $row)
  	makeDir(public_path('hbjson'));
  	$file = public_path('hbjson/file.json');
  	file_put_contents($file, json_encode($data));


  }

  function stokUpdate()
  {


  //	$xml = '';
  	$xml =	'<listings>';

  	$products = ProductMarketPlace::with('product')->whereHas('product', function ($q){
  		return $q->where('product_stok', '>', 0)->where('is_depo' , 1);
  	})->where('market_id',$this->pid)->get();

  	foreach($products as $item){

  		$adet = $item->product->product_stok > 1 ? $item->product->product_stok:0;

  		$xml .=	'<listing>';
  		$xml .=	'<HepsiburadaSku>'.$item->market_product_code.'</HepsiburadaSku>';
  		$xml .=	'<MerchantSku>'.$item->spec_barcode.'</MerchantSku>';
  		$xml .=	'<Price>'.str_replace('.',',',$item->price).'</Price>';
  		$xml .=	'<AvailableStock>'.$adet.'</AvailableStock>';
  		$xml .=	'<DispatchTime>0</DispatchTime>';
  		$xml .=	'<CargoCompany1>MNG Kargo</CargoCompany1>';
  		$xml .=	'<CargoCompany2/>';
  		$xml .=	'<CargoCompany3/>';
  		$xml .=	'</listing>';

  	}

  	$xml .=	'</listings>';



  	$response = $this->hb->stokUpdate($xml);


  	$durum = $this->hb->askDurum($response->Id);


  	$json_nots =  $durum->json();

  	DB::table('hb_results')->insert(['requestID'=>$durum['id'],"notes"=>"stok update işlemi","json_note"=>json_encode($json_nots),"created_at"=>date('Y-m-d H:i:s')]);



  }




  function getresult()
  {
  	$id = request('id');

  	$durum = $this->hb->askDurum($id);
  	$result = $durum->json();



  	DB::table('hb_results')->where('requestID',$id)->update(
  		[
  			"json_note"=>json_encode($result),
  			"updated_at"=>date('Y-m-d H:i:s')
  		]
  	);



  	return view('admin.hb.ajax.request',compact('result'));
  }


  function orders()
  {
  	$status="Created";


  	$title ="Yeni Hb Siparişleri";



  	$orders = $this->hb->getOrders();

  	$orders =  $orders->body();

  	$orders =  json_decode($orders);





  	return view('admin.order.hb.orders',compact('title','orders','status'));

  }



  function toCargo($page = 1)
  {


  	if (request()->has('saat')) {
  		$saat = request("saat");
  	}else{
  		$saat = 48;
  	}



  	$title ="Kargoya Hazır Hb Siparişleri";



  	$orders = $this->hb->getCargoOrders($saat,$page);

  	$orders =  $orders->body();

  	$orders =  json_decode($orders);



  	return view('admin.order.hb.paketli',compact('title','orders','saat','page'));

  }




  function getOrder($id = 0)
  {



  	$order = $this->hb->getOrder($id);

  	$order =  $order->body();

  	$order =  json_decode($order);

  	$varmi = DB::table('hborder')->where('order_id',$order->orderNumber)->first();

  	if (!$varmi) {

  		$this->saveSingleOrder($order->items[0]);

  	}


  	$products = [];

  	foreach($order->items as $row){
  		$sku = $row->sku;
  		$skod = hbSku($sku);
  		$products[$sku] = Product::where('product_stock_code',$skod)->first();

  	}

  	$hb_order = DB::table('hborder')->where('order_id',$id)->first();



  	return view('admin.order.hb.order',compact( 'order','products',"hb_order"));
  }

  function paketdetay($id = 0)
  {



  	$order = $this->hb->getOrder($id);

  	$order =  $order->body();

  	$order =  json_decode($order);



  	$products = [];

  	foreach($order->items as $row){
  		$sku = $row->sku;
  		$skod = hbSku($sku);
  		$products[$sku] = Product::where('product_stock_code',$skod)->first();

  	}

  	$hb_order = DB::table('hborder')->where('order_id',$id)->first();



  	return view('admin.order.hb.paketdetay',compact( 'order','products',"hb_order"));
  }


  function Picking($id)
  {


  	$order = $this->hb->getOrder($id);

  	$order =  $order->body();

  	$order =  json_decode($order);



  	$pcount = 0;
  	$lines = [];

  	foreach ($order->items as $key => $row) {
  		$pcount += $row->quantity;

  		$lines[] = ['id'=>$row->id,"quantity"=>$row->quantity];
  	}

  	$json = [
  		'parcelQuantity'=>$pcount,
  		"deci" =>$pcount*2,
  		"lineItemRequests"=>$lines
  	];



  	$order = $this->hb->paketleme($json);

  	$order =  $order->body();

  	$gelen =  json_decode($order);

  	DB::table('hborder')->where('order_id',$id)->update(['paketli'=>$order,"paket_no"=>$gelen->packageNumber,'cargo_barcode'=>$gelen->barcode]);


  }


  function tr($str){

  	$str=mb_convert_encoding($str, "ISO-8859-9","UTF-8");

  	$str =  str_replace('.', '. ', $str);

  	$str = str_replace('  ', ' ', $str);


  	return $str;


  }


  function cargoEtiket($id=0)
  {



  	$localOrder = DB::table('hborder')->where('paket_no',$id)->first();



  	$order = $this->hb->getOrder($localOrder->order_id);



  	$order =  $order->body();

  	$json =  json_decode($order);




  	$logo = asset("uploads/marketplaces/hb_logo.jpg");

  	$pdf=new PDF_Code128('P','mm','A4');
  	$pdf->AddPage('P');
  	$pdf->SetLineWidth(0.2);
  	$pdf->AddFont('arial','','arial.php');




  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(4,10);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(200,6,$this->tr( " Kargo şirketinin dikkatine, bu bir HepsiBurada.com gönderisidir. HepsiBurada anlaşmasına uygun işlem yapabilirsiniz."),'B','C');

// Logo
  	$pdf->Image($logo,5,25,80,25,"JPG");
  	$pdf->Rect( 120,20,80,38,"D");
// Kod
  	$pdf->SetDisplayMode(100,'default');
  	$pdf->SetFillColor(0,0,0);
  	$pdf->Code128(125,25,$localOrder->cargo_barcode,70,20);


  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(125,52);

  	$pdf->MultiCell(70,4,$localOrder->cargo_barcode,0,'C');

// Musteri Adı
  	$pdf->SetFont('Arial','B',12);
  	$pdf->SetXY(10,60);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(60,2,$this->tr("ALICI Bilgileri : "),0,'L');

  	$pdf->Line(10,64,60,64);


  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(12,68);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(100,2,$this->tr("Sipariş No : ".$localOrder->order_id),0,'L');


  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(12,74);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(100,2,$this->tr("Ad Soyad : ".$json->customer->name),0,'L');


  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(12,80);
  	$pdf->SetMargins(0,0,0);
  	//$pdf->MultiCell(100,5,$this->tr("Adres : ".$json->items[0]->shippingAddress->address ."\n".$json->items[0]->shippingAddress->town.' / '.$json->items[0]->shippingAddress->city),0,'L');
  	$pdf->MultiCell(100,5,$this->tr("Adres : ".$json->items[0]->shippingAddress->address),0,'L');


    //kargo şirketi


  	$pdf->SetFont('Arial','',20);
  	$pdf->SetXY(120,60);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(80,30,$this->tr($json->items[0]->cargoCompanyModel->name),1,'C');



    // Ürün Bilgileri
  	$pdf->SetFont('Arial','B',12);
  	$pdf->SetXY(10,103);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(95,2,$this->tr("Ürün Bilgileri : "),0,'L');

  	$pdf->Line(10,108,200,108);

  	$yAxis = 110;

  	$nerde = strlen($localOrder->exit_json)>5?json_decode($localOrder->exit_json,true):[];

  	foreach($json->items as $var){
  		$product = hbSkuPro($var->sku);
  		$isbn = $product->product_stock_code;
  		if (isset($nerde[$isbn])) {
  			$yol = kisalt($product->product_name,90) ." - ".$isbn  . " (". $nerde[$isbn].")";
  		}else{
  			$yol = kisalt($product->product_name,90) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
  		}

  		$pdf->Rect( 10,$yAxis+2,2,2,"F");
  		$pdf->SetFont('Arial','',8);
  		$pdf->SetXY(14,$yAxis);
  		$pdf->SetMargins(0,0,0);
  		$pdf->MultiCell(200,5,$this->tr($yol),0,'L');

  		$yAxis +=5;

  	}


  	$pdf->Output();

  }

  function cargoEtiket2($id=0)
  {

  	$localOrder = DB::table('hborder')->where('paket_no',$id)->first();



  	$order = $this->hb->getOrder($localOrder->order_id);



  	$order =  $order->body();

  	$json =  json_decode($order);


  	$pdf=new PDF_Code128('P','mm',[100,100]);
  	$pdf->AddPage('P');
  	$pdf->SetLineWidth(0.2);
  	$pdf->AddFont('arial','','arial.php');




  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(4,2);
  	$pdf->SetMargins(0,0,0);
  	$pdf->MultiCell(100,6,$this->tr( " HepsiBurada Gönderisi."),'B','C');



// Kod
  	$pdf->SetDisplayMode(100,'default');
  	$pdf->SetFillColor(0,0,0);
  	$pdf->SetAutoPageBreak(0);
  	$pdf->Code128(5,10,$localOrder->cargo_barcode,90,20);


  	$pdf->SetFont('Arial','',14);
  	$pdf->SetXY(5,33);
  	$pdf->SetAutoPageBreak(0);
  	$pdf->MultiCell(90,4,$localOrder->cargo_barcode.' - '.$this->tr( $json->items[0]->cargoCompanyModel->name),0,'C');

// Musteri Adı
  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(3,40);
  	$pdf->SetMargins(0,0,0);
  	$pdf->SetAutoPageBreak(0);
  	$pdf->MultiCell(90,2,$this->tr("ALICI Bilgileri : "),0,'L');

  	$pdf->Line(3,44,90,44);


  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(3,46);
  	$pdf->SetMargins(0,0,0);
  	$pdf->SetAutoPageBreak(0);
  	$pdf->MultiCell(90,2,$this->tr("Sipariş No : ".$localOrder->order_id),0,'L');


  	$pdf->SetFont('Arial','',10);
  	$pdf->SetXY(3,51);
  	$pdf->SetMargins(0,0,0);
  	$pdf->SetAutoPageBreak(0);
  	$pdf->MultiCell(90,2,$this->tr("Ad Soyad : ".$json->items[0]->shippingAddress->name),0,'L');


  	$pdf->SetFont('Arial','',9);
  	$pdf->SetXY(3,56);
  	$pdf->SetMargins(0,0,0);
  	$pdf->SetAutoPageBreak(0);
  	//$pdf->MultiCell(90,5,$this->tr("Adres : ".$json->items[0]->shippingAddress->address."\n".$json->items[0]->shippingAddress->town.' / '.$json->items[0]->shippingAddress->city),0,'L');
  	$pdf->MultiCell(90,5,$this->tr("Adres : ".$json->items[0]->shippingAddress->address),0,'L');








  	$pdf->Line(3,74,90,74);

  	$yAxis = 75;

  	$nerde = strlen($localOrder->exit_json)>5?json_decode($localOrder->exit_json,true):[];

  	$only_isbn = false;
  	if (count($json->items) > 3){
  		$only_isbn = true;
  	}

  	foreach($json->items as $var){
  		$product = hbSkuPro($var->sku);
  		$isbn = $product->product_stock_code;

  		if ($only_isbn){

  			if (isset($nerde[$isbn])) {
  				$yol = kisalt($product->product_name,20) ." - ".$isbn  . " (". $nerde[$isbn].")";
  			}else{
  				$yol = kisalt($product->product_name,20) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
  			}
  		}else{

  			if (isset($nerde[$isbn])) {
  				$yol = kisalt($product->product_name,30) ." - ".$isbn  . " (". $nerde[$isbn].")";
  			}else{
  				$yol = kisalt($product->product_name,30) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
  			}
  		}



  		$pdf->Rect( 3,$yAxis+1,2,2,"F");
  		$pdf->SetFont('Arial','',8);
  		$pdf->SetXY(6,$yAxis);
  		$pdf->SetMargins(0,0,0);
  		$pdf->SetAutoPageBreak(0);
  		$pdf->MultiCell(90,4,$this->tr($yol),0,'L');

  		$yAxis +=5;

  	}





  	$pdf->Output();

  }

  function cokluEtiket()
  {



  	$pdf=new PDF_Code128('P','mm',[100,100]);

  	$orders = $this->hb->getCargoOrders(48,1);

  	$orders =  $orders->body();

  	$orders =  json_decode($orders);

  	foreach($orders as $item){


  		$paket_no = $item->packageNumber;

  		$localOrder = DB::table('hborder')->where('paket_no',$paket_no)->first();

  		$order = $this->hb->getOrder($localOrder->order_id);



  		$order =  $order->body();

  		$json =  json_decode($order);


  		$pdf->AddPage('P');
  		$pdf->SetLineWidth(0.2);
  		$pdf->AddFont('arial','','arial.php');




  		$pdf->SetFont('Arial','',10);
  		$pdf->SetXY(4,2);
  		$pdf->SetMargins(0,0,0);
  		$pdf->MultiCell(100,6,$this->tr( " HepsiBurada Gönderisi."),'B','C');



// Kod
  		$pdf->SetDisplayMode(100,'default');
  		$pdf->SetFillColor(0,0,0);
  		$pdf->SetAutoPageBreak(0);
  		$pdf->Code128(5,10,$localOrder->cargo_barcode,90,20);


  		$pdf->SetFont('Arial','',14);
  		$pdf->SetXY(5,33);
  		$pdf->SetAutoPageBreak(0);
  		$pdf->MultiCell(90,4,$localOrder->cargo_barcode.' - '.$this->tr( $json->items[0]->cargoCompanyModel->name),0,'C');

// Musteri Adı
  		$pdf->SetFont('Arial','',10);
  		$pdf->SetXY(3,40);
  		$pdf->SetMargins(0,0,0);
  		$pdf->SetAutoPageBreak(0);
  		$pdf->MultiCell(90,2,$this->tr("ALICI Bilgileri : "),0,'L');

  		$pdf->Line(3,44,90,44);


  		$pdf->SetFont('Arial','',10);
  		$pdf->SetXY(3,46);
  		$pdf->SetMargins(0,0,0);
  		$pdf->SetAutoPageBreak(0);
  		$pdf->MultiCell(90,2,$this->tr("Sipariş No : ".$localOrder->order_id),0,'L');


  		$pdf->SetFont('Arial','',10);
  		$pdf->SetXY(3,51);
  		$pdf->SetMargins(0,0,0);
  		$pdf->SetAutoPageBreak(0);
  		$pdf->MultiCell(90,2,$this->tr("Ad Soyad : ".$json->items[0]->shippingAddress->name),0,'L');


  		$pdf->SetFont('Arial','',9);
  		$pdf->SetXY(3,56);
  		$pdf->SetMargins(0,0,0);
  		$pdf->SetAutoPageBreak(0);
  	//$pdf->MultiCell(90,5,$this->tr("Adres : ".$json->items[0]->shippingAddress->address."\n".$json->items[0]->shippingAddress->town.' / '.$json->items[0]->shippingAddress->city),0,'L');
  		$pdf->MultiCell(90,5,$this->tr("Adres : ".$json->items[0]->shippingAddress->address),0,'L');








  		$pdf->Line(3,74,90,74);

  		$yAxis = 75;

  		$nerde = strlen($localOrder->exit_json)>5?json_decode($localOrder->exit_json,true):[];

  		$only_isbn = false;
  		if (count($json->items) > 3){
  			$only_isbn = true;
  		}

  		foreach($json->items as $var){
  			$product = hbSkuPro($var->sku);
  			$isbn = $product->product_stock_code;

  			if ($only_isbn){

  				if (isset($nerde[$isbn])) {
  					$yol = kisalt($product->product_name,20) ." - ".$isbn  . " (". $nerde[$isbn].")";
  				}else{
  					$yol = kisalt($product->product_name,20) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
  				}
  			}else{

  				if (isset($nerde[$isbn])) {
  					$yol = kisalt($product->product_name,30) ." - ".$isbn  . " (". $nerde[$isbn].")";
  				}else{
  					$yol = kisalt($product->product_name,30) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
  				}
  			}



  			$pdf->Rect( 3,$yAxis+1,2,2,"F");
  			$pdf->SetFont('Arial','',8);
  			$pdf->SetXY(6,$yAxis);
  			$pdf->SetMargins(0,0,0);
  			$pdf->SetAutoPageBreak(0);
  			$pdf->MultiCell(90,4,$this->tr($yol),0,'L');

  			$yAxis +=5;

  		}


  	}


  	$pdf->Output();

  }


  function saveSingleOrder($row)
  {


  	$dizi = [

  		'order_id'=>$row->orderNumber,
  		'durum'=>$row->status,
  		'tarih'=>$row->orderDate,
  		'json'=>json_encode($row),

  	];

  	DB::table('hborder')->updateOrInsert(
  		['order_id' => $row->orderNumber],
  		$dizi
  	);


  	$durum = DB::table('hborder')->where('order_id',$row->orderNumber)->first();

  	$nerden = [];

  	if ($durum->depoexit == 0) {



  		$urun =   ProductMarketPlace::with('product')->where('market_product_code',$row->sku)->first();

  		$book = DB::table('depo_book')->orderBy('adet','asc')->where('isbn',$urun->spec_barcode)->where('adet','>',0)->first();

  		if ($book) {

  			$raf = DB::table('raf')->where('id',$book->raf_id)->first();

  			$isbn = $urun->spec_barcode;

  			$adets = $row->quantity;
  			DB::table('depo_book')->where('id',$book->id)->decrement('adet',$adets);
  			DB::table('products')->where('id',$book->book_id)->decrement('product_stok',$adets);

  			Depolog::create(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adets*-1),'book_id'=>$book->book_id,'notes'=>$row->orderNumber." ID li sipariş HepsiBurada Siparişi kayıt edilirken otomatik çıkış yapıldı"]);

  			$nerden[$isbn] = $raf->raf_name.' '.$adets.' adet';
  			DB::table('hborder')->where('order_id',$row->orderNumber)->update(['depoexit'=>1,'exit_json'=>json_encode($nerden)]);
  		}





  	}


  }



  function excel()
  {







  	$orders = $this->hb->getCargoOrders(48,1);

  	$orders =  $orders->body();

  	$orders =  json_decode($orders);




  	header('Content-Encoding: UTF-8');
  	header('Content-Type: text/plain; charset=utf-8');
  	header("Content-disposition: attachment; filename=".date('d-m-y')."_paket_listesi.xls");

  	echo "\xEF\xBB\xBF";



  	echo '<table border="1">';



  	foreach($orders as $val){

  		echo '<tr>';

  		echo '<td>'.$val->barcode.'</td>';

  		echo '</tr>';
  	}

  	echo '</table>';


  }




}
