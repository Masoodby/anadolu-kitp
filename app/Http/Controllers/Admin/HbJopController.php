<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MarketPlaces;
use App\Models\Depolog;
use App\Library\Hb;

use App\Library\PDF_Code128;

use App\Models\Category;
use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\MarketWriterMatch;
use App\Models\Yayinevi;
use App\Models\Yazar;


use App\Models\Product;
use App\Models\ProductMarketPlace;
use DB;

class HbJopController extends Controller
{
   public $api_key;
   public $api_secret;


   public $pid = 3;
   public $hb;
   public $hb_config;

   public function __construct()
   {
    $this->hb_config = MarketPlaces::find($this->pid);
    $config = ['seller_id'=>$this->hb_config->user_no,"api_user"=>$this->hb_config->api_id,"api_pass"=>$this->hb_config->api_key];
    $this->hb =  new Hb($config);

}



function saveOrders()
{





    $orders = $this->hb->getOrders();


    $orders =  $orders->body();

    $orders =  json_decode($orders);

    foreach ($orders->items as $row) {

        $dizi = [

            'order_id'=>$row->orderNumber,
            'durum'=>$row->status,
            'tarih'=>$row->orderDate,            
            'json'=>json_encode($row),
            
        ];

        DB::table('hborder')->updateOrInsert(
            ['order_id' => $row->orderNumber],
            $dizi
        );


        $durum = DB::table('hborder')->where('order_id',$row->orderNumber)->first();

        $nerden = [];

        if ($durum->depoexit == 0) {



            $urun =   ProductMarketPlace::with('product')->where('market_product_code',$row->sku)->first();

            $book = DB::table('depo_book')->orderBy('adet','asc')->where('isbn',$urun->spec_barcode)->where('adet','>',0)->first();

            if ($book) {

                $raf = DB::table('raf')->where('id',$book->raf_id)->first();

                $isbn = $urun->spec_barcode;

                $adets = $row->quantity;
                DB::table('depo_book')->where('id',$book->id)->decrement('adet',$adets);
                DB::table('products')->where('id',$book->book_id)->decrement('product_stok',$adets);

                Depolog::create(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adets*-1),'book_id'=>$book->book_id,'notes'=>$row->orderNumber." ID li sipariş HepsiBurada Siparişi kayıt edilirken otomatik çıkış yapıldı"]);

                $nerden[$isbn] = $raf->raf_name.' '.$adets.' adet';
                DB::table('hborder')->where('order_id',$row->orderNumber)->update(['depoexit'=>1,'exit_json'=>json_encode($nerden)]);
            }





        }



    }

}

}



