<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\BasketProduct;
use App\Models\Combo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
	public function index()
	{



		$order_adet  = Order::select(DB::raw('order_state as durum,count(id) as adet, sum(grand_total) as toplam'))->orderBy("order_state","asc")->groupBy('order_state')->get();

		$best_product = BasketProduct::with("product.detail")->select(DB::raw('*,count(id) as adet, sum(qty) as toplam'))
		->where("state",1)
		->orderBy("toplam","desc")->groupBy('product_id')->limit(20)->get();

        
		$chart = true;

		return view('admin.home.index' ,compact("chart",'order_adet','best_product'));

	}




	function resim()
	{
		$all = Product::where('resim_durum',1)->limit(1000)->get();

		ini_set('max_execution_time', '0');
		ini_set("memory_limit", "-1");

		$i = 0;
		foreach ($all as $key => $item) {

			$resim_yol = public_path('uploads/product/'.$item->product_stock_code.'.webp');
			$name = $item->product_stock_code.'.jpg';

			$resim_jpg = public_path('uploads/jpgler/jpg/'.$item->product_stock_code.'.jpg');

			if (!file_exists($resim_jpg)) {
				die("resim yok");

				jpegCreate($resim_yol,$name,'jpgler');
				Product::where('id',$item->id)->update(["resim_durum"=>3]);
			}else{
				die("resim var");
			}

			$i++;	}

			if ($i>0) {
				echo "<script> setTimeout(function(){location.href='/crudv4/resim'},2000)</script>";

			}else{
				die('bitti');
			}

		}
	}
