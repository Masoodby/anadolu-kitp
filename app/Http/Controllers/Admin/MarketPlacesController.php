<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MarketPlaces;
use App\Library\Tryol;
use App\Library\N11;
use App\Models\Product;

use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\ProductMarketPlace;


use App\Models\MarketWriterMatch;
use App\Models\Yayinevi;
use App\Models\Yazar;



class MarketPlacesController extends Controller
{

	protected $filter = ['product_name','product_cat','id','is_active','product_price','product_stok','yayinci_id','yazar_id'];
	public function form($id = 0)
	{




		$list = MarketPlaces::get();
		// $cargoList =  Tryol::cargo_list();

        $cargoList =[0,1,2];
		$title ="Pazar Yerleri Tanımları";
		$page = "add";
		$entry = new MarketPlaces;

		if ($id>0) {
			$entry = MarketPlaces::find($id);
			$page = "edit";
		}


		return view('admin.marketplaces.'.$page,compact('entry',  'title','list','cargoList'));
	}




	function save($id = 0)
	{


		$data = request()->only(
            'name',
            'cargo',
            'user_no',
            'api_id',
            'api_key',
            'percent_0-10',
            'price_0-10',
            'percent_10-20',
            'price_10-20',
            'percent_20-30',
            'price_20-30',
            'percent_30-40',
            'price_30-40',
            'percent_40-50',
            'price_40-50',
            'percent_50-100',
            'price_50-100',
            'percent_100-1000',
            'price_100-1000',
        );


		if ($id>0) {
			$entry = MarketPlaces::where('id',$id)->firstOrFail();
			$entry->update($data);
			$page = "edit";

		}else{
			$entry = MarketPlaces::create($data);
			$page = "add";
		}


		return redirect()
		->route('crudv4.marketplaces.'.$page,$entry->id)
		->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
		->with('mesaj_tur','success');
	}

	function del($id)
	{

		$del = MarketPlaces::find($id);

		$del->delete();

		return "1";
	}

	function marketproduct($pid=1)
	{

		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}




		$query = ProductMarketPlace::query();




		$query->when(request()->has('product_stock_code'), function ($q) {
			$val = request('product_stock_code');
			return $q->where('product_stock_code',  intval($val));
		});





		$query->when(request()->has('yayinci_id'), function ($q) {
			$val = request('yayinci_id');
			return $q->whereHas('extend', function ($q) use($val){
				return $q->where('yayinci_id', '=', $val);
			});
		});

		$query->when(request()->has('is_active') && request('is_active')<2 , function ($q) {
			$val = request('is_active');
			return $q->where('is_active',  intval($val));
		});




		$list = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->orderBy($orderby[0],$orderby[1] )->paginate($limit);







		$selectbox = Category::attr(['name' => 'product_cat',"class"=>"form-control selectfilter ml-3 sel2","data-filter"=>"product_cat"]);

		if (request()->has('categories_main')) {
			$main_cats =  $selectbox->selected(request('product_cat'))->renderAsDropdown();
		}else{
			$main_cats =  $selectbox->renderAsDropdown();
		}





		$title ="Ürün Yönetimi";



		return view('admin.product.index',compact('list', 'main_cats','title' ));
	}


	function usePrice($pid = 1)
	{

		echo "değişiklikler biraz zaman alabilir sayfa hataya düşse de işlem tamamalanacaktır";
		$urunler = ProductMarketPlace::with('product.detail')->where('market_id',$pid)->where('fixed',0)->get();

		$oran = MarketPlaces::where('id',$pid)->first();

		$percent_unit = $oran->percent_unit;
		$price_unit = $oran->price_unit;


		foreach ($urunler as $key => $row) {


			if ($row->product->product_price == 0) {

				$oran_price = yuzdex($row->product->detail->product_old_price,20);
				$price = yuzdePlus($oran_price,$percent_unit);
				$price = floatval($price) + floatval($price_unit);

			}else{

				$price = yuzdePlus($row->product->product_price,$percent_unit);
				$price = floatval($price) + floatval($price_unit);

			}

			$list_price = yuzdePlus($price,25);

			$row->update(['price'=>$price,"list_price"=>$list_price]);



		}
	}

}
