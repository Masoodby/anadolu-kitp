<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Library\PDF_Code128;
use Illuminate\Http\Request;
use App\Library\N11;
use App\Models\MarketPlaces;
use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\ProductMarketPlace;
use App\Models\DepoBook;
use App\Models\Product;
use Illuminate\Support\Facades\DB;


class N11Controller extends Controller
{
	public $api_key;
	public $api_secret;


	public $pid = 2;
	public $n11;
	public $n11_config;
	public function __construct()
	{
		$this->n11_config = MarketPlaces::find($this->pid);
		$config = [ "appKey"=>$this->n11_config->api_id,"appSecret"=>$this->n11_config->api_key];
		$this->n11 =  new N11($config);

	}




	function index()
	{

		$products = $this->n11->GetProductList();

		$title = "N11  Ürünler";


		//$last_queries = DB::table('trendyol_results')->orderByDesc('id')->limit(500)->get();
		$last_queries = [];

		return view('admin.n11.index',compact('title','products','last_queries'));
	}


	function products($page=0)
	{
		$title ="N11 Ürünler";
		$products_query = $this->n11->GetProductList(100,$page);

		if ($products_query->result->status =="success") {
			$products = isset($products_query->products->product)?$products_query->products->product:[];
			$pagingData = isset($products_query->pagingData)?$products_query->pagingData:[];
		}else{
			$products = [];
			$pagingData = false;

		}

		


		return view('admin.n11.products',compact('title','products',"pagingData"));

	}



	function catmatch()
	{

		$title ="N11 Kategori Eşleştirme";
		$page = "cat_match";
		$req =  $this->n11->GetTopLevelCategories();
		$cats =  Category::nested()->get();
		$matched =  MarketCatMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();

		if ($req->result->status == "success") {
			$maincat = $req->categoryList->category;
		}

		return view('admin.n11.'.$page,compact('title','maincat','cats','matched'));
	}




	function loadCat($main)
	{
		$req = $this->n11->GetSubCategories($main);

		if ($req->result->status == "success") {

			$cat_name = request('name');
			$cat_id = $main;

			if (isset($req->category->subCategoryList->subCategory) ) {

				$maincat  = $req->category->subCategoryList->subCategory;


			}else{

				$maincat  = [];

			}



			$level = request('level');
			return view('admin.n11.cat_list',compact('maincat','level','cat_name','cat_id'));


		}else{
			return "Hata";
		}
	}


	function save()
	{
		

		foreach (request('cats') as $key => $item) {
			
			//$cat = Category::where('id',$key)->firstOrFail();

			MarketCatMatch::updateOrCreate([

				'local_id'   => $key,
				'remote_id'   => request('remote_cat_id'),
				'pazaryeri_id'   => $this->pid,
			],[
				'local_id'   => $key,
				'remote_id'   => request('remote_cat_id'),
				'pazaryeri_id'   => $this->pid,
				'local_name'   => $item,
				'remote_name'   => request('remote_cat_name'),
			]);


		}



		$matched =  MarketCatMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();

		$typeval = "cat";

		return view('admin.n11.matched',compact('matched','typeval'));
	}


	function catRequired($id=1002191)
	{
		$req = $this->n11->GetCategoryAttributesId($id);

		$vals =  $this->n11->GetCategoryAttributeValue(354193946);

		prep($vals);

		$attrs = $req->categoryProductAttributeList->categoryProductAttribute;

		$attr = [];
		foreach ($attrs as $key => $var) {

			$attr[$var->id] = [
				"name"=>$var->name,
				"req"=>$var->mandatory==1?"yes":"no",
				"select"=>$var->multipleSelect==1?"yes":"no",
			];

			if ($var->multipleSelect==1) {
				$vals =  $this->n11->GetCategoryAttributeValue($var->id);


			}

		}
	}



	function orders($status = "New")
	{


		if (request()->has('status')) {
			$status = request("status");
		}


		$data = [
			"buyerName"=>"",
			"recipient"=>"",
			"period"=>"",
			"sortForUpdateDate"=>"",
			"orderNumber"=>"",
			'status'=>$status,
		];

		$orders = $this->n11->DetailedOrderList($data);



		$title ="N11 Siparişleri";

		return view('admin.order.n11.orders',compact('title', 'orders','status'));

	}


	function getOrder($id = 0)
	{


		$order = $this->n11->OrderDetail($id);



		if ($order->result->status =="success") {

			$order_data = [
				"order_id"=>$id,
				"durum"=>$order->orderDetail->status,
				"json"=>json_encode($order->orderDetail),
				"depoexit"=>0,
				"tarih"=>$order->orderDetail->createDate,
			];

			$varmi = DB::table('n11order')->where("order_id",$id)->count();
			if($varmi ==0){
				DB::table('n11order')->insert($order_data);



				$urun = $order->orderDetail->itemList->item;

				if (is_array($urun)) {

					foreach ($urun as $item) {
						$uid = $item->productSellerCode;
						$adet = $item->quantity;
						DB::table('products')->where('id',$uid)->decrement('product_stok',$adet);
					}
				}else{
					$uid = $order->orderDetail->itemList->item->productSellerCode;
					$adet = $order->orderDetail->itemList->item->quantity;

					DB::table('products')->where('id',$uid)->decrement('product_stok',$adet);
				}

			}

		} 

		$trorder = DB::table('n11order')->where("order_id",$id)->first();



		return view('admin.order.n11.order',compact( 'order',"trorder"));
	}



	function Picking($id = 0)
	{
		
		$order = $this->n11->OrderDetail($id);

		if ($order->result->status =="success") {

			$itemList = is_array($order->orderDetail->itemList->item)?$order->orderDetail->itemList->item:[$order->orderDetail->itemList->item];

			foreach ($itemList as $key => $item) {


				$itemId = $item->id;

				$req = $this->n11->OrderItemAccept($itemId);



			}


		}
	}


	function exitdepo($id)
	{

		$val = explode("-",request("name"));

		$adets = $val[2];

		$book = DB::table('depo_book')->where('id',$val[1])->first();


		DB::table('depo_book')->where('id',$val[1])->decrement('adet',$adets);

		DB::table('depo_log')->insert(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adets*-1),'book_id'=>$book->book_id]);

		DB::table('n11order')->where('order_id',$id)->update(['depoexit'=>1]);

	}


	function cargoEtiket($id=0)
	{


		$kargo_durum = [
			1 => "N11 Öder",
			2 =>"Alıcı Öder",
			3=>"Mağaza Öder",
			4=>"Şartlı Kargo (Alıcı Öder)",
			5 => "Şartlı Kargo Ücretsiz (Satıcı Öder)"

		];



		$order = $this->n11->OrderDetail($id);

		$json = $order->orderDetail;

		$itemList = is_array($json->itemList->item)?$json->itemList->item:[$json->itemList->item];

		$campaignNumber = $itemList[0]->shipmenCompanyCampaignNumber;
		$kargoName = $itemList[0]->shipmentInfo->shipmentCompany->name;
		$kargoNotKey = $itemList[0]->deliveryFeeType;
		$kargoNot = $kargo_durum[$kargoNotKey];


		$logo = asset("uploads/marketplaces/n11_logo.jpg");

		$pdf=new PDF_Code128('P','mm','A4');
		$pdf->AddPage('P');
		$pdf->SetLineWidth(0.2);
		$pdf->AddFont('arial','','arial.php');




		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(4,10);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(200,6,$this->tr( " Kampanya kodunun hata vermesi durumunda çıkış yapmayınız, gönderici firma ile irtibata geçiniz.."),'B','C');

// Logo
		$pdf->Image($logo,5,25,80,25,"JPG");
		$pdf->Rect( 120,20,80,38,"D");
// Kod
		$pdf->SetDisplayMode(100,'default');
		$pdf->SetFillColor(0,0,0);
		$pdf->Code128(125,25,$campaignNumber,70,20);


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(125,52);

		$pdf->MultiCell(70,4,$campaignNumber,0,'C');

// Musteri Adı
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY(10,60);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(60,2,$this->tr("ALICI Bilgileri : "),0,'L');

		$pdf->Line(10,64,60,64);


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(12,68);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,2,$this->tr("Sipariş No : ".$json->orderNumber),0,'L');


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(12,74);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,2,$this->tr("Ad Soyad : ".$json->shippingAddress->fullName),0,'L');

		$adres = $json->shippingAddress->address.' '.$json->shippingAddress->neighborhood.' '.$json->shippingAddress->district.' '.$json->shippingAddress->city;
		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(12,80);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,5,$this->tr("Adres : ". $adres ),0,'L');


    //kargo şirketi


		$pdf->SetFont('Arial','',16);
		$pdf->SetXY(120,60);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(80,30,$this->tr($kargoName),1,'C');


		$pdf->SetFont('Arial','',12);
		$pdf->SetXY(120,67);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(80,30,$this->tr( ' ('.$kargoNot.')'),0,'C');



    // Ürün Bilgileri
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY(10,100);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(95,2,$this->tr("Ürün Bilgileri : "),0,'L');

		$pdf->Line(10,105,200,105);

		$yAxis = 108;

		foreach($itemList as $var){

			$pdf->Rect( 10,$yAxis+2,2,2,"F");
			$pdf->SetFont('Arial','',8);
			$pdf->SetXY(14,$yAxis);
			$pdf->SetMargins(0,0,0);
			$pdf->MultiCell(200,5,$this->tr(kisalt($var->productName,90) ." - ".$var->sellerStockCode .' - '.$var->quantity.' adet '. "(". nerde($var->sellerStockCode).")"),0,'L');

			$yAxis +=5;

		}


		$pdf->Output();

	}

	function tr($str){

		$str=mb_convert_encoding($str, "ISO-8859-9","UTF-8");

		$str =  str_replace('.', '. ', $str);

		$str = str_replace('  ', ' ', $str);


		return $str;


	}



	function changeCat($id)
	{
		$urun = Product::with('market');
	}



}
