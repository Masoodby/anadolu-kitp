<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Category;


use App\Library\N11;
use App\Models\MarketPlaces;
use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\ProductMarketPlace;
use App\Models\DepoBook;
use App\Models\Product;
use DB;

class N11JopController extends Controller
{

	public $api_key;
	public $api_secret;


	public $pid = 2;
	public $n11;
	public $n11_config;
	public function __construct()
	{
		$this->n11_config = MarketPlaces::find($this->pid);
		$config = [ "appKey"=>$this->n11_config->api_id,"appSecret"=>$this->n11_config->api_key];
		$this->n11 =  new N11($config);

	}


	function saveProduct()
	{



		$mevcut = ProductMarketPlace::where('market_id',$this->pid)->get()->pluck('product_id');


		$localProducts = Product::with('detail',"extend")->where("is_active",1)->where("product_stok",">",3)->whereNotIn("id",$mevcut)->get();

		foreach ($localProducts as $key => $var) {



			if ( $var->product_price == 0) {

				$oran_price = yuzdex($var->detail->product_old_price,25);
				$price = yuzdePlus($oran_price,$this->n11_config->percent_unit);
				$price = floatval($price) + floatval($this->n11_config->price_unit);

			}else{

				$price = yuzdePlus($var->product_price,$this->n11_config->percent_unit);
				$price = floatval($price) + floatval($this->n11_config->price_unit);

			}

			$book_details = json_decode($var->extend->extend_json,true);

			$yayinci = $var->extend->getYayinci->yayinci_name;

			$cat_remote = MarketCatMatch::where( ['local_id'=>$var->product_cat, "pazaryeri_id" =>$this->pid])->first();

			if ($cat_remote) {



				$isbn = $var->product_stock_code;

				$src = public_path('uploads/product/'.$isbn.'.webp');

				if (file_exists($src)) {

					$varmi = public_path('uploads/product/jpg/'.$isbn.'.jpg');

					if (!file_exists($varmi)) {


						jpegCreate($src,$isbn.'.jpg','product');

					}
				}

				$url = "https://www.anadolukitap.com/uploads/product/jpg/".$isbn.".jpg";

				$img["image"] = [
					"url"=>$url,
					"order"=>1,
				];

				$subtitle = empty($var->product_sub_name)?$var->extend->getYazar->yazar_name.' '.$var->extend->getYayinci->yayinci_name: $var->product_sub_name;
				$product = [
					"productSellerCode" => $var->id,
					"title" => str_limit($var->product_name,65),
					"subtitle" => str_limit($subtitle,65),
					"description" => $var->product_content,
					"price" => $price,
					"currencyType" =>1,
					"domestic" =>true,
					"shipmentTemplate" =>"mng_cargo",
					"productCondition" =>1,
					"saleStartDate" =>"",
					"saleEndDate" =>"",
					"productionDate" =>"",
					"expirationDate" =>"",
					"discount" =>"",
					"unitInfo" =>"",
					"maxPurchaseQuantity" =>$var->product_stok,
					"preparingDay" =>2,
					"images" =>$img,
					"groupAttribute" =>"Kitap Detayları",
					"groupItemCode" =>"varsayilan",
					"itemName" =>"varsayilan",
					'attributes' => [
						'attribute' => $this->attrSet($book_details,$yayinci)
					],

					'category' => [
						'id' => $cat_remote->remote_id
					],


					'stockItems' => [
						'stockItem' => [
							"quantity" =>$var->product_stok,
							"gtin" =>$var->product_stock_code,
							"n11CatalogId" =>$cat_remote->remote_id,
							"sellerStockCode" =>$var->product_stock_code,
							"attributes" =>
							[
								'attribute' => []
							],
							"optionPrice" =>$price,
						]
					],


				];

			//	prep($product);

				$save_req = $this->n11->SaveProduct($product);

				sleep(1);

				unset($product);
				unset($img);
				unset($attr);

			}// kat eşleşmişse


		}
	}


	function delProductAll22($page=0)
	{






		$products_query = $this->n11->GetProductList(30,$page);

		if ($products_query->result->status =="success" && isset($products_query->products->product)) {


			$products = $products_query->products->product;



			foreach ($products as $key => $item) {



				$req = 	$this->n11->DeleteProductById($item->id);

				echo $req->product->title." Silindi<br>";



			}


			$i = $page +1;
			echo "ekleniyor -- > <br>".$i;
			echo "<script>setTimeout(function(){location.href='".url('crudv4/n11jop/delProductAll/'.$i)."'},2000)</script>";


		}else{
			die("there is not product");
		}




	}

/*


{
"id":485462341,
"productSellerCode":"108223",
"title":"Eyy\u00fcbiler - Ha\u00e7l\u0131lar Kar\u015f\u0131s\u0131nda \u0130slam'\u0131n Sa\u011flam Bir Kalesi",
"subtitle":"Bedrettin Basu\u011fuy Siyer Yay\u0131nlar\u0131",
"price":"38.30",
"displayPrice":"38.30",
"isDomestic":true,
"saleStatus":2,
"approvalStatus":1,
"stockItems":{
"stockItem":{
"sellerStockCode":"9786059283922"
,"optionPrice":"38.30",
"currencyAmount":"38.30",
"displayPrice":"38.30",
"gtin":"9786059283922",
"bundle":false,
"id":126987147925,
"version":2,
"quantity":4,
"attributes":{}}},
"currencyAmount":"38.30",
"currencyType":1}

*/




function delProductAll($page=0)
{



	$products_query = $this->n11->GetProductList(100,$page);

	if ($products_query->result->status =="success" && isset($products_query->products->product)) {


		$products = $products_query->products->product;



		foreach ($products as $key => $item) {


			$skod = is_array($item->stockItems)?$item->stockItems[0]->stockItem->sellerStockCode:$item->stockItems->stockItem->sellerStockCode;

			ProductMarketPlace::updateOrCreate(
				['market_id' => $this->pid, 'product_id' => $item->productSellerCode],
				[
					'price' =>  $item->price,
					'list_price' =>  $item->price,
					'on_sale' =>  $item->saleStatus,
					'approved' =>  $item->approvalStatus,
					'locked' =>  0,
					'fixed' =>  0,
					'spec_barcode' =>  $skod,
					'jsons' => json_encode($item),
				]
			);


		}


		$i = $page +1;
		echo "ekleniyor -- > <br>".$i;
		echo "<script>setTimeout(function(){location.href='".url('crudv4/n11jop/delProductAll/'.$i)."'},2000)</script>";


	}else{
		die("there is not product");
	}




}


function attrSet($data,$yayinci)
{
	if (strlen($data["yayin-tarihi"]['value']) > 4) {


		$attr[] = [

			"id" => 354193949,
			"name" => "Basım Tarihi",
			"value" => $data["yayin-tarihi"]['value'],

		];

	}


	if ($data["baski-sayisi"]['value'] != null) {


		$attr[] = [

			"id" => 354194000,
			"name" => "Baskı Sayısı",
			"value" => $data["baski-sayisi"]['value'],

		];

	}


	if (strlen($data["isbn"]['value']) > 5) {


		$attr[] = [

			"id" => 354193945,
			"name" => "ISBN",
			"value" => $data["isbn"]['value'],

		];

	}

	if (strlen($data["cilt-tipi"]['value']) > 3) {


		$attr[] = [

			"id" => 354194003,
			"name" => "Kitap Türü",
			"value" => $data["cilt-tipi"]['value'],

		];

	}


	if (strlen($data["sayfa-sayisi"]['value']) > 1) {


		$attr[] = [

			"id" => 354194001,
			"name" => "Sayfa Sayısı",
			"value" => $data["sayfa-sayisi"]['value'],

		];

	}

	if (strlen($data["sayfa-sayisi"]['value']) > 1) {


		$attr[] = [

			"id" => 354080711,
			"name" => "Yayınevi",
			"value" => $yayinci,

		];

	}


	$attr[] = [

		"id" => 354194002,
		"name" => "Yayın Dili",
		"value" => 7583455,

	];





	return $attr;
}


function saveOrder()
{

	$data = [
		"buyerName"=>"",
		"recipient"=>"",
		"period"=>"",
		"sortForUpdateDate"=>"",
		"orderNumber"=>"",
		'status'=>"New",
	];

	$save_req = $this->n11->OrderList($data);
	//	$save_req = $this->n11->DetailedOrderList($data);


	if (isset($save_req->orderList->order)) {


		$order = $save_req->orderList->order;

		if (is_array($order)) {

			foreach ($save_req->orderList->order as $key => $val) {


				$detay = $this->n11->OrderDetail($val->id);

				if ($detay->result->status =="success") {

					$order_data = [
						"order_id"=>$val->id,
						"durum"=>$detay->orderDetail->status,
						"json"=>json_encode($detay->orderDetail),
						"depoexit"=>0,
						"tarih"=>$detay->orderDetail->createDate,
					];
					$nerden = [];

					$varmi = DB::table('n11order')->where("order_id",$val->id)->count();
					if($varmi ==0){
						DB::table('n11order')->insert($order_data);



						$urun = $detay->orderDetail->itemList->item;

						if (is_array($urun)) {

							foreach ($urun as $item) {
								$uid = $item->productSellerCode;
								$adet = $item->quantity;

								$isbn =  barkod_rep($item->sellerStockCode);
								$book = DB::table('depo_book')->orderBy('adet','asc')->where('isbn',$isbn)->where('adet','>',-1)->first();



								DB::table('products')->where('id',$uid)->decrement('product_stok',$adet);


								$raf = DB::table('raf')->where('id',$book->raf_id)->first();




								DB::table('depo_book')->where('id',$book->id)->decrement('adet',$adet);

								DB::table('depo_log')->insert(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adet*-1),'book_id'=>$book->book_id]);

								$nerden[$isbn] = $raf->raf_name.' nolu raftan  '.$adet.' adet çıkış yapıldı';



							}
						}else{
							$uid = $detay->orderDetail->itemList->item->productSellerCode;
							$adet = $detay->orderDetail->itemList->item->quantity;

							$isbn =  barkod_rep($detay->orderDetail->itemList->item->sellerStockCode);
							$book = DB::table('depo_book')->orderBy('adet','asc')->where('isbn',$isbn)->where('adet','>',0)->first();



							DB::table('products')->where('id',$uid)->decrement('product_stok',$adet);


							$raf = DB::table('raf')->where('id',$book->raf_id)->first();




							DB::table('depo_book')->where('id',$book->id)->decrement('adet',$adet);

							DB::table('depo_log')->insert(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adet*-1),'book_id'=>$book->book_id]);

							$nerden[$isbn] = $raf->raf_name.' nolu raftan  '.$adet.' adet çıkış yapıldı';
						}


						DB::table('n11order')->where('order_id',$val->id)->update(['depoexit'=>1,'exit_json'=>json_encode($nerden)]);

					}
				}



			}

		}else{

			$order = $save_req->orderList->order;


			$detay = $this->n11->OrderDetail($order->id);



			if ($detay->result->status =="success") {

				$order_data = [
					"order_id"=>$order->id,
					"durum"=>$detay->orderDetail->status,
					"json"=>json_encode($detay->orderDetail),
					"depoexit"=>0,
					"tarih"=>$detay->orderDetail->createDate,
				];
				$varmi = DB::table('n11order')->where("order_id",$order->id)->count();
				if($varmi ==0){
					DB::table('n11order')->insert($order_data);



					$urun = $detay->orderDetail->itemList->item;

					if (is_array($urun)) {

						foreach ($urun as $item) {
							$uid = $item->productSellerCode;
							$adet = $item->quantity;
							DB::table('products')->where('id',$uid)->decrement('product_stok',$adet);
						}
					}else{
						$uid = $detay->orderDetail->itemList->item->productSellerCode;
						$adet = $detay->orderDetail->itemList->item->quantity;

						DB::table('products')->where('id',$uid)->decrement('product_stok',$adet);
					}

				}

			}



		}

		}//sipariş yok


	}


	function stok_update($indis = 0)
	{

		$mevcuts = ProductMarketPlace::with("product")->where('market_id',$this->pid)->get()->chunk(10);

		if (isset($mevcuts[$indis])) {
			$mevcut = $mevcuts[$indis];
			foreach ($mevcut as $key => $row) {


				$detay = json_decode($row->jsons,true);

				$req = $this->n11->UpdateStockByStockSellerCode($row->product->product_stock_code,$row->product->product_stok);


				sleep(1);


			}


			$i = $indis +1;
			echo "ekleniyor -- > <br>".$i;
			echo "<script>setTimeout(function(){location.href='".url('crudv4/n11jop/stok_update/'.$i)."'},2000)</script>";

		}else{
			echo "bitti";
			die();
		}


	}

	function price_update($indis = 0)
	{

		$mevcuts = ProductMarketPlace::with("product")->where('market_id',$this->pid)->get()->chunk(20);



		if (isset($mevcuts[$indis])) {
			$mevcut = $mevcuts[$indis];


			foreach ($mevcut as $key => $row) {



				$price = yuzdePlus($row->product->product_price,$this->n11_config->percent_unit);
				$price = floatval($price) + floatval($this->n11_config->price_unit);

				$dizi["productSellerCode"] = $row->product->id;
				$dizi["price"] = $price;
				$dizi["currencyType"] = "1";
				$dizi["stockItems"]["stockItem"]["sellerStockCode"] = $row->product->product_stock_code;
				$dizi["stockItems"]["stockItem"]["optionPrice"] = $price;




				$req = $this->n11->UpdateProductPriceBySellerCode($dizi);

				sleep(1);



			}


			$i = $indis +1;
			echo "ekleniyor -- > <br>".$i;
			echo "<script>setTimeout(function(){location.href='".url('crudv4/n11jop/price_update/'.$i)."'},2000)</script>";

		}else{
			echo "bitti";
			die();
		}
	}

}
