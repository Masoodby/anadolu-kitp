<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Oda;
use App\Models\Depo;
use Illuminate\Support\Facades\DB;

class OdaController extends Controller
{
	public function form($id = 0)
	{


		$odaAdetler = DB::table('depo')
		->leftJoin('oda', 'oda.depo_id', '=', 'depo.id')
		->leftJoin('raf','raf.oda_id' , '=', 'oda.id')
		->leftJoin('depo_book','depo_book.raf_id' , '=', 'raf.id')
		->select(DB::raw('sum(depo_book.adet) as adet,depo.depo_name as deponame, oda.oda_name as odaname'))
		->groupBy('oda.id')
		->get();

		$odas = Oda::with('getRaf')->get();
		$depos = Depo::get();

		$title ="Oda İşlemleri";
		$page = "add";
		$oda = new Oda;

		if ($id>0) {
			$oda = Oda::find($id);		 
			$page = "edit";
		}


		return view('admin.oda.'.$page,compact('oda',  'title','depos','odas','odaAdetler'));
	}




	function save($id = 0)
	{
		

		$data = request()->only('depo_id','oda_name','oda_yatay','oda_dikey','oda_start');


		if ($id>0) {
			$entry = Oda::where('id',$id)->firstOrFail();
			$entry->update($data);
			$page = "edit";

		}else{
			$entry = Oda::create($data);
			$page = "add";
		}


		return redirect()
		->route('crudv4.oda.'.$page,$entry->id)
		->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
		->with('mesaj_tur','success');
	}

	function del($id)
	{

		$del = Oda::find($id);

		$del->delete();

		return "1";
	}
}
