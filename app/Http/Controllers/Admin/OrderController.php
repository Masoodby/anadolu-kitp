<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Mail\OrderUpdateMail;
use App\Models\Order;
use App\Library\PDF_Code128;


class OrderController extends Controller
{

	protected $filter = ['payment_type','order_name','id','order_sname','order_state'];


	function index()
	{

		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}




		$query = Order::query();

		$query->when(request()->has('order_name'), function ($q) {
			$val = request('order_name');
			return $q->where('order_name', 'like',  "%$val%");
		});

		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});

		$query->when(request()->has('order_state'), function ($q) {
			$val = request('order_state');
			return $q->where('order_state',  intval($val));
		});


		$query->when(request()->has('payment_type'), function ($q) {
			$val = request('payment_type');
			return $q->where('payment_type',  intval($val));
		});



		$list = $query->with(["baskets.basket_products.product",'address'])->orderBy($orderby[0],$orderby[1] )->paginate($limit);

		$title ="Siparişler Yönetimi";



		return view('admin.order.index',compact('list',  'title' ));
	}


	function detail($id=0)
	{



		$order = Order::with(["baskets.basket_products.product",'address.getUser','fatadres'])->where('id',$id)->firstOrFail();

		$order_adet = Order::where('user_id',$order->user_id)->count();



		$title = "#".$order->id. " Nolu Sipariş (Total : ".price($order->grand_total).')';

		return view('admin.order.order',compact('title','order',"order_adet"));
	}

	function save($id=0)
	{
		$this->validate(request(),[
			'adsoyad' => 'required',
			'adres' => 'required',
			'telefon' => 'required',
			'durum' => 'required'
		]);

		$data = request()->only('adsoyad', 'adres', 'telefon', 'ceptelefonu', 'durum'); //güncellenecek bilgiler


		if ($id>0) {
			$entry = Order::where('id',$id)->firstOrFail();
			$entry->update($data);
		}

		if(request()->hasFile('urun_resmi')){
			$this->validate(request(),[
				'urun_resmi' => 'image|mimes:jpg,png,jpeg,gif|max:2048'
			]);
		}

		return redirect()
		->route('crudv4.order.edit',$entry->id)
		->with('mesaj','Güncellendi')
		->with('mesaj_tur','success');
	}



	function orderUpdate($id='')
	{
		$data = request()->only('order_state', 'order_admin_nots'); //güncellenecek bilgiler



		$entry = Order::where('id',$id)->firstOrFail();

		$entry->update($data);



		if (request()->has('smsonay')) {
			$to  = request('gsm');
			$message  = request('sms');

			smsSend($to,$message);

			Mail::to(request('email'))->send(new OrderUpdateMail($entry));
		}


	}


	function del($id)
	{
		Order::destroy($id);

		return redirect()
		->route('crudv4.order')
		->with('mesaj','Sipariş silindi!')
		->with('mesaj_tur','success');
	}

    function cargoEtiket($id=0)
    {
        $order = Order::with(["baskets.basket_products.product",'address.getUser','fatadres'])->where('id',$id)->firstOrFail();




        //$logo = asset("uploads/marketplaces/tryol_logo.jpg");

        $pdf=new PDF_Code128('P','mm',[100,100]);
        $pdf->AddPage('P');
        $pdf->SetLineWidth(0.2);
        $pdf->AddFont('arial','','arial.php');




        $pdf->SetFont('Arial','',16);
        $pdf->SetXY(4,5);
        $pdf->SetMargins(0,0,0);
        $pdf->MultiCell(100,6,$this->tr( " ANADOLU KİTAP."),'B','C');





// Musteri Adı
        $pdf->SetFont('Arial','',10);
        $pdf->SetXY(3,20);
        $pdf->SetMargins(0,0,0);
        $pdf->SetAutoPageBreak(0);
        $pdf->MultiCell(90,2,$this->tr("ALICI Bilgileri : "),0,'L');

        $pdf->Line(3,24,60,24);





        $pdf->SetFont('Arial','',12);
        $pdf->SetXY(3,27);
        $pdf->SetMargins(0,0,0);
        $pdf->SetAutoPageBreak(0);
        $pdf->MultiCell(90,2,$this->tr("Ad Soyad : ".$order->address->full_name),0,'L');

        $pdf->SetFont('Arial','B',12);
        $pdf->SetXY(3,33);
        $pdf->SetMargins(0,0,0);
        $pdf->SetAutoPageBreak(0);
        $pdf->MultiCell(90,2,$this->tr("GSM : ".$order->address->adres_gsm),0,'L');


        $pdf->SetFont('Arial','',10);
        $pdf->SetXY(3,38);
        $pdf->SetMargins(0,0,0);
        $pdf->SetAutoPageBreak(0);
        $pdf->MultiCell(90,5,$this->tr("Adres : ".$order->address->adres_detay .' - '.$order->address->adres_ilce.' / '.$order->address->adres_il),0,'L');








        $pdf->Line(3,55,90,55);

        $yAxis = 56;
        $artis = 10;
        $only_isbn = true;

        if (count($order->baskets->basket_products) > 4){
            $artis = 5;
            $only_isbn = true;

        }

        foreach($order->baskets->basket_products as $var){
            dd($var);

            $isbn = $var->product->product_stock_code;

            if ($only_isbn){
                $yol =  $isbn  . " (". kisalt(nerde($var->product->product_stock_code),47).")";
            }else{
                $yol = kisalt($var->product->product_name ,30) ." - ".$isbn  . " (". nerde($var->product->product_stock_code).")";
            }




            $pdf->Rect( 3,$yAxis+1,2,2,"F");
            $pdf->SetFont('Arial','',8);
            $pdf->SetXY(6,$yAxis);
            $pdf->SetMargins(0,0,0);
            $pdf->SetAutoPageBreak(0);
            $pdf->MultiCell(90,4,$this->tr($yol),0,'L');

            $yAxis +=$artis;

        }
        foreach ($order->baskets->basket_combos as $combo){

            foreach ($combo->combo->products as $var)
            {
                // dd($var);
                $isbn = $var->product_stock_code;
                // dd($isbn);

                if ($only_isbn){
                    $yol =  $isbn  . " (". kisalt(nerde($var->product_stock_code),47).")";
                }else{
                    $yol = kisalt($var->product_name ,30) ." - ".$isbn  . " (". nerde($var->product_stock_code).")";
                }




                $pdf->Rect( 3,$yAxis+1,2,2,"F");
                $pdf->SetFont('Arial','',8);
                $pdf->SetXY(6,$yAxis);
                $pdf->SetMargins(0,0,0);
                $pdf->SetAutoPageBreak(0);
                $pdf->MultiCell(90,4,$this->tr($yol),0,'L');

                $yAxis +=$artis;

            }
         }


        $pdf->Output();

    }


    function tr($str){

	   // $str = str_replace('ğ','g',$str);
        $str=mb_convert_encoding($str, "ISO-8859-9","UTF-8");

        $str =  str_replace('.', '. ', $str);

        $str = str_replace('  ', ' ', $str);


        return $str;


    }


}
