<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;


class PageController extends Controller
{
	public function index()
	{
		$list = Page::orderby('id','desc')->get();
		$title = "İçerik Sayfaları Yönetimi";
		return view('admin.pages.list',compact('list' ,'title'));
	}

	
	public function form($id = 0)
	{

		$title ="İçerik Sayfaları Ekleme Formu";
		$page = "add";
		$pages = new Page;

		if ($id>0) {
			$pages = Page::find($id);

			$title ="İçerik Sayfaları Güncelleme Formu";
			$page = "edit";
		}


		return view('admin.pages.'.$page,compact('pages',  'title'));
	}


	
	function save($id=0)
	{


		$data = request()->only('page_name', 'page_url','page_title', 'page_desc','page_keyw','page_content','page_type');

		$this->validate(request(),[
			'page_name' => 'required',
			'page_url' => 'required',	
			'page_content' => 'required',
			'page_type' => 'required'

		]);

		$data["is_active"] = 1;


		if ($id>0) {
			$sld = Page::where('id',$id)->firstOrFail();
			$sld->update($data);
			$page = "edit";

		}else{
			$sld = Page::create($data);
			$page = "add";
		}





		return redirect()
		->route('crudv4.pages.'.$page,$sld->id)
		->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
		->with('mesaj_tur','success');
	}



	function update($id)
	{


		$data = [request('label')=>request('val')];

		$entry = Page::where('id',$id)->firstOrFail();
		$entry->update($data);

	}

	function del($id)
	{

		$sld = Page::find($id);

		$sld->delete();


		return "1";
	}
}
