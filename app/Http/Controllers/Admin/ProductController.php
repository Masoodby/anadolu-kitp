<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Yayinevi;
use App\Models\Yazar;
use App\Models\ProductDetail;
use App\Models\Category;
use App\Models\MarketPlaces;
use App\Library\Tryol;
use DB;


class ProductController extends Controller
{

	protected $filter = ['product_name','product_cat','id','is_active','product_price','product_stok','yayinci_id','yazar_id'];

	function listeci()
	{



		$product = 	DB::table('products')->selectRaw('id , product_url,product_desc, count(`id`) as `tekrar`')
		->where('is_active',0)
		->groupBy('product_url')
		->having('tekrar', '>', 1)
		->get();

		echo count($product);

		foreach($product as $row){

			$slug = str_slug($row->product_desc.'-'.$row->id);
			//$slug = str_slug($row->product_desc);
			DB::table('products')->where('id',$row->id)->update( ['product_url'=>$slug]);
		}

		if (count($product) > 10) {
			echo "<script> setTimeout(function(){location.href='/crudv4/product/listeci'},2000)</script>";

		}else{
			die('bitti');
		}
	}


	function liste()
	{



		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}




		$query = Product::query();

		$query->when(request()->has('product_name'), function ($q) {
			$val = request('product_name');
			return $q->where('product_name', 'like',  "%$val%");
		});
		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});	


		$query->when(request()->has('product_stock_code'), function ($q) {
			$val = request('product_stock_code');
			return $q->where('product_stock_code',  intval($val));
		});	



		$query->when(request()->has('product_cat'), function ($q) {
			$val = request('product_cat');
			return $q->where('product_cat',  intval($val));
		});

		$query->when(request()->has('yazar_id'), function ($q) {
			$val = request('yazar_id');
			return $q->whereHas('extend', function ($q) use($val){
				return $q->where('yazar_id', '=', $val);
			});
		});

		$query->when(request()->has('yayinci_id'), function ($q) {
			$val = request('yayinci_id');
			return $q->whereHas('extend', function ($q) use($val){
				return $q->where('yayinci_id', '=', $val);
			});
		});

		$query->when(request()->has('is_market') and strlen(request('is_market'))>1, function ($q) {
			$val = request('is_market');			
			return $q->whereHas('market', function ($q) use($val){
				$vals =  explode('-', $val);
				return $q->where(['market_id'=> $vals[0],'on_sale'=>$vals[1]]);
			});
		});

		$query->when(request()->has('is_active') && request('is_active')<2 , function ($q) {
			$val = request('is_active');
			return $q->where('is_active',  intval($val));
		});

		$query->when(request()->has('is_depo') && request('is_depo')<2 , function ($q) {
			$val = request('is_depo');
			return $q->where('is_depo',  intval($val));
		});


		$query->when(request()->has('is_sale') && request('is_sale')<2 , function ($q) {
			$val = request('is_sale');
			return $q->where('is_sale',  intval($val));
		});


		$query->when(request()->has('resim_durum') , function ($q) {
			$val = request('resim_durum');
			return $q->where('resim_durum',  intval($val));
		});




		$list = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci','market'])->orderBy($orderby[0],$orderby[1] )->paginate($limit);







		$selectbox = Category::attr(['name' => 'product_cat',"class"=>"form-control selectfilter ml-3 sel2","data-filter"=>"product_cat"]);

		if (request()->has('categories_main')) {
			$main_cats =  $selectbox->selected(request('product_cat'))->renderAsDropdown();
		}else{
			$main_cats =  $selectbox->renderAsDropdown();
		}





		$title ="Ürün Yönetimi";



		return view('admin.product.list',compact('list', 'main_cats','title' ));
	}



	function index()
	{

		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}




		$query = Product::query();

		$query->when(request()->has('product_name'), function ($q) {
			$val = request('product_name');
			return $q->where('product_name', 'like',  "%$val%");
		});

		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});	

		$query->when(request()->has('product_stock_code'), function ($q) {
			$val = request('product_stock_code');
			return $q->where('product_stock_code',  intval($val));
		});	


		$query->when(request()->has('product_cat'), function ($q) {
			$val = request('product_cat');
			return $q->where('product_cat',  intval($val));
		});

		$query->when(request()->has('yazar_id'), function ($q) {
			$val = request('yazar_id');
			return $q->whereHas('extend', function ($q) use($val){
				return $q->where('yazar_id', '=', $val);
			});
		});

		$query->when(request()->has('yayinci_id'), function ($q) {
			$val = request('yayinci_id');
			return $q->whereHas('extend', function ($q) use($val){
				return $q->where('yayinci_id', '=', $val);
			});
		});

		$query->when(request()->has('is_active') && request('is_active')<2 , function ($q) {
			$val = request('is_active');
			return $q->where('is_active',  intval($val));
		});




		$list = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci','market'])->orderBy($orderby[0],$orderby[1] )->paginate($limit);







		$selectbox = Category::attr(['name' => 'product_cat',"class"=>"form-control selectfilter ml-3 sel2","data-filter"=>"product_cat"]);

		if (request()->has('categories_main')) {
			$main_cats =  $selectbox->selected(request('product_cat'))->renderAsDropdown();
		}else{
			$main_cats =  $selectbox->renderAsDropdown();
		}





		$title ="Ürün Yönetimi";



		return view('admin.product.index',compact('list', 'main_cats','title' ));
	}



	function form($id=0)
	{




		$title ="Ürün Ekleme Formu";
		$product_cats = [];
		$page = "addform";


		$product = new Product;

		if ($id>0) {
			$product = Product::with("detail",'extend','market')->find($id);

			$title ="Ürün Güncelleme Formu";
			$page = "editform";
			$product_cats = $product->categories()->pluck('category_id')->all();


		}

		$cats =  Category::nested()->get();


		return view('admin.product.'.$page,compact('product', 'cats','title','product_cats'));
	}


	function gecici()
	{


		$trendyol_config = MarketPlaces::find(1);

		$urunler = DB::table("product_market_place")
		->leftJoin('products', 'products.id', '=', 'product_market_place.product_id')
		->leftJoin('product_extend', 'product_extend.product_id', '=', 'products.id')
		->leftJoin('categories', 'categories.id', '=', 'products.product_cat')
		->leftJoin('market_cat_match', 'market_cat_match.local_id', '=', 'products.product_cat')
		->leftJoin('yayinevi', 'yayinevi.id', '=', 'product_extend.yayinci_id')
		->leftJoin('market_brand_match', 'market_brand_match.local_id', '=', 'yayinevi.id')
		->select('products.*', 'categories.categories_name', 'categories.*', 'yayinevi.*',
			"market_cat_match.local_id as cat_local_id",
			"market_cat_match.remote_id as cat_remote_id",
			"market_cat_match.local_name as cat_local_name",
			"market_cat_match.remote_name as cat_remote_name",
			"market_brand_match.*","product_market_place.id as idci"
		)->orderBy("product_market_place.id","asc")
		->get();



		foreach ($urunler as $key => $var) {


			$json = [
				"cat_local_id"=> $var->cat_local_id,
				"cat_local_name"=> $var->cat_local_name,
				"cat_remote_id"=> $var->cat_remote_id,
				"cat_remote_name"=> $var->cat_remote_name,
				"brand_local_id"=> $var->local_id,
				"brand_local_name"=> $var->local_name,
				"brand_remote_id"=> $var->remote_id,
				"brand_remote_name"=> $var->remote_name,
				"product_name"=> $var->product_name,
				"product_price"=> $var->product_price,
			];

			$price = yuzdePlus($var->product_price,$trendyol_config->percent_unit);
			$price = floatval($price) + floatval($trendyol_config->price_unit);

			echo $var->idci.' -> '.$var->product_price.' -> '.$price;
			echo "<br>";


			$data = [
				"price" => $price,
				"jsons" => json_encode($json)
			];

			DB::table("product_market_place")->where('id',$var->idci)->update($data);

		}
	}



	function save($id=0)
	{



		$data = request()->only(
			'product_name',
			'product_sub_name',
			'product_stock_code',
			'product_group_code',
			'product_url',
			'product_content',
			'product_price',
			'product_title',
			'product_desc',
			'product_keyw',
			'product_cat',
			'product_stok',
			'product_jenerik',
			'is_active',
		);



		$this->validate(request(),[
			'product_name' => 'required',
			'product_price' => 'required',
			'yazar_id' => 'required',
			'yayinci_id' => 'required',

			'product_url' =>   'required|alpha_dash|unique:products,product_url,'.$id
		]);



		$data_detay = request()->only('product_old_price', 'product_get_price');
		$data_extend = request()->only('yazar_id', 'yayinci_id');

		$values = json_decode(set('book_desc'),true);
		$book_desc = [];
		foreach( request('extend_json')  as $key => $var){

			$book_desc[$key] = ['attr'=>$values[$key]['attr'],'value'=>$var];
		}




		$data_extend['extend_json'] = json_encode($book_desc);



		$kategoriler = request('cats');
		//dd(request('kategoriler'));

		if ($id>0) {
			$product = Product::where('id',$id)->firstOrFail();

			$product->update($data);
			$page = "edit";
			$product->detail()->update($data_detay);
			$product->extend()->update($data_extend);
			$product->categories()->sync($kategoriler);


		}else{
			$product = Product::create($data);
			$product->detail()->create($data_detay);
			$product->extend()->create($data_extend);
			$product->categories()->attach($kategoriler);
			$page = "add";

		}

		if(request()->hasFile('resim')){



			$folder_q = DB::table('products')
			->select('id','product_group_code', DB::raw('COUNT(*) as `tekrar`'))
			->groupBy('product_group_code')
			->having('tekrar', '>', 1)
			->orderBy('tekrar','asc')
			->limit(1)
			->first();

			$folder = $folder_q->product_group_code;

			$jpg_folder = public_path('files/'.$folder);
			$webp_folder = public_path('files/'.$folder.'/webp/');

			makeDir($jpg_folder);
			makeDir($webp_folder);



			$this->validate(request(),[
				'resim' => 'required',
				'resim.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
			]);

			$namesi = [];
			foreach(request()->file('resim') as $key => $image){


				if ($key == 0) {
					$namesi[$key] = str_slug(request('product_stock_code')).".webp";
				}else{
					$namesi[$key] = str_slug(request('product_stock_code')). '_'.$key. ".webp";
				}

			//	my_upload($image->getRealPath(),$namesi[$key],"product",r_size("product"),'webp');

				my_upload_files($image->getRealPath(),request('product_stock_code'),$folder);

			}

			$product->detail()->update( 
				['product_image' => json_encode($namesi)]
			);	

			$product->update( 
				[
					'resim_durum' => 1,
					"product_group_code"=>$folder
				]
			);


		}

		return redirect()
		->route('crudv4.product.'.$page,$product->id)
		->with('mesaj',($id>0 ? 'Güncellendi' : 'Kaydedildi'))
		->with('mesaj_tur','success');
	}



	function update($id='')
	{
		$data = [request('label') => request('val')];

		$entry = Product::where('id',$id)->firstOrFail();
		if (request('detail') == 0) {
			$entry->update($data);
		}else{
			$entry->detail()->update($data);
		}



	}


	function updateImgList($id='')
	{

		$product = Product::where('id',$id)->firstOrFail();



		if (strlen($product->product_group_code) > 5) {
			$folder = $product->product_group_code;
		}else{

			$folder_q = DB::table('products')
			->select('id','product_group_code', DB::raw('COUNT(*) as `tekrar`'))
			->groupBy('product_group_code')
			->having('tekrar', '>', 1)
			->orderBy('tekrar','asc')
			->limit(1)
			->first();

			$folder = $folder_q->product_group_code;
		}


		if(request()->hasFile('resim')){
			$this->validate(request(),[
				'resim' => 'required',
				'resim.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
			]);




			$jpg_folder = public_path('files/'.$folder);
			$webp_folder = public_path('files/'.$folder.'/webp/');

			makeDir($jpg_folder);
			makeDir($webp_folder);



			$namesi = [];
			$namesi[0] = str_slug($product->product_stock_code).".webp";

			$image = request()->file('resim');
			my_upload_files($image->getRealPath(),$product->product_stock_code,$folder);



			$product->detail()->update( 
				['product_image' => json_encode($namesi)]
			);

			$product->update( 
				[
					'resim_durum' => 1,
					"product_group_code"=>$folder
				]
			);

			echo ress($product->product_stock_code,$folder,  "250x250").'?v='.rand(9,99);

		}else{
			echo 0;
		}

	}


	function updateimg($id='')
	{

		$product = Product::with("detail")->where('id',$id)->firstOrFail();

		if (strlen($product->product_group_code) > 5) {
			$folder = $product->product_group_code;
		}else{

			$folder_q = DB::table('products')
			->select('id','product_group_code', DB::raw('COUNT(*) as `tekrar`'))
			->groupBy('product_group_code')
			->having('tekrar', '>', 1)
			->orderBy('tekrar','asc')
			->limit(1)
			->first();

			$folder = $folder_q->product_group_code;
		}






		$res_name = request("name");
		$res_key = request("key");

		if (strlen($product->detail->product_image)>5) {



			$res_dizi = json_decode($product->detail->product_image,TRUE);
		}else{
			$res_dizi = [];
		}

		if ($res_name == "new") {


			$oldadet = count($res_dizi);

			if ($oldadet == 0) {
				$resim_name[0] =  str_slug($product->product_stock_code);

			}else{
				$resim_name[0] =  str_slug($product->product_stock_code).'_'.$oldadet;

			}

			$res_key = $oldadet;

		}else{


			removeRes($folder,$product->product_stock_code);


			$resim_name =  explode(".", $res_name);

		}



		if(request()->hasFile('resim')){

			$this->validate(request(),[
				'resim' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
			]);

			$resim = request()->file('resim');

			$namesi =  $resim_name[0]. ".webp" ;

			if ($resim->isValid()) {

				my_upload($resim->getRealPath(),$namesi,"product",r_size("product"));

				$res_dizi[$res_key] = $namesi;


				$product->detail()->update( 
					['product_image' => json_encode($res_dizi)]
				);
			}
		}


		if(request()->hasFile('resim')){
			$this->validate(request(),[
				'resim' => 'required',
				'resim.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
			]);






			$jpg_folder = public_path('files/'.$folder);
			$webp_folder = public_path('files/'.$folder.'/webp/');

			makeDir($jpg_folder);
			makeDir($webp_folder);



			$namesi = [];
			$namesi[0] = str_slug($product->product_stock_code).".webp";

			$image = request()->file('resim');
			my_upload_files($image->getRealPath(),$product->product_stock_code,$folder);

			$res_dizi[$res_key] = $namesi[0];

			$product->detail()->update( 
				['product_image' => json_encode($res_dizi)]
			);

			$product->update( 
				[
					'resim_durum' => 1,
					"product_group_code"=>$folder
				]
			);

			echo ress($product->product_stock_code,$folder,  "250x250").'?v='.rand(9,99);

		}else{
			echo 0;
		}


		//echo res( 'product',$namesi, "250x250").'?v='.rand(9,99);
	}


	function eraseimage($id='')
	{

		$res_name = request("name");
		$res_key = request("key");


		$product = Product::with("detail")->where('id',$id)->firstOrFail();


		$res_dizi = json_decode($product->detail->product_image,TRUE);
		unset($res_dizi[$res_key]);

		removeRes("product",$res_name,"product");

		$editres = count($res_dizi) == 0?"{}":$res_dizi;

		$product->detail()->update( 
			['product_image' => json_encode($editres)]
		);

	}



	function del($id)
	{
		$urun = Product::find($id);
		 $urun->kategoriler()->detach();  //manyTomany için detach
		//$urun->detay()->delete();	//oneToone için delete //softDelete yapısı kullanıldığı için ürün silinme tarihi kaydı bulunuyor detayda silinmese olur
		 $urun->delete(); 

		 return "1";
		}



		public function yazar_ara(){

			$search = request("search");


			if($search == ''){
				$employees = Yazar::orderby('yazar_name','asc')->select('id','yazar_name')->limit(5)->get();
			}else{
				$employees = Yazar::orderby('yazar_name','asc')->select('id','yazar_name')->where('yazar_name', 'like', '%' .$search . '%')->limit(20)->get();
			}

			$response = array();
			foreach($employees as $employee){
				$response[] = array(
					"id"=>$employee->id,
					"text"=>$employee->yazar_name
				);
			}

			echo json_encode($response);
			exit;
		}

		public function yayinci_ara(){

			$search = request("search");


			if($search == ''){
				$employees = Yayinevi::orderby('yayinci_name','asc')->select('id','yayinci_name')->limit(5)->get();
			}else{
				$employees = Yayinevi::orderby('yayinci_name','asc')->select('id','yayinci_name')->where('yayinci_name', 'like', '%' .$search . '%')->limit(20)->get();
			}

			$response = array();
			foreach($employees as $employee){
				$response[] = array(
					"id"=>$employee->id,
					"text"=>$employee->yayinci_name
				);
			}

			echo json_encode($response);
			exit;
		}


		function getResim()
		{
			$products = Product::orderby('product_stok',"desc")->where('product_visit_count',2)->limit(100)->get();



			$i =0;
			foreach ($products as $key => $product) {

				$i++;

				$isbn = $product->product_stock_code;




				$file = public_path('product/'.$isbn.'.jpg');
		//	echo $file;

		//	echo filesize($file);
		//	die();


				if(file_exists($file) && filesize($file) > 17448 ){

					my_upload($file,$isbn.'.webp',"product",r_size("product"));

					Product::where('id',$product->id)->update(['product_visit_count'=>1]);

			//return 1;

				}else{
					Product::where('id',$product->id)->update(['product_visit_count'=>3]);
				}


			}
			if ($i<5) {
				die('bitti');
			}else{
				echo '<meta http-equiv="refresh"  content="2; url="'.route('crudv4.product.getResim').'">';
			}

			

		}


		function jpgCreate()
		{
			$books = DB::table('depo_book')->groupBy('isbn')->get();

			foreach ($books as $key => $var) {


				$src = public_path('uploads/product/'.$var->isbn.'.webp');

				if (file_exists($src)) {

					$varmi = public_path('uploads/product/jpg/'.$var->isbn.'.jpg');

					if (!file_exists($varmi)) {


						jpegCreate($src,$var->isbn.'.jpg','product');

					}

					//echo public_path('uploads/product/jpg/'.$var->isbn.'.jpg');



				}


			}
		}


	}
