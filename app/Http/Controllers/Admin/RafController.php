<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Oda;
use App\Models\Raf;
use App\Models\DepoBook;
use App\Models\Product;
use App\Models\Depolog;

class RafController extends Controller
{
	public function index($id = 1)
	{


		$rafs = Raf::with('getBook')->where('oda_id',$id)->get();

		$raflar = [];
		foreach ($rafs as $key => $item) {

			$indis = $item->raf_name;

			$dizi = [
				'raf_id' => $item->id,
				'oda_id' => $item->oda_id,
				'adet' => $item->getBook->sum('adet'),
			];
			$raflar[$indis] = $dizi;
		}



		$odalar = Oda::get();
		$oda = Oda::where('id',$id)->firstOrFail();

		$title ="Raf İşlemleri";

		return view('admin.raf.list',compact('raflar','title','odalar','oda'));

	}




	function islem($id = 0)
	{
		
		$raf_name = request('name');
	//	$raf_name = "103B";




		$rafx = Raf::where(['oda_id'=>$id,'raf_name'=>$raf_name])->count();

		if ($rafx == 0) {
			$raf = 	Raf::create(['oda_id'=>$id,'raf_name'=>$raf_name]);
		}else{
			$raf = Raf::where(['oda_id'=>$id,'raf_name'=>$raf_name])->firstOrFail();
		}

		$books = DepoBook::with("getBook")->where('raf_id',$raf->id)->orderBy('id','desc')->get();


		return view('admin.raf.modal',compact('books','raf'));

	}





	function addbook()
	{
		
		$isbn = barkod(request('barkod'));
		$adet = request('adetinput');
		$raf_id = request('rafid');	

		

		


		$book = DepoBook::where(['raf_id'=>$raf_id,'isbn'=>$isbn]);

		$product = Product::where('product_stock_code',$isbn);

		if ($product->count() > 0) {
			$pr_id = $product->first()->id;
		}else{
			$pr_id = 0;
		}



		if ($book->count() >0) {
			
			$current = $book->first();
			$new_adet = intval($current->adet)+intval($adet);

			$current->update(['adet'=>$new_adet,'book_id'=>$pr_id]);

		}else{

			DepoBook::create(['raf_id'=>$raf_id,'isbn'=>$isbn,'adet'=>$adet,'book_id'=>$pr_id]);
		}


		Depolog::create(['raf_id'=>$raf_id,'isbn'=>$isbn,'adet'=>$adet,'book_id'=>$pr_id]);

		$books = DepoBook::with("getBook")->where('raf_id',$raf_id)->orderBy('id','desc')->get();

		//prep($books);
		return view('admin.raf.table',compact('books'));

	}

	


	function adet($id)
	{

		$adet = request('val');

		$book = DepoBook::where('id',$id)->firstOrFail();

		$old_adet = $book->adet;

		$kalan = $adet<$old_adet ? ($old_adet-$adet)*-1:$adet-$old_adet;

		$book->update(['adet'=>$adet]);

		Depolog::create(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>$kalan,'book_id'=>$book->book_id]);

		return "1";
	}


	function del($id)
	{

		$del = DepoBook::find($id);

		Depolog::create(['raf_id'=>$del->raf_id,'isbn'=>$del->isbn,'adet'=>0,'book_id'=>$del->book_id]);

		$del->delete();

		return "1";
	}
}

