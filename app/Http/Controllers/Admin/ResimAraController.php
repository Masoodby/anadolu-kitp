<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\ResimAra;
use Illuminate\Http\Request;

class ResimAraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Product::orderBy('id','DESC')->where('is_active',1)->where('resim_durum',2)->paginate(100);
        $title='resimsiz kitaplar';
        // dd($list);
        return view('admin.resim.list',compact('list','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResimAra  $resimAra
     * @return \Illuminate\Http\Response
     */
    public function show(ResimAra $resimAra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResimAra  $resimAra
     * @return \Illuminate\Http\Response
     */
    public function edit(ResimAra $resimAra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResimAra  $resimAra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResimAra $resimAra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResimAra  $resimAra
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResimAra $resimAra)
    {
        //
    }
}
