<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Setcat;



class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = 1)
    {
    	$settings = Setting::where('settings_group', $id)->get();
    	$setcats = Setcat::orderBy('setcat_sira', 'asc')->get();

    	return view("admin.settings.setting",compact("settings","setcats","id"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$title =   "Ayar Ekleme Formu";

    	$setcats = Setcat::orderBy('setcat_sira', 'asc')->get();

    	return view("admin.settings.add",compact( "setcats","title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {



    	Setting::create([
    		'settings_name' => request("settings_name"),
    		'settings_val' => request("settings_val"),
    		'settings_group' => request("settings_group"),
    		'settings_type' => request("settings_type"),
    		'settings_key' => request("settings_key")
    	]);


    	return redirect()->route("crudv4.settings.create")->with("message","İşlem Başarılı")->with("status","success");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$title =   "Ayar Güncelleme Formu";


    	$setcats = Setcat::orderBy('setcat_sira', 'asc')->get();
    	$set = Setting::findorfail($id);


    	return view("admin.settings.edit",compact( "setcats","title","set"));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


    	Setting::where('id', $id)->update([
            'settings_name' => request("settings_name"),
            'settings_val' => request("settings_val"),
            'settings_group' => request("settings_group"),
            'settings_type' => request("settings_type"),
            'settings_key' => request("settings_key")
        ]);


    	return redirect()->route("crudv4.settings.show",$id)->with("message","İşlem Başarılı")->with("status","success");
    }


    public function ajax(Request $request)
    {

    	Setting::find($request->id)->update(['settings_val' => $request->settings_val]);

    	return response()->json(['message'=>'Güncelleme Başarılı']);
    }

    public function ajaxUpload(Request $request)
    {

        $file = Setting::find($request->id);

        if(request()->hasFile('dosya')){

            $this->validate(request(),[
                'dosya' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
            ]);

            $resim = request()->file('dosya');

            $namesi =   $file->settings_val;

            if ($resim->isValid()) {

                my_upload($resim->getRealPath(),$namesi,"content",false,'png');
                
            }
        }


        return response()->json(['message'=>'Güncelleme Başarılı']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	Setting::where('id',$id)->delete();

    	return redirect()->route("crudv4.settings.index")->with("message","Silme Başarılı")->with("status","success");

    }
}
