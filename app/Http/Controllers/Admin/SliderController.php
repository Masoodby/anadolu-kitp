<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Slider::orderby('rank','asc')->get();
        $title = "Slider Yönetimi";
        return view('admin.slider.list',compact('list' ,'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form($id = 0)
    {

        $title ="Slider Ekleme Formu";
        $page = "add";
        $slider = new Slider;

        if ($id>0) {
            $slider = Slider::find($id);

            $title ="Slider Güncelleme Formu";
            $page = "edit";
        }

        
        return view('admin.slider.'.$page,compact('slider',  'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    function save($id=0)
    {


        $data = request()->only('slider_name', 'slider_link');

        $this->validate(request(),[
            'slider_name' => 'required',
            'slider_link' => 'required'
            
        ]);

        $data["slider_desc"] = json_encode(request('slider_desc'));


        if ($id>0) {
            $sld = Slider::where('id',$id)->firstOrFail();
            $sld->update($data);
            $page = "edit";

        }else{
            $sld = Slider::create($data);
            $page = "add";
        }



        if(request()->hasFile('resim')){




            $this->validate(request(),[
                'resim' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
            ]);

            $resim = request()->file('resim');

            $namesi = str_slug(request('slider_name')).  ".webp";

            if ($resim->isValid()) {

                my_upload($resim->getRealPath(),$namesi,"slider",r_size("slider"),'webp');
                Slider::updateOrCreate(
                    ['id' => $sld->id],
                    ['slider_image' => $namesi]
                );
            }
        }
        
        return redirect()
        ->route('crudv4.slider.'.$page,$sld->id)
        ->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
        ->with('mesaj_tur','success');
    }



    function update($id)
    {


        $data = [request('label')=>request('val')];

        $entry = Slider::where('id',$id)->firstOrFail();
        $entry->update($data);

    }

    function del($id)
    {

        $sld = Slider::find($id);
        removeRes("slider",$sld->slider_image,"slider");
        $sld->delete();


        return "1";
    }
}
