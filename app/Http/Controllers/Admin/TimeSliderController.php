<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\TimeSlider;
use App\Models\Product;
use Illuminate\Http\Request;

class TimeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = TimeSlider::orderby('id','DESC')->get();
        $title = "Günlük fırsatlar Yönetimi";
        return view('admin.tslider.list',compact('list' ,'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form($id = 0)
    {

        $title ="Slider Ekleme Formu";
        $page = "add";
        $tslider = new TimeSlider();

        if ($id>0) {
            $tslider = TimeSlider::find($id);

            $title ="Slider Güncelleme Formu";
            $page = "edit";
        }


        return view('admin.tslider.'.$page,compact('tslider',  'title'));
    }

    function save($id=0)
    {
        //  dd(request());

        $data = request();
        // dd($data);

        $this->validate(request(),[
            'isbn' => 'required|exists:products,product_stock_code',


        ]);
        $user=auth()->id();
        $pid=Product::where('product_stock_code',request('isbn') )->select('id')->first();

        // $product_id=$product_id->id;
        //  dd($pid);

        // $data["tslider_desc"] = json_encode(request('tslider_desc'));


        if ($id>0) {
            $sld= TimeSlider::where('id',$id)->firstOrFail();
            $sld->update([
                'user_id' =>1,
                'product_id'=>$pid->id,
                'discount'=>request('percent'),
                'end_time'=>request('time'),
                'status'=>1,
                'url'=>request('url'),
                'title'=>request('title')

            ]);
            $page = ".edit";

        }else{
            $sld =  TimeSlider::create([
                'user_id' =>1,
                'product_id'=>$pid->id,
                'discount'=>request('percent'),
                'end_time'=>request('time'),
                'status'=>1,
                'url'=>request('url'),
                'title'=>request('title')

            ]);
            $page = "";

        }





        return redirect()
        ->route('crudv4.tslider')
        ->with('mesaj',($id>0 ? 'Güncellendi ' : 'Kaydedildi'))
        ->with('mesaj_tur','success');
    }


    function update($id)
    {


        $data = [request('label')=>request('val')];

        $entry = TimeSlider::where('id',$id)->firstOrFail();
        $entry->update($data);

    }

    function del($id)
    {

        $sld = TimeSlider::find($id);

        $sld->delete();


        return "1";
    }
}
