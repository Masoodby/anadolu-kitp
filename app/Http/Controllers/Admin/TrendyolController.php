<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Tryol;
use App\Library\PDF_Code128;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\MarketWriterMatch;
use App\Models\Yayinevi;
use App\Models\Yazar;
use App\Models\Depolog;
use App\Models\MarketPlaces;
use App\Models\Product;
use App\Models\ProductMarketPlace;
use Illuminate\Support\Facades\DB;

class TrendyolController extends Controller
{
	public $pid = 1;
	public $tryol;
	public $trendyol_config;
	public function __construct()
	{
		$this->trendyol_config = MarketPlaces::find($this->pid);
		$config = ['seller_id'=>$this->trendyol_config->user_no,"api_user"=>$this->trendyol_config->api_id,"api_pass"=>$this->trendyol_config->api_key];
		$this->tryol =  new Tryol($config);

	}


	function index()
	{

		$products = $this->tryol->filterProducts(0);

		$title = "Trendyol Onaylanmış Ürünler";
		

		$last_queries = DB::table('trendyol_results')->orderByDesc('id')->limit(500)->get();



		return view('admin.trendyol.index',compact('title','products','last_queries'));
	}


	function orders($status="Created")
	{

		if (request()->has('status')) {
			$status = request("status");
		}

		$title ="Trendyol Siparişleri";

		$query = array(
			'status' => $status,
			
			'orderByField' => "PackageLastModifiedDate",
			'orderByDirection' => "ASC",
			'size'          => 50,
			'page'          => 0,
		);


		$orders = $this->tryol->get_orders($query);


		return view('admin.order.trendyol.orders',compact('title','query','orders','status'));

	}

	function saveOrders()
	{



		$query = array(
			'status' => "Created",
			
			'orderByField' => "PackageLastModifiedDate",
			'orderByDirection' => "DESC",
			'size'          => 50,
			'page'          => 0,
		);


		$orders = $this->tryol->get_orders($query);

		foreach ($orders->content as $key => $var) {

			$dizi = [
				'order_id'=>$var->orderNumber,
				'durum'=>"Created",
				'json'=>json_encode($var),
				
			];

			DB::table('trorder')->updateOrInsert(
				['order_id' => $var->orderNumber],
				$dizi
			);


			$durum = DB::table('trorder')->where('order_id',$var->orderNumber)->first();

			$nerden = [];

			if ($durum->depoexit == 0) {
				
				foreach ($var->lines as $key => $urun) {

					$isbn =  barkod_rep($urun->barcode);
					$book = DB::table('depo_book')->orderBy('adet','asc')->where('isbn',$isbn)->where('adet','>',0)->first();

					if ($book) {

						$raf = DB::table('raf')->where('id',$book->raf_id)->first();



						$adets = $urun->quantity;
						DB::table('depo_book')->where('id',$book->id)->decrement('adet',$adets);
						DB::table('products')->where('id',$book->book_id)->decrement('product_stok',$adets);
						Depolog::create(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adets*-1),'book_id'=>$book->book_id,
							'notes'=> $var->customerFirstName.' '.$var->customerLastName.' adlı kişinin'.$var->orderNumber." id li Trendyol Siparişi kayıt edilirken otomatik çıkış yapıldı"]);

						$nerden[$isbn] = $raf->raf_name.' '.$adets.' adet';

					}

				}

				DB::table('trorder')->where('order_id',$var->orderNumber)->update(['depoexit'=>1,'exit_json'=>json_encode($nerden)]);
			}



		}

	}

	function exitdepo($id)
	{

		$val = explode("-",request("name"));

		$adets = $val[2];

		$book = DB::table('depo_book')->where('id',$val[1])->first();

		$raf = DB::table('raf')->where('id',$book->raf_id)->first();


		DB::table('depo_book')->where('id',$val[1])->decrement('adet',$adets);

		DB::table('depo_log')->insert(['raf_id'=>$book->raf_id,'isbn'=>$book->isbn,'adet'=>($adets*-1),'book_id'=>$book->book_id]);

		$mevcut = DB::table('trorder')->where('order_id',$id)->first();

		$nerden = strlen($mevcut->exit_json) > 5 ? json_decode($mevcut->exit_json,true):[];

		$nerden[$book->isbn] = $raf->raf_name.' '.$adets.' adet';

		DB::table('trorder')->where('order_id',$id)->update(['depoexit'=>1,'exit_json'=>json_encode($nerden)]);

	}


	function products($page=0)
	{
		$title ="Trendyol Onaylanmış Ürünler";
		$products = $this->tryol->filterProducts($page);

		return view('admin.trendyol.products',compact('title','products'));

	}

	function addremoteproduct($page=0)
	{

		$products = $this->tryol->filterProducts($page);
		$products = json_decode($products);
		$urunler = $products->content;
		$totalElements = $products->totalElements;
		$totalPages = $products->totalPages;
		$pagem = $products->page;

		$next = $page+1;


		foreach ($urunler as $key => $item) {
			
			$urun = Product::where('product_stock_code',$item->barcode)->first();

			if ($urun) {
				ProductMarketPlace::updateOrCreate([

					'market_id'   => $this->pid,
					'product_id'   => $urun->id,
				],[
					'market_id'   => $this->pid,
					'product_id'   => $urun->id,
				]);
			}else{

			}

		}

		if ($totalPages<$page) {
			die('bitti');
		}else{


			return redirect()->route('crudv4.trendyol.products',$next);
		}
	}


	function catmatch()
	{

		$title ="Trendyol Kategori Eşleştirme";
		$page = "cat_match";
		$maincat = $this->tryol->getCatList(0);
		$cats =  Category::nested()->get();
		$matched =  MarketCatMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();
		
		return view('admin.trendyol.'.$page,compact('title','maincat','cats','matched'));
	}





	function save()
	{
		

		foreach (request('cats') as $key => $item) {
			
			//$cat = Category::where('id',$key)->firstOrFail();

			MarketCatMatch::updateOrCreate([

				'local_id'   => $key,
				'remote_id'   => request('remote_cat_id'),
				'pazaryeri_id'   => $this->pid,
			],[
				'local_id'   => $key,
				'remote_id'   => request('remote_cat_id'),
				'pazaryeri_id'   => $this->pid,
				'local_name'   => $item,
				'remote_name'   => request('remote_cat_name'),
			]);


		}



		$matched =  MarketCatMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();

		$typeval = "cat";

		return view('admin.trendyol.matched',compact('matched','typeval'));
	}



	function loadCat($main)
	{
		$list = $this->tryol->getCatList($main);

		$maincat  = [];
		$cat_name = request('name');
		$cat_id = $main;

		$maincat = $list?$list:[];
		if ($list) {
			$maincat  = $list;
		} 

		$level = request('level');
		return view('admin.trendyol.cat_list',compact('maincat','level','cat_name','cat_id'));
	}



	function catRequired($id=1642)
	{
		$url = file_get_contents("https://api.trendyol.com/sapigw/product-categories/".$id."/attributes");

		$res = json_decode($url);

		prep($res);


	}


	function adremotewriter()
	{

		$remote_cats = MarketCatMatch::where(["durum"=>0,"pazaryeri_id"=>$this->pid])->orderBy('id','asc');

		if ($remote_cats->count() > 0) {
			$remote_cat = $remote_cats->first();


			$id = $remote_cat->remote_id;

			$url = file_get_contents("https://api.trendyol.com/sapigw/product-categories/".$id."/attributes");

			$res = json_decode($url);

			if (isset($res->categoryAttributes[0])) {



				$yazarlar =  $res->categoryAttributes[0]->attributeValues;

				foreach ($yazarlar as $key => $var) {

					$remote_id = $var->id;
					$remote_name = $var->name;

					$local_id = 106995;
					$local_name ="Anadolu Anonim";

					$yazar =  Yazar::where('yazar_name', $remote_name);

					if ( $yazar->count() > 0) {

						$yazar_row =  $yazar->first();
						$local_id = $yazar_row->id;
						$local_name = $yazar_row->yazar_name;
					}


					MarketWriterMatch::updateOrCreate(
						['remote_id'=>$remote_id],
						[
							'local_id'=>$local_id,
							'local_name'=>$local_name,
							'pazaryeri_id'=>$this->pid,
							'remote_name'=>$remote_name,
						]
					);


				}



			}
			MarketCatMatch::where('remote_id',$id)->update(['durum'=>1]);
			echo "<script> setTimeout(function(){location.href='/crudv4/trendyol/adremotewriter?cid=".$id."'},2000)</script>";

		}else{
			die("bitti");
		}

	}


	function getmatched($main)
	{
		$matched =  MarketCatMatch::orderBy("id","desc")->where('pazaryeri_id',$this->pid)->get();

		$typeval = "cat";

		return view('admin.trendyol.matched',compact('matched','typeval'));
	}


	function del($id)
	{
		$matched = MarketCatMatch::find($id);		
		$matched->delete();
		return "1";
	}


	function delbrand($id)
	{
		$matched = MarketBrandMatch::find($id);		
		$matched->delete();
		return "1";
	}


	function brand()
	{

		$title ="Trendyol Marka Eşleştirme";
		$page = "brand_match";

		$macthedID =  MarketBrandMatch::where('pazaryeri_id',$this->pid)->pluck('local_id');

		$yayinci =  Yayinevi::where('is_active', 1)->whereNotIn('id',$macthedID)->get();
		$matchbrand =  MarketBrandMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();
		
		return view('admin.trendyol.'.$page,compact('title', 'matchbrand','yayinci'));
	}


	function writer()
	{

		$title ="Trendyol Yazar Eşleştirme";
		$page = "writer_match";


		$yazarlar =  MarketWriterMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->paginate(100);
		
		return view('admin.trendyol.'.$page,compact('title', 'yazarlar'));
	}


	function localbrand()
	{
		$name = request('ara');
		$yayinci =  Yayinevi::where('yayinci_name', 'LIKE', '%'.$name.'%')->get();

		return view('admin.trendyol.ajax.load_brand',compact('yayinci'));
	}

	function remotebrand()
	{

		$brand = request('ara');
		$yayinci  = $this->tryol->getBrands($brand);

		return view('admin.trendyol.ajax.load_remote_brand',compact('yayinci'));
	}



	function save_brand()
	{
		

		MarketBrandMatch::updateOrCreate([

			'local_id'   => request('local_id'),
			'remote_id'   => request('remote_id'),
			'pazaryeri_id'   => $this->pid,
		],[
			'local_id'   => request('local_id'),
			'remote_id'   => request('remote_id'),
			'pazaryeri_id'   => $this->pid,
			'local_name'   => request('local_name'),
			'remote_name'   => request('remote_name'),
		]);



		$matchbrand =  MarketBrandMatch::where('pazaryeri_id',$this->pid)->orderBy("id","desc")->get();

		return view('admin.trendyol.matched',compact('matchbrand'));
	}


	function getOrder($id = 0)
	{

		$query = array(
			'orderNumber' => $id,

		);

		$order = $this->tryol->get_order($query);

		$trorder_check = DB::table('trorder')->where("order_id",$id);

		if ($trorder_check->count() > 0) {

			$trorder = $trorder_check->first(); 
		}else{



			$dizi = [
				'order_id'=>$id,
				'durum'=>"Created",
				'json'=>json_encode($order ),
				
			];

			DB::table('trorder')->updateOrInsert(
				['order_id' => $id],
				$dizi
			);


			$trorder = DB::table('trorder')->where('order_id',$id)->first();


		}

		return view('admin.order.trendyol.order',compact( 'order',"trorder"));
	}


	function Picking($id = 0)
	{
		$query = array(
			'orderNumber' => $id,

		);

		$order = $this->tryol->get_order($query);


		foreach ($order->content[0]->lines as $key => $urun) {
			$urunler[] = ["lineId"=>$urun->id,"quantity"=>$urun->quantity];

		}

		$dizi = [
			"lines" =>$urunler,

			"status"=>"Picking"
		];

		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->trendyol_config->user_no."/shipment-packages/".$order->content[0]->id;
		$this->tryol->picking($url,$dizi);

		return "tamam";


	}


	function tr($str){

		$str=mb_convert_encoding($str, "ISO-8859-9","UTF-8");

		$str =  str_replace('.', '. ', $str);

		$str = str_replace('  ', ' ', $str);


		return $str;


	}


	function cargoEtiket($id=0)
	{
		$query = array(
			'orderNumber' => $id,

		);


		$localOrder = DB::table('trorder')->where('order_id',$id)->first();

		$order = $this->tryol->get_order($query);

		$json = $order->content[0];


		$logo = asset("uploads/marketplaces/tryol_logo.jpg");

		$pdf=new PDF_Code128('P','mm','A4');
		$pdf->AddPage('P');
		$pdf->SetLineWidth(0.2);
		$pdf->AddFont('arial','','arial.php');




		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(4,10);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(200,6,$this->tr( " Kargo şirketinin dikkatine, bu bir trendyol.com gönderisidir. Trendyol anlaşmasına uygun işlem yapabilirsiniz."),'B','C');

// Logo
		$pdf->Image($logo,5,25,80,25,"JPG");
		$pdf->Rect( 120,20,80,38,"D");
// Kod
		$pdf->SetDisplayMode(100,'default');
		$pdf->SetFillColor(0,0,0);
		$pdf->Code128(125,25,$json->cargoTrackingNumber,70,20);


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(125,52);

		$pdf->MultiCell(70,4,$json->cargoTrackingNumber,0,'C');

// Musteri Adı
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY(10,60);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(60,2,$this->tr("ALICI Bilgileri : "),0,'L');

		$pdf->Line(10,64,60,64);


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(12,68);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,2,$this->tr("Sipariş No : ".$id),0,'L');


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(12,74);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,2,$this->tr("Ad Soyad : ".$json->shipmentAddress->fullName),0,'L');


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(12,80);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,5,$this->tr("Adres : ".$json->shipmentAddress->fullAddress),0,'L');


    //kargo şirketi


		$pdf->SetFont('Arial','',20);
		$pdf->SetXY(120,60);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(80,30,$this->tr(eraseIn( $json->cargoProviderName,"Marketplace")),1,'C');



    // Ürün Bilgileri
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY(10,100);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(95,2,$this->tr("Ürün Bilgileri : "),0,'L');

		$pdf->Line(10,105,200,105);

		$yAxis = 108;

		$nerde = strlen($localOrder->exit_json)>5?json_decode($localOrder->exit_json,true):[];

		foreach($json->lines as $var){
			$isbn = barkod_rep($var->barcode);
			if (isset($nerde[$isbn])) {
				$yol = kisalt(eraseIn($var->productName,'one size'),90) ." - ".$isbn  . " (". $nerde[$isbn].")"; 
			}else{
				$yol = kisalt(eraseIn($var->productName,'one size'),90) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
			}

			$pdf->Rect( 10,$yAxis+2,2,2,"F");
			$pdf->SetFont('Arial','',8);
			$pdf->SetXY(14,$yAxis);
			$pdf->SetMargins(0,0,0);
			$pdf->MultiCell(200,5,$this->tr($yol),0,'L');

			$yAxis +=5;

		}


		$pdf->Output();

	}

	function cargoEtiket2($id=0)
	{
		$query = array(
			'orderNumber' => $id,

		);


		$localOrder = DB::table('trorder')->where('order_id',$id)->first();

		$order = $this->tryol->get_order($query);

		$json = $order->content[0];


		//$logo = asset("uploads/marketplaces/tryol_logo.jpg");

		$pdf=new PDF_Code128('P','mm',[100,100]);
		$pdf->AddPage('P');
		$pdf->SetLineWidth(0.2);
		$pdf->AddFont('arial','','arial.php');




		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(4,2);
		$pdf->SetMargins(0,0,0);
		$pdf->MultiCell(100,6,$this->tr( " Trendyol Gönderisi."),'B','C');



// Kod
		$pdf->SetDisplayMode(100,'default');
		$pdf->SetFillColor(0,0,0);
		$pdf->SetAutoPageBreak(0);
		$pdf->Code128(5,10,$json->cargoTrackingNumber,90,20);


		$pdf->SetFont('Arial','',14);
		$pdf->SetXY(5,33);
		$pdf->SetAutoPageBreak(0);
		$pdf->MultiCell(90,4,$json->cargoTrackingNumber.' - '.eraseIn( $json->cargoProviderName,"Marketplace"),0,'C');

// Musteri Adı
		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(3,40);
		$pdf->SetMargins(0,0,0);
		$pdf->SetAutoPageBreak(0);
		$pdf->MultiCell(90,2,$this->tr("ALICI Bilgileri : "),0,'L');

		$pdf->Line(3,44,90,44);


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(3,46);
		$pdf->SetMargins(0,0,0);
		$pdf->SetAutoPageBreak(0);
		$pdf->MultiCell(90,2,$this->tr("Sipariş No : ".$id),0,'L');


		$pdf->SetFont('Arial','',10);
		$pdf->SetXY(3,51);
		$pdf->SetMargins(0,0,0);
		$pdf->SetAutoPageBreak(0);
		$pdf->MultiCell(90,2,$this->tr("Ad Soyad : ".$json->shipmentAddress->fullName),0,'L');


		$pdf->SetFont('Arial','',9);
		$pdf->SetXY(3,56);
		$pdf->SetMargins(0,0,0);
		$pdf->SetAutoPageBreak(0);
		$pdf->MultiCell(90,5,$this->tr("Adres : ".$json->shipmentAddress->fullAddress),0,'L');








		$pdf->Line(3,70,90,70);

		$yAxis = 72;

		$nerde = strlen($localOrder->exit_json)>5?json_decode($localOrder->exit_json,true):[];

		$only_isbn = false;
		if (count($json->lines) > 3){
			$only_isbn = true;
		}

		foreach($json->lines as $var){
			$isbn = barkod_rep($var->barcode);
			if ($only_isbn){

				if (isset($nerde[$isbn])) {
					$yol =  kisalt(eraseIn($var->productName,'one size'),20) ." - ".$isbn  . " (". $nerde[$isbn].")";
				}else{
					$yol =  kisalt(eraseIn($var->productName,'one size'),20) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
				}
			}else{

				if (isset($nerde[$isbn])) {
					$yol = kisalt(eraseIn($var->productName,'one size'),30) ." - ".$isbn  . " (". $nerde[$isbn].")";
				}else{
					$yol = kisalt(eraseIn($var->productName,'one size'),30) ." - ".$isbn .' - '.$var->quantity.' adet '. "(". nerde($isbn).")";
				}
			}



			$pdf->Rect( 3,$yAxis+1,2,2,"F");
			$pdf->SetFont('Arial','',8);
			$pdf->SetXY(6,$yAxis);
			$pdf->SetMargins(0,0,0);
			$pdf->SetAutoPageBreak(0);
			$pdf->MultiCell(90,4,$this->tr($yol),0,'L');

			$yAxis +=5;

		}


		$pdf->Output();

	}

	/*
	{"cat_local_id":11,
	"cat_local_name":"Roman",
	"cat_remote_id":1750,
	"cat_remote_name":"Roman",
	"brand_local_id":1217,
	"brand_local_name":"Agapi Yay\u0131nlar\u0131",
	"brand_remote_id":13110,
	"brand_remote_name":"Agapi Yay\u0131nlar\u0131",
	"attr":[{"attributeId":338,"attributeName":"Beden","attributeValue":"Tek Ebat","attributeValueId":6821},{"attributeId":458,"attributeName":"Bas\u0131m Dili","attributeValue":"T\u00fcrk\u00e7e","attributeValueId":22695},{"attributeId":344,"attributeName":"Yazar","attributeValue":"Salih Memecan","attributeValueId":22805}]}
*/

	function changeCat($id)
	{
		$urun = Product::with('get_category')->where('id',$id)->first();


		$maincat = $this->tryol->getCatList(0);

		if (request()->has('remote_cat_id')) {

			$product = ProductMarketPlace::where(["product_id"=>$id,"market_id"=>1])->first();
			$json = json_decode($product->jsons,true);

			$json['cat_remote_id'] =request('remote_cat_id');
			$json['cat_remote_name'] =request('remote_cat_name');

			return	$product->update(["jsons"=>json_encode($json)]);

		}

		
		return view('admin.trendyol.singe_cat_change',compact( 'maincat','urun' ));
	}


}
