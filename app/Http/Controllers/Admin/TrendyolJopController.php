<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Tryol;
use Illuminate\Http\Request;
use App\Models\MarketPlaces;
use App\Models\MarketCatMatch;
use App\Models\MarketBrandMatch;
use App\Models\ProductMarketPlace;
use App\Models\MarketWriterMatch;

use App\Models\DepoBook;
use App\Models\Product;
use Illuminate\Support\Facades\DB;



class TrendyolJopController extends Controller
{



	public $pid = 1;
	public $tryol;
	public $trendyol_config;
	public function __construct()
	{
		$this->trendyol_config = MarketPlaces::find($this->pid);
		$config = ['seller_id'=>$this->trendyol_config->user_no,"api_user"=>$this->trendyol_config->api_id,"api_pass"=>$this->trendyol_config->api_key];
		$this->tryol =  new Tryol($config);

	}




	function stockUpdateById($id)
	{

		$book = ProductMarketPlace::with('product')->where(["product_id"=>$id,'market_id'=>$this->pid])->first();

		$price = request('price');
		$fixed = request('fix');


		$book->update(['price'=>$price,'list_price'=>yuzdePlus($price,50),'fixed'=>$fixed]);



		$data[] = [
			"barcode"=>$book->spec_barcode,
			"quantity"=>$book->product->product_stok,
			//"quantity"=>0,
			"salePrice"=>$price,
			"listPrice"=>yuzdePlus($price,50)
		];


		$result = $this->tryol->sendData("https://api.trendyol.com/sapigw/suppliers/".$this->trendyol_config->user_no."/products/price-and-inventory",["items"=>$data],"POST");

		$batchReques = $this->tryol->batchRequests($result->batchRequestId);

		$json_nots =  [
			"status"=> $batchReques->status,
			"creationDate"=> $batchReques->creationDate,
			"lastModification"=> $batchReques->lastModification,
			"sourceType"=> $batchReques->sourceType,
			"itemCount" => $batchReques->itemCount,
			"failedItemCount" => $batchReques->failedItemCount,
			"batchRequestType" => $batchReques->batchRequestType,
		];



		DB::table('trendyol_results')->insert(['requestID'=>$result->batchRequestId,"notes"=>"Tek Fiyat update işlemi","json_note"=>json_encode($json_nots),"created_at"=>date('Y-m-d H:i:s')]);

		



	}

	function readcsv()
	{




		$trendyol_config = MarketPlaces::find(1);


		$csv =  public_path('var.csv');

		ini_set('auto_detect_line_endings',TRUE);
		$handle = fopen($csv,'r');
		while ( ($data = fgetcsv($handle) ) !== FALSE ) {
			$isbns[] = $data[0];
		}
		ini_set('auto_detect_line_endings',FALSE);

		unset($isbns[0]);



		foreach ($isbns as $key => $isbn) {



			$var = DB::table("products")

			->leftJoin('product_extend', 'product_extend.product_id', '=', 'products.id')
			->leftJoin('categories', 'categories.id', '=', 'products.product_cat')
			->leftJoin('market_cat_match', 'market_cat_match.local_id', '=', 'products.product_cat')
			->leftJoin('yayinevi', 'yayinevi.id', '=', 'product_extend.yayinci_id')
			->leftJoin('market_brand_match', 'market_brand_match.local_id', '=', 'yayinevi.id')
			->select('products.*','products.id as uid', 'categories.categories_name', 'categories.*', 'yayinevi.*',
				"market_cat_match.local_id as cat_local_id",
				"market_cat_match.remote_id as cat_remote_id",
				"market_cat_match.local_name as cat_local_name",
				"market_cat_match.remote_name as cat_remote_name",
				"market_brand_match.*"
			)->where("product_stock_code", $isbn)
			->first();


			if (isset($var->cat_local_id)) {



				$json = [
					"cat_local_id"=> $var->cat_local_id,
					"cat_local_name"=> $var->cat_local_name,
					"cat_remote_id"=> $var->cat_remote_id,
					"cat_remote_name"=> $var->cat_remote_name,
					"brand_local_id"=> $var->local_id,
					"brand_local_name"=> $var->local_name,
					"brand_remote_id"=> $var->remote_id,
					"brand_remote_name"=> $var->remote_name,
					"product_name"=> $var->product_name,
					"product_price"=> $var->product_price,
				];

				$price = yuzdePlus($var->product_price,$trendyol_config->percent_unit);
				$price = floatval($price) + floatval($trendyol_config->price_unit);



/*
			DB::table('product_market_place')->insert([
				'product_id' => $var->uid, 
				'market_id' => 1,
				"price" => $price,
				"jsons" => json_encode($json),
				"fixed"=>0

			]);


*/

			DB::table('product_market_place')
			->updateOrInsert(
				[
					'product_id' => $var->uid, 
					'market_id' => 1,

				],
				[
					"price" => $price,
					"jsons" => json_encode($json),
					"fixed"=>0
				]
			);





		}else{
			echo  $isbn."<br>";
			//prep($var);
		}



	}

}

function rafstockUpdate()
{
	

}

function stockUpdate()
{
	$stoklar = ProductMarketPlace::with('product')->where('market_id',$this->pid)->get()->chunk(90);


	foreach ($stoklar as  $items) {

		$data = [];

		foreach ($items as   $var) {

			if (isset($var->product)) {

				$price  = $var->price;
				$list_price  = $var->list_price;

				
				if ($var->product->is_depo == 1) {
					$adet = $var->product->product_stok;
				}else{
					$adet = 0;
				}

			}else{
				$adet = 0;
				$price  = 0;
				$list_price  = 0;
			}



			$data[] = [
				"barcode"=>$var->spec_barcode,
				"quantity"=>$adet,
			//	"quantity"=>0,
				"salePrice"=>$price,
				"listPrice"=>$list_price,
			];




		}

		$result = $this->tryol->sendData("https://api.trendyol.com/sapigw/suppliers/".$this->trendyol_config->user_no."/products/price-and-inventory",["items"=>$data],"POST");

		unset($data);

		$batchReques = $this->tryol->batchRequests($result->batchRequestId);

		$json_nots =  [
			"status"=> $batchReques->status,
			"creationDate"=> $batchReques->creationDate,
			"lastModification"=> $batchReques->lastModification,
			"sourceType"=> $batchReques->sourceType,
			"itemCount" => $batchReques->itemCount,
			"failedItemCount" => $batchReques->failedItemCount,
			"batchRequestType" => $batchReques->batchRequestType,
		];

		DB::table('trendyol_results')->insert(['requestID'=>$result->batchRequestId,"notes"=>"stok update işlemi","json_note"=>json_encode($json_nots),"created_at"=>date('Y-m-d H:i:s')]);



	}


}


function getresult()
{
	$id = request('id');

	$result = $this->tryol->batchRequests($id);

	//prep($result);


	$json_nots =  [
		"status"=> $result->status,
		"creationDate"=> $result->creationDate,
		"lastModification"=> $result->lastModification,
		"sourceType"=> $result->sourceType,
		"itemCount" => $result->itemCount,
		"failedItemCount" => $result->failedItemCount,
		"batchRequestType" => $result->batchRequestType,
	];

	DB::table('trendyol_results')->where('requestID',$id)->update(["json_note"=>json_encode($json_nots)]);



	return view('admin.trendyol.ajax.request',compact('result'));
}


function emtpyresult()
{
	DB::table('trendyol_results')->truncate();
}




function sync()
{


	$mevcut = ProductMarketPlace::where('market_id',$this->pid)->get()->pluck('product_id');
	$urunler = Product::with('detail',"extend")->where('is_active',1)->where('is_depo',1)->where('product_stok','>',0)->whereNotIn("id",$mevcut)->get()->chunk(90);


	foreach ($urunler as $uruns) {



		foreach ($uruns as $key => $row) {





			if ( $row->product_cat > 0) {

				$cid = $row->product_cat;
				$isbn = $row->product_stock_code;
				$folder = $row->product_group_code;
				$yid = $row->extend->yayinci_id;
				$yazarid = $row->extend->yazar_id;






				$cat_remote = MarketCatMatch::where('local_id',$cid)->first();
				$brand_remote = MarketBrandMatch::where('local_id',$yid)->first();
				$yazar_remote = MarketWriterMatch::where('local_id',$yazarid)->first();


				if ($cat_remote) {
					$cat_remote_id = $cat_remote->remote_id;
				}
				if ($brand_remote) {
					$brand_remote_id = $brand_remote->remote_id;
				}
				if ($yazar_remote) {
					$yazar_remote_id = $yazar_remote->remote_id;
				}





				//$src = public_path('uploads/product/'.$resim[0]);
				$src = public_path('files/'.$folder.'/'.$isbn.'.jpg');

				if (file_exists($src)) {
					
					$url = "https://www.anadolukitap.com/files/".$folder."/".$isbn.".jpg";

					

					if ($row->product_price == 0) {

						$oran_price = yuzdex($row->detail->product_old_price,20);
						$price = yuzdePlus($oran_price,$this->trendyol_config->percent_unit);
						$price = floatval($price) + floatval($this->trendyol_config->price_unit);

					}else{

						$price = yuzdePlus($row->product_price,$this->trendyol_config->percent_unit);
						$price = floatval($price) + floatval($this->trendyol_config->price_unit);

					}



					$img[] = ['url'=>$url];


					$attributes[] = [
						"attributeId"=>458,
						"attributeValueId"=>22695,
					];

					$attributes[] = [
						"attributeId"=>344,
						"attributeValueId"=>$yazar_remote_id,
					];

					$isbn_new = $isbn;


					$data[] = [
						'barcode'=> $isbn_new,
						'title'=> trim($row->product_name),
						'productMainId'=> $isbn_new,
						'brandId'=> $brand_remote_id,
						'categoryId'=>  $cat_remote_id,
						'quantity'=> $row->product_stok,
						//'quantity'=> 0,
						'stockCode'=> $isbn_new,
						'description'=>  empty($row->product_content)?$row->product_name:$row->product_content,
						'currencyType'=> "TRY",
						"dimensionalWeight"=>0,
						"salePrice"=>round($price,2),
						"listPrice"=>yuzdePlus($price,50),
						"shipmentAddressId"=> 235426,
						"returningAddressId"=>235427,
						"vatRate" =>0,
						"cargoCompanyId" => $this->trendyol_config->cargo,
						"images" => $img,
						"attributes" => $attributes,


					];


					unset($attributes);
					unset($img);

				}// resim var olanlar

			}

		}// end each

		

		$result = $this->tryol->sendData("https://api.trendyol.com/sapigw/suppliers/".$this->trendyol_config->user_no."/v2/products",["items"=>$data],"POST");

		//prep($result);

		unset($data);
		if (isset($result->batchRequestId)) {



			$batchReques = $this->tryol->batchRequests($result->batchRequestId);

			$json_nots =  [
				"status"=> $batchReques->status,
				"creationDate"=> $batchReques->creationDate,
				"lastModification"=> $batchReques->lastModification,
				"sourceType"=> $batchReques->sourceType,
				"itemCount" => $batchReques->itemCount,
				"failedItemCount" => $batchReques->failedItemCount,
				"batchRequestType" => $batchReques->batchRequestType,
			];

			DB::table('trendyol_results')->insert(['requestID'=>$result->batchRequestId,"notes"=>"stok Ekleme işlemi","json_note"=>json_encode($json_nots),"created_at"=>date('Y-m-d H:i:s')]);
		}

			//prep($result);
	}// end 90 each

}


function update()
{


	$mevcut = ProductMarketPlace::where("on_sale",0)->where("market_id",$this->pid)->get()->pluck('product_id');
	$urunler = Product::with('detail',"extend")->where('is_active',1)->where('product_stok','>',0)->whereIn("id",$mevcut)->get()->chunk(90);


	foreach ($urunler as $uruns) {



		foreach ($uruns as $key => $row) {


			$book = ProductMarketPlace::where("product_id",$row->id)->where("market_id",$this->pid)->first();


			if ( $book->locked == 0) {


				$isbn = $row->product_stock_code;
				$resim = json_decode($row->detail->product_image,TRUE);

				$book_json = json_decode($book->jsons);

				$yazarid = $row->extend->yazar_id;

				$yazar_remote = MarketWriterMatch::where('local_id',$yazarid)->first();


				$cat_remote_id = $book_json->cat_remote_id;
				$brand_remote_id = $book_json->brand_remote_id;


				if ($yazar_remote) {

					$yazar_remote_id = $yazar_remote->remote_id;
				}else{
					$yazar_remote_id = 0;
				}


				//$src = public_path('uploads/product/'.$resim[0]);
				$src = public_path('uploads/product/'.$isbn.'.webp');

				if (file_exists($src)) {
					
					$url = "https://www.anadolukitap.com/uploads/product/".$isbn.".webp";


					if ($book->price == 0) {

						$price = yuzdePlus($row->product_price,$this->trendyol_config->percent_unit);
						$price = floatval($price) + floatval($this->trendyol_config->price_unit);
						$list_price = yuzdePlus($price,50) ;

					}else{
						$price  = $book->price;
						$list_price  = $book->list_price;
					}



					$img[] = ['url'=>$url];


					$attributes[] = [
						"attributeId"=>458,
						"attributeValueId"=>22695,
					];

					$attributes[] = [
						"attributeId"=>344,
						"attributeValueId"=>$yazar_remote_id,
					];




					$data[] = [
						'barcode'=> $book->spec_barcode,
						'title'=> trim($row->product_name),
						'productMainId'=> $book->spec_barcode,
						'brandId'=> $brand_remote_id,
						'categoryId'=>  $cat_remote_id,
					//	'quantity'=> 0,
						'quantity'=> $row->product_stok,
						'stockCode'=> $book->spec_barcode,
						'description'=>  empty($row->product_content)?$row->product_name:$row->product_content,
						'currencyType'=> "TRY",
						"dimensionalWeight"=>0,
						"salePrice"=>round($price,2),
						"listPrice"=>round($list_price,2),
						"shipmentAddressId"=> 235426,
						"returningAddressId"=>235427,
						"vatRate" =>0,
						"cargoCompanyId" => $this->trendyol_config->cargo,
						"images" => $img,
						"attributes" => $attributes,


					];


					unset($attributes);
					unset($img);

				}// resim var olanlar

			}

		}// end each

		$result = $this->tryol->sendData("https://api.trendyol.com/sapigw/suppliers/".$this->trendyol_config->user_no."/v2/products",["items"=>$data],"PUT");

		//prep($result);

		unset($data);
		if (isset($result->batchRequestId)) {



			$batchReques = $this->tryol->batchRequests($result->batchRequestId);

			$json_nots =  [
				"status"=> $batchReques->status,
				"creationDate"=> $batchReques->creationDate,
				"lastModification"=> $batchReques->lastModification,
				"sourceType"=> $batchReques->sourceType,
				"itemCount" => $batchReques->itemCount,
				"failedItemCount" => $batchReques->failedItemCount,
				"batchRequestType" => $batchReques->batchRequestType,
			];

			DB::table('trendyol_results')->insert(['requestID'=>$result->batchRequestId,"notes"=>"Ürün Güncelleme işlemi","json_note"=>json_encode($json_nots),"created_at"=>date('Y-m-d H:i:s')]);
		}

		//prep($result);
	}

}


function saveProduct($p = 0)
{

	ini_set('max_execution_time', 0);

	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
/*

	$products = $this->tryol->allProduct();

	$products = json_decode($products);
	$urunler = $products->content;
	$totalElements = $products->totalElements;
	$totalPages = $products->totalPages;

	echo $totalPages;

	die();

	*/

	//for ($i=4; $i < $totalPages; $i++) { 

	$remote_urunler = $this->tryol->filterProducts(3,true,2000);
	$r_urunler = json_decode($remote_urunler);
	$local_products = $r_urunler->content;


	$products = $r_urunler;
	$urunler = $products->content;
	$totalElements = $products->totalElements;
	$totalPages = $products->totalPages;

	//echo $totalPages;

	//die();
	$i=1;
	foreach ($local_products as $key => $item) {

		//	prep($item);

		$product = Product::with(['get_category','extend'])->where('product_stock_code', barkod_rep($item->barcode))->first();


		if ($product) {

			$idsi = $product->id;
			$json = [
				"cat_local_id" =>$product->product_cat,
				"cat_local_name" =>$product->get_category->categories_name,
				"cat_remote_id" =>$item->pimCategoryId,
				"cat_remote_name" =>$item->categoryName,
				"brand_local_id" =>$product->extend->yayinci_id,
				"brand_local_name" =>$product->extend->getYayinci->yayinci_name,
				"brand_remote_id" =>$item->brandId,
				"brand_remote_name" =>$item->brand,
				"attr"=>  $item->attributes,
			];
		}else{
			$json = [];
			$idsi = 0;
		}


		$data = [
			"market_id" =>$this->pid,
			"product_id" =>$idsi,

			"spec_barcode"=>$item->barcode,
			"price"=>$item->salePrice,
			"list_price"=>$item->listPrice,
			"jsons"=>json_encode($json),
			"fixed"=>0,
			"approved"=>$item->approved,
			"on_sale"=>$item->onSale,
			"locked"=>$item->locked,
		];

		ProductMarketPlace::updateOrCreate(
			["market_id" =>$this->pid,"spec_barcode" => $item->barcode],
			$data
		);




	//	}// end urunler foreach

$i++;	}// end for

//	$this->saveProduct2($i);

echo $i;

DB::statement('SET FOREIGN_KEY_CHECKS=1;');
}

function saveProduct2($p = 0)
{
	ini_set('max_execution_time', 0);
	$products = $this->tryol->filterProducts($p,false,1000);

	$products = json_decode($products);
	$urunler = $products->content;
	$totalElements = $products->totalElements;
	$totalPages = $products->totalPages;



	for ($i=$p; $i < $totalPages; $i++) { 

		$remote_urunler = $this->tryol->filterProducts($i,false,1000);
		$r_urunler = json_decode($remote_urunler);
		$local_products = $r_urunler->content;

		foreach ($local_products as $key => $item) {

			$product = Product::with(['get_category','extend'])->where('product_stock_code',trim($item->barcode))->first();

			if ($product) {



				$json = [
					"cat_local_id" =>$product->product_cat,
					"cat_local_name" =>$product->get_category->categories_name,
					"cat_remote_id" =>$item->pimCategoryId,
					"cat_remote_name" =>$item->categoryName,
					"brand_local_id" =>$product->extend->yayinci_id,
					"brand_local_name" =>$product->extend->getYayinci->yayinci_name,
					"brand_remote_id" =>$item->brandId,
					"brand_remote_name" =>$item->brand,
					"attr"=>  $item->attributes,
				];

				$data = [
					"market_id" =>$this->pid,
					"product_id" =>$product->id,
					"product_id" =>$product->id,
					"spec_barcode"=>$item->barcode,
					"price"=>$item->salePrice,
					"list_price"=>$item->listPrice,
					"jsons"=>json_encode($json),
					"fixed"=>0,
					"approved"=>$item->approved,
					"on_sale"=>$item->onSale,
					"locked"=>$item->locked,
				];

				ProductMarketPlace::updateOrCreate(
					["market_id" =>$this->pid,"product_id" =>$product->id],
					$data
				);



			}
		}// end urunler foreach

	}// end for


	
}



function updateByID($id)
{



	$row = Product::with('detail',"extend")->where('is_active',1)->where("id",$id)->first();





	$book = ProductMarketPlace::where("product_id",$row->id)->where("market_id",$this->pid)->first();




	$isbn = $row->product_stock_code;
	$resim = json_decode($row->detail->product_image,TRUE);

	$book_json = json_decode($book->jsons);

	$yazarid = $row->extend->yazar_id;

	$yazar_remote = MarketWriterMatch::where('local_id',$yazarid)->first();


	$cat_remote_id = $book_json->cat_remote_id;
	$brand_remote_id = $book_json->brand_remote_id;


	if ($yazar_remote) {

		$yazar_remote_id = $yazar_remote->remote_id;
	}else{
		$yazar_remote_id = 0;
	}


				//$src = public_path('uploads/product/'.$resim[0]);
	$src = public_path('uploads/product/'.$isbn.'.webp');

	if (file_exists($src)) {

		$url = "https://www.anadolukitap.com/uploads/product/".$isbn.".webp";


		if ($book->price == 0) {

			$price = yuzdePlus($row->product_price,$this->trendyol_config->percent_unit);
			$price = floatval($price) + floatval($this->trendyol_config->price_unit);
			$list_price = yuzdePlus($price,50) ;

		}else{
			$price  = $book->price;
			$list_price  = $book->list_price;
		}



		$img[] = ['url'=>$url];


		$attributes[] = [
			"attributeId"=>458,
			"attributeValueId"=>22695,
		];

		$attributes[] = [
			"attributeId"=>344,
			"attributeValueId"=>$yazar_remote_id,
		];




		$data[] = [
			'barcode'=> $book->spec_barcode,
			'title'=> trim($row->product_name),
			'productMainId'=> $book->spec_barcode,
			'brandId'=> $brand_remote_id,
			'categoryId'=>  $cat_remote_id,
			'quantity'=> $row->product_stok,
			//'quantity'=> 0,
			'stockCode'=> $book->spec_barcode,
			'description'=>  empty($row->product_content)?$row->product_name:$row->product_content,
			'currencyType'=> "TRY",
			"dimensionalWeight"=>0,
			"salePrice"=>round($price,2),
			"listPrice"=>round($list_price,2),
			"shipmentAddressId"=> 235426,
			"returningAddressId"=>235427,
			"vatRate" =>0,
			"cargoCompanyId" => $this->trendyol_config->cargo,
			"images" => $img,
			"attributes" => $attributes,


		];



	} 





	$result = $this->tryol->sendData("https://api.trendyol.com/sapigw/suppliers/".$this->trendyol_config->user_no."/v2/products",["items"=>$data],"PUT");


	if (isset($result->batchRequestId)) {



		$batchReques = $this->tryol->batchRequests($result->batchRequestId);

		$json_nots =  [
			"status"=> $batchReques->status,
			"creationDate"=> $batchReques->creationDate,
			"lastModification"=> $batchReques->lastModification,
			"sourceType"=> $batchReques->sourceType,
			"itemCount" => $batchReques->itemCount,
			"failedItemCount" => $batchReques->failedItemCount,
			"batchRequestType" => $batchReques->batchRequestType,
		];

		DB::table('trendyol_results')->insert(['requestID'=>$result->batchRequestId,"notes"=>"Ürün Güncelleme işlemi","json_note"=>json_encode($json_nots),"created_at"=>date('Y-m-d H:i:s')]);
	}

		//prep($result);



}



function zeroRep()
{
	$books = ProductMarketPlace::with('product')->where("market_id",$this->pid)->where("price",0)->get();

	foreach ($books as $key => $row) {
		
		$price = yuzdePlus($row->product->product_price,$this->trendyol_config->percent_unit);
		$price = floatval($price) + floatval($this->trendyol_config->price_unit);
		$list_price = yuzdePlus($price,50) ;

		$row->update(['price'=>$price ,'list_price'=>$list_price]);

	}


}



}


