<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\UserDetail;
use Auth;

class UserController extends Controller
{
	function login()
	{
		if (request()->isMethod('POST')) {
			$this->validate(request(), [
				'email' => 'required|email',
				'password' => 'required'
			]);

			$credentials = [
				'email' => request()->get('email'),
				'password' => request()->get('password'),
				'is_admin' => 1,
				'is_active' => 1
			];

			if (Auth::guard('yonetim')->attempt($credentials, request()->has('remember'))) {
				return redirect()->route('crudv4.home');
			}
			else{
				return back()->withInput()->withErrors(['email' => 'Giriş htalı!']);
			}
		}
		return view('admin.login');
	}

	function logout()
	{
		Auth::guard('yonetim')->logout();
		request()->session()->flush(); //session sıfırla
		request()->session()->regenerate(); //session yeniden oluştur
		return redirect()->route('crudv4.login'); //anasayfaya git
	}

	function index()
	{
		$list = User::where('is_admin',1)->orderby('created_at','asc')->get();

		$title ="Yöneticiler";

		return view('admin.user.index',compact('list','title'));
	}

	function member()
	{
		$list = User::where('is_admin',0)->orderby('created_at','asc')->get();

		$title ="Site Üyeleri";

		return view('admin.user.index',compact('list','title'));
	}


	function form($id=0)
	{
		$entry = new User;
		if ($id>0) {
			$entry = User::find($id);
		}

		$title ="Kullanıcı Düzenleme Formu";

		return view('admin.user.form',compact('entry','title',"id"));
	}

	function save($id=0)
	{
		$this->validate(request(),[
			'name' => 'required',
			'sname' => 'required',
			'email' => 'required|email'
		]);

		$data = request()->only('name','sname','email');

		if (request()->filled('pass')) {
			$data['pass'] = Hash::make(request('pass'));
		}

		$data['is_active'] = request()->has('is_active') && request('is_active')==1 ? 1 : 0;

		$data['is_admin'] = request()->has('is_admin') && request('is_admin')==1 ? 1 : 0;
		/*
		if(request()->has('aktif_mi'))
			$data['aktif_mi'] = 1;
		else
		$data['aktif_mi'] = 0; */


		if ($id>0) {
			$entry = User::where('id',$id)->firstOrFail();
			$entry->update($data);
			$page = "edit";

		}else{
			$entry = User::create($data);
			$page = "add";
		}

		UserDetail::updateOrCreate(
			['user_id' => $entry->id],
			[
				'user_address' => request('user_address'),
				'user_tel' => request('user_tel'),
				'user_gsm' => request('user_gsm')
			]
		);

		return redirect()
		->route('crudv4.user.'.$page,$entry->id)
		->with('mesaj',($id>0 ? 'Kullanici Güncellendi' : 'Kullanici Kaydedildi'))
		->with('mesaj_tur','success');
	}

	function del($id)
	{
		$user = User::find($id);
		//$user->getDetail()->delete();
		$user->delete();

		return "1";
	}
}
