<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Yayinevi;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\ProductExtend;
use Illuminate\Support\Facades\DB;

class YayinciController extends Controller
{
	protected $filter = ['yayinci_name','id','is_active','is_home'];

	function index()
	{


		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 25;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>101?100:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}


		$query = Yayinevi::query();

		$query->when(request()->has('yayinci_name'), function ($q) {
			$val = request('yayinci_name');
			return $q->where('yayinci_name', 'like',  "%$val%");
		});
		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});	

		$query->when(request()->has('is_active'), function ($q) {
			$val = request('is_active');
			return $q->where('is_active',  intval($val));
		});

		$query->when(request()->has('is_home'), function ($q) {
			$val = request('is_home');
			return $q->where('is_home',  intval($val));
		});

		$list = $query->orderBy($orderby[0],$orderby[1] )->paginate($limit);



		$title ="Yayınevi Yönetimi";


		return view('admin.yayinci.index',compact('list','title'));
	}






	function form($id=0)
	{
		
		$title ="Yayınevi Ekleme Formu";
		$page = "addform";
		$list = new Yayinevi;

		if ($id>0) {
			$list = Yayinevi::find($id);
			$title ="Yayınevi Güncelleme Formu";
			$page = "editform";
		}

		
		return view('admin.yayinci.'.$page,compact('list','title'));
	}



	function save($id=0)
	{
		$data = request()->only('yayinci_name', 'yayinevi_api_id', 'yayinci_oran','yayinci_alis_oran');


		$data['yayinci_url'] = str_slug(request('yayinci_name'));



		$this->validate(request(),[
			'yayinci_name' => 'required',
			'yayinci_alis_oran' => 'required',
			'yayinci_oran' => 'required',

		]);



		if ($id>0) {
			$yayinci = Yayinevi::where('id',$id)->firstOrFail();
			$yayinci->update($data);
			$page = "edit";

		}else{
			$yayinci = Yayinevi::create($data);
			$page = "add";
		}



		if(request()->hasFile('resim')){


			$this->validate(request(),[
				'resim' => 'image|mimes:jpg,png,jpeg,gif|max:4096'
			]);

			$resim = request()->file('resim');

			$namesi = str_slug(request('yayinci_name')).  ".webp";

			if ($resim->isValid()) {

				my_upload($resim->getRealPath(),$namesi,"yayinci",r_size("yayinci_logo"),'webp');
				Yayinevi::updateOrCreate(
					['id' => $yayinci->id],
					['yayinci_logo' => $namesi]
				);
			}
		}



		return redirect()
		->route('crudv4.yayinevi.'.$page,$yayinci->id)
		->with('mesaj',($id>0 ? 'Güncellendi ': 'Kaydedildi'))
		->with('mesaj_tur','success');
	}



	function update($id)
	{


		$data = [request('label')=>request('val')];

		$entry = Yayinevi::where('id',$id)->firstOrFail();
		$entry->update($data);

	}



	function price($id)
	{
		$yayinci = ProductExtend::with('product.detail','getYayinci')->where('yayinci_id',$id)->get();

		foreach ($yayinci as $key => $var) {

			$bg_price = $var->product->detail->product_old_price;

			echo $var->product->product_stock_code." Arka Fiyat :". $bg_price.' Eski Fiyat '.$var->product->product_price.' Satış Oran :%'.$var->getYayinci->yayinci_oran;


			if ($var->product->is_depo == 1) {

				$yayinci_oran = $var->getYayinci->yayinci_oran;
				$yayinci_alis_oran = $var->getYayinci->yayinci_alis_oran;

			}else{
				
				$yayinci_oran = $var->getYayinci->emek_satis;
				$yayinci_alis_oran = $var->getYayinci->emek_alis;


			}


			$price = yuzdex($bg_price,$yayinci_oran);
			$alis = yuzdex($bg_price,$yayinci_alis_oran);

			$var->product->detail->update(['product_get_price'=>$alis]);
			$var->product->update(['product_price'=>$price]);

			echo " Yeni Fiyat :".$var->product->product_price.'<br>';



		}
	}

	function prices()
	{


		$yayincilar = Yayinevi::where('is_active',1)->get();


		foreach ($yayincilar as $key => $item) {


			$yayinci = ProductExtend::with('product.detail','getYayinci')->where('yayinci_id',$item->id)->get();

			foreach ($yayinci as $key => $var) {

				$bg_price = $var->product->detail->product_old_price;

			//	echo $var->product->product_stock_code." Arka Fiyat :". $bg_price.' Eski Fiyat '.$var->product->product_price.' Satış Oran :%'.$var->getYayinci->yayinci_oran;

				if ($var->product->is_depo == 1) {

					$yayinci_oran = $var->getYayinci->yayinci_oran;
					$yayinci_alis_oran = $var->getYayinci->yayinci_alis_oran;

				}else{
					
					$yayinci_oran = $var->getYayinci->emek_satis;
					$yayinci_alis_oran = $var->getYayinci->emek_alis;


				}


				$price = yuzdex($bg_price,$yayinci_oran);
				$alis = yuzdex($bg_price,$yayinci_alis_oran);

				$var->product->detail->update(['product_get_price'=>$alis]);
				$var->product->update(['product_price'=>$price]);

			//	echo " Yeni Fiyat :".$var->product->product_price.'<br>';



			}


		}

		
	}


	

	function del($id)
	{

		$del = Yayinevi::find($id);

		$del->delete();

		return "1";
	}


	function emekYayincilar()
	{


		$yayinci = Yayinevi::where('emek_satis','>',0)->where('yayinci_temin',0)->first();

		if (!$yayinci) {
			die('bitti');
		}

		echo $yayinci->yayinci_name.'<br>';

		$id = $yayinci->id;

		$urunler = ProductExtend::with('product.detail')
		->whereHas('product', function ($q){

			return $q->where(['is_sale'=> 1,'is_depo'=>0]);
		})
		->where('yayinci_id',$id)->get();
		$i = 0;
		foreach ($urunler as $key => $var) {

			$bg_price = $var->product->detail->product_old_price;

			//	echo $var->product->product_stock_code." Arka Fiyat :". $bg_price.' Eski Fiyat '.$var->product->product_price.' Satış Oran :%'.$var->getYayinci->yayinci_oran;

			$price = yuzdex($bg_price,$yayinci->emek_satis);
			$alis = yuzdex($bg_price,$yayinci->emek_alis);

			$var->product->detail->update(['product_get_price'=>$alis]);
			$var->product->update(['product_price'=>$price]);


			$i++;
		}

		if ($i>0) {
			$yayinci->update(['yayinci_temin'=>1,'is_active'=>1]);
		}else{
			$yayinci->update(['yayinci_temin'=>1]);
		}

		$yayinci->update(['yayinci_temin'=>1]);
		echo $i." Adet ürün güncellendi";

		echo "<script> setTimeout(function(){location.href='/crudv4/yayinevi/emekYayincilar'},2000)</script>";
	}


}
