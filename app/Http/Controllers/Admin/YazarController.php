<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Yazar;


class YazarController extends Controller
{
	protected $filter = ['yazar_name','id','is_active','is_home'];

	function index()
	{


		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 25;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>101?100:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}


		$query = Yazar::query();

		$query->when(request()->has('yazar_name'), function ($q) {
			$val = request('yazar_name');
			return $q->where('yazar_name', 'like',  "%$val%");
		});
		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});	

		$query->when(request()->has('is_active'), function ($q) {
			$val = request('is_active');
			return $q->where('is_active',  intval($val));
		});

		$query->when(request()->has('is_home'), function ($q) {
			$val = request('is_home');
			return $q->where('is_home',  intval($val));
		});

		$list = $query->orderBy($orderby[0],$orderby[1] )->paginate($limit);


		$title ="Yazar Yönetimi";

		return view('admin.yazar.index',compact('list','title'));
	}



	function save($id=0)
	{
		$data = request()->only('yazar_name', 'yazar_api_id');


		$data['yazar_url'] = str_slug(request('yazar_name'));



		$this->validate(request(),[
			'yazar_name' => 'required',


		]);



		if ($id>0) {
			$yayinci = Yazar::where('id',$id)->firstOrFail();
			$yayinci->update($data);
			$page = "edit";

		}else{
			$data['is_active'] = 1;
			$yayinci = Yazar::create($data);
			$page = "add";
		}




		return redirect()
		->route('crudv4.yazar.'.$page,$yayinci->id)
		->with('mesaj',($id>0 ? 'Güncellendi ': 'Kaydedildi'))
		->with('mesaj_tur','success');
	}


	function update($id)
	{


		$data = [request('label')=>request('val')];

		$entry = Yazar::where('id',$id)->firstOrFail();
		$entry->update($data);

	}


	function form($id=0)
	{



		$title ="Yazar Ekleme Formu";
		$page = "addform";
		$list = new Yazar;

		if ($id>0) {
			$list = Yazar::find($id);
			$title ="Yazar Güncelleme Formu";
			$page = "editform";
		}

		
		return view('admin.yazar.'.$page,compact('list','title'));
	}

	function del($id)
	{

		$del = Yazar::find($id);

		$del->delete();

		return "1";
	}

}
