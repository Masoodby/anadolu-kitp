<?php

namespace App\Http\Controllers;
use App\Models\TimeSlider;
use App\Models\Basket;
use App\Models\BasketCombo;
use App\Models\BasketProduct;
use App\Models\Product;
use App\Models\Combo;
use Cart;
use Validator;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    function index()
    {

        return view(theme().'.cart.cart');
    }

    function add( )
    {

        $id = request('id');
        $qty = request()->has('qty')?intval(request('qty')):1;



        $product = Product::with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->find($id);
        $discount_product = TimeSlider::where('product_id',$id)->where('end_time','>', now())->first();





        if(isset($discount_product->discount)){
            $price = yuzdex($product->product_price,$discount_product->discount);
        }else{
            $price = $product->product_price;
        }



        $isVal =  Cart::search(function ($cart, $key) use($id) {
            return $cart->id == $id;
        });


        $currentCount = 0;
        if ($isVal->count() > 0) {
            $currentCount = $isVal->first()->qty;
        }

        $qty = $product->product_stok-$currentCount>$qty?$qty :$product->product_stok-$currentCount;


        $options = [
            'url'=> route("urun",['categories_url'=>$product->get_category->categories_url,'product_url'=>$product->product_url]),
            'stok_code'=>$product->product_stock_code,
            'stok'=>$product->product_stok,
            'image'=> res("product",jres($product->detail->product_image),"100x100"),
            'yazar'=> isset($product->extend->getYazar->yazar_name)?$product->extend->getYazar->yazar_name:"",
            'yayinci'=> isset($product->extend->getYayinci->yayinci_name)?$product->extend->getYayinci->yayinci_name:"",

        ];


        $cartItem = Cart::add($product->id, $product->product_name, $qty, $price, 0,$options);

        if (auth()->check()) {

            if (session()->has('active_cart_id')) {

                $active_cart_id = session('active_cart_id');

            }else{

                $active_cart = Basket::create([
                    'user_id'=>auth()->id(),
                    'is_buy'=>0
                ]);
                $active_cart_id = $active_cart->id;
                session()->put('active_cart_id',$active_cart_id);
            }



            BasketProduct::updateOrCreate(
                ['basket_id' => $active_cart_id, 'product_id' => $id],
                ['qty' => $qty, 'total_price' => $product->product_price, 'state' => 0,"cartjson"=>json_encode($options)]
            );
        }

        $item =  Cart::search(function ($cart, $key) use($id) {
            return $cart->id == $id;
        })->first();

        return view(theme().'.cart.cartmodal',compact( 'item'));


    }
    function addcombo( )
    {

        $id = request('id');
        $qty = request()->has('qty')?intval(request('qty')):1;



        $combo = Combo::with(['products'])->find($id);
       $price = $combo->combo_price;



        $isVal =  Cart::search(function ($cart, $key) use($combo) {
            return $cart->name == $combo->combo_name;
        });


        $currentCount = 0;
        if ($isVal->count() > 0) {
            $currentCount = $isVal->first()->qty;
        }

        $qty = 1;


        $options = [
            'url'=> route("kitap-setleri",['slug'=>$combo->combo_url]),
            'stok_code'=>null,
            'stok'=>null,
            'image'=> res( 'combo',$combo->combo_image,"100x100"),
            'yazar'=> null,
            'yayinci'=> null,
            'combo'=>true,
        ];


        $cartItem = Cart::add($combo->id, $combo->combo_name, $qty, $price, 0,$options);

        if (auth()->check()) {

            if (session()->has('active_cart_id')) {

                $active_cart_id = session('active_cart_id');

            }else{

                $active_cart = Basket::create([
                    'user_id'=>auth()->id(),
                    'is_buy'=>0
                ]);
                $active_cart_id = $active_cart->id;
                session()->put('active_cart_id',$active_cart_id);
            }



            BasketCombo::updateOrCreate(
                ['basket_id' => $active_cart_id, 'combo_id' => $combo->id],
                ['qty' => $qty, 'total_price' => $combo->combo_price, 'state' => 0,"cartjson"=>json_encode($options)]
            );
        }

        $item =  Cart::search(function ($cart, $key) use($combo) {
            return $cart->name == $combo->combo_name;
        })->first();

        return view(theme().'.cart.cartmodal',compact( 'item'));


    }



    function remove()
    {

        $rowid  =  request('id');

        if (auth()->check()) {
            $active_cart_id = session('active_cart_id');
            $cartItem = Cart::get($rowid);
            BasketProduct::where('basket_id',$active_cart_id)->where('product_id',$cartItem->id)->delete();
        }

        Cart::remove($rowid);
        return 'Ürün sepeten kaldırıldı!';
    }

    function removeAll()
    {
        if (auth()->check()) {
            $active_cart_id = session('active_cart_id');
            BasketProduct::where('basket_id',$active_cart_id)->delete();
        }

        Cart::destroy();
        return redirect()->route('sepet')
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'Ürün sepetiniz boşaltıldı!');
    }

    //ajax ile çağırılan bir method olduğu için with ile redirect methodu kullanılmıyor
    function edit()
    {


        $rowid  =  request('id');

        $validator = Validator::make(request()->all(), [
            'qty' => 'required|numeric|between:0,100'
        ]);



        if (auth()->check()) {
            $active_cart_id = session('active_cart_id');
            $cartItem = Cart::get($rowid);
            if (request('qty')==0)
                BasketProduct::where('basket_id',$active_cart_id)->where('product_id',$cartItem->id)->delete();
            else
                BasketProduct::where('basket_id',$active_cart_id)->where('product_id',$cartItem->id)->update(['qty'=>request('qty')]);
        }

        Cart::update($rowid,request('qty'));
        $item = Cart::get($rowid);
        $kargo = set('kargo_esik')>=Cart::total() ? price(set('kargo_cost')):"Ücretsiz Kargo";
        $grandtotal = price((Cart::total()) + (set('kargo_esik')>=Cart::total() ? set('kargo_cost'):0));
        $res = ['rowtotal'=>price($item->subtotal),"subtotal"=>price(Cart::total()) ,"kargo"=>$kargo,'grandtotal'=>$grandtotal];
        return response()->json($res);
    }


    function loadtopcart()
    {
        $items = Cart::content();
        $subtotal = Cart::subtotal();
        $tax = Cart::tax();
        $total = Cart::total();
        $count = Cart::count();



        return view(theme().'.cart.topcart', compact( 'items',"subtotal","tax","total","count"));
    }



}
