<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;




class CategoryController extends Controller
{

	protected $filter = ['price',"yayinevi",'yazar'];

	public function index($cat_url)
	{
		$category = Category::with('getProduct')->where('categories_url', $cat_url)->firstOrFail();
		$subcats = Category::where('is_active', 1)->where('categories_main', $category->id)->get();
		$levelcat = Category::where('is_active', 1)->where('categories_main', $category->categories_main)->get();



		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}


		$catID = get_child($category->id);
		$tree = get_tree($category->id);


		$filter = Product::with(['extend.getYazar','extend.getYayinci',"detail",'get_category'])->where("is_active",1)->whereIn("product_cat",$catID)->limit(1000)->get();

		$filter = filterparameters($filter);

		//$filter = [];


		$query = Product::query();



		$query->when(request()->has('product_name'), function ($q) {
			$val = request('product_name');
			return $q->where('product_name', 'like',  "%$val%");
		});
		$query->when(request()->has('id'), function ($q) {
			$val = request('id');
			return $q->where('id',  intval($val));
		});	
		$query->when(request()->has('product_cat'), function ($q) {
			$val = request('product_cat');
			return $q->where('product_cat',  intval($val));
		});



		if (request()->has('price') &&  request('price') !="") {

			$vals = explode('-', request('price'));



			$min = isset($vals[0])?floatval($vals[0]):0;
			$max =  isset($vals[1])?floatval($vals[1]):0;
			$val = [$min,$max];

			$filter['current_price'] = $val;

			$query->when(request()->has('price') , function ($q) use($val) {

				return $q->whereBetween('product_price', $val);
			});

		}

		

		$query->when(request()->has('yazar') && request('yazar') !="", function ($q) {
			$val = explode("-",request('yazar'));
			return $q->whereHas('extend.getYazar', function ($q) use($val){
				return $q->whereIn('yazar_url', $val);
			});
		});


		$query->when(request()->has('yayinevi') && request('yayinevi') !="", function ($q) {
			$val = explode("-",request('yayinevi'));
			return $q->whereHas('extend.getYayinci', function ($q) use($val){
				return $q->whereIn('yayinci_url', $val);
			});
		});



		
		$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->where("resim_durum",1)->whereIn("product_cat",$catID)->orderBy($orderby[0],$orderby[1])->paginate($limit);


		return view(theme().'.category.category',compact( 'category','products','subcats','filter','tree','levelcat'));
	}


	function bestbook()
	{

		$title ="Çok Satanlar";
		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}


		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}

		$query = Product::query();


		$query->whereHas('detail', function ($q){
			return $q->where('product_konum_cok', 1);
		});

		$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->orderBy($orderby[0],$orderby[1])->paginate($limit);


		return view(theme().'/galery',compact('products','title'));
	}

	function newbook()
	{

		$title = "Yeni Ürünler";
		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}

		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}

		$query = Product::query();


		$query->whereHas('detail', function ($q){
			return $q->where('product_konum_new', 1);
		});
		
		$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->orderBy($orderby[0],$orderby[1])->paginate($limit);
		return view(theme().'/galery',compact('products','title'));
	}


	function reducedproduct()
	{

		$title = "İndirirmli Ürünler";
		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}

		$query = Product::query();


		$products = $query->join('product_details', 'products.id', '=', 'product_details.product_id')
		->where("product_details.product_konum_ind",1)
		->paginate($limit);
		return view(theme().'/galery',compact('products', 'title'));
	}
}
