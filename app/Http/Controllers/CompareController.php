<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

use Cart;

class CompareController extends Controller
{
	function index()
	{


		$list =  Cart::instance('compare')->content();

		return $list;
	}



	function add()
	{


		$id = request('id');
		$qty = 1;



		$product = Product::with(["get_category",'detail','extend.getBrand'])->find($id);

		$cartItem = Cart::instance('compare')->add(

			$product->id, $product->product_name, $qty, $product->product_price, 0, 
			[
				'url'=> route("urun",['categories_url'=>$product->get_category->categories_url,'product_url'=>$product->product_url]),
				'stok_code'=>$product->product_stock_code,
				'stok'=>$product->product_stok,
				'image'=> res("product",jres($product->detail->product_image),"100x100"),
				'brand'=> isset($product->extend->getBrand->brand_name)?$product->extend->getBrand->brand_name:"",

			]);

		return "Karşılaştırma Listenize eklendi";

	}




	function countwishlist()
	{
		if (auth()->check()) {
			return Cart::instance('compare')->count();

		}else{
			return 0;
		}
	}
}