<?php

namespace App\Http\Controllers;

use App\Firsatlari;
use Illuminate\Http\Request;

class FirsatlariController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Firsatlari  $firsatlari
     * @return \Illuminate\Http\Response
     */
    public function show(Firsatlari $firsatlari)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Firsatlari  $firsatlari
     * @return \Illuminate\Http\Response
     */
    public function edit(Firsatlari $firsatlari)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Firsatlari  $firsatlari
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Firsatlari $firsatlari)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Firsatlari  $firsatlari
     * @return \Illuminate\Http\Response
     */
    public function destroy(Firsatlari $firsatlari)
    {
        //
    }
}
