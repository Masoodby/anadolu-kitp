<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Models\Slider;
use App\Models\Banner;
use App\Models\TimeSlider;
use App\Models\Product;
use App\Models\Blog;
use App\Models\Yayinevi;
use Carbon\Carbon;
use App\Models\Combo;


class HomeController extends Controller
{

	public function index()
	{


		$slider = Slider::where('is_active',1)->orderby('rank','asc')->get();
		$blogs = Blog::where(['is_active'=>1,"is_home"=>1])->orderby('id','desc')->get();


		$yayincilar = Yayinevi::where(['is_active'=>1,"is_home"=>1])->where('yayinci_logo',"!=","")->orderby('id','desc')->get();

		$catID = get_child(6);
		$childbooks = Product::with(['extend.getYazar','extend.getYayinci',"detail",'get_category'])->where("is_active",1)->where("resim_durum",1)->whereIn("product_cat",$catID)->limit(24)->get();


        $banners0=Banner::orderBy('id','DESC')->where('status',1)->where('position','0')->take(3)->get();
        $banners1=Banner::orderBy('id','DESC')->where('status',1)->where('position','1')->take(3)->get();
        $banners2=Banner::orderBy('id','DESC')->where('status',1)->where('position','2')->take(3)->get();
        $banners3=Banner::orderBy('id','DESC')->where('status',1)->where('position','3')->take(3)->get();
        // $tslider=TimeSlider::orderBy('id','Desc')->where('end_time','>', now())->take(3)->get();
        $urunseti=Combo::orderBy('id','desc')->with('products')->where('is_active',1)->get();

        // dd($tslider);
		return view(theme().'.home.home',compact( 'slider','childbooks','urunseti','banners0','banners1','banners2','banners3','blogs',"yayincilar"));
	}




}
