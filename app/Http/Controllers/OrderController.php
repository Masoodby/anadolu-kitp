<?php

namespace App\Http\Controllers;

use App\Models\Order;

use Illuminate\Http\Request;

class OrderController extends Controller
{
	function index()
	{
		$orders = Order::with('baskets')
		->whereHas('baskets', function ($query) //ilişkili tablo ile filtreleme
		{
			$query->where('user_id',auth()->id()); //sadece giris yapan kullanıcının bilgileri
		})
		->orderByDesc('created_at')->get();
		return view('orders', compact('orders'));
	}

	function show($id)
	{
		$siparis = Order::with('baskets.basket_products.product')
		->whereHas('baskets', function ($query)
		{
			$query->where('user_id',auth()->id());
		})
		->where('orders.id',$id)->firstOrFail();
		return view('orders', compact('orders'));
	}
}
class mng{
	
	}
	function FaturaSiparisListesi($params){
		$req='<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<FaturaSiparisListesi xmlns="http://tempuri.org/">
					  <pSiparisNo>'.$params['pSiparisNo'].'</pSiparisNo>
					  <pKullaniciAdi>'.$params['pKullaniciAdi'].'</pKullaniciAdi>
					  <pSifre>'.$params['pSifre'].'</pSifre>
					</FaturaSiparisListesi>
				  </soap:Body>
				</soap:Envelope>';
		$response=$this->curl_post($req);
		return $response['soapBody']['FaturaSiparisListesiResponse']['FaturaSiparisListesiResult']['diffgrdiffgram']['NewDataSet']['FaturaSiparisListesi'];
	}
	function KargoBilgileriByReferans($params){
		$req='<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<KargoBilgileriByReferans xmlns="http://tempuri.org/">
					  <pMusteriNo>'.$params['pMusteriNo'].'</pMusteriNo>
					  <pSifre>'.$params['pSifre'].'</pSifre>
					  <pSiparisNo>'.$params['pSiparisNo'].'</pSiparisNo>
					  <pGonderiNo>'.$params['pGonderiNo'].'</pGonderiNo>
					  <pFaturaSeri>'.$params['pFaturaSeri'].'</pFaturaSeri>
					  <pFaturaNo>'.$params['pFaturaNo'].'</pFaturaNo>
					  <pIrsaliyeNo>'.$params['pIrsaliyeNo'].'</pIrsaliyeNo>
					  <pEFaturaNo>'.$params['pEFaturaNo'].'</pEFaturaNo>
					  <pRaporType>'.$params['pRaporType'].'</pRaporType>
					</KargoBilgileriByReferans>
				  </soap:Body>
				</soap:Envelope>';
		$response=$this->curl_post($req);
	return $response['soapBody']['KargoBilgileriByReferansResponse']['KargoBilgileriByReferansResult']['diffgrdiffgram']['NewDataSet']['Table1'];
	}
	function FaturaSiparisListesiByTarih($params){
		$req='<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<FaturaSiparisListesiByTarih xmlns="http://tempuri.org/">
					  <pKullaniciAdi>'.$params['pKullaniciAdi'].'</pKullaniciAdi>
					  <pSiparisTarih>'.$params['pSiparisTarih'].'</pSiparisTarih>
					  <pSifre>'.$params['pSifre'].'</pSifre>
					</FaturaSiparisListesiByTarih>
				  </soap:Body>
				</soap:Envelope>';
		$response=$this->curl_post($req);
	return $response['soapBody']['FaturaSiparisListesiByTarihResponse']['FaturaSiparisListesiByTarihResult']['diffgrdiffgram']['NewDataSet']['FaturaSiparisListesi'];
	}
	function KargoBilgileriByTarih($params){
		$req='<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<KargoBilgileriByTarih xmlns="http://tempuri.org/">
					  <pMusteriNo>'.$params['pMusteriNo'].'</pMusteriNo>
					  <pSifre>'.$params['pSifre'].'</pSifre>
					  <pTarih>'.$params['pTarih'].'</pTarih>
					  <pRaporType>'.$params['pRaporType'].'</pRaporType>
					  <pFlAltfirma>'.$params['pFlAltfirma'].'</pFlAltfirma>
					</KargoBilgileriByTarih>
				  </soap:Body>
				</soap:Envelope>';
		$response=$this->curl_post($req);
		print_r($response);
	return $response;
	}
	function KargoTakipByReferans($params){
		$req='<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<KargoTakipByReferans xmlns="http://tempuri.org/">
					  <pKullanici>'.$params['pKullanici'].'</pKullanici>
					  <pSifre>'.$params['pSifre'].'</pSifre>
					  <pReferansId>'.$params['pReferansId'].'</pReferansId>
					</KargoTakipByReferans>
				  </soap:Body>
				</soap:Envelope>';
		$response=$this->curl_post($req);

	return $response['soapBody']['KargoTakipByReferansResponse']['KargoTakipByReferansResult']['diffgrdiffgram'];
	}
	function curl_post($req){
		global $wsdl;
		$header = array(
		"Content-type: text/xml;charset=\"utf-8\"",
		"Accept: text/xml",
		"Cache-Control: no-cache",
		"Pragma: no-cache",
		"Content-length: ".strlen($req),
		);
		$soap_do = curl_init();
		curl_setopt($soap_do, CURLOPT_URL, $wsdl );
		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($soap_do, CURLOPT_TIMEOUT,        10);
		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($soap_do, CURLOPT_POST,           true );
		curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $req);
		curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
		$result=curl_exec($soap_do);
		$xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml = simplexml_load_string($xml);
		$json = json_encode($xml);
		$responseArray = json_decode($json,true);
		return $responseArray;
	}

}
