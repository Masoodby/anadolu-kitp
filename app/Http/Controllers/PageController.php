<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Blog;

class PageController extends Controller
{
	function index($page_url='')
	{
		$page = Page::where('page_url',$page_url)->firstOrFail();

		$pages = Page::where('page_type',$page->page_type)->get();
		$title ="Kurumsal";
		return view(theme().'.page',compact( 'page','pages','title'));
	}

	function help($page_url='')
	{
		$page = Page::where('page_url',$page_url)->firstOrFail();
		$title ="Yardım";
		$pages = Page::where('page_type',$page->page_type)->get();
		return view(theme().'.page',compact( 'page','pages','title'));
	}

	function blog($page_url='')
	{
		$page = Blog::where('page_url',$page_url)->firstOrFail();
		$title ="Blog";
		$pages = Blog::where('is_active',1)->get();
		return view(theme().'.page',compact( 'page','pages','title'));
	}

	function page($id='')
	{
		$page = Page::where('id',$id)->firstOrFail();

		echo $page->page_content;
	}
}
