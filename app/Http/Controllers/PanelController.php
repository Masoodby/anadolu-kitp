<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserDetail;
use App\Mail\UserRegisterMail;
use App\Mail\UserRecoveryPassMail;
use Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

use App\Models\Basket;
use App\Models\BasketProduct;
use Cart;

use App\Models\Page;


class PanelController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	function loginform()
	{
		$title="Giriş Formu";

		return view(theme().'.panel.login',compact( 'title'));
	}

	function login()
	{
		$this->validate(request(),[
			'email'=>'required|email',
			'password'=>'required'
		]);
		$credentials = [
			'email'=>request('email'),
			'password'=>request('password'),
			'is_active'=>1
		];

		if (auth()->attempt($credentials, request()->has('remember')))
		{

			if (Cart::count()>0) {
				$this->cartIslem();
			}

			$back = request()->has('back')?request("back"):"anasayfa";
			return  redirect()->intended($back);  
		}else{

			return back()->with('mesaj','Giriş Bilgileri Hatalı');
		}

	}



	function cartIslem()
	{
		$is_there_cart_product = Basket::where(['user_id'=>auth()->id(),'is_buy'=>0]);

		if ($is_there_cart_product->count() > 0) {



			$active_cart_id = $is_there_cart_product->first()->id;
			session()->put('active_cart_id',$active_cart_id);

		}else{

			if (Cart::count()>0) {

				$active_cart = Basket::create([
					'user_id'=>auth()->id(),
					'is_buy'=>0
				]);
				$active_cart_id = $active_cart->id;
				session()->put('active_cart_id',$active_cart_id);
			}
		}


		if (Cart::count()>0) {


			if (session()->has('active_cart_id')) {

				$active_cart_id = session('active_cart_id');
			}else{
				$active_cart = Basket::create([
					'user_id'=>auth()->id(),
					'is_buy'=>0
				]);
				$active_cart_id = $active_cart->id;
				session()->put('active_cart_id',$active_cart_id);
			}


			foreach (Cart::content() as $cartItem) {
				BasketProduct::updateOrCreate(
					['basket_id'=>$active_cart_id, 'product_id'=>$cartItem->id],
					['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>0,"cartjson"=>json_encode($cartItem->options)]
				);
			}


			Cart::destroy();
			//$sepetUrunler = BasketProduct::where('sepet_id',$active_cart_id)->get(); LayzLoading
			$basketProducts = BasketProduct::with('product.extend.getYayinci','product.extend.getYazar','product.detail')->where('basket_id',$active_cart_id)->get(); //eager loading
			foreach ($basketProducts as $basketProduct) {




				$options = [
					'url'=> route("urun",['categories_url'=>$basketProduct->product->get_category->categories_url,'product_url'=>$basketProduct->product->product_url]),
					'stok_code'=>$basketProduct->product->product_stock_code,
					'stok'=>$basketProduct->product->product_stok,
					'image'=> res("product",jres($basketProduct->product->detail->product_image),"100x100"),
					'yazar'=> isset($basketProduct->product->extend->getYazar->yazar_name)?$basketProduct->product->extend->getYazar->yazar_name:"",
					'yayinci'=> isset($basketProduct->product->extend->getYayinci->yayinci_name)?$basketProduct->product->extend->getYayinci->yayinci_name:"",

				];


				Cart::add(
					$basketProduct->product->id, $basketProduct->product->product_name, $basketProduct->qty, $basketProduct->product->product_price, 0,$options
				);

			}
			


			$is_product = BasketProduct::where(['basket_id'=>$active_cart_id,'state'=>0]);
			if ($is_product->count() > 0) {


				$basketProducts = BasketProduct::with('product.extend.getYayinci','product.extend.getYazar','product.detail')->where('basket_id',$active_cart_id)->get(); //eager loading
				foreach ($basketProducts as $basketProduct) {



					$options = [
						'url'=> route("urun",['categories_url'=>$basketProduct->product->get_category->categories_url,'product_url'=>$basketProduct->product->product_url]),
						'stok_code'=>$basketProduct->product->product_stock_code,
						'stok'=>$basketProduct->product->product_stok,
						'image'=> res("product",jres($basketProduct->product->detail->product_image),"100x100"),
						'yazar'=> isset($basketProduct->product->extend->getYazar->yazar_name)?$basketProduct->product->extend->getYazar->yazar_name:"",
						'yayinci'=> isset($basketProduct->product->extend->getYayinci->yayinci_name)?$basketProduct->product->extend->getYayinci->yayinci_name:"",

					];
					Cart::add(
						$basketProduct->product->id, $basketProduct->product->product_name, $basketProduct->qty, $basketProduct->product->product_price, 0,$options

					);

				}
				

			}
		}

	}






	function loginAjax()
	{
		$this->validate(request(),[
			'email'=>'required|email',
			'password'=>'required'
		]);
		$credentials = [
			'email'=>request('email'),
			'password'=>request('password'),
			'is_active'=>1
		];

		if (auth()->attempt($credentials, request()->has('remember')))
		{

			$this->cartIslem();
			return response()->json(['message'=>'success']);
		}else{

			return response()->json(['message'=>'error']);

		}

	}

	function register_form()
	{
		$title = "Üye Kayıt Formu";

		return view(theme().'.panel.register',compact( 'title'));
	}

	function recoveryPass()
	{
		$mail =  request('mail');

		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			return response()->json(['statu'=>'error','message'=>"E-Posta Hatalı"]);
		}

		$varmi  = User::where("email",$mail);

		if ($varmi->count() > 0) {
			$user = $varmi->first();
			$pass =  Str::random(6);

			$user->update(['pass'=>Hash::make($pass)]);

			Mail::to(request('mail'))->send(new UserRecoveryPassMail($user,$pass));
			return response()->json(['statu'=>'success','message'=>"E-Posta Adresinize Yeni Şifreniz Gönderildi"]);

		}else{

			return response()->json(['statu'=>'error','message'=>"E-Posta Adresi Sistemde Kayıtlı Değil"]);
		}


	}

	function register()
	{
		$this->validate(request(),[
			'name'=>'required|min:3|max:60',
			'sname'=>'required|min:3|max:60',
			'email'=>'required|email|unique:users',
			'password'=> 'required|min:1|max:9'
		]);

		$user = User::create([
			'name'=>request('name'),
			'sname'=>request('sname'),
			'email'=>request('email'),
			'pass'=>Hash::make(request('password')),
			'activation_key'=>Str::random(60),
			'is_active'=>1
		]);

		$user->getDetail()->create(["user_tel"=>request('gsm'),"user_gsm"=>request('gsm')]);



		Mail::to(request('email'))->send(new UserRegisterMail($user));

		auth()->login($user);

		if (Cart::count()>0) {
			$this->cartIslem();

		}


		$back = request()->has('back')?request("back"):"anasayfa";
		return  redirect()->intended($back); 

	}

	function activeRun($anahtar)
	{
		$user = User::where('activation_key', $anahtar)->first();
		if (!is_null($user)) {
			$user->activation_key = null;
			$user->is_active =1;
			$user->save();

			return redirect()->to('/')
			->with('mesaj','Aktivasyon işlemi başarılı kullanıcı aktif')
			->with('mesaj_tur','success');
		}else{
			return redirect()->to('/')
			->with('mesaj','Kullanıcı kaydı aktifleştirilemedi')
			->with('mesaj_tur','warning');
		}
	}

	function logout()
	{

		if (auth()->check()) {

			auth()->logout();
			request()->session()->flush();  
			request()->session()->regenerate();  
		}

		return redirect()->route('anasayfa'); //anasayfaya git
	}
}
