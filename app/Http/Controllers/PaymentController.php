<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserAdres;
use Mail;
use App\Mail\UserRegisterMail;
use App\Mail\OrderTakenMail;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Basket;
use App\Models\BasketProduct;
use App\Models\BasketCombo;
use Cart;
use App\Library\Paytr;

class PaymentController extends Controller
{
	function index()
	{




		if (!auth()->check()) {//kullanıcı girişi yapılmamış
			return redirect()->route('panel.login',["back"=>"siparisi-tamamla"])
			->with('mesaj_tur','info')
			->with('mesaj_header','Hemen Üye Olabilirsiniz')
			->with('mesaj','Ödeme işlemleri için oturum açmanız veya kullanıcı kaydı yapmanız gerekmektedir.');
		}
		else if(count(Cart::content())==0){ //sepette ürün yok
			return redirect()->route('anasayfa')
			->with('mesaj_tur','info')
			->with('mesaj_header','Sepetiniz Boş')
			->with('mesaj','Ödeme işlemleri için sepetinizde ürün bulunmalıdır.');
		}

		if (!session()->has('active_cart_id')) {
			$this->cartIslem();
		}

		$delivery = UserAdres::where(['user_id'=>auth()->id(),'adres_tur'=>0,'adres_current'=>1])->first();
		$invoice = UserAdres::where(['user_id'=>auth()->id(),'adres_tur'=>1,'adres_current'=>1])->first();

		$all_adress = UserAdres::where('user_id',auth()->id())->get();


		return view(theme().'.payment.order', compact('all_adress','delivery','invoice'));
	}





	function quickOrder()
	{
		//prep(session('hizliform'));

		$cities = DB::table('city_counties')->groupBy('city')->get();


	if(count(Cart::content())==0){ //sepette ürün yok
		return redirect()->route('anasayfa')
		->with('mesaj_tur','info')
		->with('mesaj_header','Sepetiniz Boş')
		->with('mesaj','Ödeme işlemleri için sepetinizde ürün bulunmalıdır.');
	}

	return view(theme().'.payment.quick_order', compact('cities'));
}




function SaveOrder()
{
	session()->put('hizliform',request()->all());

	$orderData = [
		'payment_type' => request("payment_type"),
		'order_name' => request("order_name"),
		'order_sname' => request("order_sname"),
		'order_adres' => request("adres_detay"),
		'order_il' => request("adres_il"),
		'order_ilce' => request("adres_ilce"),
		'order_nots' => request("order_nots"),
	];


	$adresData = [
		'adres_title' => "Default",
		'fatura_unvan' => request("fatura_tur") == 1?request("fatura_unvan"):request("order_name").' '.request("order_sname"),
		'fatura_vd' => request("fatura_vd"),
		'fatura_vno' => request("fatura_vno"),
		'fatura_tck' => request("fatura_tck"),
		'fatura_tur' => request("fatura_tur"),
		'adres_name' => request("order_name"),
		'adres_sname' =>  request("order_sname"),
		'adres_il' => request("adres_il"),
		'adres_ilce' => request("adres_ilce"),
		'adres_detay' => request("adres_detay"),
		'adres_gsm' => request("adres_gsm"),
		'adres_current' => 1,
		'adres_tur' => 0,
	];

	$user_pass = rand(99999,999999);
	$userData = [
		'name' => request("order_name"),
		'sname' => request("order_sname"),
		'email' => request("email"),
		'pass' => Hash::make($user_pass),
		'is_active' => 1,
		'is_admin' => 0,

	];


	$userdetailData = [
		'user_address' => request("adres_detay").' '.request("adres_ilce").' '.request("adres_il"),
		'user_tel' => request("adres_gsm"),
		'user_gsm' => request("adres_gsm"),

	];



	$this->validate(request(),[
		'order_name' => 'required',
		'order_sname' => 'required',
		'adres_detay' => 'required',
		'adres_il' => 'required',
		'adres_ilce' => 'required',
		'adres_gsm' => 'required',
		'email' => 'required',

	]);


	$orderData['basket_id'] = session('active_cart_id');
	$orderData['order_state'] = 0;
	$orderData['order_total'] = Cart::total();
	$orderData['cargo_cost'] =  set('kargo_esik') >= Cart::total() ? set('kargo_cost'):0;
	$orderData['grand_total'] =  Cart::total() + $orderData['cargo_cost'];

	$orderData['order_ip'] =  request()->ip();
	$orderData['order_rand_id'] =  time();




// user register

	$user_control = User::where('email',request("email"));

	if ($user_control->count() > 0) {

		$user = $user_control->first();

			//$adresData['user_id'] = $user->id

	}else{

		$user = User::create($userData);

			//Mail::to(request('email'))->send(new UserRegisterMail($user));

		$user->getDetail()->create($userdetailData);


	}

	$teslimat_adres = $user->getAdres()->create($adresData);
	$adresData["adres_tur"] = 1;
	$fat_adres = $user->getAdres()->create($adresData);

	auth()->login($user);

	$adres_id = $teslimat_adres->id;
	$fat_adres_id = $fat_adres->id;

	$orderData["order_adres_id"] = $adres_id;
	$orderData["order_fat_adres_id"] = $fat_adres_id;



	$orderData['user_id'] = auth()->id();

// user register end




	session()->put('orderData',$orderData);



	if (request("payment_type") == 2) {

		$orderData['basket_id'] = $this->cartSave();

		$order = Order::create($orderData);

		$mesaj = "Sn. ".$orderData['order_name'].' '.price($orderData['grand_total']).' değerindeki siparişiniz başarıyla kaydedildi. '.set("sms_fix");
		$to = $adresData['adres_gsm'];

		// Mail::to(request('email'))->send(new OrderTakenMail($order));

		Cart::destroy();

		session()->forget('active_cart_id');
		session()->forget('orderData');

		session()->forget('paytr');
		session()->forget('hizliform');

		smsSend($to,$mesaj);

		return redirect()->route('siparis-basarili');


	}elseif (request("payment_type") == 1) {


		$cart = [];

		foreach(Cart::content() as $item){

			$cart[] = array($item->name, $item->price, $item->qty);

		}
		session()->put('orderData',$orderData);

		$paytrdata = [
			"ip"=>$orderData['order_ip'],
			"email"=>$userData['email'],
			"grand_total"=>$orderData['grand_total'],
			"time"=>$orderData['order_rand_id'],
			"fullname"=>$userData['name'].' '.$userData['sname'],
			"adres"=>$userdetailData['user_address'],
			"gsm"=>$userdetailData['user_gsm'],
			"basket"=>base64_encode(json_encode($cart)),

		];

		session()->put('paytr',$paytrdata);

		return redirect()->route('payment3d');
	}


}




function save()
{

	if (session()->has('active_cart_id')) {


		$delivery = UserAdres::where(['user_id' => auth()->id(), 'id' => request("order_adres_id")])->first();

		$orderData = [
			'payment_type' => request("payment_type"),
			'user_id' => auth()->id(),
			'order_name' => auth()->user()->name,
			'order_sname' => auth()->user()->sname,
			'order_adres' => $delivery->adres_detay,
			'order_il' => $delivery->adres_il,
			'order_ilce' => $delivery->adres_ilce,
			'order_nots' => request("order_nots"),
			'order_adres_id' => request("order_adres_id"),
			'order_fat_adres_id' => request("order_fat_adres_id"),
		];

		$orderData['order_rand_id'] =  time();

		$orderData['basket_id'] = session('active_cart_id');
		$orderData['order_state'] = 0;
		$orderData['order_total'] = Cart::total();
		$orderData['cargo_cost'] =  set('kargo_esik') >= Cart::total() ? set('kargo_cost'):0;
		$orderData['grand_total'] =  Cart::total() + $orderData['cargo_cost'];

		$orderData['order_ip'] =  request()->ip();


		if (request("payment_type") == 2) {

			Basket::where('id',session('active_cart_id') )->update(['is_buy'=>1]);
			$this->stockUpdate(session('active_cart_id'));


			Order::create($orderData);
			Cart::destroy();
			session()->forget('active_cart_id');

			return redirect()->route('siparis-basarili');

		}elseif (request("payment_type") == 1) {



			$cart = [];

			foreach(Cart::content() as $item){

				$cart[] = array($item->name, $item->price, $item->qty);

			}


			$paytrdata = [
				"ip"=>$orderData['order_ip'],
				"email"=>auth()->user()->email,
				"grand_total"=>$orderData['grand_total'],
				"time"=>$orderData['order_rand_id'],
				"fullname"=>$orderData['order_name'].' '.$orderData['order_sname'],
				"adres"=>$orderData['order_adres'],
				"gsm"=>$delivery->adres_gsm,
				"basket"=>base64_encode(json_encode($cart)),

			];

			session()->put('paytr',$paytrdata);
			session()->put('orderData',$orderData);



			return redirect()->route('payment3d');

		}



	}else{


		return redirect()->route('sepet');
	}
}




function getCounties(Request $request){

	if($request->ajax()){

		$select = $request->get('select');
		$value = $request->get('value');
		$data = DB::table('city_counties')
		->where($select, $value)
		->get();
		$output = '<option value="">İlçe Seçiniz</option>';
		foreach($data as $row){
			$output .= '<option value="'.$row->county.'">'.$row->county.'</option>';
		}
		echo $output;
	}
}


function cartSave()
{
	$active_cart_id = Basket::active_cart_id();

	if (is_null($active_cart_id)) {

		$aktif_sepet= Basket::create([
			'user_id'=>auth()->id(),
			'is_buy'=>1
		]);
		$active_cart_id =$aktif_sepet->id;
	}

	session()->put('active_cart_id',$active_cart_id);

	if (Cart::count()>0) {

		foreach (Cart::content() as $cartItem) {
            if($cartItem->options->combo ?? null){
                BasketCombo::updateOrCreate(
                    ['basket_id'=>$active_cart_id, 'combo_id'=>$cartItem->id],
                    ['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>1,"cartjson"=>json_encode($cartItem->options)]
                );
            } else {
                BasketProduct::updateOrCreate(
                    ['basket_id'=>$active_cart_id, 'product_id'=>$cartItem->id],
                    ['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>1,"cartjson"=>json_encode($cartItem->options)]
                );

            }

		}
	}

	return $active_cart_id;

}




function stockUpdate($active_cart_id = 0)
{


	if (Cart::count()>0) {
		foreach (Cart::content() as $cartItem) {
            if($cartItem->options->combo ?? null){
                BasketCombo::updateOrCreate(
                    ['basket_id'=>$active_cart_id, 'combo_id'=>$cartItem->id],
                    ['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>1,"cartjson"=>json_encode($cartItem->options)]
                );
            } else {
                BasketProduct::updateOrCreate(
                    ['basket_id'=>$active_cart_id, 'product_id'=>$cartItem->id],
                    ['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>1,"cartjson"=>json_encode($cartItem->options)]
                );
            }

		}
	}
}


function success()
{

	if (session()->has('paytr')) {


		$orderData = session('orderData');
		$paytrdata = session('paytr');
		$orderData['basket_id'] = $this->cartSave();

		$order = Order::create($orderData);

		$mesaj = "Sn. ".$orderData['order_name'].' '.price($orderData['grand_total']).' değerindeki siparişiniz başarıyla kaydedildi. '.set("sms_fix");
		$to = $paytrdata['gsm'];

		// Mail::to($paytrdata['email'])->send(new OrderTakenMail($order));

		Cart::destroy();

		session()->forget('active_cart_id');
		session()->forget('orderData');
		session()->forget('paytr');

		// smsSend($to,$mesaj);


	}

	$order = Order::with('baskets.basket_products.product','address',"fatadres")->where('user_id',auth()->id())->orderByDesc('id')->first();

	return view(theme().'.payment.success', compact('order'));
}



function cartIslem()
{
	$is_there_cart_product = Basket::where(['user_id'=>auth()->id(),'is_buy'=>0]);

	if ($is_there_cart_product->count() > 0) {



		$active_cart_id = $is_there_cart_product->first()->id;
		session()->put('active_cart_id',$active_cart_id);

	}else{

		if (Cart::count()>0) {

			$active_cart = Basket::create([
				'user_id'=>auth()->id(),
				'is_buy'=>0
			]);
			$active_cart_id = $active_cart->id;
			session()->put('active_cart_id',$active_cart_id);
		}
	}


	if (Cart::count()>0) {


		if (session()->has('active_cart_id')) {

			$active_cart_id = session('active_cart_id');
		}else{
			$active_cart = Basket::create([
				'user_id'=>auth()->id(),
				'is_buy'=>0
			]);
			$active_cart_id = $active_cart->id;
			session()->put('active_cart_id',$active_cart_id);
		}


		foreach (Cart::content() as $cartItem) {
            if($cartItem->options->combo ?? null){
                BasketCombo::updateOrCreate(
                    ['basket_id'=>$active_cart_id, 'combo_id'=>$cartItem->id],
                    ['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>0,"cartjson"=>json_encode($cartItem->options)]
                );

            } else {
                BasketProduct::updateOrCreate(
                    ['basket_id'=>$active_cart_id, 'product_id'=>$cartItem->id],
                    ['qty'=>$cartItem->qty, 'total_price'=>$cartItem->price, 'state'=>0,"cartjson"=>json_encode($cartItem->options)]
                );

            }

		}


		Cart::destroy();
			//$sepetUrunler = BasketProduct::where('sepet_id',$active_cart_id)->get(); LayzLoading
			$basketProducts = BasketProduct::with('product.extend.getYayinci','product.extend.getYazar','product.detail')->where('basket_id',$active_cart_id)->get(); //eager loading
			if(count($basketProducts)){
                foreach ($basketProducts as $basketProduct) {




                    $options = [
                        'url'=> route("urun",['categories_url'=>$basketProduct->product->get_category->categories_url,'product_url'=>$basketProduct->product->product_url]),
                        'stok_code'=>$basketProduct->product->product_stock_code,
                        'stok'=>$basketProduct->product->product_stok,
                        'image'=> res("product",jres($basketProduct->product->detail->product_image),"100x100"),
                        'yazar'=> isset($basketProduct->product->extend->getYazar->yazar_name)?$basketProduct->product->extend->getYazar->yazar_name:"",
                        'yayinci'=> isset($basketProduct->product->extend->getYayinci->yayinci_name)?$basketProduct->product->extend->getYayinci->yayinci_name:"",

                    ];


                    Cart::add(
                        $basketProduct->product->id, $basketProduct->product->product_name, $basketProduct->qty, $basketProduct->product->product_price, 0,$options
                    );

                }
            }
            $basketcombos = BasketCombo::where('basket_id',$active_cart_id)->get(); //eager loading
			if(count($basketcombos)){
                foreach ($basketcombos as $combo) {




                    $options = [
                        'url'=> route("kitap-setleri",['slug'=>$combo->combo_url]),
                        'stok_code'=>null,
                        'stok'=>null,
                        'image'=> res( 'combo',$combo->combo_image,"100x100"),
                        'yazar'=> null,
                        'yayinci'=> null,
                        'combo'=>true,

                    ];


                    Cart::add(
                        $combo->combo->id, $combo->combo->name, $combo->qty, $combo->combo->combo_price, 0,$options
                    );

                }
            }

		}

		$is_product = BasketProduct::where(['basket_id'=>$active_cart_id,'state'=>0]);
		if ($is_product->count() > 0) {


				$basketProducts = BasketProduct::with('product.extend.getYayinci','product.extend.getYazar','product.detail')->where('basket_id',$active_cart_id)->get(); //eager loading
				foreach ($basketProducts as $basketProduct) {



					$options = [
						'url'=> route("urun",['categories_url'=>$basketProduct->product->get_category->categories_url,'product_url'=>$basketProduct->product->product_url]),
						'stok_code'=>$basketProduct->product->product_stock_code,
						'stok'=>$basketProduct->product->product_stok,
						'image'=> res("product",jres($basketProduct->product->detail->product_image),"100x100"),
						'yazar'=> isset($basketProduct->product->extend->getYazar->yazar_name)?$basketProduct->product->extend->getYazar->yazar_name:"",
						'yayinci'=> isset($basketProduct->product->extend->getYayinci->yayinci_name)?$basketProduct->product->extend->getYayinci->yayinci_name:"",

					];
					Cart::add(
						$basketProduct->product->id, $basketProduct->product->product_name, $basketProduct->qty, $basketProduct->product->product_price, 0,$options

					);

				}


			}

		}



		function PaytrForm()
		{



			$data = session("paytr");
			$pform =  new Paytr();

			$pform->merchant_id =  set("merchant_id");
			$pform->merchant_key  = set("merchant_key");
			$pform->merchant_salt = set("merchant_salt");


			$pform->email = $data['email'];
			$pform->payment_amount = $data['grand_total']*100;
			//$pform->payment_amount = 1*100;
			$pform->merchant_oid = $data['time'];

			$pform->user_name = $data['fullname'];
			$pform->user_address = $data['adres'];
			$pform->user_phone = $data['gsm'];
			$pform->merchant_ok_url = url("siparis-basarili");
			$pform->merchant_fail_url = url("siparis-basarisiz");
			$pform->user_basket = $data['basket'];
			$pform->user_ip = $data['ip'];
			$pform->timeout_limit = "30";
			$pform->debug_on = 0;
			$pform->test_mode = 0;
			$pform->no_installment = 0;
			$pform->max_installment = 0;
			$pform->currency =  "TL";

			$token = $pform->form();


			return view(theme().'.payment.paytrform', compact('token'));



		}



		function PaytrCallBack()
		{
			$post = $_POST;

			$merchant_key 	= set("merchant_key");
			$merchant_salt	= set("merchant_salt");

			$hash = base64_encode( hash_hmac('sha256', $post['merchant_oid'].$merchant_salt.$post['status'].$post['total_amount'], $merchant_key, true) );

			if( $hash != $post['hash'] )
				die('PAYTR notification failed: bad hash');


			if( $post['status'] == 'success' ) {

			} else {

			}

			echo "OK";
			exit;
		}


		function error()
		{
			return "sipariş kaydedilemedi";
		}



	}
