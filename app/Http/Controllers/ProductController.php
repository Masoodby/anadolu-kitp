<?php

namespace App\Http\Controllers;
use App\Models\TimeSlider;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Combo;
use Cart;

class ProductController extends Controller
{
	function Index($categories_url,$product_url)
	{

		$category = Category::where('categories_url', $categories_url)->firstOrFail();
		$product = Product::with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->where("product_cat",$category->id)->where('product_url',$product_url)->firstOrFail();
		$related = Product::with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->where("product_cat",$category->id)->where("id","<>",$product->id)->where('resim_durum',1)->limit(20)->get();
		$tree = get_tree($category->id);

		$id = $product->id;
         $firset=TimeSlider::where('product_id',$id)->where('end_time','>',now())->where('status',1)->select('discount')->first();

		$isVal =  Cart::search(function ($cart, $key) use($id) {
			return $cart->id == $id;
		});

		$currentCount = 0;
		if ($isVal->count() > 0) {
			$currentCount = $isVal->first()->qty;
		}
		return view(theme().'.product.product',compact( 'product','related',"category","tree","currentCount","firset"));
	}
	function combo( $slug)
	{
        $combo = Combo::with(['products'])->where("is_active",1)->where('combo_url',$slug)->firstOrFail();
		$related = Combo::orderBy('id','desc')->with('products')->where('is_active',1)->get();
        $id = $combo->combo_name;


		$isVal =  Cart::search(function ($cart, $key) use($id) {
			return $cart->name == $id;
		});

		$currentCount = 0;
		if ($isVal->count() > 0) {
			$currentCount = $isVal->first()->qty;
		}



// dd($product);

		return view(theme().'.product.combo-product',compact( 'combo','currentCount','related'));
	}

	function search()
	{

		if (request()->has('q')) {



			$title ="Arama (".request('q').')';
			$limit = 30;
			if (request()->has('limit')) {
				$limit =  intval(request('limit'))>122?120:intval(request('limit'));
				$limit =  $limit<10?10:$limit;

			}


			$orderby = ["id","desc"];
			if (request()->has('orderby')) {
				$orderby = explode('-',request('orderby'));

				if (!in_array($orderby[0], $this->filter)) {
					$orderby[0] = "id";
				}
				if (!in_array($orderby[1], ['asc','desc'])) {
					$orderby[1] = "desc";
				}
			}

			$val = request('q');

			$query = Product::query();


			$query->where('is_active', '=', 1);
			$query->where(function($q)  use($val) {
				return $q->where('product_name', 'like',  "%$val%")
				->orWhere('product_stock_code', 'like',  "%$val%");

			});


			$query->whereHas('extend.getYazar', function ($q) use($val){
				return $q->orWhere('yazar_name', 'like',  "%$val%");
			});

			$query->whereHas('extend.getYayinci', function ($q) use($val){
				return $q->orWhere('yayinci_name', 'like',  "%$val%");
			});





			$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->orderBy("id","desc")->orderBy($orderby[0],$orderby[1])->paginate($limit);


			return view(theme().'/galery',compact('products','title'));
		}else{
			return response('Error', 404)
			->header('Content-Type', 'text/plain');
		}

	}



	function ajaxSearch()
	{
		if (request()->has('q')) {

			$limit = 30;
			$orderby = ["id","desc"];
			$val = request('q');

			$query = Product::query();


			$query->where('is_active', '=', 1);
			$query->where(function($q)  use($val) {
				return $q->where('product_name', 'like',  "%$val%")
				->orWhere('product_stock_code', 'like',  "%$val%");

			});


			$query->whereHas('extend.getYazar', function ($q) use($val){
				return $q->orWhere('yazar_name', 'like',  "%$val%");
			});

			$query->whereHas('extend.getYayinci', function ($q) use($val){
				return $q->orWhere('yayinci_name', 'like',  "%$val%");
			});





			$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->orderBy("id","desc")->limit($limit)->orderBy($orderby[0],$orderby[1])->get();


			return view(theme().'/product.ajax_search_result',compact('products'));

		}
	}
}
