<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use Cart;

class WishlistController extends Controller
{
	function index()
	{
		if (auth()->check()) {

			$list =  Cart::instance('wishlist')->content();

		}else{
			return redirect()->route('panel.login')
			->with('mesaj_tur','info')
			->with('mesaj', 'Favori Listenizi Görebilmek için Oturum Açmalısınız');
		}
	}



	function add()
	{
		if (auth()->check()) {

			$id = request('id');
			$qty = 1;



			$product = Product::with(["get_category",'detail','extend.getBrand'])->find($id);

			$cartItem = Cart::instance('wishlist')->add(

				$product->id, $product->product_name, $qty, $product->product_price, 0, 
				[
					'url'=> route("urun",['categories_url'=>$product->get_category->categories_url,'product_url'=>$product->product_url]),
					'stok_code'=>$product->product_stock_code,
					'stok'=>$product->product_stok,
					'image'=> res("product",jres($product->detail->product_image),"100x100"),
					'brand'=> isset($product->extend->getBrand->brand_name)?$product->extend->getBrand->brand_name:"",

				]);

			return "Favori Listenize eklendi";
		}else{
			return "Favori Listenizi oluşturmak için üye girişi yapmalısınız";
		}
	}




	function countwishlist()
	{
		if (auth()->check()) {
			return Cart::instance('wishlist')->count();

		}else{
			return 0;
		}
	}
}