<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class XmlFeedController extends Controller
{
	function googleFeed()
	{

		$products = Product::with("get_category","detail")->where("is_active",">",0)->where("is_depo",">",0)->where("product_price",">",0)->get();

		header("Content-type: text/xml");

		echo '<?xml version="1.0" encoding="utf-8" ?>'. PHP_EOL;
		echo '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'. PHP_EOL;
		echo "<channel>" . PHP_EOL;
		echo "<title><![CDATA[anadolukitap.com | Anadolu kadar farklı]]></title>" . PHP_EOL;
		echo "<link>https://www.anadolukitap.com</link>" . PHP_EOL;
		echo "<description><![CDATA[anadolukitap.com ile aynı gün kargo kitaplarla tanışın.]]></description>" . PHP_EOL;

		foreach ($products as $var) {
			$id = $var->id;

			echo   "<item>" . PHP_EOL;
			echo  "<g:id>" . $id . "</g:id>" . PHP_EOL;
			echo  "<g:google_product_category>784</g:google_product_category>" . PHP_EOL;
		//	echo  "<g:google_product_category>" . $this->unturkce($this->xml_filter($var->get_category->categories_name)) . "</g:google_product_category>" . PHP_EOL;
			echo  "<g:title>"  . $this->xml_filter($var->product_name) . "</g:title>" . PHP_EOL;	
			echo  "<g:description>"  . $this->xml_filter($var->product_name) . "</g:description>" . PHP_EOL;	
			
			echo  "<g:link>https://www.anadolukitap.com/urun/".$var->get_category->categories_url.'/'.$var->product_url . "</g:link>" .PHP_EOL;
			echo  "<g:image_link>".res("product",jres($var->detail->product_image))."</g:image_link>" .PHP_EOL;

			echo "<g:condition>new</g:condition>";
			
			echo "<g:availability>in stock</g:availability>";
			echo "<g:price>" . $var->product_price . " TRY</g:price>" . PHP_EOL;
			
			

			echo  "<g:gtin>".$var->product_stock_code."</g:gtin>" . PHP_EOL;
			echo  "<g:mpn>" . $id . "</g:mpn>" . PHP_EOL;


			echo PHP_EOL . "</item>" . PHP_EOL;

		}
		echo "</channel></rss>";


	}


	public function xml_filter($q) {

		$q = str_replace("&uuml;","ü",$q);
		$q = str_replace("&Uuml;","Ü",$q);
		$q = str_replace("&Ouml;","Ö",$q);
		$q = str_replace("&ouml;","ö",$q);
		$q = str_replace("&Ccedil;","Ç",$q);
		$q = str_replace("&ccedil;","ç",$q);
		$q = str_replace("&nbsp;"," ",$q);
		$q = str_replace("&middot;","",$q);
		$q = str_replace("&bull;","",$q);
		$q = str_replace("&rsquo;","",$q);
		$q = str_replace("&lsquo;","",$q);
		$q = str_replace('w:st="on"',"",$q);
		$q = str_replace("&ndash;","-",$q);
		$q = str_replace("&acirc;","a",$q);
		$q = str_replace("&","",$q);

		$q = str_replace("allowfullscreen>",">",$q);
		$q = str_replace('o:',"",$q);
		$q = str_replace('v:',"",$q);
		$q = str_replace('x:',"",$q);
		$q = str_replace('<',"&lt;",$q);
		$q = str_replace('>',"&gt;",$q);
		$q = str_replace('"',"&quot;",$q);



		return $q;

	}


	public function unturkce($q) {

		$q = str_replace("Ç","C",$q);
		$q = str_replace("ç","c",$q);
		$q = str_replace("Ü","U",$q);
		$q = str_replace("ü","u",$q);
		$q = str_replace("Ö","O",$q);
		$q = str_replace("ö","o",$q);
		$q = str_replace("Ş","S",$q);
		$q = str_replace("ş","s",$q);
		$q = str_replace("İ","I",$q);
		$q = str_replace("ı","i",$q);
		$q = str_replace("ğ","g",$q);
		$q = str_replace("Ğ","G",$q);




		return $q;

	}


	function sitemap()
	{
		$files = scandir(public_path("sitemaps"));

		unset($files[1]);
		unset($files[0]);


		$html = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;		
		$html .='<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;

		foreach ($files as $key => $row) {
			
			$lastmod =   date("Y-m-d H:i:s", filemtime(public_path("sitemaps/".$row)));
			$datetime = new \DateTime($lastmod);
			$result = $datetime->format('Y-m-d\TH:i:sP');
			$html .='<sitemap>'.PHP_EOL;
			$html .='<loc>https://www.anadolukitap.com/sitemaps/'.$row.'</loc>'.PHP_EOL;
			$html .='<lastmod>'.$result.'</lastmod>'.PHP_EOL;
			$html .='</sitemap>'.PHP_EOL;

		}

		$html .='</sitemapindex>';




		return response($html, 200, [
			'Content-Type' => 'application/xml'
		]);


	}


	function siteMapsCreate()
	{

		makeDir(public_path("sitemaps"));

		$cats = Category::where('is_active',1)->get();


		$html = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;		
		$html .='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;

		foreach ($cats as $key => $row) {

			$lastmod =   $row->updated_at;
			$datetime = new \DateTime($lastmod);
			$result = $datetime->format('Y-m-d\TH:i:sP');
			$html .='<url>'.PHP_EOL;
			$html .='<loc>https://www.anadolukitap.com/kategori/'.$row->categories_url.'</loc>'.PHP_EOL;
			$html .='<lastmod>'.$result.'</lastmod>'.PHP_EOL;
			$html .='<changefreq>weekly</changefreq>'.PHP_EOL;
			$html .='</url>'.PHP_EOL;

		}


		$html .='</urlset>';
		
		$file = public_path("sitemaps/sitemap_categories.xml");
		file_put_contents($file, $html);





		$yayinci = Category::where('is_active',1)->get();


		$html = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;		
		$html .='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;

		foreach ($cats as $key => $row) {

			$lastmod =   $row->updated_at;
			$datetime = new \DateTime($lastmod);
			$result = $datetime->format('Y-m-d\TH:i:sP');
			$html .='<url>'.PHP_EOL;
			$html .='<loc>https://www.anadolukitap.com/kategori/'.$row->categories_url.'</loc>'.PHP_EOL;
			$html .='<lastmod>'.$result.'</lastmod>'.PHP_EOL;
			$html .='<changefreq>weekly</changefreq>'.PHP_EOL;
			$html .='</url>'.PHP_EOL;

		}


		$html .='</urlset>';
		
		$file = public_path("sitemaps/sitemap_categories.xml");
		file_put_contents($file, $html);








		$product_list = Product::with("get_category")->where('is_sale',1)->get()->chunk(10000);

		$i = 0;	foreach ($product_list as   $prood) {

			$html = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;		
			$html .='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;
			
			foreach ($prood as $key => $row) {

				$lastmod =   $row->updated_at;
				$datetime = new \DateTime($lastmod);
				$result = $datetime->format('Y-m-d\TH:i:sP');
				$html .='<url>'.PHP_EOL;
				$html .='<loc>https://www.anadolukitap.com/urun/'.$row->get_category->categories_url.'/'.$row->product_url.'</loc>'.PHP_EOL;
				$html .='<lastmod>'.$result.'</lastmod>'.PHP_EOL;
				$html .='<changefreq>weekly</changefreq>'.PHP_EOL;
				$html .='</url>'.PHP_EOL;

			}


			$html .='</urlset>';

			$file = public_path("sitemaps/sitemap_product_".$i.'.xml');
			file_put_contents($file, $html);

			$i++;
		}






	}


}
