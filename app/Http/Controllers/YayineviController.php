<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Yayinevi;
use App\Models\Product;

class YayineviController extends Controller
{
	function index($harf = "A")
	{

		$list = Yayinevi::where('yayinci_name', 'like', $harf.'%')->get();

		$title ="Yayınevi Yönetimi";

		return view(theme().'.yayinevi.index',compact('list','title'));
	}


	function books($page_url = "")
	{
		$yayinci = Yayinevi::where('yayinci_url', $page_url)->firstOrFail();

		$title = $yayinci->yayinci_name.' Kitapları';

		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}



		$query = Product::query();

		$query->whereHas('extend.getYayinci', function ($q) use($page_url){
			return $q->where('yayinci_url', $page_url);
		});


		$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->orderBy($orderby[0],$orderby[1])->paginate($limit);


		return view(theme().'.galery',compact( 'yayinci','products','title'));
	}

	


}
