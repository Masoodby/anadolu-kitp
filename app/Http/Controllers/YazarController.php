<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Yazar;
use App\Models\Product;

class YazarController extends Controller
{
	function index($harf = "A")
	{ 
		$list = Yazar::where('yazar_name', 'like', $harf.'%')->get();

		$title ="Yazar Yönetimi";

		return view(theme().'.yazar.index',compact('list','title'));
	}


	function books($page_url = "")
	{
		$yazar = Yazar::where('yazar_url', $page_url)->firstOrFail();

		$title = $yazar->yazar_name.' Kitapları';

		$orderby = ["id","desc"];
		if (request()->has('orderby')) {
			$orderby = explode('-',request('orderby'));

			if (!in_array($orderby[0], $this->filter)) {
				$orderby[0] = "id";
			}
			if (!in_array($orderby[1], ['asc','desc'])) {
				$orderby[1] = "desc";
			}
		}


		$limit = 30;
		if (request()->has('limit')) {
			$limit =  intval(request('limit'))>122?120:intval(request('limit'));
			$limit =  $limit<10?10:$limit;

		}



		$query = Product::query();

		$query->whereHas('extend.getYazar', function ($q) use($page_url){
			return $q->where('yazar_url', $page_url);
		});


		$products = $query->with(["get_category",'detail','extend.getYazar','extend.getYayinci'])->where("is_active",1)->orderBy($orderby[0],$orderby[1])->paginate($limit);


		return view(theme().'.galery',compact( 'yazar','products','title'));
	}

}
