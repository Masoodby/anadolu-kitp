<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('yonetim')->check() && Auth::guard('yonetim')->user()->is_admin) {
            return $next($request);
        }else{
            return redirect()->route('crudv4.login');
        }
    }
}
