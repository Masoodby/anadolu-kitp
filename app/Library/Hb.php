<?php
namespace App\Library;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Hb   {

	public $seller_id;
	public $api_user;
	public $api_pass;
	public $cats;

	public $json_file;
	public $token;
	public $test = false;
	public $mpop_endpoint = "";



	public function __construct(array $settings = array()) {

		
		$this->seller_id =  $settings['seller_id'];
		$this->api_user =  $settings['api_user'];
		$this->api_pass =  $settings['api_pass'];

		$endtime = now()->addMinutes(29);

/*
		$this->token = Cache::remember('id_token', $endtime, function ()
		{ 
			$response = $this->setToken();

			return $response;
		});
*/
		$this->mpop_endpoint = $this->test?"https://mpop-sit.hepsiburada.com":"https://mpop.hepsiburada.com";

		$this->token = $this->setToken();

	}


	function getToken()
	{
		return  $this->token;
	}


	
	function setToken()
	{

		$endpoint = $this->mpop_endpoint."/api/authenticate";
		$client = new \GuzzleHttp\Client();
		$response = $client->post($endpoint, [
			\GuzzleHttp\RequestOptions::JSON => ['username' => $this->api_user, 'password' => $this->api_pass,'authenticationType'=>"INTEGRATOR"],
		]);


		return json_decode($response->getBody())->id_token;
	}


	function getCategory()
	{

		for ($i=0; $i <3 ; $i++) { 
			$url = $this->mpop_endpoint."/product/api/categories/get-all-categories?leaf=true&status=ACTIVE&available=true&page=".$i."&size=2000&version=1";

			$response[] = Http::withToken($this->token)->get($url);


		}
		
		$cats = [];



		foreach ($response as $key => $var) {

			$resp = json_decode($var);

			foreach ($resp->data as $key => $ct) {
				$cats[] = $ct;
			}
			
		}
		echo count($cats);
		file_put_contents(public_path('hbcat.json'),  json_encode($cats));
	}


	
	function getCatAttr($id = 0)
	{
		$url = $this->mpop_endpoint.'/product/api/categories/'.$id.'/attributes';


		$response = Http::withToken($this->token)->get($url);

		return $response;
	}

	

	function getsub($dizi,$main)
	{

	}
	

	function sendData($url,$data)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>json_encode($data),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;

	}
	

	function getProducts($page = 0)
	{
		$url = 'https://listing-external.hepsiburada.com/listings/merchantid/'.$this->seller_id;

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);

		return $response;
	}


	function delProduct()
	{

		$urunler = simplexml_load_file('response.xml');
	//	$products = $this->getProducts();

	//	$urunler = simplexml_load_string($products);

		foreach ($urunler->Listings->Listing as $key => $var) {



			$hbsku = $var->HepsiburadaSku;
			$merchantsku = $var->MerchantSku;
			$url = "https://listing-external.hepsiburada.com/listings/merchantid/".$this->seller_id."/sku/".$hbsku."/merchantsku/".$merchantsku;


			$response = Http::withBasicAuth($this->api_user, $this->api_pass)->delete($url)->json();

			//prep($response);

		}
	}


	function addProduct()
	{
		$url = $this->mpop_endpoint."/product/api/products/import";

		$response = Http::withToken($this->token)->get($url);
	}



	function stokUpdate($xml='')
	{


		$url = "https://listing-external.hepsiburada.com/listings/merchantid/".$this->seller_id."/inventory-uploads";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
		$header = array(    
			'Authorization: Basic '. base64_encode($this->api_user.':'.$this->api_pass),
			'Content-Type: application/xml'
		);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$response = curl_exec($curl);

		return simplexml_load_string($response);

	}



	function getOrders()
	{
		$url = "https://oms-external.hepsiburada.com/orders/merchantid/".$this->seller_id;

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);

		return $response;
	}


	function getCargoOrders($saat,$page)
	{
		$url = "https://oms-external.hepsiburada.com/packages/merchantid/".$this->seller_id."?timespan=".$saat."&offset=".$page."&limit=10";
		$url = "https://oms-external.hepsiburada.com/packages/merchantid/".$this->seller_id."?timespan=240";

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);

		return $response;
	}


	function askDurum($kod)
	{
		$url = "https://listing-external.hepsiburada.com/listings/merchantid/".$this->seller_id."/inventory-uploads/id/".$kod;

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);

		return $response;
	}


	function getOrder($id='')
	{

		$url = "https://oms-external.hepsiburada.com/orders/merchantid/".$this->seller_id."/ordernumber/".$id;

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);

		return $response;
	}
	
	function hbItems($id='')
	{
		
		$url = "https://oms-external.hepsiburada.com/lineitems/merchantid/".$this->seller_id."/packageablewith/lineitemid/".$id;


		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);

		$orders =  $response->body();



		return json_decode($orders);
	}


	function paketleme($json)
	{


		$url = "https://oms-external.hepsiburada.com/packages/merchantid/".$this->seller_id;

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->post($url,$json);

		return $response;
	}
	
	
 }// end class

 