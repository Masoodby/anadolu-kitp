<?php
namespace App\Library;

Class N11 {
	protected static $_appKey, $_appSecret, $_parameters, $_sclient;
	public $_debug = false;

	public function __construct(array $attributes = array()) {
		self::$_appKey = $attributes['appKey'];
		self::$_appSecret = $attributes['appSecret'];
		self::$_parameters = ['auth' => ['appKey' => self::$_appKey, 'appSecret' => self::$_appSecret]];
	}

	public function setUrl($url) {
		self::$_sclient = new \SoapClient($url);
	}

	
	public function GetTopLevelCategories() {
		$this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');
		return self::$_sclient->GetTopLevelCategories(self::$_parameters);
	}	

	
	public function GetParentCategory($cid) {
		$this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');
		self::$_parameters['categoryId'] = $cid;
		return self::$_sclient->GetParentCategory(self::$_parameters);
	}	
	
	public function GetSubCategories($cid) {
		$this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');
		self::$_parameters['categoryId'] = $cid;
		return self::$_sclient->GetSubCategories(self::$_parameters);
	}	

	
	public function GetCategoryAttributesId($cid) {
		$this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');
		self::$_parameters['categoryId'] = $cid;
		return self::$_sclient->GetCategoryAttributesId(self::$_parameters);
	}	

	
	public function GetCategoryAttributes($cid = 0,$itemsPerPage=100,$currentPage=0) {

		$this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');

		
		self::$_parameters['categoryId'] = $cid;
		self::$_parameters['pagingData'] = ['itemsPerPage' => $itemsPerPage, 'currentPage' => $currentPage];

		return self::$_sclient->GetCategoryAttributes(self::$_parameters);
	}


	
	public function GetCategoryAttributeValue($c_attb_id = 0,$itemsPerPage=100,$currentPage=0) {

		$this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');

		
		self::$_parameters['categoryProductAttributeId'] = $c_attb_id;
		self::$_parameters['pagingData'] = ['itemsPerPage' => $itemsPerPage, 'currentPage' => $currentPage];

		return self::$_sclient->GetCategoryAttributeValue(self::$_parameters);
	}

	

	public function GetCities() {
		$this->setUrl('https://api.n11.com/ws/CityService.wsdl');
		return self::$_sclient->GetCities(self::$_parameters);
	}

	public function ProductAllStatusCountsRequest() {
		$this->setUrl('https://api.n11.com/ws/CityService.wsdl');
		return self::$_sclient->GetCities(self::$_parameters);
	}

	public function GetProductList($itemsPerPage = 100, $currentPage = 0) {
		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
		self::$_parameters['pagingData'] = ['pageSize' => $itemsPerPage, 'currentPage' => $currentPage];
		return self::$_sclient->GetProductList(self::$_parameters);
	}

	public function GetProductBySellerCode($sellerCode) {
		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
		self::$_parameters['sellerCode'] = $sellerCode;
		return self::$_sclient->GetProductBySellerCode(self::$_parameters);
	}

	public function SaveProduct(array $product = Array()) {
		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
		self::$_parameters['product'] = $product;
		return self::$_sclient->SaveProduct(self::$_parameters);
	}

	public function DeleteProductBySellerCode($sellerCode) {
		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
		self::$_parameters['productSellerCode'] = $sellerCode;
		return self::$_sclient->DeleteProductBySellerCode(self::$_parameters);
	}

	public function DeleteProductById($id) {
		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
		self::$_parameters['productId'] = $id;
		return self::$_sclient->DeleteProductById(self::$_parameters);
	}

	public function OrderList(array $searchData = Array()) {
		$this->setUrl('https://api.n11.com/ws/OrderService.wsdl');
		self::$_parameters['searchData'] = $searchData;
		return self::$_sclient->OrderList(self::$_parameters);
	}

	public function DetailedOrderList(array $searchData = Array()) {
		$this->setUrl('https://api.n11.com/ws/OrderService.wsdl');
		self::$_parameters['searchData'] = $searchData;
		return self::$_sclient->DetailedOrderList(self::$_parameters);
	}

	public function OrderDetail($id = 0) {
		$this->setUrl('https://api.n11.com/ws/OrderService.wsdl');
		self::$_parameters['orderRequest']["id"] = $id;
		return self::$_sclient->OrderDetail(self::$_parameters);
	}

	public function OrderItemAccept($id = 0) {
		$this->setUrl('https://api.n11.com/ws/OrderService.wsdl');
		self::$_parameters['orderItemList']['orderItem']["id"] = $id;
		self::$_parameters['numberOfPackages'] = "";
		return self::$_sclient->OrderItemAccept(self::$_parameters);
	}

	public function UpdateStockByStockSellerCode($id = 0,$adet = 0) {

		$this->setUrl('https://api.n11.com/ws/ProductStockService.wsdl');
		self::$_parameters['stockItems']['stockItem']["sellerStockCode"] = $id;
		self::$_parameters['stockItems']['stockItem']["quantity"] = $adet;
		self::$_parameters['stockItems']['stockItem']["version"] = "";

		return self::$_sclient->UpdateStockByStockSellerCode(self::$_parameters);
	}



	public function UpdateProductBasic($array) {

		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');

		self::$_parameters = array_merge($array,self::$_parameters);

		return self::$_sclient->UpdateProductBasic(self::$_parameters);
	}

	public function UpdateProductPriceBySellerCode($array) {

		$this->setUrl('https://api.n11.com/ws/ProductService.wsdl');

		

		self::$_parameters = array_merge($array,self::$_parameters);


		return self::$_sclient->UpdateProductPriceBySellerCode(self::$_parameters);
	}

	

	public function __destruct() {
		if ($this->_debug) {
			print_r(self::$_parameters);
		}
	}

}