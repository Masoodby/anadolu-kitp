<?php
namespace App\Library;

class NestedSortable
{
	



	function sort($menuler)
	{ 

		$ref   = [];
		$items = [];
		foreach($menuler as $var) {
			$thisRef = &$ref[$var->id];
			$thisRef['parent'] = $var->categories_main;
			$thisRef['label'] = $var->categories_name;
			$thisRef['link'] = $var->categories_url;
			$thisRef['id'] = $var->id;
			if($var->categories_main == 0) {
				$items[$var->id] = &$thisRef;
			} else {
				$ref[$var->categories_main]['child'][$var->id] = &$thisRef;
			}
		}

		return $this->get_menu($items);


	}


	function get_menu($items,$class = 'dd-list') {
		$html = "<ol class=\"".$class."\" id=\"menu-id\">";
		foreach($items as $key=>$value) {
			$html.= '<li class="dd-item dd3-item" data-id="'.$value['id'].'" >
			<div class="dd-handle dd3-handle">Drag</div>
			<div class="dd3-content"><span id="label_show'.$value['id'].'">'.$value['label'].'</span> 
			<span class="span-right">/<span id="link_show'.$value['id'].'">'.$value['link'].'</span> &nbsp;&nbsp; 
			<a class="edit-button" id="'.$value['id'].'" label="'.$value['label'].'" link="'.$value['link'].'" ><i class="fa fa-pencil"></i></a>
			<a class="del-button" id="'.$value['id'].'"><i class="fa fa-trash"></i></a></span> 
			</div>';
			if(array_key_exists('child',$value)) {
				$html .= $this->get_menu($value['child'],'child');
			}
			$html .= "</li>";
		}
		$html .= "</ol>";
		return $html;
	}




	function parseJsonArray($jsonArray,  $parentID = 0) {
		$return = array();
		foreach ($jsonArray as $subArray) {

			if (isset($subArray->children)) {
				$returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
			}

			$return[] = array('id' => $subArray->id, 'parentID' => $parentID);
			$return = array_merge($return, $returnSubSubArray);
		}
		return $return;
	}



}