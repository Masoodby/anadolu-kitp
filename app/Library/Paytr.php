<?php 

namespace App\Library;


class Paytr  
{

	public $merchant_id;
	public $merchant_key;
	public $merchant_salt;
	public $email; 	## Müşterinizin sitenizde kayıtlı veya form vasıtasıyla aldığınız eposta adresi
	public $payment_amount; ## Tahsil edilecek tutar.
	public $merchant_oid; ## Sipariş numarası: Her işlemde benzersiz olmalıdır!! Bu bilgi bildirim sayfanıza yapılacak bildirimde geri gönderilir.
	public $user_name; 	## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız ad ve soyad bilgisi
	public $user_address; 	## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız adres bilgisi
	public $user_phone; ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız telefon bilgisi
	public $merchant_ok_url; ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız telefon bilgisi
	public $merchant_fail_url; ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız telefon bilgisi
	public $user_basket; ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız telefon bilgisi
	public $user_ip;  
	public $timeout_limit = "30";  
	public $debug_on = 0;  
	public $test_mode = 0;  
	public $no_installment = 0;  
	public $max_installment = 0;  
	public $currency =  "TL"; 
	
	function form()
	{  
		$hash_str = $this->merchant_id .$this->user_ip .$this->merchant_oid .$this->email .$this->payment_amount .$this->user_basket.$this->no_installment.$this->max_installment.$this->currency.$this->test_mode;
		$paytr_token=base64_encode(hash_hmac('sha256',$hash_str.$this->merchant_salt,$this->merchant_key,true));
		$post_vals=array(
			'merchant_id'=>$this->merchant_id,
			'user_ip'=>$this->user_ip,
			'merchant_oid'=>$this->merchant_oid,
			'email'=>$this->email,
			'payment_amount'=>$this->payment_amount,
			'paytr_token'=>$paytr_token,
			'user_basket'=>$this->user_basket,
			'debug_on'=>$this->debug_on,
			'no_installment'=>$this->no_installment,
			'max_installment'=>$this->max_installment,
			'user_name'=>$this->user_name,
			'user_address'=>$this->user_address,
			'user_phone'=>$this->user_phone,
			'merchant_ok_url'=>$this->merchant_ok_url,
			'merchant_fail_url'=>$this->merchant_fail_url,
			'timeout_limit'=>$this->timeout_limit,
			'currency'=>$this->currency,
			'test_mode'=>$this->test_mode
		);

		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.paytr.com/odeme/api/get-token");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1) ;
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);

	 // XXX: DİKKAT: lokal makinanızda "SSL certificate problem: unable to get local issuer certificate" uyarısı alırsanız eğer
     // aşağıdaki kodu açıp deneyebilirsiniz. ANCAK, güvenlik nedeniyle sunucunuzda (gerçek ortamınızda) bu kodun kapalı kalması çok önemlidir!
     // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$result = @curl_exec($ch);

		if(curl_errno($ch))
			die("PAYTR IFRAME connection error. err:".curl_error($ch));

		curl_close($ch);

		$result=json_decode($result,1);

		if($result['status']=='success')
			$token=$result['token'];
		else
			die("PAYTR IFRAME failed. reason:".$result['reason']);


		return $token;
	}
}