<?php
namespace App\Library;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Tryol   {

	public $seller_id;
	public $api_user;
	public $api_pass;
	public $cats;

	public $json_file;

	public function __construct(array $settings = array()) {

		
		$this->seller_id =  $settings['seller_id'];
		$this->api_user =  $settings['api_user'];
		$this->api_pass =  $settings['api_pass'];

		$endtime = now()->addMinutes(300);

		$this->json_file = Cache::remember('json_file', $endtime, function ()
		{

			$response = file_get_contents("https://api.trendyol.com/sapigw/product-categories");

			return $response;
		});

		//prep($this->json_file);
		

	}
	


	function jsonfile()
	{
		return $this->json_file;
	}


	
	function getKat()
	{

		$json =  $this->json_file;


		
		$cats = json_decode($json,TRUE);

		
		
		foreach($cats["categories"] as $ct){
			$ct['parentId'] = 0;
			
			$sub = $ct["subCategories"];
			
			unset($ct["subCategories"]);
			
			$ct["sub"] = count($sub) > 0?"true":"false";
			
			
			$this->cats[0][] = $ct;

			if(count($sub) > 0){

				$id = $ct['id'];
				
				$this->getsub($sub,$id);


			}
		}
		
	}

	

	function getsub($dizi,$main)
	{
		foreach ($dizi as $key => $item) {
			
			
			$sub = $item["subCategories"];
			
			unset($item["subCategories"]);
			
			$item["sub"] = count($sub) > 0?"true":"false";
			
			$this->cats[$main][] = $item;


			if(count($sub) > 0){

				$id = $item['id'];

				$this->getsub($sub,$id);


			}


		}
	}
	
	
	function getCatList($main = 0){
		
		$this->getKat();
		
		if (isset($this->cats[$main])) {
			return $this->cats[$main];
		}else{
			return false;
		}
		

	}
	

	function getBrands($name){
		
		
		$name = urlencode($name);


		$response = Http::get("https://api.trendyol.com/sapigw//brands/by-name?name=".$name);

		return json_decode($response);
		


	}


	function catDetay($id)
	{
		$json = Http::get("https://api.trendyol.com/sapigw/product-categories/".$id."/attributes");

		
		return json_decode($json,TRUE);
	}
	
	public static function cargo_list($id=0)
	{
		


		$json = Http::get("https://api.trendyol.com/sapigw/shipment-providers");
		$ret =[];
		foreach (json_decode($json) as $key => $value) {
			$ret[$value->id] = $value->name;
		}

		if ($id==0) {
			return $ret;
		}else{
			return $ret[$id];
		}
		
		
	}


	public function filterProducts($page=0,$approved = 'true',$size = 50)
	{

		$query = array(
			'approved'      => $approved,
			'barcode'       => '',
			'startDate'     => time()-60*60*24*30,
			'endDate'       => time(),
			'page'          => $page,
		//	'dateQueryType' => array('required' => array('CREATED_DATE' , 'LAST_MODIFIED_DATE')),
			'size'          => $size
		);



		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->seller_id."/products?approved=".$approved."&size=".$size."&page=".$page;


		return	Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);
	}

	public function allProduct()
	{




		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->seller_id."/products?approved=true";


		return	Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);
	}

	public function getProduct($isbn=0)
	{

		$query = array(
			'approved'      => TRUE,
			'barcode'       => $isbn,
			'startDate'     => time()-60*60*24*30,
			'endDate'       => time(),
			'page'          => 0,
		//	'dateQueryType' => array('required' => array('CREATED_DATE' , 'LAST_MODIFIED_DATE')),
			'size'          => 1
		);



		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->seller_id."/products";


		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url, $query);

		return json_decode($response);
	}

	public function get_orders($query = [])
	{

		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->seller_id."/orders";

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url, $query);

		return json_decode($response);
	}


	public function get_order($query = [])
	{

		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->seller_id."/orders";

		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url, $query);

		return json_decode($response);
	}


	function sendData($url,$data = "",$metod = "GET")
	{ 

		if ($metod == "GET") {
			$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url,$data);
		}elseif ($metod == "PUT") {
			$response = Http::withBasicAuth($this->api_user, $this->api_pass)->put($url, $data);
		}else{
			$response = Http::withBasicAuth($this->api_user, $this->api_pass)->post($url, $data);
		}
		

		return json_decode($response);
	}


	function picking($url,$data )
	{ 

		return	Http:: withBasicAuth($this->api_user, $this->api_pass)->put($url, $data);


	}



	function batchRequests($id)
	{
		
		$url = "https://api.trendyol.com/sapigw/suppliers/".$this->seller_id."/products/batch-requests/".$id;
		
		$response = Http::withBasicAuth($this->api_user, $this->api_pass)->get($url);                                                                                                              

		return json_decode($response);
	}

	
	
	
 }// end class

 