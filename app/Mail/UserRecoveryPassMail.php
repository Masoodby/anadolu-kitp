<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class UserRecoveryPassMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $pass;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$pass)
    {
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this

        ->subject(  'Anadolukitap - Şifre Kurtarma')
        ->view('mails.user_recovery');
    }
}
