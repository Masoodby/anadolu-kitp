<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'user_id',
        'image',
        'link',
        'position',
        'status',
        'title',
        'text',
        'button'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
