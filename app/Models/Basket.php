<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Basket extends Model
{
	use SoftDeletes;

	protected $table = "baskets";

	protected $guarded = [];



	public function getOrder()
	{
		return $this->hasOne('App\Models\Order');
	}

	function basket_products()
	{
		return $this->hasMany('App\Models\BasketProduct');
	}
	function basket_combos()
	{
		return $this->hasMany('App\Models\BasketCombo');
	}

	public static function active_basket_id()
	{
		$active_basket = DB::table('baskets as b')
		->leftJoin('orders as o', 'o.basket_id', '=', 'b.id')
		->where('b.user_id', auth()->id())
		->whereRaw('o.id is null')
		->orderByDesc('b.created_at')
		->select('b.id')
		->first();

		if (!is_null($active_basket))
			return $active_basket->id;
	}

	public static function active_cart_id()
	{
		$active_basket = DB::table('baskets as b')
		->leftJoin('orders as o', 'o.basket_id', '=', 'b.id')
		->where('b.user_id', auth()->id())
		->whereRaw('o.id is null')
		->orderByDesc('b.created_at')
		->select('b.id')
		->first();

		if (!is_null($active_basket))
			return $active_basket->id;
	}

	function basket_product_count()
	{
		return DB::table('basket_products')->where('basket_id', $this->id)->sum('qty');
	}

	function getUser()
	{
		return $this->belongsTo('App\Models\User');
	}
}
