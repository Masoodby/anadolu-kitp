<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BasketCombo extends Model
{
	use SoftDeletes;

	protected $table = "basket_combos";

	protected $guarded = [];




	function combo()
	{
		return $this->belongsTo('App\Models\Combo');
	}
}
