<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nestable\NestableTrait;


class Category extends Model
{
	use NestableTrait;
	use SoftDeletes;
    //
	protected $table = "categories";
	public $tree;
	protected $parent = 'categories_main';
//	protected $fillable = ['categories_name','categories_url'];
	protected $guarded = [];



	function getProduct()
	{
		return $this->belongsToMany('App\Models\Product','catgories_join');
	}

	function mainCategory()
	{
		return $this->belongsTo('App\Models\Category','categories_main')->withDefault([
			'categories_name'=>'Ana Kategori'
		]);
	}


	

}
