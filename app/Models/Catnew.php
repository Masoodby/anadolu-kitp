<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nestable\NestableTrait;


class Catnew extends Model
{
	use NestableTrait;
	use SoftDeletes;
    //
	protected $table = "categories_new";
	public $tree;
	protected $parent = 'categories_main';
//	protected $fillable = ['categories_name','categories_url'];
	protected $guarded = [];




	function oldCat()
	{
		return $this->hasOne('App\Models\Category','new_id','id');
	}


	function mainCategory()
	{
		return $this->belongsTo('App\Models\Catnew','categories_main')->withDefault([
			'categories_name'=>'Ana Kategori'
		]);
	}

	

}
