<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Combo extends Model
{
    protected $fillable=[
        'combo_name',
        'combo_price',
        'combo_image',
        'is_active',
        'combo_desc',
        'combo_url'
    ];
    public function products(){
        return $this->belongsToMany(Product::class,'combo_products');
    }
}
