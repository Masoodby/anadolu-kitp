<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Depo extends Model
{
	protected $table = "depo";
	
	protected $guarded = [];


	function getRoom()
	{
		return $this->hasMany('App\Models\Oda','depo_id');
	}

}
