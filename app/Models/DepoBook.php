<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepoBook extends Model
{
	protected $table = "depo_book";
	
	protected $guarded = [];



	function getBook()
	{
		
		return $this->belongsTo('App\Models\Product','book_id','id');
	}

	function getRaf()
	{
		
		return $this->belongsTo('App\Models\Raf','raf_id');
	}
}
