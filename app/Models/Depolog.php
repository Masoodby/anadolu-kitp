<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Depolog extends Model
{
	protected $table = "depo_log";
	
	protected $guarded = [];



	function getRaf()
	{
		
		return $this->belongsTo('App\Models\Raf','raf_id');
	}

	function getBook()
	{
		
		return $this->belongsTo('App\Models\Product','isbn','product_stock_code');
	}
}
