<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DnrYayinci extends Model
{
	protected $table = "dnr_yayinci";

	protected $guarded = [];
}
