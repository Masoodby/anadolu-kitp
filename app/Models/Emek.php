<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emek extends Model
{


	public static function catIslem($data,$main = 0){

		$id = 0;


		foreach($data as $key => $cat){

			$addCat = [
				"categories_api_id"=>$cat->ca1,
				"categories_main"=>$main,
				"categories_name"=>$cat->ca2,
				"categories_title"=>$cat->ca2,
				"categories_desc"=>$cat->ca2,
				"categories_keyw"=>$cat->ca2,
				"categories_url"=>str_slug($cat->ca2),
			];

			$main = Emek::addCat($addCat);




			if(count((array)$cat->ca3)){

				if($key == 0){
					$id =  Emek::subCatIslem($cat->ca3,$main);
				}else{
					Emek::subCatIslem($cat->ca3,$main);
				}


			}else{

				if($key == 0){
					$id = $main;
				}
			}
			$main = 0;
		}

		return $id;

	}


	public static function subCatIslem($cat,$main = 0){

		$id = 0;

		$addCat = [
			"categories_api_id"=>$cat->ca1,
			"categories_main"=>$main,
			"categories_name"=>$cat->ca2,
			"categories_title"=>$cat->ca2,
			"categories_desc"=>$cat->ca2,
			"categories_keyw"=>$cat->ca2,
			"categories_url"=>str_slug($cat->ca2),
		];

		$main = Emek::addCat($addCat);

		if( count((array)$cat->ca3)){

			$id =   Emek::subCatIslem($cat->ca3,$main);

		}else{
			$id = $main;
		}

		return $id;
	}


	public static function addCat($data)
	{
		$varmi = DB::table('categories')->where('categories_api_id',$data['categories_api_id']);

		if ($varmi->count()>0) {
			return $varmi->first()->id;
		}else{
			return DB::table('categories')->insertGetId($data);

		}
	}




	public static function product2Array($item){

		return [
			'product_name' => $item->p3,
			'product_sub_name' => $item->p4,
			'api_id' => $item->p1,
			'api_web_id' => $item->p2,
			'product_url' => str_slug($item->p3),
			'product_title' => str_replace('  '," ",$item->p3.' '.$item->p4." ".$item->p15.' '.$item->p24[0]->a2),
			'product_desc' => str_replace('  '," ",$item->p3.' '.$item->p4." ".$item->p15.' '.$item->p24[0]->a2),
			'product_keyw' => str_replace('  '," ",$item->p3.' '.$item->p4." ".$item->p15.' '.$item->p24[0]->a2),
			'product_stock_code' => $item->p7,
			'product_stok' => 0,
			'product_estok' => $item->p18==0?0:100,
			'product_content' => $item->p6,
			'is_active' => $item->p18,
			'is_sale' => $item->p18,

		];
	}

	public static function bookData($item){

		return [
			'yayin-tarihi' => ["attr"=>"Yayın Tarihi","value"=>$item->pb1.'-'.$item->pb2],
			'isbn' => ["attr"=>"ISBN","value"=>$item->p7],
			'baski-sayisi' => ["attr"=>"Baskı Sayısı","value"=>$item->pb3],
			'dil' => ["attr"=>"Dil","value"=>$item->pb13[0]->la2],
			'sayfa-sayisi' => ["attr"=>"Sayfa Sayısı","value"=>$item->pb4],
			'cilt-tipi' => ["attr"=>"Cilt Tipi","value"=>$item->pb8],
			'kagit-cinsi' => ["attr"=>"Kağıt Cinsi","value"=>$item->pb9],
			'boyut' => ["attr"=>"Boyut","value"=>$item->pb6.'cm x'.$item->pb7."cm"],
			'gram' => ["attr"=>"Ağırlık","value"=>$item->pb5."gr"],
			"cevirmen"=>["attr"=>"Çevirmen","value"=>$item->p25]

		];
	}


	public static function addYazar($name,$id)
	{


		$varmi =  DB::table('yazar')->where('yazar_api_id',trim($id));

		if ($varmi->count()>0) {
			return $varmi->first()->id;
		}else{

			return DB::table('yazar')->insertGetId(['yazar_name'=>$name,'yazar_url'=>str_slug($name),'yazar_api_id'=>$id]);

		}
	}



	public static function addYayinci($name,$id)
	{

		$varmi =  DB::table('yayinevi')->where('yayinevi_api_id',$id);
		
		if ($varmi->count()>0) {
			return $varmi->first()->id;
		}else{

			return DB::table('yayinevi')->insertGetId(['yayinci_name'=>$name,'yayinci_url'=>str_slug($name),'yayinevi_api_id'=>$id]);

		}

	}


	public static function addBook($params,$extend,$details)
	{


		$varmi =  DB::table('products')->where('api_id',$params['api_id']);

		if ($varmi->count()>0) {

			$urun =  $varmi->first();
			$id =  $urun->id;
            DB::table('products')->where("id",$id)->update($params);
			DB::table('product_details')->where("product_id",$id)->update(
			    [
			        'product_old_price'=> $details['product_old_price']
                ]
            );
			return $id;

		}else{

			$id =  DB::table('products')->insertGetId($params);
			

			$details['product_id'] = $id;
			$extend['product_id'] = $id;

			DB::table('product_details')->insert($details);
			DB::table('product_extend')->insert($extend);

			return $id;
		}

	}





}
