<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketBrandMatch extends Model
{
	protected $table = "market_brand_match";

	protected $guarded = [];

	function extend()
	{
		return $this->hasOne('App\Models\Yayinevi')->withDefault();
	}
}
