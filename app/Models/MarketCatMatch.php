<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketCatMatch extends Model
{

	protected $table = "market_cat_match";

	protected $guarded = [];

	function extend()
	{
		return $this->hasOne('App\Models\Category')->withDefault();
	}

}
