<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketWriterMatch extends Model
{

	protected $table = "market_writer_match";

	protected $guarded = [];

	function extend()
	{
		return $this->hasOne('App\Models\Yazar')->withDefault();
	}

}
