<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Oda extends Model
{
	protected $table = "oda";
	
	protected $guarded = [];


	function getRaf()
	{
		return $this->hasMany('App\Models\Raf','oda_id');
	}
}
