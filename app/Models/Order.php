<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
	use SoftDeletes;

	protected $table ='orders';
	protected $guarded = [];

	

	public function baskets()
	{
		return $this->belongsTo('App\Models\Basket',"basket_id",'id');
	}


	public function address()
	{
		return $this->belongsTo('App\Models\UserAdres','order_adres_id','id');
	}


	public function fatadres()
	{
		return $this->belongsTo('App\Models\UserAdres','order_fat_adres_id','id');
	}


	public function getUser()
	{
		return $this->belongsTo('App\Models\User','user_id','id');
	}



	public function getFullNameAttribute()
	{
		return "{$this->name} {$this->sname}";
	}
}
