<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
	use SoftDeletes;

	protected $table = "products";

	protected $guarded = [];



	function categories()
	{
		return $this->belongsToMany('App\Models\Category','catgories_join');
	}

	function detail()
	{
		return $this->hasOne('App\Models\ProductDetail')->withDefault();
	}


	function extend()
	{
		return $this->hasOne('App\Models\ProductExtend')->withDefault();
	}

	function market()
	{
		return $this->hasMany('App\Models\ProductMarketPlace');
	}



	function get_category()
	{
		return $this->belongsTo('App\Models\Category','product_cat')->withDefault([
			'categories_name'=>'Ana kategori'
		]);
	}
    public function tsliders(){
        return $this->hasMany(TimeSlider::class);
    }

}
