<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductExtend extends Model
{
	protected $table = 'product_extend';
	protected $guarded = [];
	public $timestamps= false;

	function product()
	{
		return $this->hasOne('App\Models\Product','id');
	}

	function getYazar()
	{ 
		return $this->hasOne('App\Models\Yazar','id','yazar_id');
	}

	function getYayinci()
	{ 
		return $this->hasOne('App\Models\Yayinevi','id','yayinci_id');
	}


}
