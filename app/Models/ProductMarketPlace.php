<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductMarketPlace extends Model
{
	
	protected $table = 'product_market_place';
	protected $guarded = [];


	function product()
	{
		return $this->belongsTo('App\Models\Product','product_id','id');
	}

}
