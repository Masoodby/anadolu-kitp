<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Raf extends Model
{
	protected $table = "raf";
	
	protected $guarded = [];



	function getBook()
	{
		return $this->hasMany('App\Models\DepoBook','raf_id');
	}

	function getOda()
	{
		return $this->belongsTo('App\Models\Oda','oda_id');
	}
}
