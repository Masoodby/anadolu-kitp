<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetProduct extends Model
{
	protected $table = 'setproduct';
	protected $guarded = [];


	function sub()
	{
		return $this->hasMany('App\Models\SetSubProduct');
	}
}
