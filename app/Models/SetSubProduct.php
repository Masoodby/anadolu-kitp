<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetSubProduct extends Model
{
	protected $table = 'setsub_product';
	protected $guarded = [];


	function getSet()
	{
		return $this->hasMany('App\Models\SetProduct');
	}
}
