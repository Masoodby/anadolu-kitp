<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $fillable = ['settings_name', 'settings_val', 'settings_group',"settings_type","settings_key"];
}
