<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubMessage extends Model
{
	use SoftDeletes;
	protected $table = "submessage";
	
	protected $guarded = [];


	public function getSupport()
	{
		return $this->hasOne('App\Models\Support');
	}
}
