<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Support extends Model
{
	use SoftDeletes;
	protected $table = "supports";
	
	protected $guarded = [];



	public function getOrder()
	{
		return $this->hasOne('App\Models\Order');
	}

	public function getUser()
	{
		return $this->belongsTo('App\Models\User','user_id','id');
	}


	function subMessages()
	{
		return $this->hasMany('App\Models\SubMessage');
	}


}
