<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class TimeSlider extends Model
{
    protected $fillable = [
        'user_id',
        'product_id',
        'discount',
        'end_time',
        'status',
        'title',
        'url',
        'button'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
