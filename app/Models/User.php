<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
use LamaLama\Wishlist\HasWishlists;


class User extends Authenticatable
{
  use SoftDeletes;
  use HasWishlists;

  protected $table = 'users';

  protected $fillable = ['name','sname', 'email', 'pass', 'activation_key', 'is_active', 'is_admin' ];

  protected $hidden = ['pass', 'activation_key'];



  function getAuthPassword()
  {
    return $this->pass;
  }

  public function getDetail()
  {
    return $this->hasOne('App\Models\UserDetail')->withDefault();
  }

  public function getAdres()
  {
    return $this->hasOne('App\Models\UserAdres')->withDefault();
  }

  public function getFullNameAttribute()
  {
   return ucwords_tr($this->name) . ' ' . ucwords_tr($this->sname);
 }

}
