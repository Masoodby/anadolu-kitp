<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAdres extends Model
{
	protected $table = 'user_adress';
	public $timestamps =false;
	protected $guarded = []; //tüm kolonlar eklenebilir


	public function getUser()
	{
		return $this->belongsTo('App\Models\User','user_id','id');
	}


	public function getFullNameAttribute()
	{
		return ucwords_tr($this->adres_name) . ' ' . ucwords_tr($this->adres_sname);
	}

}
