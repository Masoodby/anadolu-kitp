<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Yayinevi extends Model
{
	use SoftDeletes;

	protected $table = "yayinevi";
	
	protected $guarded = [];



	function getProduct()
	{
		return $this->hasMany('App\Models\ProductExtend','yayinci_id');
	}

}
