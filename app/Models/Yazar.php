<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Yazar extends Model
{ 
	use SoftDeletes;
	
	protected $table = "yazar";
	
	protected $guarded = [];

	function getProduct()
	{
		return $this->hasMany('App\Models\ProductExtend','yazar_id');
	}


}
