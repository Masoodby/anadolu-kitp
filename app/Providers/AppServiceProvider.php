<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\Category;
use App\Models\Setting;

use Illuminate\Support\Facades\Cache;




class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Schema::defaulfStringLength(191);
    /* tüm viewlerde kullanılması için
     $bitisZamani = now()->addMinutes(10);
       $istatistikler = Cache::remember('istatistikler', $bitisZamani, function ()
       {
        return [
            'bekleyen_siparis' => Siparis::where('durum','Siparişiniz alındı.')->count()
        ];
    });
       View::share('istatistikler', $istatistikler);
    */

       


     }
   }
