<?php

return [
	
    /*
     * The Facebook Pixel id, should be a code that looks something like "XXXXXXXXXXXXXXXX".
     */
    'facebook_pixel_id' => env('FACEBOOK_PIXEL_ID', '2738843083111816'),
    
    /*
     * Enable or disable script rendering. Useful for local development.
     */
    'enabled'           => true,
];