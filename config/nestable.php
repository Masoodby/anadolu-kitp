<?php

return [
    'parent'=> 'categories_main',
    'primary_key' => 'id',
    'generate_url'   => true,
    'childNode' => 'child',
    'body' => [
        'id',
        'categories_name',
        'categories_url',
        'is_active'
    ],
    'html' => [
        'label' => 'categories_name',
        'href'  => 'categories_url'
    ],
    'dropdown' => [
        'prefix' => '-',
        'label' => 'categories_name',
        'value' => 'id'
    ]
];
