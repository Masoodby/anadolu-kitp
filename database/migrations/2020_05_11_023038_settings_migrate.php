<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SettingsMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {




      Schema::create('settings', function (Blueprint $table) {
        $table->increments('id');
        $table->string('settings_name',255);
        $table->string('settings_val',1000)->nullable();
        $table->integer('settings_group')->unsigned();
        $table->string('settings_type',50);
        $table->string('settings_key',100);
        $table->timestamps();

        $table->foreign('settings_group')->references('id')->on('setcats')->onDelete('cascade');
        
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
  }
