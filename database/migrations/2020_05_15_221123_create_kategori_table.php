<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            //$table->id();
            $table->increments('id');
            $table->integer('categories_main')->nullable();
            $table->integer('categories_api_id')->nullable();
            $table->string('categories_name',255);
            $table->string('categories_title',255)->nullable();
            $table->string('categories_desc',500)->nullable();
            $table->string('categories_keyw',500)->nullable();
            $table->string('categories_url',255);
            $table->text('categories_content')->nullable();
            $table->string('categories_image',255)->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_top')->default(1);
            $table->integer('rank')->default(0);
            $table->timestamps();
          //  $table->timestamp('olusturulma_tarihi')->default(DB::raw('CURRENT_TIMESTAMP'));
           // $table->timestamp('guncellenme_tarihi')->default(DB::raw('CURRENT_TIMESTAMP on UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->index("categories_url");
          //  $table->timestamp('silinme_tarihi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
