<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrunTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            //$table->id();
            $table->increments('id');
            $table->integer('api_id')->nullable();
            $table->string('api_web_id',100)->nullable();
            $table->string('product_url',160);
            $table->string('product_stock_code',160);
            $table->string('product_group_code')->nullable()->default('');

            $table->string('product_name',255);
            $table->string('product_sub_name',255)->nullable()->default('');

            $table->string('product_title',260)->nullable()->default('');
            $table->string('product_desc',500)->nullable()->default('');
            $table->string('product_keyw',600)->nullable()->default('');
            

            $table->integer('product_cat')->unsigned();
            $table->integer('product_stok')->nullable()->default(0);;
            $table->decimal('product_price',10,2);




            $table->string('product_jenerik',500)->nullable()->default('');
            $table->string('product_tags',600)->nullable()->default('');

            $table->integer('product_visit_count')->nullable();
            $table->integer('product_rate')->nullable();


            $table->text('product_content')->nullable();


            $table->boolean('is_active')->default(1);


            $table->timestamps();
           // $table->timestamp('olusturulma_tarihi')->default(DB::raw('CURRENT_TIMESTAMP'));
           // $table->timestamp('guncellenme_tarihi')->default(DB::raw('CURRENT_TIMESTAMP on UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
           //  $table->timestamp('silinme_tarihi')->nullable();



            $table->foreign('product_cat')->references('id')->on('categories')->onDelete('cascade');




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
