<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrunDetayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
           // $table->id();
            $table->increments('id');
            $table->integer('product_id')->unsigned()->unique();

            $table->boolean('product_konum_home')->nullable()->default(0);
            $table->boolean('product_konum_new')->nullable()->default(0);
            $table->boolean('product_konum_today')->nullable()->default(0);
            $table->boolean('product_konum_cat')->nullable()->default(0);
            $table->boolean('product_konum_cok')->nullable()->default(0);
            $table->boolean('product_konum_ind')->nullable()->default(0);

            
            $table->string('product_image',1200)->nullable();

            $table->decimal('product_old_price',10,2)->nullable();
            $table->decimal('product_get_price',10,2)->nullable();

            //$table->unique('urun_id');

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
