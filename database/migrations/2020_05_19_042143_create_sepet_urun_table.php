<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSepetUrunTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::dropIfExists('basket_products');
       Schema::create('basket_products', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('basket_id')->unsigned();
        $table->integer('product_id')->unsigned ();
        $table->integer('qty');
        $table->decimal('total_price',10,2);
        $table->boolean('state')->default(0);
        $table->text('cartjson')->nullable();

        $table->timestamps();
        
        $table->softDeletes();
        

        $table->foreign('basket_id')->references('id')->on('baskets')->onDelete('cascade');
        $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket_products');
    }
}
