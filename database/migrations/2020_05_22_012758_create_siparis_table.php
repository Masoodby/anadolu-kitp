<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiparisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('orders');
      Schema::create('orders', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('basket_id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->decimal('order_total',10,4);
        $table->decimal('cargo_cost',10,4);
        $table->decimal('grand_total',10,4);
        $table->tinyInteger('order_state')->default(0);
        $table->tinyInteger('payment_type')->default(0);

        $table->string('order_name',50)->nullable();
        $table->string('order_sname',50)->nullable();
        $table->string('order_adres',255)->nullable();
        $table->string('order_il',50)->nullable();
        $table->string('order_ilce',50)->nullable();
        $table->string('order_ip',20)->nullable();

        $table->integer('order_adres_id')->default(0);
        $table->integer('order_fat_adres_id')->default(0);

        $table->string('order_nots',500)->nullable();
        $table->string('order_admin_nots',500)->nullable();


        $table->timestamps(); 
        $table->softDeletes();

        $table->unique('basket_id');
        $table->foreign('basket_id')->references('id')->on('baskets')->onDelete('cascade');
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('orders');
    }
  }
