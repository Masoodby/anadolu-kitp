<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SliderMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slider_name',255)->nullable();
            $table->string('slider_desc',1000)->nullable();
            $table->string('slider_image',100)->nullable();
            $table->string('slider_link',255)->nullable();
            $table->integer('rank')->nullable()->default(0);
            $table->boolean('is_active')->default(1);
            $table->timestamps();
            
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('slider');
 }
}
