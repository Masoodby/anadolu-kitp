<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class YazarMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('yazar', function (Blueprint $table) {
        $table->increments('id');
        $table->string('yazar_name',255)->nullable();        
        $table->string('yazar_url',255)->nullable();        
        $table->integer('yazar_api_id')->default(0);      
        $table->boolean('is_active')->default(1);
        $table->boolean('is_home')->default(0);
        $table->timestamps();

        $table->softDeletes(); 
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('yazar');
   }
}
