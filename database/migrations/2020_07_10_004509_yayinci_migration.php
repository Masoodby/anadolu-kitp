<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class YayinciMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('yayinevi', function (Blueprint $table) {
        $table->increments('id');
        $table->string('yayinci_name',255)->nullable();        
        $table->string('yayinci_url',255)->nullable();     
        $table->string('yayinci_logo',255)->nullable();     
        $table->integer('yayinevi_api_id')->default(0);    
        $table->boolean('is_active')->default(1);
        $table->boolean('is_home')->default(0);
        $table->float('yayinci_oran')->default(0);
        $table->float('yayinci_alis_oran')->default(0);
        $table->integer('yayinci_temin')->default(0);
        $table->timestamps();

        $table->softDeletes(); 
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yayinevi');
    }
}
