<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductExtentMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_extend', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->unique();      
            $table->integer('yazar_id')->unsigned();      
            $table->integer('yayinci_id')->unsigned();      
            $table->text('extend_json')->nullable();      


            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('yazar_id')->references('id')->on('yazar')->onDelete('cascade');
            $table->foreign('yayinci_id')->references('id')->on('yayinevi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('product_extend');
   }
}
