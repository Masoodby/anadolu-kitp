<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketCatMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_cat_match', function (Blueprint $table) {
            $table->id();
            $table->integer('local_id')->unsigned();
            
            $table->integer('remote_id');
            $table->integer('pazaryeri_id');
            $table->string('local_name',100);
            $table->string('remote_name',100);
            $table->timestamps();


            $table->foreign('local_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_cat_match');
    }
}
