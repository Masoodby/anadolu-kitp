<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('oda', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('depo_id')->unsigned()->index();
            $table->string('oda_name',100); 
            $table->integer('oda_yatay'); 
            $table->integer('oda_dikey'); 
            $table->integer('oda_start'); 
            $table->integer('rank')->nullable()->default(0);      
            
            $table->timestamps();

            $table->foreign('depo_id')->references('id')->on('depo')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('oda');
        
    }
}
