<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('raf', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('oda_id')->unsigned()->index();        
            $table->string('raf_name',100);  
            $table->integer('rank')->nullable()->default(0);   
            $table->timestamps();

            $table->foreign('oda_id')->references('id')->on('oda')->onDelete('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     
        Schema::dropIfExists('raf');
        
    }
}
