<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepoLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depo_log', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('isbn')->unsigned()->index();
            $table->bigInteger('raf_id')->unsigned()->index();
            $table->bigInteger('book_id')->unsigned()->index(); 
            $table->integer('adet');               
            $table->timestamps();

            $table->foreign('raf_id')->references('id')->on('raf')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depo_log');
    }
}
