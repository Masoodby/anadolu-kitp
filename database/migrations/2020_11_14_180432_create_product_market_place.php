<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductMarketPlace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_market_place', function (Blueprint $table) {
            $table->id();
            $table->foreignId('market_id')->unsigned(); 
            $table->foreignId('product_id')->unsigned(); 
            $table->timestamps();

            $table->index("market_id");
            $table->index("product_id");

        //    $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('market_id')->references('id')->on('market_places')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_market_place');
    }
}
