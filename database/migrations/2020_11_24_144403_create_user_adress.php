<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAdress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('user_adress');
      Schema::create('user_adress', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id')->unsigned();
        $table->string('adres_title',100)->nullable();
        $table->string('fatura_unvan',255)->nullable();
        $table->string('fatura_vd',100)->nullable();
        $table->string('fatura_vno',100)->nullable();
        $table->string('fatura_tck',11)->nullable();
        $table->boolean('fatura_tur')->default(0);
        $table->boolean('adres_tur')->default(0);
        $table->boolean('efatura')->default(0);
        $table->string('adres_name',50)->nullable();
        $table->string('adres_sname',50)->nullable();
        $table->string('adres_il',50)->nullable();
        $table->string('adres_ilce',50)->nullable();
        $table->string('adres_detay',300)->nullable();
        $table->string('adres_gsm',15)->nullable();
        $table->boolean('adres_current')->default(0);
        $table->string('adres_nots',300)->nullable();
        

        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

    });



  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_adress');
    }
}
