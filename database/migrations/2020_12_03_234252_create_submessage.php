<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submessage', function (Blueprint $table) {
          $table->id();
          $table->biginteger('support_id')->unsigned();   
          $table->string('message',1000)->nullable();
          $table->boolean('is_read')->default(0);            
          $table->boolean('is_admin')->default(0); 
          $table->timestamps();
          $table->softDeletes();
          $table->foreign('support_id')->references('id')->on('supports')->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submessage');
    }
}
