<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_name',255)->nullable();        
            $table->string('page_url',255)->nullable();        

            $table->string('page_title',260)->nullable()->default('NULL');
            $table->string('page_desc',500)->nullable()->default('NULL');
            $table->string('page_keyw',600)->nullable()->default('NULL');
            $table->string('page_img',255)->nullable();  
            $table->text('page_content')->nullable();

            $table->boolean('is_home')->default(1);      
            $table->boolean('is_active')->default(1);

            $table->timestamps();

            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
