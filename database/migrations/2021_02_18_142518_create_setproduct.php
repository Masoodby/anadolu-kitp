<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetproduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setproduct', function (Blueprint $table) {
            $table->id();
            $table->string('set_name',255)->nullable();        
            $table->string('set_url',255)->nullable();        

            $table->string('set_title',260)->nullable();
            $table->string('set_desc',500)->nullable();
            $table->string('set_keyw',600)->nullable();
            $table->string('set_img',255)->nullable();  
            $table->decimal('set_price',10,4);
            $table->text('set_content')->nullable();

            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setproduct');
    }
}
