<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetsubProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setsub_product', function (Blueprint $table) {
            $table->id();
            $table->biginteger('set_id')->unsigned();
            $table->integer('p_id')->unsigned();
            $table->timestamps();


            $table->foreign('set_id')->references('id')->on('setproduct')->onDelete('cascade');
            $table->foreign('p_id')->references('id')->on('products')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setsub_product');
    }
}
