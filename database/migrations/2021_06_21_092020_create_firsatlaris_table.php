<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFirsatlarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firsatlaris', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->timestamps('expire_time');
            $table->integer('price')->nullable();
            $table->integer('discount')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('title')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firsatlaris');
    }
}
