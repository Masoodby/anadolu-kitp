<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      DB::table('depo_book')->truncate();
      
      $json =  file_get_contents(__DIR__."../../json/kitap.json");
      $data = json_decode($json);
      foreach ($data->data as   $item) {


        DB::table('depo_book')->insert([

         'isbn'=> $item->kitap_isbn,
         'raf_id'=> $item->kitap_raf_id,
         'book_id'=> $item->kitap_uid,
         'adet'=> $item->kitap_adet,

       ]);


      }
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
  }
