<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     
     /*
      $this->call(KategoriTableSeeder::class);
      $this->call(YayinciSeed::class);
      $this->call(YazarSeed::class);
      $this->call(UrunTableSeeder::class);
      $this->call(KullaniciTableSeeder::class);
      $this->call(SetcatSeeder::class);
      $this->call(SettingsSeeder::class);
      
      $this->call(PageSeed::class);
      

      $this->call(SliderSeed::class);
      
      
      $this->call(DepooSeeder::class);
      $this->call(OdaSeeder::class);
      $this->call(RafSeeder::class);
      $this->call(BookSeeder::class);

      */

      $this->call(CityCountiesTableSeeder::class);
    }
  }
