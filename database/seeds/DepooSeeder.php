<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DepooSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::statement('SET FOREIGN_KEY_CHECKS=0;');
     DB::table('depo')->truncate();
     $json =  file_get_contents(__DIR__."../../json/depo.json");
     $data = json_decode($json);
     foreach ($data->data as   $item) {


      DB::table('depo')->insert([
       'id'=> $item->depo_id,
       'depo_name'=> $item->depo_name,

     ]);


    }

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
  }
}
