<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;



class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('kategori')->truncate();

      
        $json =  file_get_contents(__DIR__."../../json/categories.json");
        $data = json_decode($json);
        foreach ($data->data as   $item) {


            DB::table('categories')->insert([
                'categories_main'=> $item->categories_main,
                'categories_api_id'=>$item->categories_api_id,
                'categories_name'=>$item->categories_name,
                'categories_title'=>$item->categories_title,
                'categories_desc'=>$item->categories_desc,
                'categories_keyw'=>$item->categories_keyw,
                'categories_url'=>$item->categories_url,

                'is_active'=>$item->categories_durum,
            ]);


        }

    }
}
