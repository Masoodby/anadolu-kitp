<?php

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Database\Seeder;



class KullaniciTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    	User::truncate();
    	UserDetail::truncate();

    	$kullanici_yonetici = User::create(
    		[
    			'name' => 'Muhammed',
                'sname' => 'Hunkar',
                'email' => 'theparaklit@gmail.com',
                'pass' => bcrypt('123123'),
                'is_active' => 1,
                'is_admin' => 1
            ]);

    	$kullanici_yonetici->getDetail()->create([
    		'user_address' => 'Sivas',
    		'user_tel' => '(123) 123 12 33',
    		'user_gsm' => '(111) 122 23 33'
    	]);


        
    	DB::statement('SET FOREIGN_KEY_CHECKS=1;');


    }
}
