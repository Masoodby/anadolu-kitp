<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class OdaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::statement('SET FOREIGN_KEY_CHECKS=0;');

     $json =  file_get_contents(__DIR__."../../json/oda.json");
     DB::table('oda')->truncate();
     $data = json_decode($json);
     foreach ($data->data as   $item) {


      DB::table('oda')->insert([
         'id'=> $item->oda_id,
         'depo_id'=> $item->oda_depo_id,
         'oda_name'=> $item->oda_name,
         'oda_yatay'=> $item->oda_yatay,
         'oda_dikey'=> $item->oda_dikey,
         'oda_start'=> $item->oda_start,
         'rank'=> $item->oda_sira,

     ]);


  }
  DB::statement('SET FOREIGN_KEY_CHECKS=1;');
}
}
