<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json =  file_get_contents(__DIR__."../../json/page.json");
        $data = json_decode($json);
        foreach ($data->data as   $item) {


          DB::table('pages')->insert([

             'page_name'=> $item->page_name,
             'page_url'=>$item->page_url,
             'page_type'=>$item->page_cat,
             'page_title'=>$item->page_title,
             'page_desc'=>$item->page_desc,
             'page_keyw'=>$item->page_keyw,
             'page_content'=>$item->page_content,
             'is_active'=>1,
         ]);


      }

  }
}
