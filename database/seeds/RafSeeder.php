<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class RafSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

     DB::statement('SET FOREIGN_KEY_CHECKS=0;');
     DB::table('raf')->truncate();
     $json =  file_get_contents(__DIR__."../../json/raf.json");
     $data = json_decode($json);
     foreach ($data->data as   $item) {


      DB::table('raf')->insert([
       'id'=> $item->raf_id,
       'oda_id'=> $item->raf_oda_id,
       'raf_name'=> $item->raf_name,
       
     ]);


    }
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
  }
}
