<?php

use Illuminate\Database\Seeder;
use App\Models\Setcat;
class SetcatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


    	$json = '[
    	{"setcat_id":"1","setcat_name":"Genel Ayarlar","setcat_sira":"1","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:49:55"},
    	{"setcat_id":"2","setcat_name":"Resim Boyutları","setcat_sira":"2","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:50:16"},
    	{"setcat_id":"3","setcat_name":"Tema Ayarları","setcat_sira":"3","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:50:24"},
    	{"setcat_id":"4","setcat_name":"Seo Ayarları","setcat_sira":"4","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:50:32"},
    	{"setcat_id":"5","setcat_name":"Yedek Ayarları","setcat_sira":"5","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:50:46"},
    	{"setcat_id":"6","setcat_name":"Google API Ayarları","setcat_sira":"6","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:51:43"},
    	{"setcat_id":"7","setcat_name":"Facebook API Ayarları","setcat_sira":"7","setcat_durum":"1","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 05:51:57"},
    	{"setcat_id":"8","setcat_name":"Sosyal Medya Hesapları","setcat_sira":"8","setcat_durum":"0","setcat_admin_id":"1","setcat_insert_tarih":"2018-02-24 21:36:20"},
    	{"setcat_id":"9","setcat_name":"Tema Parametreleri","setcat_sira":"9","setcat_durum":"0","setcat_admin_id":"1","setcat_insert_tarih":"2019-03-11 13:47:51"},
    	{"setcat_id":"10","setcat_name":"E-ticaret Ayarları","setcat_sira":"11","setcat_durum":"0","setcat_admin_id":"1","setcat_insert_tarih":"2019-07-25 23:06:35"},
    	{"setcat_id":"11","setcat_name":"Kitap Satış Ayarları","setcat_sira":"12","setcat_durum":"0","setcat_admin_id":"1","setcat_insert_tarih":"2019-07-29 16:10:36"}
    ]';


    foreach (json_decode($json) as $key => $item) {
	# code...

      Setcat::create([
          'setcat_name' => $item->setcat_name,
          'setcat_sira' => $item->setcat_sira
      ]);


  }
}


}
