<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;


class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $json =  file_get_contents(__DIR__."../../json/settings.json");
        foreach (json_decode($json) as $key => $item) {
	# code...

            Setting::create([
             'settings_name' => $item->settings_name,
             'settings_val' => $item->settings_val,
             'settings_group' => $item->settings_group,
             'settings_type' => $item->settings_type,
             'settings_key' => $item->settings_key
         ]);


        }


    }
}
