<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class SliderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json =  file_get_contents(__DIR__."../../json/slider.json");

        $data = json_decode($json);
        foreach ($data->data as   $item) {


          DB::table('slider')->insert([
             'slider_name'=> $item->slider_name,
             'slider_desc'=> $item->slider_desc,
             'slider_link'=> $item->slider_link,
             'slider_image'=>"",
             'rank'=>$item->slider_sira,
             'is_active'=>1,
         ]);


      }
  }
}
