<?php


use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\ProductExtend;



class UrunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::statement('SET FOREIGN_KEY_CHECKS=0;');

      Product::truncate();
      ProductDetail::truncate();
      ProductExtend::truncate();


      $json =  file_get_contents(__DIR__."../../json/product.json");
      $data = json_decode($json);

      foreach ($data->data as   $item) {



        $urun =Product::create([
          'product_name'=> $item->product_name,
          'product_sub_name'=> $item->product_sub_name,
          'api_id'=> $item->api_id,
          'api_web_id'=> $item->api_web_id,
          'product_url'=> $item->product_url,
          'product_stock_code'=> $item->product_stock_code,
          'product_group_code'=> $item->product_group_code,
          'product_title'=> $item->product_title,
          'product_desc'=> $item->product_desc,
          'product_keyw'=> $item->product_keyw,
          'product_cat'=> $item->product_cat,
          'product_stok'=> $item->product_stok,
          'product_price'=> $item->product_price,
          'product_jenerik'=> $item->product_jenerik,
          'product_tags'=> $item->product_tags,
          'product_content'=> $item->product_content,
          'is_active'=> $item->product_durum,
          

        ]);

        $detay = $urun->detail()->create([
          'product_old_price'=>$item->product_old_price,
          'product_get_price'=>$item->product_get_price,
          'product_konum_home'=>$item->product_konum_home,
          'product_konum_new'=>$item->product_konum_new,
          'product_konum_today'=>$item->product_konum_today,
          'product_konum_cat'=>$item->product_konum_cat,
          'product_konum_ind'=>$item->product_konum_ind,
          'product_image'=>$item->product_image,


        ]);


        $extend = $urun->extend()->create([
          'yazar_id'=>$item->product_yazar_id,
          'yayinci_id'=>$item->product_yayinevi_id,
          'extend_json'=>$item->book_desc


        ]);
      }

      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
  }