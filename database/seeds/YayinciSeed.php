<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class YayinciSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $json =  file_get_contents(__DIR__."../../json/yayinevi.json");
     $data = json_decode($json);
     foreach ($data->data as   $item) {


      DB::table('yayinevi')->insert([
       'id'=> $item->yayinevi_id,
       'yayinci_name'=>$item->yayinevi_name,
       'yayinci_url'=>$item->yayinevi_url,
       'yayinevi_api_id'=>$item->yayinevi_api_id,
       'yayinci_oran'=>$item->yayinevi_oran,
       'yayinci_alis_oran'=>$item->yayinevi_oran_alis,  
       'is_active'=>$item->yayinevi_durum,
   ]);


  }
}
}
