<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class YazarSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $json =  file_get_contents(__DIR__."../../json/yazar.json");
        $data = json_decode($json);
        foreach ($data->data as   $item) {


          DB::table('yazar')->insert([
             'id'=> $item->yazar_id,
             'yazar_name'=> $item->yazar_name,
             'yazar_url'=>$item->yazar_url,
             'yazar_api_id'=>$item->yazar_api_id,
             'is_active'=>$item->yazar_durum,
         ]);


      }


  }
}
