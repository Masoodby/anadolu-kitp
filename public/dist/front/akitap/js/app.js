 
function sole(title,text) {
  $.toast({
    text : text,
    heading: title,
    position: 'top-right',
    bgColor: "rgba(0,0,0,.8)",
    textColor: '#fff',
})
}

function repUrl(key, value) {
    key = encodeURIComponent(key);
    value = encodeURIComponent(value);

    // kvp looks like ['key1=value1', 'key2=value2', ...]
    var kvp = document.location.search.substr(1).split('&');
    let i=0;

    for(; i<kvp.length; i++){
        if (kvp[i].startsWith(key + '=')) {
            let pair = kvp[i].split('=');
            pair[1] = value;
            kvp[i] = pair.join('=');
            break;
        }
    }

    if(i >= kvp.length){
        kvp[kvp.length] = [key,value].join('=');
    }

    // can return this or...
    let params = kvp.join('&');

    // reload page with new params
    document.location.search = params;
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


function seola(text)
{
    var trMap = {
        'çÇ':'c',
        'ğĞ':'g',
        'şŞ':'s',
        'üÜ':'u',
        'ıİ':'i',
        'öÖ':'o'
    };
    for(var key in trMap) {
        text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
    }
    return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') 
    .replace(/\s/gi, "-") 
    .replace(/[-]+/gi, "-") 
    .toLowerCase();

}



$(".float").on("keyup", function() { 

    var intsayi = $(this).val().replace(/[^.0-9]/gi,"").toLowerCase();  

    $(this).val(intsayi);



}); 


$(".int").on("keyup", function() { 



    var intsayi = $(this).val().replace(/[^0-9]/gi,"").toLowerCase();  



    $(this).val(intsayi);



}); 


function maske(classi,durum) {

    if (durum == "on") {
        $('.'+classi).fadeIn()
    }else{
        $('.'+classi).fadeOut(1000)
    }
}

$(".sbmt_btn").click(function() {

    data_id  = $(this).data('id');
    

    $('#'+data_id+' .req').each(function(index, element) {

        var degert = $(this).val();

        if(degert ==''){

            $(this).css('border-color','red');

            $(this).focus();

            die();
        } else {

            $(this).css('border-color','green');

        }
    });
    
    
    $(this).attr("disabled","disabled");

    $(this).css("opacity","0.6");

    $(this).html("Bekleyiniz");




    setTimeout(function(){ $('#'+data_id).submit(); }, 1000);


});


function loadsidecart() {
    $.ajax({
        type: 'POST',
        url: '/sepet/loadsidecart',
        data: {id: "id"},
        success: function(msg){
            $('#sidecart').html(msg)
        }
    }); 
}


$('.logoutbtn').click(function() {
    $.ajax({
        type: 'POST',
        url: '/panel/logout',
        data: {id: "id"},
        success: function(){
            location.href='/'
        }
    }); 
});




