$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

genx = $('body').width()
if (genx>900) {
	$(".gsm").mask("(599) 999-9999");
}


$('.tipsi').tooltip()


$(".float").on("keyup", function() { 
	var intsayi = $(this).val().replace(/[^.0-9]/gi,"").toLowerCase();  
	$(this).val(intsayi);
}); 


$(".int").on("keyup", function() { 

	var intsayi = $(this).val().replace(/[^0-9]/gi,"").toLowerCase();  
	$(this).val(intsayi);
}); 



function allConfirm(fName,id ,mesaj = "İşlem Onayı. Bu işlem geri alınamaz. Emin misiniz?") {
	
	bootbox.confirm({
		message: mesaj ,
		buttons: {
			confirm: {
				label: 'Evet',
				className: 'btn-success'
			},
			cancel: {
				label: 'Hayır',
				className: 'btn-danger'
			}
		},
		callback: function (result) {


			if (result) {
				eval(fName)(id);
			}else{
				sole('İptal Edildi',"İşlem İşlemi İptal Edildi")
			}

		}
	});

}


function sole(title,text) {
	$.toast({
		text : text,
		heading: title,
		position: 'top-right',
		bgColor: "rgba(0,0,0,.8)",
		textColor: '#fff',
	})
}


$(".submit_btn").click(function() {
	
	data_id  = $(this).data('id');


	$('#'+data_id+' .req').each(function(index, element) {

		var degert = $(this).val();

		if(degert == '' || degert == -1 ){

			$(this).css('border-color','red');

			$(this).focus();

			die();
		} else {

			$(this).css('border-color','green');

		}
	});
	
	
	$(this).attr("disabled","disabled");

	$(this).css("opacity","0.6");

	$(this).html("Bekleyiniz");




	setTimeout(function(){ $('#'+data_id).submit(); }, 1000);


});




function maske(classi,durum) {
	
	if (durum == "on") {
		$('.'+classi).fadeIn()
	}else{
		$('.'+classi).fadeOut(1000)
	}
}

function repUrl(key, value) {
	key = encodeURIComponent(key);
	value = encodeURIComponent(value);

    // kvp looks like ['key1=value1', 'key2=value2', ...]
    var kvp = document.location.search.substr(1).split('&');
    let i=0;

    for(; i<kvp.length; i++){
    	if (kvp[i].startsWith(key + '=')) {
    		let pair = kvp[i].split('=');
    		pair[1] = value;
    		kvp[i] = pair.join('=');
    		break;
    	}
    }

    if(i >= kvp.length){
    	kvp[kvp.length] = [key,value].join('=');
    }

    // can return this or...
    let params = kvp.join('&');

    // reload page with new params
    document.location.search = params;
}






function removeParam(key, sourceURL) {
	var rtn = sourceURL.split("?")[0],
	param,
	params_arr = [],
	queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
	if (queryString !== "") {
		params_arr = queryString.split("&");
		for (var i = params_arr.length - 1; i >= 0; i -= 1) {
			param = params_arr[i].split("=")[0];
			if (param === key) {
				params_arr.splice(i, 1);
			}
		}
		rtn = rtn + "?" + params_arr.join("&");
	}
	return rtn;
}



function modalyap(name,header,content) {


	$('#'+name+'_title').html(header);
	$('#'+name+'_content').html(content);

	$('#'+name).modal();

}


$('.top-login-btn').click(function() {

	$('.top_login').each(function(index, element) {

		var degert = $(this).val();

		if(degert == '' || degert == -1 ){

			$(this).css('border-color','red');

			$(this).focus();

			die();
		} else {

			$(this).css('border-color','green');

		}
	});
	
	
	$(this).attr("disabled","disabled");

	$(this).css("opacity","0.6");

	$(this).html("Bekleyiniz");

	$.ajax({
		type: 'POST',
		url: '/panel/loginAjax',
		data: $('.top_login').serialize(),
		success: function (data) {
			
			if(data.message =="success"){
				location.reload()
			}else{
				$('#top-notify').removeClass('d-none').html("Giriş Başarısız.")
				$('.top-login-btn').removeAttr("disabled");

				$('.top-login-btn').css("opacity","1");

				$('.top-login-btn').html("Giriş Yap");
			}
		} 

	});
});


function recoveryPass() {

	val = $('#reset-email-modal').val()
	$('#reset-email-modal-btn').attr("disabled","disabled");

	$('#reset-email-modal-btn').css("opacity","0.6");

	$('#reset-email-modal-btn').html("Bekleyiniz");


	$.ajax({
		type: 'POST',
		url: '/panel/recoveryPass',
		data: {mail:val},
		success: function (data) {
			
			if(data.statu =="success"){
				$('#hataRec').removeClass('d-none').addClass('alert-success').html(data.message)
				setTimeout(function(){
					location.reload()
				},5000)


			}else{
				$('#hataRec').removeClass('d-none').addClass('alert-danger').html(data.message)
				$('#reset-email-modal-btn').removeAttr("disabled");

				$('#reset-email-modal-btn').css("opacity","1");

				$('#reset-email-modal-btn').html("Şifre Talebi");
			}
		} 

	});
}
