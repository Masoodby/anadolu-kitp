$('.add-to-cart').click(function() {

	id = $(this).data('id')
	max = $(this).attr('max')
	stok = $(this).data('stok')


	if ($('#varyant').length && $('#varyant').val() == 0) {

		sole('Hata',"Varyant Seçmediniz")
		
	}else{

		adet = $('#product-quantity').length ? $('#product-quantity').val():1

		if (adet <= max) {



			$.ajax({
				type: 'POST',
				url: '/sepet/add',
				data: {qty:adet,id:id},
				success: function (data) {

					modalyap('globalmodal','Ürün Sepete Eklendi',data)
					loadcart()
				} 

			});

		}else{
			sole("Hata","Sepetinizdekilerle beraber toplam "+stok+" adet sipariş edebiliriniz.")
		}

	}

});

function delcart(id) {

	$.ajax({
		type: 'DELETE',
		url: '/sepet/remove',
		data: {id: id},
		success: function(msg){
			location.reload()
		}
	});	


}


$('.quick-buy').click(function() {

	id = $(this).data('id')
	max = $(this).attr('max')
	stok = $(this).data('stok')

	$(this).html('<i class="fa fa-spinner rotating"></i>')


	if ($('#varyant').length && $('#varyant').val() == 0) {

		sole('Hata',"Varyant Seçmediniz")

		$(this).html('<i class="fa fa-shopping-cart"></i>')
		
	}else{

		$(this).html('<i class="fa fa-spinner rotating"></i>')
		adet = $('#product-quantity').length ? $('#product-quantity').val():1

		if (adet <= max) {



			$.ajax({
				type: 'POST',
				url: '/sepet/add',
				data: {qty:adet,id:id},
				success: function (data) {

					location.href="/hizli-satin-al"
				} 

			});

		}else{
			$(this).html('<i class="fa fa-shopping-cart"></i>')
			sole("Hata","Sepetinizdekilerle beraber toplam "+stok+" adet sipariş edebiliriniz.")
		}

	}

});







$('.add-to-wishlist').click(function() {

	id = $(this).data('id')

	$.ajax({
		type: 'POST',
		url: '/favori/add',
		data: {id:id},
		success: function (data) {

			modalyap('globalmodal','Favori Listesi',data)
			loadwish()
		} 

	});

});





$(document).ready(function() {


	loadcart()
	loadwish()
});


function loadcart() {
	$.ajax({
		type: 'GET',
		url: '/sepet/loadtopcart',		
		success: function (data) {

			$('#topcart-content').html(data)
		} 

	});
}

function loadwish() {
	$.ajax({
		type: 'POST',
		url: '/favori/countwishlist',		
		success: function (data) {

			if (data > 0) {				 
				$('#top-wish-count').removeClass('d-none').html(data)
			}
		} 

	});
}