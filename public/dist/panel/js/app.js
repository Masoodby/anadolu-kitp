
function allConfirm(fName,id ,mesaj = "İşlem Onayı. Bu işlem geri alınamaz. Emin misiniz?") {
   // body...



   bootbox.confirm({
    message: mesaj ,
    buttons: {
        confirm: {
            label: 'Evet',
            className: 'btn-success'
        },
        cancel: {
            label: 'Hayır',
            className: 'btn-danger'
        }
    },
    callback: function (result) {


        if (result) {
         eval(fName)(id);
     }else{
        sole('İptal Edildi',"İşlem İşlemi İptal Edildi")
    }

}
});

}


function sole(title,text) {
  $.toast({
    text : text,
    heading: title,
    position: 'top-right',
    bgColor: "rgba(0,0,0,.8)",
    textColor: '#fff',
})
}

function repUrl(key, value) {
    key = encodeURIComponent(key);
    value = encodeURIComponent(value);

    // kvp looks like ['key1=value1', 'key2=value2', ...]
    var kvp = document.location.search.substr(1).split('&');
    let i=0;

    for(; i<kvp.length; i++){
        if (kvp[i].startsWith(key + '=')) {
            let pair = kvp[i].split('=');
            pair[1] = value;
            kvp[i] = pair.join('=');
            break;
        }
    }

    if(i >= kvp.length){
        kvp[kvp.length] = [key,value].join('=');
    }

    // can return this or...
    let params = kvp.join('&');

    // reload page with new params
    document.location.search = params;
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function readURL(input,div) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
        $('#'+div).addClass("mt10 p10 bg-dark").html('<img src="'+ e.target.result+'" class="img-thumbnail w100" >');
    }

    reader.readAsDataURL(input.files[0]); // convert to base64 string
}
}

$(".file").change(function() {
    div = $(this).data("div")
    readURL(this,div);
});


function seola(text)
{
    var trMap = {
        'çÇ':'c',
        'ğĞ':'g',
        'şŞ':'s',
        'üÜ':'u',
        'ıİ':'i',
        'öÖ':'o'
    };
    for(var key in trMap) {
        text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
    }
    return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') 
    .replace(/\s/gi, "-") 
    .replace(/[-]+/gi, "-") 
    .toLowerCase();

}

$('.seoname').keyup(function() {

    val = $(this).val()
    $('.seoname_target').val(seola(val))
    
});

$('.copyname').keyup(function() {

    val = $(this).val()


    $('.copyname_target').val(val)
    $('.copyname_target').html(val)
    
});


$(".say").keyup(function() {

    sayi = $(this).val().length;
    limit = $(this).data("say");
    help = $(this).next('p')
    if(sayi>limit){
       help.css("color","#F00");
       help.html("Karakter Sayısı : <b>"+sayi+"</b> Limiti Aştınız");
   }else{
    help.css("color","#000");
    help.html("Karakter Sayısı : <b>"+sayi+"</b>");
}


});

$(".float").on("keyup", function() { 

    var intsayi = $(this).val().replace(/[^.0-9]/gi,"").toLowerCase();  

    $(this).val(intsayi);



}); 


$(".int").on("keyup", function() { 



    var intsayi = $(this).val().replace(/[^0-9]/gi,"").toLowerCase();  



    $(this).val(intsayi);



}); 


if ($('#resimsec').length) {

    window.onload = function(){


    //Check File API support
    if(window.File && window.FileList && window.FileReader)
    {
        var filesInput = document.getElementById('resimsec');

        filesInput.addEventListener("change", function(event){

            var files = event.target.files; //FileList object
            var output = document.getElementById('image_result');
            output.className = "mt10 p10 row bg-dark";
            
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];

                //Only pics
                if(!file.type.match('image'))
                    continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){

                    var picFile = event.target;

                    var div = document.createElement("div");
                    div.className = "col-md-2 mb10";

                    div.innerHTML = "<img class='img-thumbnail w100 pull-left'   src='" + picFile.result + "'/>";


                    output.insertBefore(div,null);            

                });
                
                 //Read the image
                 picReader.readAsDataURL(file);
             }     





         });
    }
    else
    {
        alert("Your browser does not support File API");
    }
    
}

}




$(".sbmt_btn").click(function() {

    data_id  = $(this).data('id');
    

    $('#'+data_id+' .req').each(function(index, element) {

        var degert = $(this).val();

        if(degert ==''){

            $(this).css('border-color','red');

            $(this).focus();

            die();
        } else {

            $(this).css('border-color','green');

        }
    });
    
    
    $(this).attr("disabled","disabled");

    $(this).css("opacity","0.6");

    $(this).html("Bekleyiniz");




    setTimeout(function(){ $('#'+data_id).submit(); }, 1000);


});



function uploadImg(fileID,UploadUrl,oldname) {


  var name = document.getElementById(fileID).files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
     sole("Geçersiz Dosya uzantısı Sadece Resim Formatları Yüklenebilir");
 }
 var oFReader = new FileReader();
 oFReader.readAsDataURL(document.getElementById(fileID).files[0]);
 var f = document.getElementById(fileID).files[0];
 var fsize = f.size||f.fileSize;
 if(fsize > 6000000)
 {
    sole("Geçersiz Dosya Boyutu Max 5mb boyutunda dosya yüklenebilir");
}
else
{
 form_data.append(fileID, document.getElementById(fileID).files[0]);
 $.ajax({
    url:UploadUrl,
    method:"POST",
    data: form_data,
    contentType: true,
    cache: true,
    processData: true,
    beforeSend:function(){
       sole('Resim Yükleniyor',"Resim Yükleme İşlemi Başladı")
   },   
   success:function(data)
   {
       console.log(data);
   }
});
}

}

function maske(m,durum) {

    if (durum==1) {
        $('.'+m).fadeIn();
    }else{
        $('.'+m).fadeOut();

    }
}


function modalyap(name,header,content) {


    $('#'+name+'_title').html(header);
    $('#'+name+'_content').html(content);

    $('#'+name).modal();

}

$('.li_active').parent("ul").addClass('show').parent("li").addClass('active')
