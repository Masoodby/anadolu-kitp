@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.banner') }}" class="btn btn-primary btn-lg float-right"  ><i class="fa fa-angle-left"></i> Geri</a>
			</div>

		</div>
		<div class="iq-card-body">

			<form action="{{ route('crudv4.banner.save') }}" class="container-fluid row" id="Bannerform" method="POST" enctype="multipart/form-data">
				@csrf

				@include('admin.layouts.errors')
				@include('admin.layouts.alert')


				<div class="form-group col-md-12">
					<label  >Banner Başlık:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="title" value="">
					</div>
				</div>
				<div class="form-group col-md-12">
					<label  >Banner Link:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  >{{ config('app.url') }}</span>
						</div>
						<input type="text" class="form-control req" name="link" value="#">
					</div>
				</div>
				<div class="form-group col-md-12">
					<label  >Banner Metin:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="text" value="">
					</div>
				</div>
				<div class="form-group col-md-12">
					<label  >Banner Düğme :</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="button" value="">
					</div>
				</div>
                <select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby" name="position">
                    <option >Konum</option>

                    <option value="0" >ilk satır</option>
                    <option value="1" >ikinci sıra </option>
                    <option value="2" >Üçüncü sıra </option>
                    <option value="3">dördüncü sıra </option>


                </select>

                    {{-- @foreach (setpar('banner_size') as $item)
                    <li>{{ $item}}</li>
                    @endforeach --}}
				<div class="form-group col-md-6">
					<div class="custom-file">
						<input type="file" name="image" class="custom-file-input file" data-div="image_result" id="customFile" lang="tr">
						<label class="custom-file-label" for="customFile" data-browse="Resim Seç">Banner Resmi</label>
					</div>

					<div id="image_result"></div>
				</div>


				<button type="submit" data-id="Bannerform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
			</form>

		</div>
	</div>
</div>


@endsection


@section('script')



@endsection
