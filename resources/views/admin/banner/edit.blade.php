@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.banner') }}" class="btn btn-primary btn-lg float-right"  ><i class="fa fa-angle-left"></i> Geri</a>
			</div>

		</div>
		<div class="iq-card-body">

			<form action="{{ route('crudv4.banner.save' ,$banner->id) }}" class="container-fluid row" id="bannerform" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}

				@include('admin.layouts.errors')
				@include('admin.layouts.alert')


				<div class="form-group col-md-12">
					<label  >banner Başlık:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="banner_name" value="{{ $banner->banner_name }}">
					</div>
				</div>


				<div class="form-group col-md-12">
					<label  >banner Link:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  >{{ config('app.url') }}</span>
						</div>
						<input type="text" class="form-control req" name="banner_link" value="{{ $banner->banner_link }}">
					</div>
				</div>
				@php( $texts = json_decode($banner->banner_desc,TRUE))

				@foreach (setpar('banner_text') as $key => $element)
				@php( $element = trim($element))

				<div class="form-group col-md-12">
					<label  >banner Yazısı {{ $key+1 }}:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="banner_desc[{{ @$element }}]" value="{{ @$texts[$element] }}">
					</div>
				</div>
				@endforeach


				<div class="form-group col-md-6">
					<div class="custom-file">
						<input type="file" name="resim" class="custom-file-input file" data-div="image_result" id="customFile" lang="tr">
						<label class="custom-file-label" for="customFile" data-browse="Resim Seç">banner Resmi</label>
					</div>

					<div id="image_result">
						<img src="{{ res("banner",$banner->image,"100x100") }}" class="img-thumbnail "   >
					</div>
				</div>


				<button type="button" data-id="bannerform" class="btn btn-block btn-primary sbmt_btn">Güncelle</button>
			</form>

		</div>
	</div>
</div>


@endsection


@section('script')



@endsection
