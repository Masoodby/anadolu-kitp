@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>

		</div>
		<div class="iq-card-body">

		</div>
	</div>
</div>


@endsection


@section('script')



@endsection