@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.category.save') }}" class="container-fluid row" id="catform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Kategori Adı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control copyname req seoname" name="categories_name" value="">
					</div>
				</div>


				<div class="form-group">
					<label for="email">Üst Kategorisi:</label>
					{!! addSelectone($main_cats,"Ana Kategori") !!}
				</div>

				<div class="form-group">
					<div class="custom-file">
						<input type="file" name="resim" class="custom-file-input file" data-div="image_result" id="customFile" lang="tr">
						<label class="custom-file-label" for="customFile" data-browse="Resim Seç">Kategori Resmi</label>
					</div>

					<div id="image_result"></div>
				</div>




				<div class="form-group">
					<label >Kategori Makalesi  </label>
					<textarea class="form-control h290 ckeditor lh24" name="categories_content"></textarea>
					<p class="form-text"></p>
				</div>





			</div>
		</div>

	</div>	

	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Seo Ayarları </h4>
				</div>

				<div class="col-md-4">
					<a href="{{ route('crudv4.category') }}" class="btn btn-primary float-right btn-sm"><i class="fa fa-angle-left"></i>Geri</a>
				</div>

			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Kategori Seo URL <small>(slug)</small>	:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text bg-secondary"  >{{ config('app.url') }}/</span>
						</div>
						<input type="text" class="form-control seoname_target req" name="categories_url" value="">
					</div>
				</div>


				<div class="form-group">
					<label >Kategori Sayfa Başlığı <small>(Page Title)</small></label>
					<textarea class="form-control say  copyname_target lh23" data-say="{{ seoLimit('title') }}" name="categories_title"></textarea>
					<p class="form-text"></p>
				</div>




				<div class="form-group">
					<label >Sayfa Açıklaması <small>(meta desc.)</small></label>
					<textarea class="form-control say copyname_target h100 lh23" data-say="{{ seoLimit('desc') }}" name="categories_desc"></textarea>
					<p class="form-text"></p>
				</div>



				<div class="form-group">
					<label >Anahtar Kelimeler <small>(meta keyw.)</small></label>
					<textarea class="form-control say copyname_target h100 lh23" data-say="{{ seoLimit('desc') }}" name="categories_keyw"></textarea>
					<p class="form-text"></p>
				</div>





				


			</div>
		</div>
		<button type="button" data-id="catform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
	</div>



</form>


@endsection


@section('script')
<script src="//cdn.ckeditor.com/4.14.1/basic/ckeditor.js"></script>


@endsection