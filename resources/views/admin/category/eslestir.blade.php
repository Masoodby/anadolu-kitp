@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-md-12">
	
	<div class="card">
		
		<div class="card-header"><h5>Kategori Eşleştirme</h5></div>
		<div class="card-body">
			<div class="row">
				<div class="form-group col-md-5">
					<label for="email">Mevcut Kategoriler:</label>
					{!! addSelectone($main_cats_old,"Seçiniz") !!}
				</div>


				<div class="form-group col-md-5">
					<label for="email">Yeni Kategoriler:</label>
					{!! addSelectone($main_cats_new,"Seçiniz") !!}
				</div>

				<div class="form-group col-md-2 pt30">
					<button class="btn btn-success btnok">Eşleştir</button>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="col-md-12">
	
	<div class="card">
		
		<div class="card-header"><h5>Eşleşen Yeni Kategoriler</h5></div>
		<div class="card-body">

			<table class="table table-striped">

				<tr>
					<th class="text-left">Eski</th>
					<th class="text-left">Yeni</th>
				</tr>
				@foreach ($news as $item)
				@if ($item->old_id > 0 )

				@php
				$unsetler[] = '$("#oldcat option[value=\''.$item->oldCat->id.'\']").remove();';
				@endphp




				<tr>

					<td>{{ @$item->oldCat->mainCategory->categories_name }} --> {{ $item->oldCat->categories_name }}</td>
					<td>{{ @$item->mainCategory->categories_name }} --> {{ $item->categories_name }}</td>
				</tr>

				@endif

				@endforeach

			</table>
		</div>
	</div>
</div>





@endsection


@section('script')

<script>

	$('.btnok').click(function() {

		oldcat = parseInt($('#oldcat').val())
		newcat = parseInt($('#newcat').val())

		if (oldcat > 0 && newcat > 0) {


			$.ajax({
				type: 'POST',
				url: '/crudv4/category/esles',
				data: {oldcat: oldcat,newcat:newcat},
				success: function(){
					location.reload()
				}
			});	

			
		}else{
			alert("Seçim yapma işlemi hatalı")
		}

	});

	
	@isset ($unsetler)
	@foreach ($unsetler as $row)
	{!! $row !!}
	@endforeach
	@endisset

</script>




@endsection