@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.category.save', @$cat->id) }}" class="container-fluid row" id="catform">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Kategori Adı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="categories_name" value="">
					</div>
				</div>


				<div class="form-group">
					<label for="email">Üst Kategorisi:</label>
					{!! addSelectone($main_cats,"Ana Kategori") !!}
				</div>



				<button type="button" data-id="catform" class="btn btn-primary">{{@$cat->id >0 ? "Güncelle" : "Kaydet"}}</button>


			</div>
		</div>

	</div>

</form>


@endsection


@section('script')



@endsection