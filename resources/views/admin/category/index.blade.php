@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $list->total() }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
							<option value="rank,asc">Sırala</option>

							<option value="categories_name-asc" @if(request()->has('orderby') and request('orderby') == "categories_name-asc") selected @endif>Ad Artan</option>
							<option value="categories_name-desc" @if(request()->has('orderby') and request('orderby') == "categories_name-desc") selected @endif>Ad Azalan</option>
							<option value="rank-asc" @if(request()->has('orderby') and request('orderby') == "rank-asc") selected @endif>Sıra Artan</option>
							<option value="rank-desc" @if(request()->has('orderby') and request('orderby') == "rank-desc") selected @endif>Sıra Azalan</option>


						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 25; $i <101 ; $i+=25)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">
			
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>

							<th scope="col">Ad</th>
							<th scope="col"></th>
							<th scope="col">Sıra</th>
							<th scope="col">Durum</th>
							<th scope="col">Üst Menü</th>

							<th scope="col">İşlem</th>

						</tr>
						<tr>
							<th scope="col"> <input type="text" class="form-control textfilter w50 from-control-sm" data-filter="id"  placeholder="ID..." value= "{{ request()->has('id') ? request('id'):''}}"></th>

							<th scope="col"> <input type="text" class="form-control textfilter from-control-sm" data-filter="categories_name"  name="categories_name" id="categories_name" placeholder="Kategori Adı..." value= "{{ request()->has('categories_name') ? request('categories_name'):''}}"></th>
							<th scope="col">	


								{!! addSelectone($main_cats,"Tüm Kategoriler") !!}
							</th>
							<th scope="col"> </th>
							<th scope="col">
								<select class="form-control selectfilter" data-filter="is_active" >
									<option value="">Tümü</option>
									<option value="1" @if(request()->has('is_active') and request('is_active') == 1) selected @endif>Aktif</option>
									<option value="0" @if(request()->has('is_active') and request('is_active') == 0) selected @endif>Pasif</option>
								</select>
							</th>	
							<th scope="col">
								<select class="form-control selectfilter" data-filter="is_top" >
									<option value="">Tümü</option>
									<option value="1" @if(request()->has('is_top') and request('is_top') == 1) selected @endif>Aktif</option>
									<option value="0" @if(request()->has('is_top') and request('is_top') == 0) selected @endif>Pasif</option>
								</select>
							</th>
							<th scope="col"> 
								<a href="{{ route('crudv4.category.add') }}" class="btn btn-success btn-sm tipsi" title="Yeni Ekle"><i class="fa fa-plus"></i></a>

								@if (request()->hasAny(['id',"categories_name","categories_main","limit","orderby","is_active"]))
								<a href="{{ route('crudv4.category') }}" class="btn btn-dark btn-sm tipsi" title="Filtreyi Sıfırla"><i class="fa fa-refresh"></i></a>
								@endif


								<a href="{{ route('crudv4.category.sortable') }}" class="btn btn-success btn-sm tipsi" title="Sırala"><i class="fa fa-list"></i></a>

							</th>


						</tr>
					</thead>
					<tbody>
						@if(count($list)==0)
						<div class="alert alert-warning">Kategori bulunamadı..</div>
						@else
						@foreach($list as $row)
						<tr id="tr_{{ $row->id }}">

							<td scope="row">{{$loop->iteration}}</td>
							<td colspan="2"><small>{{ $row->mainCategory->categories_name }}</small> -> {{ $row->categories_name }}</td>

							<td>
								<div class="input-group input-group-sm mb-3 w-50">

									<input type="text" class="form-control rank_update" data-name="rank" id="rank_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->rank }}">

									<div class="input-group-append">
										<span class="input-group-text cur-p rank_update_click"   data-name="rank" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
									</div>
								</div>

							</td>
							<td>



								<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
									<div class="custom-switch-inner">

										<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitch-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_active"

										{{ $row->is_active === 1 ? "checked" : "" }}>
										<label class="custom-control-label" for="customSwitch-{{$loop->iteration}}">
											<span class="switch-icon-left"><i class="fa fa-check"></i></span>
											<span class="switch-icon-right"><i class="fa fa-check"></i></span>
										</label>
									</div>
								</div>

							</td>	
							<td>



								<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
									<div class="custom-switch-inner">

										<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitchtop-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_top"

										{{ $row->is_top === 1 ? "checked" : "" }}>
										<label class="custom-control-label" for="customSwitchtop-{{$loop->iteration}}">
											<span class="switch-icon-left"><i class="fa fa-check"></i></span>
											<span class="switch-icon-right"><i class="fa fa-check"></i></span>
										</label>
									</div>
								</div>

							</td>
							<td>
								<a href="{{ route('crudv4.category.edit',$row->id) }}" class="btn btn-success btn-sm tipsi mb-3" title="Düzenle"><i class="las la-edit"></i></a>
								<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>
							</td>


						</tr>

						@endforeach
						@endif

					</tbody>
				</table>



				{{ $list->withQueryString()->links() }}
			</div>


			
		</div>
	</div>
</div>



@endsection


@section('script')
<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/category/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}

	$('.textfilter').keyup(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()

		if(event.keyCode == 13){

			repUrl(filterType,val)

		}
	});



	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	$('.rank_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}
		

	});

	
	$('.rank_update').focusout(function() {
		

		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label)

	}); 



	$('.rank_update_click').click(function() {
		id = $(this).data("id")
		val= $("#rank_"+id).val()
		label = $(this).data('name')

		updater(id,val,label)


	});


	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')


		updater(id,val,label)


	});



	function updater(id,value,lbl) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/category/update/'+id,
			data: {val: value,label:lbl},
			success: function(){
				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});	
	}






</script>
@endsection
