@extends('admin.layouts.master')
@section('title', $title)
@section('body')
<link rel="stylesheet" href="{{ asset('dist/panel/libs/nestable/jquery.nestable.css')}}">
<link rel="stylesheet" href="{{ asset('dist/panel/libs/nestable/custom_nestable.css')}}">
<div class="col-md-12">
	
	<div class="card">
		
		<div class="card-header"><h5>Sırala</h5></div>
		<div class="card-body">


			<div class="maske ms1"></div>
			<div class="col-md-12">

				<div  class="col-md-2 pull-right">
					<button id="save" class="btn btn-success ks-rounded">Tümünü Kaydet</button>
				</div>

				<div class="col-md-3 pull-left">
					<input type="text" class="form-control" id="label" placeholder="Menu Adı" required>
				</div>
				<div class="col-md-3 pull-left">
					<input type="text" class="form-control" id="link" placeholder="Menu Linki" required>
				</div>
				<div class="col-md-3 pull-left">
					<button id="submit" class="btn btn-success">Kategori Ekle</button> <button id="reset" class="btn  btn-danger">Sıfırla</button>
				</div>
				<div class="clearfix"></div>


				<input type="hidden" id="id" value="0">
			</div>

			<hr>

			<div class="cf nestable-lists pl30">
				<div class="dd" id="nestable">

					{!! $cats !!}
				</div>
			</div>

			<input type="hidden" id="nestable-output">
			<!--	<textarea name="" id="nestable-output" cols="30" rows="10"></textarea> -->


		</div>
	</div>
</div>

@endsection




@section('script')
<script src="{{ asset('dist/panel/libs/nestable/jquery.nestable.js')}}"></script>

<script>
	if($('#nestable').length){

		$(document).ready(function()
		{
			var updateOutput = function(e)
			{
				var list   = e.length ? e : $(e.target),
				output = list.data('output');
				if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
        	output.val('JSON browser support required for this demo.');
        }
    };
    // activate Nestable for list 1
    $('#nestable').nestable({
    	group: 1
    })
    .on('change', updateOutput);
    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    $('#nestable-menu').on('click', function(e)
    {
    	var target = $(e.target),
    	action = target.data('action');
    	if (action === 'expand-all') {
    		$('.dd').nestable('expandAll');
    	}
    	if (action === 'collapse-all') {
    		$('.dd').nestable('collapseAll');
    	}
    });
});

		$(document).ready(function(){
			maske('ms1',0);

			$("#submit").click(function(){
				maske('ms1',1);
				var dataString = { 
					categories_name : $("#label").val(),
					categories_url : $("#link").val(),
					id : $("#id").val(),

				};
				$.ajax({
					type: "POST",
					url: "/crudv4/category/save-menu",
					data: dataString,
					dataType: "json",
					cache : false,
					success: function(data){


						
						if(data.type == 'add'){
							$("#menu-id").prepend(data.menu);
						} else if(data.type == 'edit'){
							$('#label_show'+data.id).html(data.label);
							$('#link_show'+data.id).html(data.link);
						}
						$('#label').val('');
						$('#link').val('');
						$('#id').val('');
						maske('ms1',0);
					} ,error: function(xhr, status, error) {
						alert(error);

					},
				});
			});
			$('.dd').on('change', function() {
				maske('ms1',1);


				$.ajax({
					type: "POST",
					url: "/crudv4/category/savesort",
					data : {data : $("#nestable-output").val()},
					cache : false,
					success: function(data){
						maske('ms1',0);
						
					} ,error: function(xhr, status, error) {
						alert(error);

					},
				});
			});
			$("#save").click(function(){
				maske('ms1',1);


				$.ajax({
					type: "POST",
					url: "/categories/savesort",
					data : {data : $("#nestable-output").val()},
					cache : false,
					success: function(data){
						maske('ms1',0);
						
						sole('<span class="fa fa-check"></span>','Menüler Kaydedildi');

					} ,error: function(xhr, status, error) {
						alert(error);

					},
				});
			});

			$(document).on("click",".del-button",function() {
				var x = confirm('Silmek İstediğinizden emin misiniz');
				var id = $(this).attr('id');

				if(x){
					maske('ms1',1);
					$.ajax({
						type: "GET",
						url: '/crudv4/category/del/' + id,
						data: { id : id },
						cache : false,
						success: function(data){
							maske('ms1',0);
							
							$("li[data-id='" + id +"']").remove();
						} ,error: function(xhr, status, error) {
							alert(error);
						},
					});
				}
			});


			$(document).on("click",".edit-button",function() {
				var id = $(this).attr('id');
				var label = $(this).attr('label');
				var link = $(this).attr('link');
				$("#id").val(id);
				$("#label").val(label);
				$("#link").val(link);
			});
			$(document).on("click","#reset",function() {
				$('#label').val('');
				$('#link').val('');
				$('#id').val('');
			});
		});
	}



</script>

@endsection