@extends('admin.layouts.master')
@section('title', $title)
@section('body')
    <form action="{{ route('crudv4.combo.save') }}" class="container-fluid row" id="productform" method="POST"
        enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.layouts.errors')
        @include('admin.layouts.alert')
        <div class="col-md-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title col-md-6">
                        <h4 class="card-title">{{ $title }} </h4>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('crudv4.combo') }}" class="btn btn-primary float-right"><i
                                class="fa fa-angle-left"></i> Geri</a>
                    </div>
                </div>
                <div class="iq-card-body">
                    @php($listDizi = ['list-alt' => ' Ürün seti', 'book' => 'Ürün seti detaileri'])
                    <ul class="nav nav-tabs bg-f4 color-white" id="myTab-three" role="tablist">
                        @foreach ($listDizi as $key => $item)
                            <li class="nav-item">
                                <a class="nav-link {{ $loop->iteration == 1 ? 'active' : '' }}"
                                    id="tab-btn-{{ $loop->iteration }}" data-toggle="tab"
                                    href="#tab{{ $loop->iteration }}" role="tab"
                                    aria-controls="tab-btn-{{ $loop->iteration }}" aria-selected="true">
                                    <i class="fa fa-{{ $key }}"></i> {{ $item }} </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content" id="myTabContent-4">
                        <div class="tab-pane active show fade" id="tab1" role="tabpanel" aria-labelledby="tab-btn-1">
                            <div class="row">
                                <div class="col-md-12 " style="border-left:1px solid #ccc">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label>Ürün seti Adı:</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text  bg-primary"><i
                                                            class="fa fa-folder-o"></i></span>
                                                </div>
                                                <input type="text" class="form-control copyname req seoname"
                                                    data-tab="tab-btn-1" name="combo_name"
                                                    value="{{ old('combo_name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <div class="form-group">
                                                <label>Ürün Seç :</label>
                                                <div class="input-group ">
                                                    <select class="form-control form-control-sm mb-3" onchange="myfunc()" id="product_id"
                                                        name="product_id[]" multiple="multiple">
                                                        <option>Tüm kitaplar</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label>Satış Fiyatı <small class="text-muted">(Çoklu Fiyat için diğer tablardan
                                                    devam edin)</small>:</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text  bg-primary"><i
                                                            class="fa fa-try"></i></span>
                                                </div>
                                                <input type="text" class="form-control req float"
                                                    onkeyup="$('.pricex').val($(this).val())" data-tab="tab-btn-1"
                                                    name="combo_price" value="{{ old('combo_price') }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text  bg-secondary">TL</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <textarea name="" id="pricelist" cols="102" rows="5" disabled ></textarea>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label>Ürün setı Açıklama <small class="text-muted">(max 500 karakter)</small>
                                        </label>
                                        <textarea class="form-control say h70 lh23" data-say="500"
                                            name="combo_desc">{{ old('combo_desc') }}</textarea>
                                        <p class="form-text"></p>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <input type="file" name="combo_image" id="resimsec" class="custom-file-input"
                                                lang="tr">
                                            <label class="custom-file-label" for="resim" data-browse="Çoklu Resim Seç">Ürün
                                                seti Resmi</label>
                                        </div>

                                        <div id="image_result">

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <button type="submit" data-id="productform" class="btn btn-block btn-primary sbmt_btn_spec">Kaydet</button>
        </div>
    </form>
@endsection
@section('script')
    <script>
        $(".sbmt_btn_spec").click(function() {
            data_id = $(this).data('id');
            if ($('#product_cat').val() == 0) {
                $('#catpanel').css('border', '1px solid red');
                sole('Hata', 'Kategori Seçmediniz')
                die();
            } else {
                $('#catpanel').css('border', '1px solid green');
            }
            $('#' + data_id + ' .req').each(function(index, element) {

                var degert = $(this).val();

                if (degert == '') {

                    $(this).css('border-color', 'red');
                    tabID = $(this).data('tab');
                    $('#' + tabID).click()
                    $(this).focus();

                    die();
                } else {

                    $(this).css('border-color', 'green');

                }
            });
            if (CKEDITOR.instances["product_content"].getData() == "") {
                $('#tab-btn-2').click()
                sole('Hata', 'Ürün içeriği Boş')
                die();
            }
            $(this).attr("disabled", "disabled");
            $(this).css("opacity", "0.6");
            $(this).html("Bekleyiniz");
            setTimeout(function() {
                $('#' + data_id).submit();
            }, 1000);
        });




        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        $("#product_id").select2({
            theme: 'bootstrap4',
            width: '100%',
            dropdownAutoWidth: true,
            ajax: {
                url: '/crudv4/combo/urunara',
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term // search term
                    };
                },
                processResults: function(response) {
                    console.log(response);
                    return {
                        results: response
                    };

                },
                cache: true
            }




        });






    </script>


@endsection
