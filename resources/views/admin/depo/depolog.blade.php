    @extends('admin.layouts.master')
    @section('title', $title)
    @section('body')
    <div class="col-md-12 d-inline mb-5">
     <a class="btn btn-dark" target="_blank" href="{{ route('crudv4.depolog.depoTosite') }}">Depoda Var Sitede Yok</a>
     <a class="btn btn-success" target="_blank" href="{{ route('crudv4.depolog.stok') }}">Depoyu Siteye Uygula</a>
   </div>

   <div class="col-sm-6 col-md-6 col-lg-3">
     <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
      <div class="iq-card-body">
       <div class="d-flex align-items-center justify-content-between">
        <h6>Depo Maliyeti</h6>
        <span class="iq-icon"><i class="ri-information-fill"></i></span>
      </div>
      <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
        <div class="d-flex align-items-center">
         <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-inbox-fill"></i></div>
         <h2 class="fsz18">{{ price($maliyet->alis) }}</h2>
       </div>
       <div class="iq-map text-primary font-size-32"><i class="ri-bar-chart-grouped-line"></i></div>
     </div>
   </div>
 </div>
</div>
<div class="col-sm-6 col-md-6 col-lg-3">
 <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
  <div class="iq-card-body">
   <div class="d-flex align-items-center justify-content-between">
    <h6>Depo Ciro</h6>
    <span class="iq-icon"><i class="ri-information-fill"></i></span>
  </div>
  <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
    <div class="d-flex align-items-center">
     <div class="rounded-circle iq-card-icon iq-bg-danger mr-2"><i class="ri-radar-line"></i></div>
     <h2 class="fsz18">{{ price($maliyet->price) }}</h2>  

   </div>

   <div class="iq-map text-danger font-size-32"><i class="ri-bar-chart-grouped-line"></i></div>
 </div>
 <small class="fsz10">Arka Fiyata Göre :{{ price($maliyet->satis) }}</small>
</div>
</div>
</div>

<div class="col-sm-6 col-md-6 col-lg-3">
  <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
   <div class="iq-card-body">
    <div class="d-flex align-items-center justify-content-between">
     <h6>Depo Kar</h6>
     <span class="iq-icon"><i class="ri-information-fill"></i></span>
   </div>
   <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
     <div class="d-flex align-items-center">
      <div class="rounded-circle iq-card-icon iq-bg-warning mr-2"><i class="ri-price-tag-3-line"></i></div>
      <h2 class="fsz18">{{ price($maliyet->price-$maliyet->alis) }}</h2></div>
      <div class="iq-map text-warning font-size-32"><i class="ri-bar-chart-grouped-line"></i></div>
    </div>
  </div>
</div>
</div>
<div class="col-sm-6 col-md-6 col-lg-3">
 <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
  <div class="iq-card-body">
   <div class="d-flex align-items-center justify-content-between">
    <h6>Kitap Adedi</h6>
    <span class="iq-icon"><i class="ri-information-fill"></i></span>
  </div>
  <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
    <div class="d-flex align-items-center">
     <div class="rounded-circle iq-card-icon iq-bg-info mr-2"><i class="ri-refund-line"></i></div>
     <h2>{{ $toplam_kitap->tadet }}</h2><br>Çeşit {{ $kitap_cesit }}</div>
     <div class="iq-map text-info font-size-32"><i class="ri-bar-chart-grouped-line"></i></div>
   </div>
 </div>
</div>
</div>


<div class="col-sm-6">
  <div class="iq-card">
   <div class="iq-card-header d-flex justify-content-between">
    <div class="iq-header-title col-md-6">

     <h4 class="card-title">Son 50 Depo İşlemi </h4>
   </div>

 </div>
 <div class="iq-card-body">

  <div style="height: 300px; overflow-y: auto;">

    <ul class="suggestions-lists m-0 p-0">


      @foreach ($depo_actions as $var)


      <li class="d-flex mb-4 align-items-center">
       <div class="profile-icon iq-bg-{{ $var->adet<0?"warning":"success" }}"><span><i class="ri-check-fill"></i></span></div>
         <div class="media-support-info ml-3">
          <h6>{{ depoIslem($var->adet) }}</h6>
          <p class="mb-0"><span class="text-success">Raf : {{ $var->getRaf->raf_name }}</span> ISBN : {{ $var->isbn }}  </p>
        </div>
        <div class="media-support-amount ml-3">
          <h6><span class="text-secondary"></span><b>  Adet : {{ $var->adet }}</b></h6>
          <p class="mb-0">{{  \Carbon\Carbon::parse($var->created_at)->diffForHumans() }}</p>
        </div>
      </li>

      @endforeach

    </ul>
  </div>
</div>
</div>
</div>

<div class="col-sm-6">
  <div class="iq-card">
   <div class="iq-card-header d-flex justify-content-between">
    <div class="iq-header-title col-md-12 pt10">

     <h4 class="card-title">İkaz veren Raflar <a class="fsz12 float-right text-primary" target="_blank" href="{{ route('crudv4.settings.index',11) }}">[İkaz Ayarı]</a></h4>

     <small>({{ $alert_raf->count() }} adet {{ set('ikaz') }} ve altındaki kitaplar listelenmiştir)</small>
   </div>

 </div>
 <div class="iq-card-body">

  <div style="height: 300px; overflow-y: auto;">
   <table class="table">
    @foreach ($alert_raf as $var)
    <tr>
      <td>{{ $var->getRaf->raf_name }}</td>
      <td>{{ $var->book_id == 0? "Eklenmemiş Kitap":$var->getBook->product_name }}<br>{{ $var->isbn }}</td>

      <td>Son {{ $var->adet }} Adet</td>
    </tr>
    @endforeach
  </table>

</div>



</div>
</div>
</div>


@endsection


@section('script')



@endsection


