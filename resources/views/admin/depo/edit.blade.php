@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.depo.save',$depo->id) }}" class="container-fluid row" id="catform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>

				<div class="col-md-4">
					<a href="{{ route('crudv4.depo.add') }}" class="btn btn-primary float-right btn-sm"><i class="fa fa-plus"></i>Yeni Ekle</a>
				</div>
			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Depo Tanımı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="depo_name" value="{{ $depo->depo_name }}">
					</div>
				</div>


			</div>
		</div>

		<button type="button" data-id="catform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>

	</div>	

	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Depolar </h4>
				</div>



			</div>
			<div class="iq-card-body">

				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Depo</th>
							<th scope="col">Oda Sayısı</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>

						@foreach ($depos as $var)

						<tr id="tr_{{ $var->id }}">

							<td scope="row">{{$loop->iteration}}</td>
							<td>{{ $var->depo_name }}</td>
							<td>{{ $var->getRoom->count() }}</td>
							<td>
								
								<div class="flex align-items-center list-user-action">

									<a class="iq-bg-primary tipsi" title="Düzenle" href="{{ route('crudv4.depo.edit',$var->id) }}" ><i class="ri-pencil-line"></i></a>
									<a  class="iq-bg-primary tipsi" title="Sil"  onclick="allConfirm('eraseci',{{ $var->id }})" href="javascript:;"><i class="ri-delete-bin-line"></i></a>

								</div>


							</td>
						</tr>

						@endforeach


					</tbody>
				</table>

			</div>
		</div>
		
	</div>



</form>


@endsection


@section('script')
<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/depo/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}

	
</script>


@endsection