@extends('admin.layouts.master')
@section('title', $title)
@section('body')








<div class="col-sm-12 col-lg-6">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body">




			<div class="form-group">
				<label  >ISBN:</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text  bg-faded"  ><i class="fa fa-barcode"></i></span>
					</div>
					<input type="text" class="form-control" id="isbn" placeholder="Barkod Okuyucu ile kullanın">

					<div class="input-group-prepend">
						<span class="input-group-text  bg-dark"  ><i class="fa fa-search"></i></span>
					</div>
				</div>
			</div>

			<hr>

			<div id="urunler_result"></div>


		</div>
	</div>



</div>	

<div class="col-sm-12 col-lg-6 d-none">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">Çıkan Ürünler</h4>
			</div>
		</div>
		<div class="iq-card-body">






		</div>
	</div>



</div>	




</form>


@endsection


@section('script')
<script>

	$(document).ready(function() {
		setTimeout(function() { $('#isbn').focus() }, 1000);
		
	});


	$('#isbn').keypress(function (event) {

		var keycode = (event.keyCode ? event.keyCode : event.which);

		isbn = $(this).val()

		if (keycode == '13') {


			if ($(this).val().length > 5 && $(this).val().length < 15){

				$.ajax({
					type: 'POST',
					url: '/crudv4/depolog/exitdepo',
					data:{isbn:isbn},
					success: function (data) {

						$('#isbn').val('')
						$('#isbn').focus()					
						$('#urunler_result').html(data);
					} 

				});


			}else{
				sole('Hata','Hatalı Barkod')
			}

		}
	});

	
</script>


@endsection