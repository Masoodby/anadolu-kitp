<table class="table">
	<tbody>
		@if(count($list)==0)
		<div class="alert alert-warning">Kitap bulunamadı..</div>
		@else
		@foreach($list as $row)
		<tr id="tr_{{ $row->id }}">
			<td scope="row">{{$loop->iteration}}</td>
			<th>{{ $row->book_id==0? " Eklenmemiş Kitap":$row->getBook->product_name }}<br><small>{{ $row->isbn }}</small></th>
			<th>{{ $row->adet }} Adet</th>


			<th>{{ $row->getRaf->getOda->oda_name }} <i class="fa fa-angle-right"></i> {{ $row->getRaf->raf_name }}</th>

			<th><button class="btn btn-dark btn-xs"  onclick="allConfirm('exitci',{{ $row->id }})"  data-id="{{ $row->id }}" >Çıkışı Onayla</button></th>
		</tr>

		@endforeach
		@endif

	</tbody>
</table>


<script>


	function exitci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/depolog/exitdeporun/' + id,
			success: function (data) {
				sole('İşlem Başarılı',"Depo Cıkışı Sağlandı."+data)
				$('#tr_'+id).html('<td colspan="5" class="text-center fsz18 text-success">Çıkış Yapıldı</td>')
			} 

		});
	}

	
</script>
