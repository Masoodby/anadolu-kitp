@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $list->total() }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
							<option value="rank,asc">Sırala</option>

							<option value="adet-asc" @if(request()->has('orderby') and request('orderby') == "adet-asc") selected @endif>Adet Artan</option>
							<option value="adet-desc" @if(request()->has('orderby') and request('orderby') == "adet-desc") selected @endif>Adet Azalan</option>



						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 25; $i <101 ; $i+=25)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">







			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>

							<th scope="col">Ad</th>
							<th scope="col">ISBN</th>
							<th scope="col">Adet</th>						
							<th scope="col">Konum</th>						

							<th scope="col">İşlem</th>

						</tr>
						<tr>
							<th scope="col"> #</th>

							<th scope="col"> <input type="text" class="form-control textfilter from-control-sm" data-filter="isbn"  name="isbn" id="isbn" placeholder="ISBN ile Arama..." value= "{{ request()->has('isbn') ? request('isbn'):''}}"></th>




							<th scope="col"> 


								@if (request()->hasAny(['isbn']))
								<a href="{{ route('crudv4.depolog.depolist') }}" class="btn btn-dark btn-sm tipsi" title="Filtreyi Sıfırla"><i class="fa fa-refresh"></i></a>
								@endif

							</th>


						</tr>
					</thead>
					<tbody>
						@if(count($list)==0)
						<div class="alert alert-warning">Kitap bulunamadı..</div>
						@else
						@foreach($list as $row)
						<tr id="tr_{{ $row->id }}">
							<td scope="row">{{$loop->iteration}}</td>
							<th>{{ $row->book_id==0? " Eklenmemiş Kitap":$row->getBook->product_name }}</th>
							<th>{{ $row->isbn }}</th>
							<th> 
								<div class="input-group input-group-sm mb-3 w-50">

									<input type="text" class="form-control adet_update" data-name="adet" id="adet_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->adet }}">

									<div class="input-group-append">
										<span class="input-group-text cur-p adet_update_click"   data-name="adet" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
									</div>
								</div>

							</th>

							<th>{{ $row->getRaf->getOda->oda_name }} <i class="fa fa-angle-right"></i> {{ $row->getRaf->raf_name }}</th>

							<th><button class="btn btn-danger btn-xs"  onclick="allConfirm('eraseci',{{ $row->id }})"  data-id="{{ $row->id }}" ><i class="fa fa-times"></i></button></th>
						</tr>

						@endforeach
						@endif

					</tbody>
				</table>



				{{ $list->withQueryString()->links() }}
			</div>



		</div>
	</div>
</div>



@endsection


@section('script')
<script>

	

	$('.textfilter').keyup(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()

		if(event.keyCode == 13){

			repUrl(filterType,val)

		}
	});



	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/raf/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}


	$('.adet_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}


	});

/*
		$('.adet_update').focusout(function() {


			value = $(this).val()
			id = $(this).data("id")
			label = $(this).data('name')

			updater(id,value,label)

		}); 
		*/


		$('.adet_update_click').click(function() {
			id = $(this).data("id")
			val= $("#adet_"+id).val()
			label = $(this).data('name')

			updater(id,val,label)


		});




		function updater(id,value,lbl) {
			$.ajax({
				type: 'POST',
				url: '/crudv4/raf/adet/'+id,
				data: {val: value,label:lbl},
				success: function(){
					sole("İşlem Başarılı","Güncelleme işlemi başarılı")
				}
			});	
		}




	</script>
	@endsection
