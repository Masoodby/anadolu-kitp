@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $list->total() }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
							<option value="id,desc">Sırala</option>

							<option value="id-asc" @if(request()->has('orderby') and request('orderby') == "id-asc") selected @endif>ID Artan</option>
							<option value="id-desc" @if(request()->has('orderby') and request('orderby') == "id-desc") selected @endif>ID Azalan</option>



						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 25; $i <101 ; $i+=25)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">







			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>

							<th scope="col">Ad</th>
							<th scope="col">ISBN</th>
							<th scope="col">Adet</th>						
							<th scope="col">Konum</th>						

							<th scope="col">Not</th>

						</tr>
						<tr>
							<th scope="col"> #</th>

							<th scope="col"> <input type="text" class="form-control textfilter from-control-sm" data-filter="isbn"  name="isbn" id="isbn" placeholder="ISBN ile Arama..." value= "{{ request()->has('isbn') ? request('isbn'):''}}"></th>

							<th scope="col"> <input type="text" class="form-control textfilter from-control-sm" data-filter="notes"  name="notes" id="notes" placeholder="Notlarda   Arama..." value= "{{ request()->has('notes') ? request('notes'):''}}"></th>




							<th>
								<select class="form-control selectfilter" data-filter="durum" >
									<option value="">Depo Durum</option>
									<option value="1" @if(request()->has('durum') and request('durum') == 1) selected @endif>Depo Girişleri</option>
									<option value="0" @if(request()->has('durum') and request('durum') == 0) selected @endif>Depo Çıkışları</option>
								</select>
							</th>

							<th scope="col"> 


								@if (request()->hasAny(['notes','isbn','durum']))
								<a href="{{ route('crudv4.depolog.depoLogs') }}" class="btn btn-dark btn-sm tipsi" title="Filtreyi Sıfırla"><i class="fa fa-refresh"></i></a>
								@endif

							</th>
						</tr>
					</thead>
					<tbody>
						@if(count($list)==0)
						<div class="alert alert-warning">Log bulunamadı..</div>
						@else
						@foreach($list as $row)
						<tr id="tr_{{ $row->id }}">
							<td scope="row">{{$loop->iteration}}</td>

							<td><a href="{{ @url('crudv4/depolog/depolist?&isbn='.$row->isbn) }}" target="_blank">{{ @$row->isbn }}</a><br>{{ @$row->getBook->product_name }}</td>
							<td>{{ $row->adet }}</td>


							<td>{{ $row->getRaf->getOda->oda_name }} <i class="fa fa-angle-right"></i> {{ $row->getRaf->raf_name }}</td>

							<td>{{ $row->notes }} <br><span class="badge badge-primary">{{ $row->created_at }}</span> </td>
						</tr>

						@endforeach
						@endif

					</tbody>
				</table>



				{{ $list->withQueryString()->links() }}
			</div>



		</div>
	</div>
</div>



@endsection


@section('script')
<script>



	$('.textfilter').keyup(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()

		if(event.keyCode == 13){

			repUrl(filterType,val)

		}
	});



	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/raf/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}


	$('.adet_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}


	});

/*
		$('.adet_update').focusout(function() {


			value = $(this).val()
			id = $(this).data("id")
			label = $(this).data('name')

			updater(id,value,label)

		}); 
		*/


		$('.adet_update_click').click(function() {
			id = $(this).data("id")
			val= $("#adet_"+id).val()
			label = $(this).data('name')

			updater(id,val,label)


		});




		function updater(id,value,lbl) {
			$.ajax({
				type: 'POST',
				url: '/crudv4/raf/adet/'+id,
				data: {val: value,label:lbl},
				success: function(){
					sole("İşlem Başarılı","Güncelleme işlemi başarılı")
				}
			});	
		}




	</script>
	@endsection
