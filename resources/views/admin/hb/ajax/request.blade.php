 <div style="height: 500px; overflow-y: auto;">
     
    <table class="table table-bordered">
        <tr>
            <th>Son Durum </th>
            <td>{{ $result["status"] }}</td>
        </tr>
        <tr>
            <th>İşlem Tarihi</th>
            <td>{{ date('d.m.Y H:i' , strtotime($result['createdAt'])) }}</td>
        </tr> 
        <tr>
            <th>Ürün Adedi</th>
            <td>{{ $result['total'] }}</td>
        </tr>
        <tr>
            <th>Hata Adedi</th>
            <td>{{ $result['errors'] }}</td>
        </tr>
    </table>

    @if (!empty($result['errors']))
    {{ prep($result['errors']) }}
    @endif


</div>

