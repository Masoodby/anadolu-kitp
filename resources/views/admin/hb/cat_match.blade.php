@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<style>
   #trend-cat{
      width: 60%;
      height: 300px;
      overflow-x: auto;
      overflow-y: hidden; 
      float: left;
   } 

   #local-cat{
      width: 40%;


      float: right;
   }
</style>

<div class="col-sm-12">
   <div class="iq-card">
      <div class="iq-card-header d-flex justify-content-between">
         <div class="iq-header-title col-md-6">

            <h4 class="card-title">{{ $title }} </h4>
         </div>


      </div>
      <div class="iq-card-body pos-r">
         <div id="result"></div>
         <div class="maske ms1"></div>


         <div id="local-cat" class="col-md-5 float-left">

            <div class="form-group mt5">

               <input type="text" class="form-control form-control-sm search-cat" placeholder="Kategori Ara..." >
            </div>

            <hr>


            <div id="brand-list" class="scrolls h300 p5">
               <ul class="list-group cat_list_hnk iq-list-style-1">
                  @foreach ($cats as $item)


                  <li class="mb-2 mr-0 bg-light listcat-item">
                     <div class="d-flex justify-content-between">
                        <div class="custom-control custom-checkbox">
                           <input type="checkbox" class="custom-control-input catsec sendly" name="cats[{{ $item['id'] }}]"  data-name="{{ $item['categories_name']  }}"  id="cats{{ $item['id'] }}"    value="{{ $item['categories_name'] }}"  >
                           <label class="custom-control-label fsz12" for="cats{{ $item['id'] }}"><b>{{ $item['categories_name'] }}</b></label>
                        </div>

                     </div>
                  </li>


                  @foreach ($item['child'] as $childItem)
                  <li class="mb-2 mr-0 pl10 listcat-item">
                     <div class="d-flex justify-content-between">
                        <div class="custom-control custom-checkbox">
                           <input type="checkbox" class="custom-control-input catsec sendly" name="cats[{{ $childItem['id'] }}]" data-name="{{ $childItem['categories_name']  }}" id="cats{{ $childItem['id'] }}"     value="{{ $childItem['categories_name'] }}"  >
                           <label class="custom-control-label fsz11" for="cats{{ $childItem['id'] }}">{{ $childItem['categories_name'] }}</label>
                        </div>

                     </div>
                  </li>


                  @foreach ($childItem['child'] as $lastchild)
                  <li class="mb-2 mr-0 pl20 listcat-item">
                     <div class="d-flex justify-content-between">
                        <div class="custom-control custom-checkbox">
                           <input type="checkbox" class="custom-control-input catsec sendly" name="cats[{{ $lastchild['id'] }}]"  data-name="{{ $lastchild['categories_name']  }}"  id="cats{{ $lastchild['id'] }}"    value="{{ $lastchild['categories_name'] }}"  >
                           <label class="custom-control-label fsz10" for="cats{{ $lastchild['id'] }}">{{ $lastchild['categories_name'] }}</label>
                        </div>

                     </div>
                  </li>
                  {{-- expr --}}
                  @endforeach

                  @endforeach




                  @endforeach


               </ul>
            </div>



            <div class="clearfix"></div>
         </div>

         <div class="col-md-4 float-left"  >
            <label for="">Hepsi Burada Kategorilerinde Arayınız</label>
            <div class="input-group ">
               <select name="remotecat" id="remotecat"   class="form-control sel2">

                  <option value="0">Seçiniz</option>

                  @foreach (json_decode($hbcat) as $var)

                  <option value="{{ $var->categoryId }}">{{ implode(' -> ',$var->paths) }} </option>
                  @endforeach

               </select>
            </div>
            <div class="clearfix"></div>
            <hr>
            <button type="button"   class="btn btn-block mt10 btn-primary float-right save_cat">Kategori Eşleştir</button>
         </div>

         <input type="hidden" id="remote_cat_id" class="sendly" name="remote_cat_id" value="0">
         <input type="hidden" id="remote_cat_name" class="sendly" name="remote_cat_name" value="0">


         <div class="clearfix"></div>


      </div>

   </div>
</div>




<div class="col-sm-12">
   <div class="iq-card">
      <div class="iq-card-header d-flex justify-content-between">
         <div class="iq-header-title col-md-6">

            <h4 class="card-title">HB için Eşleşmiş Kategoriler</h4>
         </div>


      </div>
      <div class="iq-card-body" id="eslesenler">

         <table class="table">
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">Site Kategorisi</th>
                  <th scope="col">HB Kategorisi</th>
                  <th scope="col">İşlem</th>
               </tr>
            </thead>
            <tbody>
               @php($local_dizi = [])
               @foreach ($matched as $row)


               @php($local_dizi[$row->local_id] = $row->local_id)

               <tr id="tr_{{ $row->id }}">

                  <td scope="row">{{$loop->iteration}}</td>

                  <td>{{ $row->local_name }} ({{ $row->local_id }})</td>
                  <td>{{ $row->remote_name }} <a target="_blank" href="{{ url('crudv4/hb/catRequired/'.$row->remote_id) }}">({{ $row->remote_id }})</a></td>

                  

                  <td>

                     <button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

                  </td>
               </tr>

               @endforeach


            </tbody>
         </table>
      </div>
   </div>
</div>



@endsection


@section('script')

<script>

   @foreach ($local_dizi as $item)
   $('#cats{{ $item }}').attr('disabled','disabled').parent('div').addClass('tipsi').attr('title', 'Eşleştirilmiş bir kategori seçilemez');
   @endforeach


   function eraseci(id) {

      $.ajax({
         type: 'GET',
         url: '/crudv4/hb/del/' + id,
         success: function () {
            sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
            $('#tr_'+id).remove()
         } 

      });
   }


   $(".search-cat").on('keyup', function() {
    var search = $(this).val().toLowerCase();
      //Go through each list item and hide if not match search

      $(".cat_list_hnk .listcat-item").each(function() {
        if ($(this).children('div').children('div').children('input').data('name').toLowerCase().indexOf(search) != -1) {
          $(this).show();

       }
       else {
          $(this).hide();
       }

    });


   });




   $('#remotecat').change(function() { 
      vals = $(this).val()
      name = $(this).children('option:selected').text()
      $('#remote_cat_id').val(vals)
      $('#remote_cat_name').val(name)

   });

   $('.save_cat').click(function() {

      durum  = $('#remote_cat_id').val()
      durum2  = $('.catsec').serialize()




      if (parseInt(durum) != 0 && durum2.length > 0) {



         $.ajax({
            type: 'POST',
            url: '/crudv4/hb/saveMatchCat',
            data: $(".sendly").serialize(),
            success: function(data){

               $("#eslesenler").html(data)

               $('#remote_cat_id').val(0)
               $('#remotecat').val(0)

            }
         });


      }else{

         if (parseInt(durum) == 0) {
            sole("Hata","HB Kategorisi Seçmediniz.")
         }
         else if (durum2.length == 0) {
            sole("Hata","Site Kategorisi Seçmediniz.En az bir Kategori Seçiniz")
         }else{
            sole("Hata","Eşleşecek Kategorileri Seçmediniz")
         }
      }
      
   });






</script>

@endsection