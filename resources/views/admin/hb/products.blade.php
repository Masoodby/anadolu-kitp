@extends('admin.layouts.master')
@section('title', $title)
@section('body')

@php

$products = json_decode($products);
$urunler = $products->listings;

@endphp
<div class="col-sm-12 col-lg-12">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body"  >



			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Ürün</th>

						<th scope="col">Fiyatı</th>
						<th scope="col">Adet</th>
						<th scope="col">Kargo</th>
						<th scope="col">Son Güncelleme</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($urunler as $var)
					@php

					@endphp
					
					<tr>
						<td scope="row">{{$loop->iteration}}</td>
						<td>{{ $var->merchantSku }}<br>{{ $var->hepsiburadaSku }}</td>
						<td>{{ $var->price }}</td>
						<td>{{ $var->availableStock }}</td>
						<td>{{ $var->cargoCompany1 }}</td>
						<td>  
							bilinmiyor

						</td>
					</tr>

					@endforeach


				</tbody>
			</table>

		</div>
	</div>

</div>	



<nav  class="col-md-6 float-right text-right">
	<ul class="pagination">


		

	</ul>
</nav>





@endsection


@section('script')



@endsection