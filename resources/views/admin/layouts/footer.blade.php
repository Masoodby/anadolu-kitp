   <footer class="bg-white iq-footer">
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-6">
               <ul class="list-inline mb-0">
                  <li class="list-inline-item"><a href="#">Destek</a></li>
                  <li class="list-inline-item"><a href="#">Lisans Sözleşmesi</a></li>
               </ul>
            </div>
            <div class="col-lg-6 text-right">
               Copyright 2020 <a href="#">Eticarix</a> Dijital Pazarlama Sistemleri
            </div>
         </div>
      </div>
   </footer>