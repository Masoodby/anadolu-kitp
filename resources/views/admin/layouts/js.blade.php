   <script src="{{ asset('dist/panel/js/jquery.min.js')}}"></script>
   <script src="{{ asset('dist/panel/js/popper.min.js')}}"></script>
   <script src="{{ asset('dist/panel/js/bootstrap.min.js')}}"></script>
   <!-- Appear JavaScript -->
   <script src="{{ asset('dist/panel/js/jquery.appear.js')}}"></script>
   <!-- Countdown JavaScript -->
   <script src="{{ asset('dist/panel/js/countdown.min.js')}}"></script>
   <!-- Counterup JavaScript -->
   <script src="{{ asset('dist/panel/js/waypoints.min.js')}}"></script>
   <script src="{{ asset('dist/panel/js/jquery.counterup.min.js')}}"></script>
   <!-- Wow JavaScript -->
   <script src="{{ asset('dist/panel/js/wow.min.js')}}"></script>
   <!-- Apexcharts JavaScript -->
   <script src="{{ asset('dist/panel/js/apexcharts.js')}}"></script>
   <!-- Slick JavaScript -->
   <script src="{{ asset('dist/panel/js/slick.min.js')}}"></script>
   <script src="{{ asset('dist/panel/libs/tags/tagsinput.js')}}"></script>
   <!-- Select2 JavaScript -->
   <script src="{{ asset('dist/panel/js/select2.min.js')}}"></script>
   <!-- Owl Carousel JavaScript -->
   <script src="{{ asset('dist/panel/js/owl.carousel.min.js')}}"></script>
   <!-- Magnific Popup JavaScript -->
   <script src="{{ asset('dist/panel/js/jquery.magnific-popup.min.js')}}"></script>
   <!-- Smooth Scrollbar JavaScript -->
   <script src="{{ asset('dist/panel/js/smooth-scrollbar.js')}}"></script>
   <!-- lottie JavaScript -->
   <script src="{{ asset('dist/panel/js/lottie.js')}}"></script>
   <!-- Chart Custom JavaScript -->
 
   <script src="{{ asset('dist/panel/js/core.js')}}"></script>

   <script src="{{ asset('dist/panel/js/kelly.js')}}"></script>
   <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
   <script src="{{ asset('dist/panel/js/chart-custom.js')}}"></script>


   <script src="{{ asset('dist/panel/js/animated.js')}}"></script>
   <script src="{{ asset('dist/panel/js/notify.min.js')}}"></script>
   <script src="{{ asset('dist/panel/js/bootbox.min.js')}}"></script>

   <script src="{{ asset('dist/panel/js/jquery.toast.min.js')}}"></script>
   <!-- Custom JavaScript -->
   <script src="{{ asset('dist/panel/libs/scrollable/perfect-scrollbar.min.js')}}"></script>
   <script src="{{ asset('dist/panel/libs/scrollable/scrollable-custom.js')}}"></script>
   <script src="{{ asset('dist/panel/js/custom.js')}}"></script>
   <script src="{{ asset('dist/panel/js/app.js')}}"></script>

   @yield('script')
