<!doctype html>
<html lang="tr">
<head>
 <!-- Required meta tags -->
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <title>@yield('title', config('app.name') . " | Yönetim")</title>
 @include('admin.layouts.src')
</head>
<body>
 <!-- loader Start -->
 @include('admin.layouts.loader')
 <!-- loader END -->
 <!-- Wrapper Start -->
 <div class="wrapper">
  <!-- Sidebar  -->
  @include('admin.layouts.side')
  <!-- TOP Nav Bar -->
  @include('admin.layouts.top')

  <!-- TOP Nav Bar END -->
  <!-- Responsive Breadcrumb End-->
  <!-- Page Content  -->
  <div id="content-page" class="content-page">
   <div class="container-fluid">
    <div class="row">

     
      @yield('body')

    </div>
  </div>
</div>
</div>
<!-- Wrapper END -->
<!-- Footer -->
@include('admin.layouts.footer')
@include('admin.layouts.notify')
<!-- Footer END -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
@include('admin.layouts.js')

</body>
</html>
