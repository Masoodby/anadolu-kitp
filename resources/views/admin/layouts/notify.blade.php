@if(session()->has('message'))
<div class="toast fade bg-success text-white border-0" id="alertmessage" role="alert" aria-live="assertive" aria-atomic="true">
 <div class="toast-header bg-success text-white">
  <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
   <rect width="100%" height="100%" fill="#fff"></rect>
 </svg>
 <strong class="mr-auto text-white">İşlem {{ session()->get('status')=="success"?"Başarılı":"Başarısız" }}</strong>

 <button type="button" class="ml-2 mb-1 close text-white" onclick="$('#alertmessage').hide()" data-dismiss="toast" aria-label="Close">
   <span aria-hidden="true">×</span>
 </button>
</div>
<div class="toast-body">
  {{ session()->get('message') }}
</div>
</div>

@section('script')
<script>
 $("#alertmessage").addClass("show");

 setTimeout(function(){
   $("#alertmessage").removeClass("show");
 },5000)
</script>
@endsection

@endif