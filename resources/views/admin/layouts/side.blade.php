    <div class="iq-sidebar">
      <div class="iq-sidebar-logo d-flex justify-content-between">
       <a href="{{ route('crudv4.home')}}">
        <img src="{{ asset('dist/panel/favicon.png')}}" class="img-fluid" alt="">
        <span class="text-capitalize" style="color: #957AF2">Ticarix</span>
      </a>
      <div class="iq-menu-bt-sidebar">
        <div class="iq-menu-bt align-self-center">
         <div class="wrapper-menu">
          <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
          <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div id="sidebar-scrollbar">
   <nav class="iq-sidebar-menu">

    <ul id="iq-sidebar-toggle" class="iq-menu">



     @foreach (sideNav() as $key => $nav)


     @if ($nav['sub'])



     <li class="{{ Route::currentRouteName() == $nav['link'] ?'active':''}}" >
      <a href="#menu-{{ $key }}" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="false"><i class="{{ $nav['icon'] }}"></i><span>{{ $nav["name"] }}</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
      <ul id="menu-{{ $key }}" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

        @foreach ($nav["sub"]  as $key1 => $item)


        @if ($item['sub'])

        <li><a href="#sub-menu-{{ $key1 }}" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="false"><i class="{{ $item['icon'] }}"></i><span>{{ $item["name"] }}</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>

          <ul id="sub-menu-{{ $key1 }}" class="iq-submenu iq-submenu-data collapse">
            @foreach ($item['sub'] as $element)
            <li class="{{ Route::currentRouteName() == $element['link'] ?'active':''}}" ><a href="{{ route($element['link']) }}"><i class="{{ $element['icon'] }}"></i>{{ $element['name'] }}</a></li>
            @endforeach


          </ul>
        </li>
        @else
        <li class="{{ Route::currentRouteName() == $item['link'] ?'active li_active':''}}"><a href="{{ route($item['link']) }}"><i class="{{ $item['icon'] }}"></i>{{ $item['name'] }}</a></li>

        @endif

        @endforeach
      </ul>
    </li>

    @else 

    <li>
     <a href="{{ route($nav['link']) }}" class="iq-waves-effect"><i class="{{ $nav["icon"] }}"></i><span>{{ $nav["name"] }}</span></a>
   </li>

   @endif

   @endforeach


 </ul>
</nav>
<div class="p-3"></div>
</div>
</div>
