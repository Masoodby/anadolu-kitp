 <link rel="shortcut icon" href="{{ asset('dist/panel/favicon.png')}}" type="image/x-icon">
 <link rel="icon" href="{{ asset('dist/panel/favicon.png')}}" type="image/x-icon">
 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="{{ asset('dist/panel/css/bootstrap.min.css')}}">
 <!-- Typography CSS -->
 <link rel="stylesheet" href="{{ asset('dist/panel/css/typography.css')}}">
 <!-- Style CSS -->
 <link rel="stylesheet" href="{{ asset('dist/panel/css/style.css')}}">
 <link rel="stylesheet" href="{{ asset('dist/panel/libs/tags/tagsinput.css')}}">
 <link rel="stylesheet" href="{{ asset('dist/panel/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{ asset('dist/panel/css/jquery.toast.min.css')}}">
 <!-- Responsive CSS -->
 <link rel="stylesheet" href="{{ asset('dist/panel/css/responsive.css')}}">
 <link rel="stylesheet" href="{{ asset('dist/panel/libs/scrollable/scrollable.css')}}">
 <link rel="stylesheet" href="{{ asset('dist/panel/css/mp.css')}}">
 <link rel="stylesheet" href="{{ asset('dist/panel/css/tema.css')}}">
 <meta name="csrf-token" content="{{ csrf_token() }}">

 @yield('style')