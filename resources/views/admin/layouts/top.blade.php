      <div class="iq-top-navbar">
         <div class="iq-navbar-custom">
            <div class="iq-sidebar-logo">
               <div class="top-logo">
                  <a href="index.html" class="logo">
                     <img src="{{ asset('dist/panel/images/logo.gif')}}" class="img-fluid" alt="">
                     <span>Eticarix</span>
                  </a>
               </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light p-0">
               <div class="navbar-left">
                  <ul id="topbar-data-icon" class="d-flex p-0 topbar-menu-icon">
                     <li class="nav-item">
                       <a href="index.html" class="nav-link font-weight-bold search-box-toggle"><i class="ri-home-4-line"></i></a>
                    </li>
                    <li><a href="chat.html" class="nav-link"><i class="ri-message-line"></i></a></li>
                    <li><a href="e-commerce-product-list.html" class="nav-link"><i class="ri-file-list-line"></i></a></li>
                    <li><a href="profile.html" class="nav-link"><i class="ri-question-answer-line"></i></a></li>
                    <li><a href="todo.html" class="nav-link router-link-exact-active router-link-active"><i class="ri-chat-check-line"></i></a></li>
                    <li><a href="app/index.html" class="nav-link"><i class="ri-inbox-line"></i></a></li>
                 </ul>
                 <div class="iq-search-bar">
                  <form action="#" class="searchbox">
                     <input type="text" class="text search-input" placeholder="Sipariş Ara..."> 
                     <a class="search-link" href="#"><i class="ri-search-line"></i></a>

                  </form>
               </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"  aria-label="Toggle navigation">
               <i class="ri-menu-3-line"></i>
            </button>
            <div class="iq-menu-bt align-self-center">
               <div class="wrapper-menu">
                  <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
                  <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>
               </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav ml-auto navbar-list">


                  <li class="nav-item">
                     <a href="#" class="search-toggle iq-waves-effect">
                        <div id="lottie-beil"></div>
                        <span class="bg-danger dots"></span>
                     </a>
                     <div class="iq-sub-dropdown">
                        <div class="iq-card shadow-none m-0">
                           <div class="iq-card-body p-0 ">
                              <div class="bg-primary p-3">
                                 <h5 class="mb-0 text-white">Yönetici Bildirimleri<small class="badge  badge-light float-right pt-1">0</small></h5>
                              </div>

                              @if (bildirim())
                              {{-- expr --}}

                              <a href="#" class="iq-sub-card" >
                                 <div class="media align-items-center">
                                    <div class="">
                                       <img class="avatar-40 rounded" src="{{ asset('dist/panel/images/user/01.jpg')}}" alt="">
                                    </div>
                                    <div class="media-body ml-3">
                                       <h6 class="mb-0 ">Emma Watson Nik</h6>
                                       <small class="float-right font-size-12">Just Now</small>
                                       <p class="mb-0">95 MB</p>
                                    </div>
                                 </div>
                              </a>

                              @else
                              <b class="p30 lh50">Bildirim Bulunmamaktadır.</b>
                              @endif



                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a href="#" class="search-toggle iq-waves-effect">
                      <div id="lottie-mail"></div>
                      <span class="bg-primary count-mail"></span>
                   </a>
                   <div class="iq-sub-dropdown">
                     <div class="iq-card shadow-none m-0">
                        <div class="iq-card-body p-0 ">
                           <div class="bg-primary p-3">
                              <h5 class="mb-0 text-white">Sistem Mesajlar<small class="badge  badge-light float-right pt-1">0</small></h5>
                           </div>

                           @if (mesajlar())
                           <a href="#" class="iq-sub-card" >
                              <div class="media align-items-center">
                                 <div class="">
                                    <img class="avatar-40 rounded" src="{{ asset('dist/panel/images/user/05.jpg')}}" alt="">
                                 </div>
                                 <div class="media-body ml-3">
                                    <h6 class="mb-0 ">Lorem Ipsum generators</h6>
                                    <small class="float-left font-size-12">5 Dec</small>
                                 </div>
                              </div>
                           </a>

                           @else
                           <b class="p30 lh50">Mesaj Bulunmamaktadır.</b>
                           @endif


                        </div>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
         <ul class="navbar-list">
            <li class="bg-primary rounded">
               <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center">
                  <img src="{{ asset('dist/panel/images/user/1.jpg')}}" class="img-fluid rounded mr-3" alt="user">
                  <div class="caption">
                     <h6 class="mb-0 line-height text-white">{{ loginUser()->name }}</h6>
                     <span class="font-size-12 text-white">Yönetici</span>
                  </div>
               </a>
               <div class="iq-sub-dropdown iq-user-dropdown">
                  <div class="iq-card shadow-none m-0">
                     <div class="iq-card-body p-0 ">
                        <div class="bg-primary p-3">
                           <h5 class="mb-0 text-white line-height">{{ loginUser()->fullname }}</h5>
                           <span class="text-white font-size-12">Administrator</span>
                        </div>
                        <a href="{{ route('crudv4.user.admin') }}" class="iq-sub-card iq-bg-primary-hover">
                           <div class="media align-items-center">
                              <div class="rounded iq-card-icon iq-bg-primary">
                                 <i class="ri-file-user-line"></i>
                              </div>
                              <div class="media-body ml-3">
                                 <h6 class="mb-0 ">Yönetici Ayarları</h6>
                                 <p class="mb-0 font-size-12">Bilgileri görüntüle.</p>
                              </div>
                           </div>
                        </a>
                        <a href="{{ route('crudv4.settings.index') }}" class="iq-sub-card iq-bg-primary-hover">
                           <div class="media align-items-center">
                              <div class="rounded iq-card-icon iq-bg-primary">
                                 <i class="ri-settings-4-fill"></i>
                              </div>
                              <div class="media-body ml-3">
                                 <h6 class="mb-0 ">Site Ayarları</h6>
                                 <p class="mb-0 font-size-12">Genel Ayarlar.</p>
                              </div>
                           </div>
                        </a>

                        <a href="{{ set('domain') }}" target="_blank" class="iq-sub-card iq-bg-primary-hover">
                           <div class="media align-items-center">
                              <div class="rounded iq-card-icon iq-bg-primary">
                                 <i class="ri-reply-line"></i>
                              </div>
                              <div class="media-body ml-3">
                                 <h6 class="mb-0 ">Siteye Git</h6>
                                 <p class="mb-0 font-size-12">Mağaza Sayfasını Görüntüle.</p>
                              </div>
                           </div>
                        </a>
                        <div class="d-inline-block w-100 text-center p-3">
                           <a class="bg-primary iq-sign-btn" href="{{ route('crudv4.logout') }}" role="button">Çıkış<i class="ri-login-box-line ml-2"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </li>
         </ul>
      </nav>


   </div>
</div>