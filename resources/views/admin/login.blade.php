<!doctype html>
<html lang="tr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Eticarix Yönetim Paneli') }}</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('dist/panel/favicon.png')}}" />

    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('dist/panel/css/bootstrap.min.css')}}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{{ asset('dist/panel/css/typography.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('dist/panel/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('dist/panel/css/responsive.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/panel/css/mp.css')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <!-- loader Start -->
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <!-- loader END -->
    <!-- Sign in Start -->
    <section class="sign-in-page">
        <div class="container bg-white mt-5 p-0">
            <div class="row no-gutters">
                <div class="col-sm-6 align-self-center">
                    <div class="sign-in-from">
                        <h1 class="mb-0">Yönetici Giriş Paneli</h1>
                        <p>Yönetici Girişi</p>

                        <form method="POST" action="{{ route('crudv4.login') }}"  class="mt-4">
                            @csrf


                            <div class="form-group">
                                <label for="email">E-Posta Adresi</label>
                                <input id="email" type="email" class="form-control  mb-0 @error('email') is-invalid @enderror"  placeholder="E-Posta Adresi" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>



                            <div class="form-group">
                                <label for="password">Şifre</label>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>


                            <div class="d-inline-block w-100">
                                <div class="custom-control custom-checkbox d-inline-block mt-2 pt-1">
                                    <input type="checkbox" class="custom-control-input" id="remember"  name="remember"  {{ old('remember') ? 'checked' : '' }}  >
                                    <label class="custom-control-label" for="remember">  Beni Hatırla</label>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">  Giriş Yap</button>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-6 text-center">
                    <div class=" bg-dark pt30 pb30 text-white">
                        <a class="sign-in-logo mb-5" href="#"><img src="{{ asset('dist/panel/images/logo-white.png')}}" class="img-fluid" alt="logo"></a>
                        <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="0">
                            <div class="item">
                                <img src="{{ asset('dist/panel/images/login/1.png')}}" class="img-fluid mb-4" alt="logo">
                                <h4 class="mb-1 text-white">Gelişmiş Eticaret Sistemleri</h4>
                                <p>Geleneksel değil Yenilikçi. En Kolay Kullanım Garantisi</p>
                            </div>
                            <div class="item">
                                <img src="{{ asset('dist/panel/images/login/1.png')}}" class="img-fluid mb-4" alt="logo">
                                <h4 class="mb-1 text-white">Ürün Bazlı Kişiselleştirme altyapısı</h4>
                                <p>Dilediğiniz ürünü tasarlanabilir hale getirin.</p>
                            </div>
                            <div class="item">
                                <img src="{{ asset('dist/panel/images/login/1.png')}}" class="img-fluid mb-4" alt="logo">
                                <h4 class="mb-1 text-white">Pazaryerleriyle Entegrasyon</h4>
                                <p>Satışınızı global yapabilme adına tüm pazaryerleriyle entegrasyon sağlayın.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Sign in END -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('dist/panel/js/jquery.min.js')}}"></script>
    <script src="{{ asset('dist/panel/js/popper.min.js')}}"></script>
    <script src="{{ asset('dist/panel/js/bootstrap.min.js')}}"></script>
    <!-- Appear JavaScript -->
    <script src="{{ asset('dist/panel/js/jquery.appear.js')}}"></script>
    <!-- Countdown JavaScript -->


    <!-- Owl Carousel JavaScript -->
    <script src="{{ asset('dist/panel/js/owl.carousel.min.js')}}"></script>
    <!-- Magnific Popup JavaScript -->
    <script src="{{ asset('dist/panel/js/jquery.magnific-popup.min.js')}}"></script>

    <!-- Custom JavaScript -->
    <script src="{{ asset('dist/panel/js/custom.js?v=2')}}"></script>
</body>
</html>