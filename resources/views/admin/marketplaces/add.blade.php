@extends('admin.layouts.master')
@section('title', $title)
@section('body')
<form action="{{ route('crudv4.marketplaces.save') }}" class="container-fluid row" id="catform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}
	@include('admin.layouts.errors')
	@include('admin.layouts.alert')
	<div class="col-sm-12 col-lg-6">
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">
				<div class="row">
					<div class="form-group col-6">
						<label  >Pazar Yeri:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<select name="name" class="form-control req">
								<option value="">Seçiniz</option>
								@foreach (setpar('market_places') as $var)
								<option value="{{ $var}}">{{ $var }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group col-6">
						<label  >Kargo Seçeneği:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<select name="cargo" class="form-control req">
								<option value="">Seçiniz</option>
								@foreach ($cargoList as $key => $var)
								<option value="{{ $key }}">{{ $var }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-6">
						<label  >Satıcı Kimlik No:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="user_no" value="">
						</div>
					</div>
					<div class="form-group col-6">
						<label  >Api Kullanıcı Anahtarı</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="api_id" value="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-12">
						<label  >Api Anahtar Şifresi:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="api_key" value="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>0-10TL</small> </label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_0-10" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
                    <div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_0-10" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>
                </div>
                <hr>
                <div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>10-20TL</small></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_10-20" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
                    <div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_10-20" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>
                </div>
                <hr>
                <div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>20-30TL</small></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_20-30" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
                    <div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_20-30" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>
                </div>
                <hr>
                <div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>30-40TL</small></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_30-40" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
                    <div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_30-40" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>
                </div>
                <hr>
                <div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>40-50TL</small></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_40-50" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
                    <div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_40-50" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>

                </div>
                <div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>50-100TL</small></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_50-100" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
                    <div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_50-100" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>

                </div>
                <hr>
                <div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi: <small>100 liradan fazla</small></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_100-1000" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
					<div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_100-1000" value="">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<button type="button" data-id="catform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
	</div>
	<div class="col-sm-12 col-lg-6">
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Pazar Yerleri </h4>
				</div>
			</div>
			<div class="iq-card-body">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Pazar Yeri</th>
							<th scope="col">Oranlar</th>
							<th scope="col">Kargo</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($list as $var)
						<tr id="tr_{{ $var->id }}">
							<td scope="row">{{$loop->iteration}}</td>
							<td>
								{{ $var->name }}
							</td>
							<td>%{{ $var->percent_unit }} + <small>{{ $var->price_unit }} TL</small> </td>
							{{-- <td> {{ App\Library\Tryol::cargo_list($var->cargo) }} </td> --}}
							<td>
								<div class="flex align-items-center list-user-action">
									<a href="{{ route('crudv4.marketplaces.usePrice',$var->id) }}" title="Trendyol'dan Fiyat Değişikliğini Uygula" target="_blank" class="iq-bg-secondary tipsi"><i class="ri-refresh-fill"></i></a>
									<a class="iq-bg-primary tipsi" title="Düzenle" href="{{ route('crudv4.marketplaces.edit',$var->id) }}" ><i class="ri-pencil-line"></i></a>
									<a  class="iq-bg-primary tipsi" title="Sil"  onclick="allConfirm('eraseci',{{ $var->id }})" href="javascript:;"><i class="ri-delete-bin-line"></i></a>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
@endsection
@section('script')
<script>
	function eraseci(id) {
		$.ajax({
			type: 'GET',
			url: '/crudv4/oda/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			}

		});
	}
</script>
@endsection
