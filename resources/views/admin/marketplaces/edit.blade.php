@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.marketplaces.save', $entry->id) }}" class="container-fluid row" id="catform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')
	<div class="col-sm-12 col-lg-6">
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
				<div class="col-md-4">
					<a href="{{ route('crudv4.marketplaces.add') }}" class="btn btn-primary float-right btn-sm"><i class="fa fa-plus"></i>Yeni Ekle</a>
				</div>
			</div>
			<div class="iq-card-body">
				<div class="row">
					<div class="form-group col-6">
						<label  >Pazar Yeri:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<select name="name" class="form-control req">
								<option value="">Seçiniz</option>
								@foreach (setpar('market_places') as $var)
								<option value="{{ $var}}" {{ $var == $entry->name?"selected":"" }}>{{ $var }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group col-6">
						<label  >Kargo Seçeneği:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<select name="cargo" class="form-control req">
								<option value="">Seçiniz</option>
								@foreach ($cargoList as $key => $var)
								<option value="{{ $key }}" {{ $key == $entry->cargo?"selected":"" }}>{{ $var }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-6">
						<label  >Satıcı Kimlik No:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="user_no" value="{{ $entry->user_no }}">
						</div>
					</div>
					<div class="form-group col-6">
						<label  >Api Kullanıcı Anahtarı</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="api_id" value="{{ $entry->api_id }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-12">
						<label  >Api Anahtar Şifresi:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="api_key" value="{{ $entry->api_key }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-6">
						<label  >Artış Yüzdesi:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req int" name="percent_unit" value="{{ $entry->percent_unit }}">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >%</span>
							</div>
						</div>
					</div>
					<div class="form-group col-6">
						<label  >TL Ekle:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req float" name="price_unit" value="{{ $entry->price_unit }}">
							<div class="input-group-append">
								<span class="input-group-text  bg-light"  >TL</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="button" data-id="catform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
	</div>
	<div class="col-sm-12 col-lg-6">
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Pazar Yerleri </h4>
				</div>
			</div>
			<div class="iq-card-body">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Pazar Yeri</th>
							<th scope="col">Oranlar</th>
							<th scope="col">Kargo</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($list as $var)
						<tr id="tr_{{ $var->id }}">
							<td scope="row">{{$loop->iteration}}</td>
                            <td>
								{{ $var->name }}
							</td>
							<td>%{{ $var->percent_unit }} + <small>{{ $var->price_unit }} TL</small> </td>
							<td> {{ App\Library\Tryol::cargo_list($var->cargo) }} </td>
                            <td>
								<div class="flex align-items-center list-user-action">
									<a href="{{ route('crudv4.marketplaces.usePrice',$var->id) }}" title="Trendyol'dan Fiyat Değişikliğini Uygula" target="_blank" class="iq-bg-secondary tipsi"><i class="ri-refresh-fill"></i></a>
									<a class="iq-bg-primary tipsi" title="Düzenle" href="{{ route('crudv4.marketplaces.edit',$var->id) }}" ><i class="ri-pencil-line"></i></a>
									<a  class="iq-bg-primary tipsi" title="Sil"  onclick="allConfirm('eraseci',{{ $var->id }})" href="javascript:;"><i class="ri-delete-bin-line"></i></a>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>
		</div>

	</div>



</form>


@endsection


@section('script')
<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/oda/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			}

		});
	}


</script>


@endsection
