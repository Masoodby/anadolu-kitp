 <div style="height: 500px; overflow-y: auto;">
 	<div class="row">

 		<button class="btn btn-primary mr10 ml30 float-left col-3">Son Durum : {{ $result->status }}</button>
 		<button class="btn btn-dark mr10 float-left col-3">İşlem Tarihi : {{ gmdate('d.m.Y H:i' , ($result->creationDate/1000)) }}</button>
 		<button class="btn btn-secondary filterci mr10 float-left col-2" data-durum="tr">Ürün Adedi : {{ $result->itemCount }}</button>
 		<button class="btn btn-danger filterci mr10 float-left col-2" data-durum="FAILED">Hata Adedi : {{ $result->failedItemCount }}</button>
 	</div>


 	<table class="table">
 		<thead>
 			<tr>
 				<th scope="col">#</th>
 				<th scope="col">Barkod</th>
 				<th scope="col">Satış Fiyatı</th>
 				<th scope="col">Liste Fiyatı</th>
 				<th scope="col">Adet</th>
 				<th scope="col">Durum</th>


 			</tr>
 		</thead>
 		<tbody>

 			@foreach ($result->items as $var)

 			<tr class="tr {{ $var->status}}">

 				<td scope="row">{{$loop->iteration}}</td>
 				<td>{{ @$var->requestItem->barcode }} </td>
 				<td>{{ @$var->requestItem->salePrice }} </td>
 				<td>{{ @$var->requestItem->listPrice }} </td>
 				<td>{{ @$var->requestItem->quantity }} </td>

 				<td>
 					<b>{{ @$var->status}} </b>
 					<ul>
 						@foreach ($var->failureReasons as $nots)
 						<li>{{  $nots }}</li>
 						@endforeach

 					</ul>
 				</td>

 			</tr>

 			@endforeach


 		</tbody>
 	</table>

 </div>

 <script>
 	$('.filterci').click(function() {
 		durum = $(this).data('durum')

 		$('.tr').hide()
 		$('.'+durum).show()
 	});
 </script>