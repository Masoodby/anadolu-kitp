@extends('admin.layouts.master')
@section('title', $title)
@section('body')




<div class="col-sm-12 col-lg-12">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body">


			<div class="row">

				<div class="form-group col-5">
					<label  >Yayınevleri:</label>


					<div class="input-group ">
						<select name="localbrand" id="localbrand"   class="form-control sel2">

							<option value="0">Seçiniz</option>

							@foreach ($yayinci as $var)

							<option value="{{ $var->id }}">{{ $var->yayinci_name }}</option>
							@endforeach

						</select>
					</div>

				</div>


				<div class="form-group col-5">
					<label  >Trendyol Markaları:</label>
					<div class="input-group">

						<input type="text" class="form-control"  id="remotebrand" data-id="0" placeholder="Trendyolda Marka Arayınız..">
						<div class="input-group-append">
							<span class="input-group-text  bg-default"  ><i class="fa fa-search"></i></span>
						</div>
					</div>

					<div id="remote_brand_result" class="dnone col-md-10 pr5 pl0 bg-white pos-a"></div>
				</div>


				<div class="col-md-2 pt40">
					<button type="button"  class="btn btn-block btn-primary save_marka"><i class="fa fa-refresh"></i> Marka Eşleştir</button>
				</div>

				

			</div>


		</div>
	</div>

</div>	




<div class="col-sm-12 col-lg-12">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body" id="eslesenler">


			@if($matchbrand)

			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Site Markaları</th>
							<th scope="col">TrendYol Markaları</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($matchbrand as $row)
						{{-- expr --}}
						<tr id="tr_{{ $row->id }}">

							<td scope="row">{{$loop->iteration}}</td>

							<td>{{ $row->local_name }} ({{ $row->local_id }})</td>
							<td>{{ $row->remote_name }} ({{ $row->remote_id }})</td>



							<td>

								<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

							</td>
						</tr>

						@endforeach


					</tbody>
				</table>
			</div>


			@else
			<div class="alert alert-info">Eşleştirilmiş bir <b>Markanız</b> bulunmamaktadır.</div>

			@endif

		</div>
	</div>

</div>	









@endsection


@section('script')

<script>


	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/trendyol/delbrand/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}



	$('.sel2').select2()


	$("#localbrand").change(function() {


		var inpVal = $(this).children('option:selected').text();


		$.ajax({
			type: 'POST',
			url: '/crudv4/trendyol/remotebrand',
			data: {ara:inpVal},
			success: function(data){

				$("#remote_brand_result").css("z-index","9999");
				$("#remote_brand_result").slideDown();
				$("#remote_brand_result").html(data)

			}
		});


	});
	


	$("#remotebrand").on('keyup', function (e) {

		var inpVal = $(this).val();

		if (inpVal.length > 2) {
			
			var inpVal = $(this).val();

			$.ajax({
				type: 'POST',
				url: '/crudv4/trendyol/remotebrand',
				data: {ara:inpVal},
				success: function(data){

					$("#remote_brand_result").css("z-index","9999");
					$("#remote_brand_result").slideDown();
					$("#remote_brand_result").html(data)

				}
			});


		}else{

			$("#remote_brand_result").slideUp();
			$("#remote_brand_result").html("")
		}
	});





	$('.save_marka').click(function() {


		remotebrand_name = $('#remotebrand').val()
		remotebrand_id = $('#remotebrand').data("id")
		localbrand_name = $('#localbrand').children('option:selected').text();
		localbrand_id = $('#localbrand').val()



		if (remotebrand_name != "" && remotebrand_id !="" && localbrand_name !="" && localbrand_id !="") {

			$.ajax({
				type: 'POST',
				url: '/crudv4/trendyol/save_brand',
				data: {local_id:localbrand_id,remote_id:remotebrand_id,local_name:localbrand_name,remote_name:remotebrand_name},
				success: function(data){

					$("#eslesenler").html(data)
					$('#remotebrand').val("")
					$('#remotebrand').data("id","")
					$('#localbrand').children('option:selected').remove();
					$('#localbrand').children('option:first').attr("selected","selected");

				}
			});


		}else{
			soleh("Hata","Marka Seçmediniz")
		}
	});


</script>

@endsection