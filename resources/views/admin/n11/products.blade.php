@extends('admin.layouts.master')
@section('title', $title)
@section('body')

@php


$totalElements = $pagingData->totalCount;
$totalPages = $pagingData->pageCount;
$page = $pagingData->currentPage;
$curpage = $page+1;
$next = $page+2;
@endphp
<div class="col-sm-12 col-lg-12">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body"  >

			<div class="col-md-12 text-right ">
				<b>Toplam ürün : {{ $totalElements }}</b>

			</div>

			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Ürün</th>
						<th scope="col">Barkod</th>
						<th scope="col">Adet</th>
						<th scope="col">Fiyatı</th>
						<th scope="col">Satış Durumu</th>
						<th scope="col">Onay Durumu</th>


					</tr>
				</thead>
				<tbody>
					@foreach ($products as $var)
					@php
				//	prep($var);
					@endphp
					
					<tr>
						<td scope="row">{{$loop->iteration}}</td>
						<td>{{ $var->title }}</td>
						<td><a href="{{ url('crudv4/product?&product_stock_code='. $var->stockItems->stockItem->gtin ) }}" target="_blank">{{ $var->stockItems->stockItem->gtin }}</a></td>
						<td>{{ $var->stockItems->stockItem->quantity }}</td>
						<td>{{ $var->displayPrice }}</td>
						<td>{{ n11SaleStatus($var->saleStatus) }}</td>
						<td>{{ n11ApprovalStatus($var->saleStatus) }}</td>

					</tr>

					@endforeach


				</tbody>
			</table>

		</div>
	</div>

</div>	



<nav  class="col-md-6 float-right text-right">
	<ul class="pagination">


		@if ($curpage > 1)
		<li class="page-item"><a class="page-link" href="{{ route('crudv4.n11.products',$page) }}"><i class="fa fa-angle-left"></i></a></li>
		@endif

		@for ($i = 1; $i <= $totalPages ; $i++)

		@if ($i == $curpage + 10)
		<li class="page-item"><a class="page-link" href="{{ route('crudv4.n11.products',$i) }}">...</a></li>
		@endif
		@if ($i < $curpage + 10 && $i>$curpage - 10 )

		<li class="page-item"><a class="page-link" href="{{ route('crudv4.n11.products',$i) }}"><?php echo $i ?></a></li>
		@endif



		@if ($i >  $totalPages - 10 )

		<li class="page-item"><a class="page-link" href="{{ route('crudv4.n11.products',$i) }}"><?php echo $i ?></a></li>

		@endif



		@endfor


		@if ($curpage < $totalPages)
		<li class="page-item"><a class="page-link" href="{{ route('crudv4.trendyol.products',$next) }}"><i class="fa fa-angle-right"></i></a></li>
		@endif



	</ul>
</nav>





@endsection


@section('script')



@endsection