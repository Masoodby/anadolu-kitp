@extends('admin.layouts.master')
@section('title', $title)
@section('body')


@foreach ($odaAdetler as $item)


<div class="col-sm-6 col-md-6 col-lg-3">
	<div class="iq-card iq-card-block iq-card-stretch iq-card-height">
		<div class="iq-card-body">
			<div class="d-flex align-items-center justify-content-between">
				<h6>{{ $item->deponame }} -> {{ $item->odaname }} </h6>
				<span class="iq-icon"><i class="ri-information-fill"></i></span>
			</div>
			<div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
				<div class="d-flex align-items-center">
					<div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-inbox-fill"></i></div>
					<h2>{{  $item->adet }}</h2>
				</div>
				<div class="iq-map text-primary font-size-32"><i class="ri-bar-chart-grouped-line"></i></div>
			</div>
		</div>
	</div>
</div>

@endforeach


<form action="{{ route('crudv4.oda.save') }}" class="container-fluid row" id="catform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">


				<div class="row">

					<div class="form-group col-6">
						<label  >Oda Tanımı:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="oda_name" value="">
						</div>
					</div>



					<div class="form-group col-6">
						<label  >Depo:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<select name="depo_id" class="form-control req">
								@foreach ($depos as $var)

								<option value="{{ $var->id }}">{{ $var->depo_name }}</option>

								@endforeach
							</select>
						</div>
					</div>
				</div>

				
				<div class="row">

					<div class="form-group col-6">
						<label  >Yatay Raf Adedi:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="oda_yatay" value="">
						</div>
					</div>

					<div class="form-group col-6">
						<label  >Dikey Raf Adedi:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="oda_dikey" value="">
						</div>
					</div>

				</div>	

				<div class="row">

					<div class="form-group col-6">
						<label  >Başlangıç No:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
							</div>
							<input type="text" class="form-control req" name="oda_start" value="">
						</div>
					</div>


				</div>

			</div>
		</div>

		<button type="button" data-id="catform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>

	</div>	

	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Odalar </h4>
				</div>



			</div>
			<div class="iq-card-body">

				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Oda Tanımı</th>
							<th scope="col">Raf Dizilimi</th>
							<th scope="col">Aktif Raf</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>

						@foreach ($odas as $var)

						<tr id="tr_{{ $var->id }}">

							<td scope="row">{{$loop->iteration}}</td>
							<td>
								<a href="{{ route('crudv4.raf.list',$var->id) }}" >
									{{ $var->oda_name }}</a>
								</td>
								<td>{{ $var->oda_yatay }}x{{ $var->oda_dikey }} <small>({{ $var->oda_start }} )</small></td>
								<td>{{ $var->getRaf->count() }} Adet</td>
								<td>

									<div class="flex align-items-center list-user-action">

										<a class="iq-bg-success tipsi" title="Raflar" href="{{ route('crudv4.raf.list',$var->id) }}" >
											<i class="ri-file-list-line"></i></a>

											<a class="iq-bg-primary tipsi" title="Düzenle" href="{{ route('crudv4.oda.edit',$var->id) }}" ><i class="ri-pencil-line"></i></a>
											<a  class="iq-bg-primary tipsi" title="Sil"  onclick="allConfirm('eraseci',{{ $var->id }})" href="javascript:;"><i class="ri-delete-bin-line"></i></a>

										</div>


									</td>
								</tr>

								@endforeach


							</tbody>
						</table>

					</div>
				</div>

			</div>



		</form>


		@endsection


		@section('script')
		<script>

			function eraseci(id) {

				$.ajax({
					type: 'GET',
					url: '/crudv4/oda/del/' + id,
					success: function () {
						sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
						$('#tr_'+id).remove()
					} 

				});
			}


		</script>


		@endsection