		<textarea   id="resimler_json" class="d-none" data-id="{{ $product->id }}"></textarea>
		<div id="resimlerload" class=" p10 h200">
			
			@foreach (json_decode($product->detail->product_image) as $var)
			<div class="col-md-2 p0  float-left mt5 pos-r resim-item" id="{{ $var }}">
				<div class="handler"></div>
				<img src="{{ res("product",($var),"250x250") }}" class="img-thumbnail "  >

				<button class="btn btn-primary btn-sm  left20 actbtn"><i class="fa fa-upload"></i></button>
				<button class="btn btn-danger btn-sm   left60 actbtn"><i class="fa fa-trash"></i></button>
			</div>
			@endforeach


		</div>