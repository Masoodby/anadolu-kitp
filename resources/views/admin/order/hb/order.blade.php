

<div class="table-responsive">
	<table class="table table-bordered">
		<tr>
			<th>Sipariş Sahibi</th>
			<td>{{ $order->customer->name }}</td>
		</tr>	
		<tr>
			<th>GSM</th>
			<td>{{ $order->deliveryAddress->phoneNumber }}</td>
		</tr>
		<tr>
			<th>Teslimat Adresi</th>
			<td>{{ $order->deliveryAddress->address }}<br>{{ $order->deliveryAddress->district }} / {{ $order->deliveryAddress->town }} / {{ $order->deliveryAddress->city }}</td>
		</tr>
	</table>
</div>
<div class="table-responsive">


	<table class="table table-hover">

		<thead class="thead-light">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Ürün</th>

				<th scope="col">Sipariş Adedi</th>
				<th scope="col">Konum</th>

			</tr>
		</thead>
		<tbody>
			@foreach ($order->items as $var)
			@php
			$sku = $var->sku;
			$pname = $products[$sku]->product_name;
			$isbn = $products[$sku]->product_stock_code;
			@endphp
			<tr>
				<td>{{ $pname }}<br><a target="_blank" href="{{ url('crudv4/product?&product_stock_code='.barkod_rep($isbn)) }}">{{ $isbn}}</a></td>
				<td>{{ $var->quantity }}</td>
				<td>{{ $var->totalPrice->amount }}{{ $var->totalPrice->currency }}</td>
				<td>{{ nerde($isbn,true) }}</td>
			</tr>
			@if ($hb_order)
			@if ($hb_order->depoexit == 0)
			<tr class="bg-light" id="tr_{{ $isbn }}">
				<td colspan="3">
					
					<table class="table table-bordered">


						@foreach (lokasyon($isbn,true); as $key => $item)

						<tr>
							<td>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="raf{{ $isbn }}" value="{{ $isbn.'-'.$key.'-'.$var->quantity }}" {{$loop->iteration == 1?"checked":""}}>
									<label class="form-check-label" for="raf{{ $isbn  }}">
										Konum : {{ $item['oda'].' '.$item['raf'] }}
									</label>
								</div>
							</td>
							<td>
								{{ $item['adet'] ." Adet" }}
							</td>

						</tr>

						@endforeach

						<tr>
							<td colspan="2">
								<button class="btn btn-dark exitdepo float-right" data-id="{{ $order->orderId }}" data-barkod="{{ $isbn }}"  type="button">Depodan Çıkış Yap</button>
							</td>
						</tr>


					</table>
				</td>
			</tr>
			@else

			<tr class="bg-secondary">
				<td colspan="4">
					<p>Gerçek Depodan Çıkış Yapılmış</p>



					@php
					$jsons = strlen($hb_order->exit_json) ? json_decode($hb_order->exit_json,true):[];

					@endphp

					@foreach ($jsons as $keyisbn => $elitem)
					ISBN : {{ $keyisbn }} -> {{ $elitem }}<br>
					@endforeach


				</td>
			</tr>

			@endif
			@endif
			@endforeach


		</tbody>
	</table>




</div>
@if (isset($hb_order->paketli) && strlen($hb_order->paketli) > 3)

<a href="{{ route('crudv4.hb.cargoEtiket',$order->orderNumber) }}" target="_blank" class="btn btn-success">Kargo Etiketi (A5) </a>
<a href="{{ route('crudv4.hb.cargoEtiket2',$order->orderNumber) }}" target="_blank" class="btn btn-info">Kargo Etiketi (100x100)</a>
@endif

@if ($order->items[0]->status =="Open")
<button data-id="{{ $order->orderNumber }}" type="button" class="btn btn-primary float-right picking">Paketlendi olarak Kaydet</button>
@else
<button class="btn btn-secondary float-right">İşleme Alınmış</button>
@endif
<div id="picsonuc"></div>
<script>


	$('.picking').click(function() {

		id = $(this).data('id')

		$.ajax({
			type: 'GET',
			url: '/crudv4/hb/picking/'+id,

			success: function (data) {
				sole('İşlem Başarılı','Sipariş İşleme Alındı');
				paketlendi(id) 

			} 

		});
	});


	$('.exitdepo').click(function() {

		id = $(this).data('id')
		barkod = $(this).data('barkod')

		raf = $('input[name=raf'+barkod+']:checked').val()
		$.ajax({
			type: 'POST',

			url: '/crudv4/trendyol/exitdepo/'+id,
			data:{name:raf},
			success: function (data) {
				$('#tr_'+barkod).html("<td colspan='3' class='alert alert-success'>Reel Depodan Çıkış Başarılı</td>")
			} 

		});
	});



</script>