@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $orders->totalCount }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<a href="{{ url('crudv4/hb/toCargo') }}" class="btn btn-primary float-right">Kargoya Verilecekleri Görüntüle</a>
					</div>

				</div>

			</div>
		</div>
		<div class="iq-card-body">







			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Ad Soyad</th>
							<th scope="col">Total</th>
							<th scope="col">Ürün Adet</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($orders->items as $row)

						<tr id="tr_{{$row->orderNumber}}">
							<td scope="row">
								{{$row->orderNumber}}
								<br>
								
								{{ $row->orderDate }}<br>	Kalan Süre : {{ kalanSure($row->dueDate)}}
							</td>
							<td>{{ $row->customerName }} <br>{{ $row->shippingAddress->city  }}</td>
							<td>{{ price($row->totalPrice->amount) }}</td>
							<td>{{ $row->quantity }}</td>

							<td>
								@if ($status =="Created")
								<button class="btn btn-dark islem" data-id="{{$row->orderNumber}}">İşlem</button>
								@else
								<button class="btn btn-success detayorder" data-id="{{$row->orderNumber}}">Detay</button>
								@endif

							</td>
						</tr>

						@endforeach

					</tbody>
				</table>




			</div>


			
		</div>
	</div>
</div>
<div class="modal fade" id="bigmodal" tabindex="-1" role="dialog"   aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="bigmodal_title">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="bigmodal_content">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="bigmodalbtnright" data-dismiss="modal">Kapat</button>

			</div>
		</div>
	</div>
</div>



@endsection


@section('script')
<script>





	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	$('.detayorder').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/hb/getOrder/'+id,

			success: function (data) {
				modalyap('bigmodal','Sipariş Detayı',data);
			} 

		});
	});

	$('.islem').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/hb/getOrder/'+id,

			success: function (data) {
				modalyap('bigmodal','Sipariş Detayı',data);
			} 

		});
	});


	function paketlendi(oid) {
		$('#tr_'+id).html('<th colspan="5" class="text-center text-success fsz20">Paketlendi</th>')

		$('#bigmodal').modal('hide')
	}



</script>
@endsection
