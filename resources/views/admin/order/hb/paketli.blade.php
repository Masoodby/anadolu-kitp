@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ count($orders)  }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-4 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="saat">

							<option value="48">Son 48 Saat</option>

							@foreach ([24,48,72,96,120] as $key => $madde)

							<option value="{{ $key }}" @if(request()->has('saat') and request('saat') == $key) selected @endif>Son {{ $madde }} Saat</option>
							@endforeach



						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 25; $i <101 ; $i+=25)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>

					<div class="form-group col-md-2 pt20">
						<a href="{{ url('crudv4/hb/orders') }}" class="btn btn-primary float-right">Yeni Siparişler</a>
					</div>	
					<div class="form-group col-md-2 pt20">
						<a href="{{ url('crudv4/hb/excel') }}" target="_blank" class="btn btn-dark float-right">Excelle Aktar</a>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">



			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Ad Soyad</th>
							<th scope="col">Total</th>
							<th scope="col">Ürün Adet</th>
							<th scope="col">İşlem <a href="{{ url('crudv4/hb/cokluEtiket') }}" target="_blank"  class="btn btn-sm btn-success float-right tipsi" title="Tümünü Etiket Olarak Yazdır"><i class="fa fa-print"></i></a></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($orders as $row)

						<tr id="tr_{{$row->items[0]->orderNumber}}">
							<td scope="row">
								{{$row->items[0]->orderNumber}}<br>
								Kargo Barkodu = {{$row->barcode}}
								<br>
								
								Kalan Süre : {{ kalanSure($row->dueDate)}}
							</td>
							<td>{{ $row->recipientName }} <br>{{ $row->shippingCity  }}</td>
							<td>{{ price($row->totalPrice->amount) }}</td>
							<td>{{ count($row->items) }}</td>

							<td>

								<button class="btn btn-success btn-sm detayorder btn-block mb5" data-id="{{$row->items[0]->orderNumber}}">Detay</button> 
								<a href="{{ route('crudv4.hb.cargoEtiket',$row->packageNumber) }}" target="_blank" class="btn btn-secondary btn-sm btn-block mb5">Kargo Etiketi (A5) </a> 
								<a href="{{ route('crudv4.hb.cargoEtiket2',$row->packageNumber) }}" target="_blank" class="btn btn-dark btn-sm btn-block ">Kargo Etiketi (100x100)</a>


							</td>
						</tr>

						@endforeach

					</tbody>
				</table>




			</div>


			
		</div>
	</div>
</div>
<div class="modal fade" id="bigmodal" tabindex="-1" role="dialog"   aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="bigmodal_title">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="bigmodal_content">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="bigmodalbtnright" data-dismiss="modal">Kapat</button>

			</div>
		</div>
	</div>
</div>



@endsection


@section('script')
<script>





	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	$('.detayorder').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/hb/paketdetay/'+id,

			success: function (data) {
				modalyap('bigmodal','Sipariş Detayı',data);
			} 

		});
	});



	function paketlendi(oid) {
		$('#tr_'+id).html('<th colspan="5" class="text-center text-success fsz20">Paketlendi</th>')

		$('#bigmodal').modal('hide')
	}



</script>
@endsection
