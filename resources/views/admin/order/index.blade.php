@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $list->total() }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
							<option value="rank,asc">Sırala</option>

							<option value="id-asc" @if(request()->has('orderby') and request('orderby') == "id-asc") selected @endif>Son -> İlk</option>
							<option value="id-desc" @if(request()->has('orderby') and request('orderby') == "id-desc") selected @endif>İlk -> Son</option>
							<option value="grand_total-asc" @if(request()->has('orderby') and request('orderby') == "grand_total-asc") selected @endif>Fiyat Artan</option>
							<option value="grand_total-desc" @if(request()->has('orderby') and request('orderby') == "grand_total-desc") selected @endif>Fiyat Azalan</option>



						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 30; $i <121 ; $i+=30)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">



			<div id="filter">
				<table class="table">
					<tr>
						<th  >
							<input type="text" class="form-control textfilter w50 from-control-sm" data-filter="id"  placeholder="ID..." value= "{{ request()->has('id') ? request('id'):''}}">
						</th>

						<th  > 
							<input type="text" class="form-control textfilter from-control-sm" data-filter="order_name"  name="product_name" id="order_name" placeholder="Müşteri Adı  ile ara..." value= "{{ request()->has('order_name') ? request('order_name'):''}}">
						</th>

						<th> 
							<input type="text" class="form-control textfilter from-control-sm" data-filter="gsm"  name="gsm" id="gsm" placeholder="GSM ile ara..." value= "{{ request()->has('gsm') ? request('gsm'):''}}">
						</th>


						<th  >
							<select class="form-control selectfilter" data-filter="order_state" >
								<option value="">Sipariş Durumu</option>
								@foreach (order_durum() as $key => $var)
								<option value="{{ $key }}" @if(request()->has('order_state') and request('order_state') == $key) selected @endif>{{ $var }}</option>
								@endforeach

							</select>
						</th>	


						<th  >
							<select class="form-control selectfilter" data-filter="payment_type" >
								<option value="">Ödeme Metodu</option>
								@foreach (pay_metod() as $key => $var)								
								<option value="{{ $key }}" @if(request()->has('payment_type') and request('payment_type') == $key) selected @endif>{{ $var }}</option>
								@endforeach

							</select>
						</th>	




						<td > 
							@if (request()->hasAny(['id',"categories_name","categories_main","limit","orderby","is_active",'product_stock_code']))
							<a href="{{ route('crudv4.order') }}" class="btn btn-dark mt10 float-right btn-sm tipsi" title="Filtreyi Sıfırla"><i class="fa fa-refresh"></i></a>
							@endif


						</td>

					</tr>
				</table>
			</div>



			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>

							<th scope="col"> Sipariş Bilgileri </th>
							<th scope="col">Ürünler</th>
							<th scope="col">Teslim Adresi</th>

							<th scope="col">İşlem</th>

						</tr>

					</thead>
					<tbody>
						@if(count($list)==0)
						<div class="alert alert-warning">Sipariş bulunamadı..</div>
						@else
						@foreach($list as $row)
						<tr id="tr_{{ $row->id }}">

							<td scope="row">{{$loop->iteration}}</td>

							<td  >
								<a href="javascript:void(0)" class="fsz12">Sipariş No : #{{ $row->id }}</a>
								<p class="font-size-12 mb-0"> {{ $row->order_name.' '.$row->order_sname }}  </p>
								

								<b  >{{ date('d.m.Y H:i',strtotime($row->created_at)) }}</b>


							</td>

							<td >
								@if ($row->baskets->basket_products)

								<ul  style="border-radius: 10px; background: #f7d9cf; border:1px solid #ccc; padding: 10px;">
									@foreach ($row->baskets->basket_products as $var)

									<li style=" border-bottom:2px dotted #fff"><i class="fa fa-circle fsz10 mr5"></i><small>{{ $var->product->product_name }} <b class="text-success">x {{ $var->qty }}</b></small><br>
										<small class="text-primary">SKU : <a href="{{ @url('crudv4/depolog/depolist?&isbn='.$var->product->product_stock_code ) }}" target="_blank">{{ $var->product->product_stock_code }}</a></small></li>
										@endforeach
									</ul>
									@else
									<b>Hatalı Sipariş</b>
									@endif

									<b>Total : {{ price($row->grand_total) }}</b><br>
								</td>
								<td>
									@if ($row->address)

									{{ $row->address->adres_detay }}<br>
									{{ $row->address->adres_ilce }} - {{ $row->address->adres_il }} <br>
									{{ $row->address->adres_gsm }} 
									@else
									<b>Hatalı Sipariş</b>
									@endif

								</td>	


								<td>
									<span class="btn btn-warning mb5">{{ order_durum($row->order_state) }}</span> <br>
									<div class="btn-group" role="group" aria-label="Button group with nested dropdown">

										<a href="{{ route('crudv4.order.detail',$row->id) }}"  class="btn btn-secondary"><i class="las la-search"></i></a>
										<div class="btn-group" role="group">
											<button id="btnGroupDrop1" type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												İşlem
											</button>
											<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="">
												<a class="dropdown-item text-primary" href="{{ route('crudv4.order.detail',$row->id) }}">Sipariş Detayı</a>
												<a class="dropdown-item" target="_blank" href="{{ route('crudv4.order.cargoEtiket',$row->id) }}">PDF Form Oluştur</a>
												<a class="dropdown-item" href="javascript:;" onclick="allConfirm('eraseci',{{ $row->id }})">Sil</a>
											</div>
										</div>
									</div>


								</td>


							</tr>

							@endforeach
							@endif

						</tbody>
					</table>



					{{ $list->withQueryString()->links() }}
				</div>



			</div>
		</div>
	</div>



	@endsection


	@section('script')
	<script>


		function eraseci(id) {

			$.ajax({
				type: 'GET',
				url: '/crudv4/order/del/' + id,
				success: function () {
					sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
					$('#tr_'+id).remove()
				} 

			});
		}

		$('.textfilter').keyup(function(event) {

			filterType  = $(this).data('filter')
			val  = $(this).val()

			if(event.keyCode == 13){

				repUrl(filterType,val)

			}
		});



		$('.selectfilter').change(function(event) {



			filterType  = $(this).data('filter')
			val  = $(this).val()


			repUrl(filterType,val)

		});


		$('.radiofilter').click(function(event) {

			filterType  = $(this).data('filter')
			val = $('.radiofilter:checked').val();


			repUrl(filterType,val)

		});

		

	</script>
	@endsection
