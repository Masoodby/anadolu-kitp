<div class="table-responsive">



	<table class="table table-hover">

		<tr>
			<th>Sipariş Sahibi</th>
			<td>{{ $order->orderDetail->buyer->fullName }}</td>
		</tr>

		<tr>
			<th>E-Posta Adresi</th>
			<td>{{ $order->orderDetail->buyer->email }}</td>
		</tr>	
		<tr>
			<th>GSM</th>
			<td>{{ $order->orderDetail->shippingAddress->gsm }}</td>
		</tr>	
		<tr>
			<th>Kargo Adresi</th>
			<td>{{ $order->orderDetail->shippingAddress->address." ".$order->orderDetail->shippingAddress->district." ".$order->orderDetail->shippingAddress->city }}</td>
		</tr>
	</table>
	<table class="table table-hover">

		<thead class="thead-light">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Ürün</th>

				<th scope="col">Sipariş Adedi</th>
				<th scope="col">Konum</th>

			</tr>
		</thead>
		<tbody>
			@php
			$itemList = is_array($order->orderDetail->itemList->item)?$order->orderDetail->itemList->item:[$order->orderDetail->itemList->item];
			@endphp
			@foreach ($itemList as $var)

			@php
			$isApproved = empty($var->approvedDate)?false:true;
			@endphp

			<tr>
				<td>{{ $var->productName }}<br><a target="_blank" href="{{ url('crudv4/product?&product_stock_code='.$var->sellerStockCode) }}">{{ $var->sellerStockCode }}</a></td>
				<td>{{ $var->quantity }}</td>
				<td>{{ $var->price }}</td>
				<td>{{ nerde($var->sellerStockCode,true) }}</td>
			</tr>
			@if ($trorder->depoexit == 0)
			<tr class="bg-light" id="tr_{{ $var->sellerStockCode }}">
				<td colspan="3">
					
					<table class="table table-bordered">


						@foreach (lokasyon($var->sellerStockCode,true); as $key => $item)

						<tr>
							<td>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="raf{{ $var->sellerStockCode }}" value="{{ $var->sellerStockCode.'-'.$key.'-'.$var->quantity }}" {{$loop->iteration == 1?"checked":""}}>
									<label class="form-check-label" for="raf{{ $var->sellerStockCode }}">
										Konum : {{ $item['oda'].' '.$item['raf'] }}
									</label>
								</div>
							</td>
							<td>
								{{ $item['adet'] ." Adet" }}
							</td>

						</tr>

						@endforeach

						<tr>
							<td colspan="2">
								<button class="btn btn-dark exitdepo float-right" data-id="{{ $order->orderDetail->id }}" data-barkod="{{ $var->sellerStockCode }}"  type="button">Depodan Çıkış Yap</button>
							</td>
						</tr>


					</table>
				</td>
			</tr>
			@else

			<tr class="bg-secondary">
				<td colspan="4">
					<p>Gerçek Depodan Çıkış Yapılmış</p>

					@php
					$jsons = strlen($trorder->exit_json) ? json_decode($trorder->exit_json,true):[];

					@endphp

					@foreach ($jsons as $keyisbn => $elitem)
					ISBN : {{ $keyisbn }} -> {{ $elitem }}<br>
					@endforeach

				</td>
			</tr>
			@endif

			@endforeach


		</tbody>
	</table>




</div>

<a href="{{ route('crudv4.n11.cargoEtiket',$order->orderDetail->id) }}" target="_blank" class="btn btn-success">Kargo Etiketi Yazdır</a>
@if (!$isApproved)
<button data-id="{{ $order->orderDetail->id }}" class="btn btn-primary float-right picking">İşleme Al</button>
@else
<button class="btn btn-secondary float-right">İşleme Alınmış</button>
@endif

<script>


	$('.picking').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/n11/picking/'+id,

			success: function (data) {
				sole('İşlem Başarılı','Sipariş İşleme Alındı');
			} 

		});
	});


	$('.exitdepo').click(function() {

		id = $(this).data('id')
		barkod = $(this).data('barkod')

		raf = $('input[name=raf'+barkod+']:checked').val()
		$.ajax({
			type: 'POST',

			url: '/crudv4/n11/exitdepo/'+id,
			data:{name:raf},
			success: function (data) {
				$('#tr_'+barkod).html("<td colspan='3' class='alert alert-success'>Reel Depodan Çıkış Başarılı</td>")
			} 

		});
	});



</script>