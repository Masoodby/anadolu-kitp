@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ @$orders->pagingData->totalCount }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="status">

							<option value="New">Yeni</option>

							@foreach (n11OrderStatus() as $key => $madde)

							<option value="{{ $key }}" @if(request()->has('status') and request('status') == $key) selected @endif>{{ $madde }}</option>
							@endforeach



						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 25; $i <101 ; $i+=25)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">




			@if ($orders->result->status =="success" && isset($orders->orderList->order))

			


			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Alıcı ID</th>
							<th scope="col">Total</th>
							<th scope="col">Ürünler</th>
							<th scope="col">İşlem</th>
						</tr>
					</thead>
					<tbody>

						@php
						$order_list = is_array($orders->orderList->order)?$orders->orderList->order:[$orders->orderList->order];
						@endphp


						@foreach ($order_list as $row)

						<tr>
							<td scope="row">
								{{$row->orderNumber}}
								<br>
								
								{{  $row->createDate }}
							</td>
							<td>{{ $row->citizenshipId }}</td>
							<td>{{ price($row->totalAmount) }}</td>
							<td>


								@php
								$orderItems = is_array($row->orderItemList->orderItem)?$row->orderItemList->orderItem:[$row->orderItemList->orderItem];
								@endphp

								@foreach ($orderItems as $key => $var)
								<small><i class="fa fa-angle-right"></i> <a target="_blank" href="{{ url('crudv4/product?id='.$var->productSellerCode) }}"> {{ $var->productName }}</a><br>
									<b>adet : {{ $var->quantity }}</b><br>

								</small>
								@endforeach
							</td>
							<td>
								@if ($status =="New")
								<button class="btn btn-dark islem" data-id="{{$row->id}}">Onayla</button>
								@else
								<button class="btn btn-success detayorder" data-id="{{$row->id}}">Detay</button>
								@endif

							</td>
						</tr>

						@endforeach

					</tbody>
				</table>




			</div>

			@else
			<div class="alert alert-info">
				Sipariş Yok.
			</div>
			@endif
			
		</div>
	</div>
</div>
<div class="modal fade" id="bigmodal" tabindex="-1" role="dialog"   aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="bigmodal_title">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="bigmodal_content">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="bigmodalbtnright" data-dismiss="modal">Kapat</button>

			</div>
		</div>
	</div>
</div>



@endsection


@section('script')
<script>





	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	$('.detayorder').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/n11/getOrder/'+id,

			success: function (data) {
				modalyap('bigmodal','Sipariş Detayı',data);
			} 

		});
	});

	$('.islem').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/n11/getOrder/'+id,

			success: function (data) {
				modalyap('bigmodal','Sipariş Detayı',data);
			} 

		});
	});






</script>
@endsection
