@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-3">


	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between border-bottom bg-light ">
			<div class="iq-header-title">

				<h4 class="card-title"><i class="fa fa-user"></i> Müşteri Bilgileri </h4>
			</div>

		</div>
		<div class="iq-card-body p10">

			<b class="text-primary">{{ $order->getUser->full_name }} </b>	<br>
			<i class="fa fa-envelope mr5"></i> {{ $order->getUser->email }}<br>
			<span class="pl10">Toplam Sipariş Adedi : {{ $order_adet }} </span>
		</div>
	</div>

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between border-bottom bg-light">
			<div class="iq-header-title">

				<h4 class="card-title"><i class="fa fa-truck"></i> Teslimat adresi </h4>
			</div>

		</div>
		<div class="iq-card-body">
			@if ($order->address)
			<b>{{ $order->address->full_name }}</b><br>
			<small>	{{  $order->address->adres_detay }}</small><br>
			<small>{{  $order->address->adres_ilce }} - {{  $order->address->adres_il }}</small><br>
			<b>{{ $order->address->adres_gsm }}</b><br>
			@else
			<p class="text-danger">Müşteri Teslimat adresini silmiş olabilir. Teslimat adresi kayıtlı değil</p>
			@endif
		</div>
	</div>

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between border-bottom bg-light">
			<div class="iq-header-title">

				<h4 class="card-title"><i class="fa fa-tag"></i> Fatura adresi </h4>
			</div>

		</div>


		<div class="iq-card-body">
			@if ($order->fatadres)
			<b>{{ $order->fatadres->fatura_tur == 0 ? "Bireysel Fatura":"Kurumsal Fatura" }}</b><br>
			<b>Ünvan : {{ $order->fatadres->fatura_tur == 0 ? $order->fatadres->full_name:$order->fatadres->fatura_unvan }}</b><br>
			<small>	{{ $order->fatadres->fatura_tur == 0 ?"TCK : ". $order->fatadres->fatura_tck:"V.No : ".$order->fatadres->fatura_vno.' V.D : '.$order->fatadres->fatura_vd }}</small><br>
			<small>{{  $order->fatadres->adres_detay }}</small><br>
			<small>{{  $order->fatadres->adres_ilce }} - {{  $order->fatadres->adres_il }}</small><br>
			@else
			<p class="text-danger">Müşteri Fatura adresini silmiş olabilir. Fatura adresi kayıtlı değil</p>
			@endif
		</div>



	</div>


</div>

<div class="col-sm-9">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between border-bottom">

			<div class="iq-header-title col-md-6">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.order') }}" class="btn btn-primary float-right"> <i class="fa fa-angle-left"></i> Geri</a>
			</div>



		</div>
		<div class="iq-card-body">

			<div class="row">
				<div class="col-md-8">
					<table class="table table-hover table-sm">
						<tr>
							<th colspan="2">Ürün</th>
							<th>Fiyat</th>
							<th>Adet</th>
							<th>Total</th>
						</tr>

						@foreach ($order->baskets->basket_products as $var)

						<tr>
							<td>
								<a href="{{ route('crudv4.product.edit',$var->product_id) }}" target="_blank"><img src="{{ ress( $var->product->product_stock_code,$var->product->product_group_code,"250x250") }}" class="w100"  ></a>
							</td>
							<td>
								<a href="{{ route('crudv4.product.edit',$var->product_id) }}" target="_blank">	{{ $var->product->product_name }}</a><br>
								{{ $var->product->product_stock_code.' - '.nerde($var->product->product_stock_code)}}
							</td>
							<td>{{ price($var->total_price) }}</td>
							<td>{{ $var->qty }}</td>
							<td>{{ price($var->total_price*$var->qty) }}</td>
						</tr>

						@endforeach
                            {{-- @php
                                dd($order->baskets->basket_combos);
                            @endphp --}}
                        @foreach ($order->baskets->basket_combos as $combo)
                         {{-- @php
                            dd($combo);
                        @endphp --}}
						@foreach ($combo->combo->products as $var)

                        <tr>
							<td>
								<a href="{{ route('crudv4.product.edit',$var->id) }}" target="_blank"><img src="{{ ress( $var->product_stock_code,$var->product_group_code,"250x250") }}" class="w100"  ></a>
							</td>
							<td>
								<a href="{{ route('crudv4.product.edit',$var->id) }}" target="_blank">	{{ $var->product_name }}</a><br>
								{{ $var->product_stock_code.' - '.nerde($var->product_stock_code)}}
							</td>
							<td>{{ price($var->product_price) }}</td>
							<td>{{ $combo->qty }}</td>
							<td>{{ price($combo->total_price*$var->qty) }}</td>
						</tr>

                        @endforeach

						@endforeach


						<tr>
							<td colspan="3"></td>
							<td  >Ara Toplam</td>
							<td  >{{ price($order->order_total) }}</td>
						</tr>
						<tr>
							<td colspan="3"></td>
							<td  >Kargo Ücreti</td>
							<td  >{{ price($order->cargo_cost) }}</td>
						</tr>
						<tr>
							<td colspan="3"></td>
							<th  >Genel Toplam</th>
							<td  >{{ price($order->grand_total) }}</td>
						</tr>

					</table>

					<hr>
					<div class="row">
						<div class="col-6">
							@if (strlen($order->order_nots) > 5)
							<i class="fa fa-envelope"></i>	<small>Müşteri Sipariş Notu:</small> <br><div class="alert alert-success"><i class="fa fa-envelope float-left fsz30 pr5 mr5 border-right"></i>{{ $order->order_nots }}</div>
							@endif
							<div class="form-group">
								<label for="musteri_mesaj">Müşteriye Mesaj Yaz</label>
								<textarea class="form-control fsz12 lh15" id="musteri_mesaj" rows="5"></textarea>
							</div>
							<button class="btn btn-sm btn-primary sendmessage float-right">Gönder</button>

						</div>
						<div class="col-6">


							@if (strlen($order->order_admin_nots) > 5)
							<i class="fa fa-info-circle"></i>	<small>Yönetici Notu:</small> <br><div class="alert alert-dark"><i class="fa fa-info-circle float-left fsz30 pr5 mr5 border-right"></i>{{ $order->order_admin_nots }}</div>
							@endif
							<div class="form-group">
								<label for="admin_mesaj">Müşteriye Mesaj Yaz</label>
								<textarea class="form-control fsz12 lh15" id="admin_mesaj" rows="5"></textarea>
							</div>
							<button class="btn btn-sm btn-primary saveAdminNote float-right">Kaydet</button>


						</div>
					</div>
				</div>
				<div class="col-md-4 bg-f4">


					<div class="form-group pt10">
						<b class="text-center p10 text-primary">Sipariş Durumu</b>
						<select class="form-control bg-white req" id="order_state" name="order_state">

							@foreach (order_durum() as $key => $var)
							<option value="{{ $key }}" {{ $key == $order->order_state?"selected":"" }}>{{ $var }}</option>
							@endforeach

						</select>
					</div>

					<div class="row">
						<b class="text-center pl15 pr5 text-primary">Ödeme Metodu : </b>
						<b class="">{{ pay_metod($order->payment_type) }}</b>
					</div>
					<hr>


					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input req" id="smsonay" name="smsonay">
							<label class="custom-control-label" for="smsonay">Değişikliği Mail/SMS olarak gönder.</label>
						</div>


					</div>


					<div class="form-group">
						<label for="sms">SMS Metni</label>
						<textarea class="form-control fsz12 lh15 req" name="sms" id="sms"></textarea>
						@isset ($order->address)
						<input type="hidden" name="gsm" class="req" value="{{ $order->address->adres_gsm }}">
						<input type="hidden" name="email" class="req" value="{{ $order->getUser->email }}">
						<small class="text-muted">{{ $order->address->adres_gsm }} nolu Telefona SMS Gidecektir.</small>
						@endisset

					</div>

					<hr>

					<button class="btn btn-primary btn-block saveOrder" data-id="{{ $order->id }}" type="button">Siparişi Güncelle</button>
					<hr>
					<a class="btn btn-dark btn-block" target="_blank" href="{{ route('crudv4.order.cargoEtiket',$order->id) }}" >Etiket Yazdır</a>







				</div>
			</div>

		</div>
	</div>
</div>


@endsection


@section('script')

<script>
	$('#order_state').change(function() {

		liste = [
		'Siparişiniz Onay Aşamasına Alınmıştır.',
		'Siparişiniz Onaylanmıştır',
		'Siparişiniz Kargoya Verilmiştir. Takip No : ',
		"Siparişiniz Teslim Edildi",
		"Siparişiniz Teslim Edilemedi",
		"Siparişiniz İptal Edildi"
		]

		indis =  $(this).val()

		$('#sms').val(liste[indis])
	});


	$('.saveOrder').click(function() {

		id = $(this).data('id')

		$.ajax({
			type: 'POST',
			url: '/crudv4/order/orderUpdate/'+id,
			data: $('.req').serialize(),
			success: function(){

				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});

	});


</script>

@endsection
