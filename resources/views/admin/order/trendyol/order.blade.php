<div class="table-responsive">

	<table class="table table-hover">

		<thead class="thead-light">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Ürün</th>

				<th scope="col">Sipariş Adedi</th>
				<th scope="col">Konum</th>

			</tr>
		</thead>
		<tbody>
			@foreach ($order->content[0]->lines as $var)

			<tr>
				<td>{{ $var->productName }}<br><a target="_blank" href="{{ url('crudv4/product?&product_stock_code='.barkod_rep($var->barcode)) }}">{{ $var->barcode }}</a></td>
				<td>{{ $var->quantity }}</td>
				<td>{{ $var->price }}</td>
				<td>{{ nerde(barkod_rep($var->barcode),true) }}</td>
			</tr>
			@if ($trorder->depoexit == 0)
			<tr class="bg-light" id="tr_{{ barkod_rep($var->barcode) }}">
				<td colspan="3">
					
					<table class="table table-bordered">


						@foreach (lokasyon(barkod_rep($var->barcode),true); as $key => $item)

						<tr>
							<td>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="raf{{ barkod_rep($var->barcode) }}" value="{{ barkod_rep($var->barcode).'-'.$key.'-'.$var->quantity }}" {{$loop->iteration == 1?"checked":""}}>
									<label class="form-check-label" for="raf{{ barkod_rep($var->barcode) }}">
										Konum : {{ $item['oda'].' '.$item['raf'] }}
									</label>
								</div>
							</td>
							<td>
								{{ $item['adet'] ." Adet" }}
							</td>

						</tr>

						@endforeach

						<tr>
							<td colspan="2">
								<button class="btn btn-dark exitdepo float-right" data-id="{{ $order->content[0]->orderNumber }}" data-barkod="{{ barkod_rep($var->barcode) }}"  type="button">Depodan Çıkış Yap</button>
							</td>
						</tr>


					</table>
				</td>
			</tr>
			@else

			<tr class="bg-secondary">
				<td colspan="4">
					<p>Gerçek Depodan Çıkış Yapılmış</p>

					@php
					$jsons = strlen($trorder->exit_json) >4 ? json_decode($trorder->exit_json,true):[];

					@endphp

					@foreach ($jsons as $keyisbn => $elitem)
					ISBN : {{ $keyisbn }} -> {{ $elitem }}<br>
					@endforeach

				</td>
			</tr>
			@endif

			@endforeach


		</tbody>
	</table>




</div>

<a href="{{ route('crudv4.trendyol.cargoEtiket',$order->content[0]->orderNumber) }}" target="_blank" class="btn btn-success">Kargo Etiketi (A5) </a>
<a href="{{ route('crudv4.trendyol.cargoEtiket2',$order->content[0]->orderNumber) }}" target="_blank" class="btn btn-info">Kargo Etiketi (100x100)</a>
@if ($order->content[0]->shipmentPackageStatus =="ReadyToShip")
<button data-id="{{ $order->content[0]->orderNumber }}" class="btn btn-primary float-right picking">İşleme Al</button>
@else
<button class="btn btn-secondary float-right">İşleme Alınmış</button>
@endif

<script>


	$('.picking').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/trendyol/picking/'+id,

			success: function (data) {
				sole('İşlem Başarılı','Sipariş İşleme Alındı');
			} 

		});
	});


	$('.exitdepo').click(function() {

		id = $(this).data('id')
		barkod = $(this).data('barkod')

		raf = $('input[name=raf'+barkod+']:checked').val()
		$.ajax({
			type: 'POST',

			url: '/crudv4/trendyol/exitdepo/'+id,
			data:{name:raf},
			success: function (data) {
				$('#tr_'+barkod).html("<td colspan='3' class='alert alert-success'>Reel Depodan Çıkış Başarılı</td>")
			} 

		});
	});



</script>