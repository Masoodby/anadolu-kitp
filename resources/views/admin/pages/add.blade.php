@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.pages') }}" class="btn btn-primary btn-lg float-right"  ><i class="fa fa-angle-left"></i> Geri</a>
			</div>

		</div>
		<div class="iq-card-body">

			<form action="{{ route('crudv4.pages.save') }}" class="container-fluid row" id="pagesform" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}

				@include('admin.layouts.errors')
				@include('admin.layouts.alert')


				<div class="form-group col-md-12">
					<label  >Sayfa Başlık:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control copyname req seoname" name="page_name" value="">
					</div>
				</div>


				<div class="form-group col-md-6">
					<label  >Sayfa Url:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  >{{ config('app.url') }}</span>
						</div>
						<input type="text" class="form-control seoname_target req" name="page_url" value="#">
					</div>
				</div>

				<div class="col-sm-6 pt40">
					<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
						<input type="radio" id="page_type1" name="page_type" value="1" class="custom-control-input bg-primary" checked="">
						<label class="custom-control-label" for="page_type1"> Kurumsal Sayfalar</label>
					</div>
					<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
						<input type="radio" id="page_type2" value="2" name="page_type" class="custom-control-input bg-danger">
						<label class="custom-control-label" for="page_type2"> Yardım Makaleleri </label>
					</div>
				</div>
				
				<div class="form-group col-md-12">
					<label > Sayfa Başlığı <small>(Page Title)</small></label>
					<textarea class="form-control say  copyname_target lh23" data-say="{{ seoLimit('title') }}" name="page_title"></textarea>
					<p class="form-text"></p>
				</div>




				<div class="form-group col-md-12">
					<label >Sayfa Açıklaması <small>(meta desc.)</small></label>
					<textarea class="form-control say copyname_target h100 lh23" data-say="{{ seoLimit('desc') }}" name="page_desc"></textarea>
					<p class="form-text"></p>
				</div>



				<div class="form-group col-md-12">
					<label >Anahtar Kelimeler <small>(meta keyw.)</small></label>
					<textarea class="form-control say copyname_target h100 lh23" data-say="{{ seoLimit('desc') }}" name="page_keyw"></textarea>
					<p class="form-text"></p>
				</div>

				<div class="form-group col-md-12">
					<label >Genel İçerik  </label>
					<textarea class="form-control h290 ckeditor lh24" name="page_content"></textarea>
					<p class="form-text"></p>
				</div>



				<button type="button" data-id="pagesform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
			</form>

		</div>
	</div>
</div>


@endsection


@section('script')

<script src="//cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>

@endsection