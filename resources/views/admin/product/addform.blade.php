@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.product.save') }}" class="container-fluid row" id="productform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-md-12">
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-6">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
				<div class="col-md-6">
					<a href="{{ route('crudv4.product') }}" class="btn btn-primary float-right"> <i class="fa fa-angle-left"></i> Geri</a>
				</div>
			</div>
			<div class="iq-card-body">
				@php ($listDizi = ['list-alt'=>'Genel Tanımlama','cog'=>'Detay Tanımlama','file-text-o'=>'Ücret/Stok Ayarları','star-o'=>"Seo Ayarları"])


				<ul class="nav nav-tabs bg-f4 color-white" id="myTab-three" role="tablist">

					@foreach ($listDizi as $key => $item)

					<li class="nav-item">
						<a class="nav-link {{$loop->iteration == 1?'active':''}}" id="tab-btn-{{ $loop->iteration }}" data-toggle="tab" href="#tab{{ $loop->iteration }}" role="tab" aria-controls="tab-btn-{{ $loop->iteration }}" aria-selected="true"> 
							<i class="fa fa-{{ $key }}"></i> {{ $item }}   </a>
						</li>

						@endforeach


					</ul>

					

					<div class="tab-content" id="myTabContent-4">
						<div class="tab-pane active show fade" id="tab1" role="tabpanel" aria-labelledby="tab-btn-1">


							<div class="row">
								<div class="col-md-3 " id="catpanel">

									<div class="form-group mt5">

										<input type="email" class="form-control form-control-sm search-cat" placeholder="Kategori Ara..." >
									</div>

									<hr>


									<div id="brand-list" class="scrolls h500 p5">
										<ul class="list-group cat_list_hnk iq-list-style-1">
											@foreach ($cats as $item)


											<li class="mb-2 mr-0 bg-light listcat-item">
												<div class="d-flex justify-content-between">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input catsec" name="cats[]" {{ old('cats') !==null && in_array($item['id'], old('cats'))?"checked":"" }}  data-name="{{ $item['categories_name']  }}"  id="cats{{ $item['id'] }}" {{ count($item['child'])>0?"disabled":"" }}    value="{{ $item['id'] }}"  >
														<label class="custom-control-label fsz12" for="cats{{ $item['id'] }}"><b>{{ $item['categories_name'] }}</b></label>
													</div>

												</div>
											</li>


											@foreach ($item['child'] as $childItem)
											<li class="mb-2 mr-0 pl10 listcat-item">
												<div class="d-flex justify-content-between">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input catsec" name="cats[]" {{ old('cats') !==null && in_array($item['id'], old('cats'))?"checked":"" }} data-name="{{ $childItem['categories_name']  }}" id="cats{{ $childItem['id'] }}"  {{ count($childItem['child'])>0?"disabled":"" }}    value="{{ $childItem['id'] }}"  >
														<label class="custom-control-label fsz11" for="cats{{ $childItem['id'] }}">{{ $childItem['categories_name'] }}</label>
													</div>

												</div>
											</li>


											@foreach ($childItem['child'] as $lastchild)
											<li class="mb-2 mr-0 pl20 listcat-item">
												<div class="d-flex justify-content-between">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input catsec" name="cats[]" {{ old('cats') !==null &&  in_array($item['id'], old('cats'))?"checked":"" }}  data-name="{{ $lastchild['categories_name']  }}"  id="cats{{ $lastchild['id'] }}"    value="{{ $lastchild['id'] }}"  >
														<label class="custom-control-label fsz10" for="cats{{ $lastchild['id'] }}">{{ $lastchild['categories_name'] }}</label>
													</div>

												</div>
											</li>
											{{-- expr --}}
											@endforeach

											@endforeach




											@endforeach


										</ul>
									</div>


								</div>

								<div class="col-md-9 " style="border-left:1px solid #ccc">


									<input type="hidden" value="0" name="product_cat" id="product_cat">
									<div class="cat-selected">

									</div>
									<div class="clearfix"></div>


									<div class="form-group">
										<label  >Ürün Adı:</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-primary"  ><i class="fa fa-folder-o"></i></span>
											</div>
											<input type="text" class="form-control copyname req seoname" data-tab="tab-btn-1" name="product_name" value="{{ old('product_name') }}">
										</div>
									</div>

									<div class="form-group">
										<label  >Ürün Alt Adı <small class="text-muted">(bir alt adı yoksa boş bırakabilirsiniz)</small> :</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-secondary"  ><i class="ri-file-list-line"></i></span>
											</div>
											<input type="text" class="form-control " name="product_sub_name" value="{{ old('product_sub_name') }}">
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label  >Stok Kodu:</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text  bg-primary"  ><i class="fa fa-barcode"></i></span>
												</div>
												<input type="text" class="form-control req " data-tab="tab-btn-1" name="product_stock_code" value="{{ old('product_stock_code') }}">
											</div>
										</div>		

										<div class="form-group col-sm-6">
											<label  >Grup Kodu <small class="text-muted">(Çift Kırılım Varyantlı Ürünler için gereklidir)</small>:</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text  bg-primary"  ><i class="fa fa-list"></i></span>
												</div>
												<input type="text" class="form-control " name="product_group_code" value="{{ old('product_group_code') }}">
											</div>
										</div>

									</div>

									<hr>

									<div class="row">
										<div class="form-group col-sm-6">
											<label  >Satış Fiyatı <small class="text-muted">(Çoklu Fiyat için diğer tablardan devam edin)</small>:</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
												</div>
												<input type="text" class="form-control req float" onkeyup="$('.pricex').val($(this).val())" data-tab="tab-btn-1" name="product_price" value="{{ old('product_sub_name') }}">
												<div class="input-group-append">
													<span class="input-group-text  bg-secondary"  >TL</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6 pt40">
											<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
												<input type="radio" id="is_active1" name="is_active" value="1" class="custom-control-input bg-primary" checked="">
												<label class="custom-control-label" for="is_active1"> Hemen Yayınla </label>
											</div>
											<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
												<input type="radio" id="is_active2" value="0" name="is_active" class="custom-control-input bg-danger">
												<label class="custom-control-label" for="is_active2"> Pasif Kalsın </label>
											</div>
										</div>

									</div>




									<div class="form-group">
										<label >Kısa Açıklama <small class="text-muted">(max 500 karakter)</small> </label>
										<textarea class="form-control say h70 lh23" data-say="500" name="product_jenerik">{{ old('product_jenerik') }}</textarea>
										<p class="form-text"></p>
									</div>

									<div class="form-group">
										<div class="custom-file">
											<input type="file" name="resim[]" id="resimsec" multiple class="custom-file-input"   lang="tr">
											<label class="custom-file-label" for="resim" data-browse="Çoklu Resim Seç">Ürün Resmi</label>
										</div>

										<div id="image_result">
											
											<div class="clearfix"></div>
										</div>
									</div>

									
								</div>
							</div>


						</div>
						<div class="tab-pane fade" id="tab2"  role="tabpanel" aria-labelledby="tab-btn-2">



							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label  >Yayıncı Seç :</label>
										<div class="input-group ">
											<select class="form-control form-control-sm mb-3" id="yayinci_id" name="yayinci_id"  >
												<option value="{{ old('yayinci_id') }}">Tüm Yayıncılar</option>
											</select>

										</div>
									</div>

								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label>Yazar Seç</label>
										<div class="input-group ">
											<select class="form-control form-control-sm mb-3" id="yazar_id" name="yazar_id" >
												<option value="{{ old('yazar_id') }}">Tüm Yazarlar</option>


											</select>
										</div>
									</div>

								</div>
								
								@foreach (json_decode(set('book_desc')) as $key => $var)

								@if ($key == "cevirmen")

								@else


								<div class="form-group col-sm-4">
									<label  >{{ $var->attr }}  :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-book"></i></span>
										</div>
										<input type="text" class="form-control" data-tab="tab-btn-3" name="extend_json[{{ $key }}]" value="{{ old('extend_json[$key]') }}">

									</div>
								</div>
								@endif

								@endforeach
							</div>

							<div class="form-group">
								<label >Ürünün Detaylı Açıklama   </label>
								<textarea class="form-control ckeditor" data-tab="tab-btn-2"  name="product_content">{{ old('product_content') }}</textarea>
								<p class="form-text"></p>
							</div>


						</div>
						<div class="tab-pane fade " id="tab3"  role="tabpanel" aria-labelledby="tab-btn-3">

							<div class="row">
								<div class="form-group col-sm-6">
									<label  >Satış Fiyatı  :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
										</div>
										<input type="text" class="form-control pricex req float" data-tab="tab-btn-3" name="product_price_max" value="{{ old('product_price_max') }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >TL</span>
										</div>
									</div>
								</div>


								<div class="form-group col-sm-6">
									<label  >Piyasa Fiyatı :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
										</div>
										<input type="text" class="form-control float"   name="product_old_price" value="{{ old('product_old_price') }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >TL</span>
										</div>
									</div>
								</div>

								<div class="form-group col-sm-6">
									<label  >Alış Fiyatı :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
										</div>
										<input type="text" class="form-control float"  name="product_get_price" value="{{ old('product_get_price') }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >TL</span>
										</div>
									</div>
								</div>

								<div class="form-group col-sm-6">
									<label  >Stok Adedi <small class="text-muted">(Varyant var ise girmeyiniz Toplam hesaplanacaktır.)</small>:</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-refresh"></i></span>
										</div>
										<input type="text" class="form-control int"  name="product_stok" value="{{ old('product_stok') }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >Adet</span>
										</div>
									</div>
								</div>

							</div>
						</div>	
						<div class="tab-pane fade " id="tab4"  role="tabpanel" aria-labelledby="tab-btn-4">


							<div class="form-group">
								<label  >Ürün Seo URL <small>(slug)</small>	:</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text bg-secondary"  >{{ config('app.url') }}/</span>
									</div>
									<input type="text" class="form-control seoname_target req" name="product_url" value="{{ old('product_url') }}">
								</div>
							</div>


							<div class="form-group">
								<label >Ürün Sayfa Başlığı <small>(Page Title)</small></label>
								<textarea class="form-control say  copyname_target lh23" data-say="{{ seoLimit('title') }}" name="product_title">{{ old('product_title') }}</textarea>
								<p class="form-text"></p>
							</div>




							<div class="form-group">
								<label >Sayfa Açıklaması <small>(meta desc.)</small></label>
								<textarea class="form-control say copyname_target h100 lh23" data-say="{{ seoLimit('desc') }}" name="product_desc">{{ old('product_desc') }}</textarea>
								<p class="form-text"></p>
							</div>



							<div class="form-group">
								<label >Anahtar Kelimeler <small>(meta keyw.)</small></label>
								<textarea class="form-control say copyname_target h100 lh23" data-say="{{ seoLimit('desc') }}" name="product_keyw">{{ old('product_keyw') }}</textarea>
								<p class="form-text"></p>
							</div>




						</div>
					</div>
				</div>
			</div>

			<button type="button" data-id="productform" class="btn btn-block btn-primary sbmt_btn_spec">Kaydet</button>
		</div>





	</form>


	@endsection


	@section('script')
	<script src="//cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>

	<script>


		$(".sbmt_btn_spec").click(function() {

			data_id  = $(this).data('id');

			if($('#product_cat').val() == 0){
				$('#catpanel').css('border','1px solid red');
				sole('Hata','Kategori Seçmediniz')
				die();
			}else{
				$('#catpanel').css('border','1px solid green');
			}


			$('#'+data_id+' .req').each(function(index, element) {

				var degert = $(this).val();

				if(degert ==''){

					$(this).css('border-color','red');
					tabID = $(this).data('tab');
					$('#'+tabID).click()
					$(this).focus();

					die();
				} else {

					$(this).css('border-color','green');

				}
			});


			if(CKEDITOR.instances["product_content"].getData() == ""){

				$('#tab-btn-2').click()
				sole('Hata','Ürün içeriği Boş')
				die();

			}


			$(this).attr("disabled","disabled");

			$(this).css("opacity","0.6");

			$(this).html("Bekleyiniz");




			setTimeout(function(){ $('#'+data_id).submit(); }, 1000);


		});



		function secilicats() {

			labelsiler = "";
			i = 0;
			idx= 0;
			master_id = 0

			$('.catsec').each(function(index, element) {

				if ($(this).prop( "checked" ) == true )
				{
					labelsiler =  labelsiler+'<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline"><input id="mcat'+$(this).val()+'" onclick="$(\'#product_cat\').val('+$(this).val()+')" name="product_main_cat" value="'+$(this).val()+'" type="radio" class="custom-control-input bg-primary req" ><label class="custom-control-label" for="mcat'+$(this).val()+'">'+$(this).data('name')+'</label></div>';
					i++;
					idx = $(this).val()

					if(i==1){
						master_id = idx
					}



				}
			});


			if(i == 0){
				$('#product_cat').val(0)
			}

			if(i >= 1){
				$('#product_cat').val(master_id)
			}


			if(i>0){
				$('.cat-selected').html("<b>Birincil Kategori </b>: "+labelsiler+'<br><p class="fsz10"> Diğerleri Ek kategorisi olacaktır</p>');
			}else{
				$('.cat-selected').html('<span class="color-red"> En az Bir Kategori Seçmelisiniz !</span>');
			}



			new_id = $('#product_cat').val()

			$('#mcat'+new_id).prop('checked', 'checked')


		}




		$(".search-cat").on('keyup', function() {
			var search = $(this).val().toLowerCase();
			//Go through each list item and hide if not match search

			$(".cat_list_hnk .listcat-item").each(function() {
				if ($(this).children('div').children('div').children('input').data('name').toLowerCase().indexOf(search) != -1) {
					$(this).show();

				}
				else {
					$(this).hide();
				}

			});


		});


		@if (old("cats") !==null)
		jQuery(document).ready(function($) {
			secilicats()

		});

		@endif


		$('.catsec').click(function() {

			product_cat = $('#product_cat').val()

			if(product_cat == 0){
				vals = $(this).val()
				$('#product_cat').val(vals)
			}

			secilicats()


		});




		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


		$( "#yazar_id" ).select2({
			theme: 'bootstrap4',
			width: '100%',
			dropdownAutoWidth: true,
			ajax: { 
				url: '/crudv4/product/yazar_ara',
				type: "post",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});


		$( "#yayinci_id" ).select2({
			theme: 'bootstrap4',
			width: '100%',
			dropdownAutoWidth: true,
			ajax: { 
				url: '/crudv4/product/yayinci_ara',
				type: "post",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});


</script>


@endsection