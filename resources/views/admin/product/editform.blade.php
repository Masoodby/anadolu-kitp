@extends('admin.layouts.master')
@section('title', $title)
@section('style')
<link rel="stylesheet" href="{{ asset('dist/panel/css/tema.css') }}">
@endsection
@section('body')


<form action="{{ route('crudv4.product.save',$product->id) }}" class="container-fluid row" id="productform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-md-12">
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-6">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
				<div class="col-md-6">
					<a href="{{ route('crudv4.product') }}" class="btn btn-primary float-right"> <i class="fa fa-angle-left"></i> Geri</a>
				</div>
			</div>
			<div class="iq-card-body">
				@php
				$listDizi = ['list-alt'=>'Genel Tanımlama','cog'=>'Detay Tanımlama','file-text-o'=>'Ücret/Stok Ayarları','star-o'=>"Seo Ayarları",'shopping-basket'=>'Pazaryerleri Ayarları'];
				@endphp



				<ul class="nav nav-tabs bg-f4 color-white" id="myTab-three" role="tablist">

					@foreach ($listDizi as $key => $item)

					<li class="nav-item">
						<a class="nav-link {{$loop->iteration == 1?'active':''}}" id="tab-btn-{{ $loop->iteration }}" data-toggle="tab" href="#tab{{ $loop->iteration }}" role="tab" aria-controls="tab-btn-{{ $loop->iteration }}" aria-selected="true">
							<i class="fa fa-{{ $key }}"></i> {{ $item }}   </a>
						</li>

						@endforeach


					</ul>



					<div class="tab-content" id="myTabContent-4">
						<div class="tab-pane active show fade" id="tab1" role="tabpanel" aria-labelledby="tab-btn-1">


							<div class="row">
								<div class="col-md-3 " id="catpanel">

									<div class="form-group mt5">

										<input type="email" class="form-control form-control-sm search-cat" placeholder="Kategori Ara..." >
									</div>

									<hr>


									<div id="brand-list" class="scrolls h700 p5">
										<ul class="list-group cat_list_hnk iq-list-style-1">
											@foreach ($cats as $item)


											<li class="mb-2 mr-0 bg-light listcat-item">
												<div class="d-flex justify-content-between">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input catsec" name="cats[]"  {{  in_array($item['id'], $product_cats) ? 'checked' : ''}}  data-name="{{ $item['categories_name']  }}"  id="cats{{ $item['id'] }}" {{ count($item['child'])>0?"disabled":"" }}    value="{{ $item['id'] }}"  >
														<label class="custom-control-label fsz12" for="cats{{ $item['id'] }}"><b>{{ $item['categories_name'] }}</b></label>
													</div>

												</div>
											</li>


											@foreach ($item['child'] as $childItem)
											<li class="mb-2 mr-0 pl10 listcat-item">
												<div class="d-flex justify-content-between">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input catsec" name="cats[]" {{  in_array($childItem['id'], $product_cats) ? 'checked' : ''}}  data-name="{{ $childItem['categories_name']  }}" id="cats{{ $childItem['id'] }}"  {{ count($childItem['child'])>0?"disabled":"" }}    value="{{ $childItem['id'] }}"  >
														<label class="custom-control-label fsz11" for="cats{{ $childItem['id'] }}">{{ $childItem['categories_name'] }}</label>
													</div>

												</div>
											</li>


											@foreach ($childItem['child'] as $lastchild)
											<li class="mb-2 mr-0 pl20 listcat-item">
												<div class="d-flex justify-content-between">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input catsec" name="cats[]"  {{  in_array($lastchild['id'], $product_cats) ? 'checked' : ''}}  data-name="{{ $lastchild['categories_name']  }}"  id="cats{{ $lastchild['id'] }}"    value="{{ $lastchild['id'] }}"  >
														<label class="custom-control-label fsz10" for="cats{{ $lastchild['id'] }}">{{ $lastchild['categories_name'] }}</label>
													</div>

												</div>
											</li>
											{{-- expr --}}
											@endforeach

											@endforeach




											@endforeach


										</ul>
									</div>


								</div>

								<div class="col-md-9 " style="border-left:1px solid #ccc">


									<input type="hidden" value="{{ $product->product_cat }}" name="product_cat" id="product_cat">
									<div class="cat-selected">

									</div>
									<div class="clearfix"></div>


									<div class="form-group">
										<label  >Ürün Adı:</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-primary"  ><i class="fa fa-folder-o"></i></span>
											</div>
											<input type="text" class="form-control copyname req seoname" data-tab="tab-btn-1" name="product_name" value="{{ $product->product_name }}">
										</div>
									</div>

									<div class="form-group">
										<label  >Ürün Alt Adı <small class="text-muted">(bir alt adı yoksa boş bırakabilirsiniz)</small> :</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-secondary"  ><i class="ri-file-list-line"></i></span>
											</div>
											<input type="text" class="form-control " name="product_sub_name" value="{{ $product->product_sub_name }}">
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label  >Stok Kodu:</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text  bg-primary"  ><i class="fa fa-barcode"></i></span>
												</div>
												<input type="text" class="form-control req " data-tab="tab-btn-1" name="product_stock_code" value="{{ $product->product_stock_code }}">
											</div>
										</div>

										<div class="form-group col-sm-6">
											<label  >Grup Kodu <small class="text-muted">(Çift Kırılım Varyantlı Ürünler için gereklidir)</small>:</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text  bg-primary"  ><i class="fa fa-list"></i></span>
												</div>
												<input type="text" class="form-control " name="product_group_code" value="{{ $product->product_group_code }}">
											</div>
										</div>

									</div>

									<hr>

									<div class="row">
										<div class="form-group col-sm-6">
											<label  >Satış Fiyatı <small class="text-muted">(Çoklu Fiyat için diğer tablardan devam edin)</small>:</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
												</div>
												<input type="text" class="form-control req float" onkeyup="$('.pricex').val($(this).val())" data-tab="tab-btn-1" name="product_price" value="{{ $product->product_price }}">
												<div class="input-group-append">
													<span class="input-group-text  bg-secondary"  >TL</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6 pt40">
											<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
												<input type="radio" id="is_active1" name="is_active" value="1" class="custom-control-input bg-primary"  {{ $product->is_active==1?"checked":"" }}>
												<label class="custom-control-label" for="is_active1"> Hemen Yayınla </label>
											</div>
											<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
												<input type="radio" id="is_active2" value="0" name="is_active" class="custom-control-input bg-danger" {{ $product->is_active==0?"checked":"" }}>
												<label class="custom-control-label" for="is_active2"> Pasif Kalsın </label>
											</div>
										</div>

									</div>




									<div class="form-group">
										<label >Kısa Açıklama <small class="text-muted">(max 500 karakter)</small> </label>
										<textarea class="form-control say h70 lh23" data-say="500" name="product_jenerik">{{ $product->product_jenerik }}</textarea>
										<p class="form-text"></p>
									</div>
									<input type="file" class="d-none" name="resim" id="resim">
									<div class="form-group" id="img-area">
										@include('admin.product.img')
									</div>


								</div>
							</div>


						</div>
						<div class="tab-pane fade" id="tab2"  role="tabpanel" aria-labelledby="tab-btn-2">
							<div class="row">


								<div class="col-md-6">
									<div class="form-group">
										<label  >Yayıncı Seç :</label>
										<div class="input-group ">
											<select class="form-control form-control-sm mb-3 req" id="yayinci_id" name="yayinci_id"  >
												<option value="{{ $product->extend->yayinci_id }}">{{ $product->extend->getYayinci->yayinci_name }} </option>
											</select>

										</div>
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Yazar Seç</label>
										<div class="input-group ">
											<select class="form-control form-control-sm mb-3 req" id="yazar_id" name="yazar_id" >
												<option value="{{ $product->extend->yazar_id }}">{{ $product->extend->getYazar->yazar_name }} </option>


											</select>
										</div>
									</div>

								</div>



								@php($extend_json = json_decode($product->extend->extend_json))

								@foreach (json_decode(set('book_desc')) as $key => $var)

								@if ($key == "cevirmen")

								@else


								<div class="form-group col-sm-4">
									<label  >{{ $var->attr }}  :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-book"></i></span>
										</div>
										<input type="text" class="form-control" data-tab="tab-btn-3" name="extend_json[{{ $key }}]" value="{{ @$extend_json->$key->value }}">

									</div>
								</div>
								@endif

								@endforeach
							</div>
							<div class="form-group" id="image-area">
								<label >Ürünün Detaylı Açıklama   </label>
								<textarea class="form-control ckeditor" data-tab="tab-btn-2"  name="product_content">{{ $product->product_content }}</textarea>
								<p class="form-text"></p>
							</div>


						</div>
						<div class="tab-pane fade " id="tab3"  role="tabpanel" aria-labelledby="tab-btn-3">

							<div class="row">
								<div class="form-group col-sm-6">
									<label  >Satış Fiyatı  :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
										</div>
										<input type="text" class="form-control pricex req float" data-tab="tab-btn-3" name="product_price" value="{{ $product->product_price }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >TL</span>
										</div>
									</div>
								</div>


								<div class="form-group col-sm-6">
									<label  >Piyasa Fiyatı :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
										</div>
										<input type="text" class="form-control float" name="product_old_price" value="{{ $product->detail->product_old_price }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >TL</span>
										</div>
									</div>
								</div>

								<div class="form-group col-sm-6">
									<label  >Alış Fiyatı :</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
										</div>
										<input type="text" class="form-control float"  name="product_get_price" value="{{ $product->detail->product_get_price }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >TL</span>
										</div>
									</div>
								</div>

								<div class="form-group col-sm-6">
									<label  >Stok Adedi <small class="text-muted">(Varyant var ise girmeyiniz Toplam hesaplanacaktır.)</small>:</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-primary"  ><i class="fa fa-refresh"></i></span>
										</div>
										<input type="text" class="form-control int"  name="product_stok" value="{{ $product->product_stok }}">
										<div class="input-group-append">
											<span class="input-group-text  bg-secondary"  >Adet</span>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="tab-pane fade " id="tab4"  role="tabpanel" aria-labelledby="tab-btn-4">


							<div class="form-group">
								<label  >Ürün Seo URL <small>(slug)</small>	:</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text bg-secondary"  >{{ config('app.url') }}/</span>
									</div>
									<input type="text" class="form-control seoname_target req" name="product_url" value="{{ $product->product_url }}">
								</div>
							</div>


							<div class="form-group">
								<label >Ürün Sayfa Başlığı <small>(Page Title)</small></label>
								<textarea class="form-control say lh23" data-say="{{ seoLimit('title') }}" name="product_title">{{ $product->product_title }}</textarea>
								<p class="form-text"></p>
							</div>




							<div class="form-group">
								<label >Sayfa Açıklaması <small>(meta desc.)</small></label>
								<textarea class="form-control say h100 lh23" data-say="{{ seoLimit('desc') }}" name="product_desc">{{ $product->product_desc }}</textarea>
								<p class="form-text"></p>
							</div>



							<div class="form-group">
								<label >Anahtar Kelimeler <small>(meta keyw.)</small></label>
								<textarea class="form-control say h100 lh23" data-say="{{ seoLimit('desc') }}" name="product_keyw">{{ $product->product_keyw }}</textarea>
								<p class="form-text"></p>
							</div>




						</div>

						<div class="tab-pane fade " id="tab5"  role="tabpanel" aria-labelledby="tab-btn-5">

							<?php foreach ($product->market  as $var):
								if ($var->market_id == 1):
									$json = json_decode($var->jsons);


									?>



									<div class="col-md-6">
										<table class="table table-bordered table-sm">
											<tr>
												<th>Trendyol Kategorisi</th>
												<td>{{ $json->cat_remote_name }}  <button type="button" class="btn btn-dark btn-sm float-right" onclick="tryol_change_cat({{ $product->id }})">Değiştir</button></td>
											</tr>
											<tr>
												<th>Eşleşen Kategorisi</th>
												<td>{{ $json->cat_local_name }}</td>
											</tr>
											<tr>
												<th>Marka/Yayınevi</th>
												<td>{{ $json->brand_local_name }} </td>
											</tr>

											<tr>
												<th>Fiyat</th>
												<td>



													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text  bg-primary"  ><i class="fa fa-try"></i></span>
														</div>
														<input type="text" class="form-control  float" id="tryol_price" name="tryol_price" value="{{ $var->price }}">
														<div class="input-group-append">
															<span class="input-group-text  bg-secondary"  >TL</span>
														</div>
													</div>


												</td>
											</tr>

											<tr>
												<td>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="trend_fixed"  {{  $var->fixed == 1 ? 'checked' : ''}} id="trend_fixed" value="1"  >
														<label class="custom-control-label fsz12" for="trend_fixed">Fiyatı Sabitle</label>
													</div>
												</td>
												<td  >
													<button class="btn btn-dark btn-sm tr_price_update" data-id="" type="button">Sadece Firsat Güncelle</button>
													<button class="btn btn-dark btn-sm tr_price_update" data-id="{{ $product->id }}" type="button">Sadece Fiyatı Güncelle</button>
													<button class="btn btn-dark btn-sm" type="button" onclick="saveMarketProduct({{ $product->id }})" >Kategoriyi Güncelle</button>
												</td>
											</tr>
										</table>
									</div>

									<?php endif; endforeach;?>



								</div>
							</div>
						</div>
					</div>

					<button type="button" data-id="productform" class="btn btn-block btn-primary sbmt_btn_spec">Kaydet</button>
				</div>





			</form>




			<div class="modal fade " id="globalmodal" tabindex="-1" role="dialog" aria-labelledby="globalmodal_title" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="globalmodal_title">Şifre Kurtarma</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="globalmodal_content">



						</div>

					</div>
				</div>
			</div>


			@endsection


			@section('script')
			<script src="//cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
			<script src="{{ asset('dist/panel/js/simpleUpload.min.js')}}"></script>
			<script>


				$(".tr_price_update").click(function() {

					price = $('#tryol_price').val()
					fixedmi = $('#trend_fixed').prop('checked') ?1:0
					id = $(this).data('id')


					$.ajax({
						type: 'POST',
						url: '/crudv4/trendyoljop/stockUpdateById/'+id,
						data: {price: price,fix:fixedmi},
						success: function(){
							sole("İşlem Başarılı","Güncelleme işlemi başarılı")
						}
					});


				});



				$(".sbmt_btn_spec").click(function() {

					data_id  = $(this).data('id');

					if($('#product_cat').val() == 0){
						$('#catpanel').css('border','1px solid red');
						sole('Hata','Kategori Seçmediniz')
						die();
					}else{
						$('#catpanel').css('border','1px solid green');
					}


					$('#'+data_id+' .req').each(function(index, element) {

						var degert = $(this).val();

						if(degert ==''){

							$(this).css('border-color','red');
							tabID = $(this).data('tab');
							$('#'+tabID).click()
							$(this).focus();

							die();
						} else {

							$(this).css('border-color','green');

						}
					});


					if(CKEDITOR.instances["product_content"].getData() == ""){

						$('#tab-btn-2').click()
						sole('Hata','Ürün içeriği Boş')
						die();

					}


					$(this).attr("disabled","disabled");

					$(this).css("opacity","0.6");

					$(this).html("Bekleyiniz");




					setTimeout(function(){ $('#'+data_id).submit(); }, 1000);


				});



				function secilicats() {

					labelsiler = "";
					i = 0;
					idx= 0;
					master_id = 0

					$('.catsec').each(function(index, element) {

						if ($(this).prop( "checked" ) == true )
						{
							labelsiler =  labelsiler+'<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline"><input id="mcat'+$(this).val()+'" onclick="$(\'#product_cat\').val('+$(this).val()+')" name="product_main_cat" value="'+$(this).val()+'" type="radio" class="custom-control-input bg-primary req" ><label class="custom-control-label" for="mcat'+$(this).val()+'">'+$(this).data('name')+'</label></div>';
							i++;
							idx = $(this).val()




						}
					});


					if(i == 0){
						$('#product_cat').val(0)
					}




					if(i>0){
						$('.cat-selected').html("<b>Birincil Kategori </b>: "+labelsiler+'<br><p class="fsz10"> Diğerleri Ek kategorisi olacaktır</p>');
					}else{
						$('.cat-selected').html('<span class="color-red"> En az Bir Kategori Seçmelisiniz !</span>');
					}



					new_id = $('#product_cat').val()

					$('#mcat'+new_id).prop('checked', 'checked')


				}




				$(".search-cat").on('keyup', function() {
					var search = $(this).val().toLowerCase();
			//Go through each list item and hide if not match search

			$(".cat_list_hnk .listcat-item").each(function() {
				if ($(this).children('div').children('div').children('input').data('name').toLowerCase().indexOf(search) != -1) {
					$(this).show();

				}
				else {
					$(this).hide();
				}

			});


		});


				$('.catsec').click(function() {

					product_cat = $('#product_cat').val()

					if(product_cat == 0){
						vals = $(this).val()
						$('#product_cat').val(vals)
					}

					secilicats()


				});

				jQuery(document).ready(function($) {
					secilicats()

				});




				if($("#resimlerload").length  ){


					$("#resimlerload").sortable({

						cursor: "move",
						opacity: 0.5,
						stop : function(event, ui){
							$("#resimler_json").val(
								JSON.stringify(
									$("#resimlerload").sortable(
										'toArray',
										{
											attribute : 'id'
										}
										)
									)
								);


							imageSiraEdit()
						}
					});

				}




				$('.upload-img').click(function() {

					id=$(this).data('id')
					name=$(this).data('name')
					key=$(this).data('key')

					$('#resim').data('id',id);
					$('#resim').data('name',name);
					$('#resim').data('key',key);
					$('#resim').click();

				});


				$('.del-img').click(function() {

					id=$(this).data('id')
					name=$(this).data('name')
					key=$(this).data('key')



					$.ajax({
						type: 'POST',
						url: '/crudv4/product/eraseimage/'+id,
						data: {name: name,key:key},
						success: function(){
							location.reload()
						}
					});



				});




				$('#resim').change(function() {


					id=$(this).data('id')
					name=$(this).data('name')
					UploadUrl = '/crudv4/product/updateimg/'+id
					key=$(this).data('key')


					var file_data = document.getElementById('resim').files[0];
					var form_data = new FormData();
					form_data.append("resim", file_data)
					form_data.append("name", name)
					form_data.append("key", key)



					$.ajax({
						url: UploadUrl,

						cache: false,
						contentType: false,
						processData: false,
						data: form_data,
						type: 'POST',
						success: function(msg){

							if(name == "new"){
								location.reload()
							}else{
								$('#resim__'+key).attr('src', msg);
							}
						}
					})



				});






				function imageSiraEdit() {



					var resimler = $('#resimler_json').val();
					var id = $('#resimler_json').data("id");



					const obj = JSON.parse(resimler);


					let value="yeniresim";
					var tempData = [];

					Object.keys(obj).forEach(function(key){
						if(obj[key]==value){
							delete obj[key];
						}else{
							tempData.push(obj[key] )
						}
					});

					const resimlerx = JSON.stringify(tempData);



					$.ajax({
						type: 'POST',
						url: '/crudv4/product/update/'+id,
						data: {val: resimlerx,label:"product_image",detail:1},
						success: function(){
							sole("İşlem Başarılı","Güncelleme işlemi başarılı")
						}
					});

				}


				function updater(id,value,lbl) {
					$.ajax({
						type: 'POST',
						url: '/crudv4/product/update/'+id,
						data: {val: value,label:lbl},
						success: function(){
							sole("İşlem Başarılı","Güncelleme işlemi başarılı")
						}
					});
				}



				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


				$( "#yazar_id" ).select2({
					theme: 'bootstrap4',
					width: '100%',
					val:'{{ $product->extend->yazar_id }}',
					dropdownAutoWidth: true,
					ajax: {
						url: '/crudv4/product/yazar_ara',
						type: "post",
						dataType: 'json',
						delay: 250,
						data: function (params) {
							return {
								_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});


				$( "#yayinci_id" ).select2({
					theme: 'bootstrap4',
					width: '100%',
					val:'{{ $product->extend->yayinci_id }}',
					dropdownAutoWidth: true,
					ajax: {
						url: '/crudv4/product/yayinci_ara',
						type: "post",
						dataType: 'json',
						delay: 250,
						data: function (params) {
							return {
								_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});




				function tryol_change_cat(id) {
					$.ajax({
						type: 'GET',
						url: '/crudv4/trendyol/changeCat/'+id,
						success: function(data){
							modalyap('globalmodal','Kategori Güncelleme',data)
						}
					});
				}




				function saveMarketProduct(id) {
					$.ajax({
						type: 'GET',
						url: '/crudv4/trendyoljop/updateByID/'+id,
						success: function(data){
							location.reload()
						}
					});
				}




			</script>


			@endsection
