	
<textarea   id="resimler_json" class="d-none" data-id="{{ $product->id }}"></textarea>
<div id="resimlerload" class=" p10 h200">
	@if (strlen($product->detail->product_image)>5 )


	@foreach (json_decode($product->detail->product_image) as $key =>  $var)
	<div class="col-md-2 p0  float-left mt5 pos-r resim-item" id="{{ $var }}" >
		<div class="handler"></div>
		<img src="{{ res2($product->product_group_code,($var),"250x250") }}" class="img-thumbnail " id="resim__{{ $key }}"  >

		<button type="button"  class="btn btn-primary btn-sm left20 actbtn upload-img" data-key="{{ $key }}"  data-id="{{ $product->id }}" data-name="{{ $var }}"><i class="fa fa-upload"></i></button>
		<button  type="button" class="btn btn-danger btn-sm left60 actbtn del-img" data-key="{{ $key }}" data-id="{{ $product->id }}" data-name="{{ $var }}"><i class="fa fa-trash"></i></button>



	</div>

	@endforeach
	@endif


	<div class="col-md-2 p0  float-left mt5 pos-r resim-item" id="yeniresim" >

		<img src="{{ asset('uploads/content/resimsec.jpg') }}" class="img-thumbnail cur-p upload-img"  id="resim_new"  data-key="new"  data-id="{{ $product->id }}" data-name="new"    >

	</div>

</div>



