@extends('admin.layouts.master')
@section('title', $title)



@section('style')
<style>
	.iq-card:hover .konum{
		display: block;

	}
</style>
@endsection
@section('body')



<div class="col-md-12">
	<div class="ais-InstantSearch">
		<div class="row">
			<div class="col-md-3">
				<div class="left-panel">
					<div class="iq-card py-2 px-3">
						<div id="clear-refinements" class="float-right">
							<a  href="{{ route('crudv4.product.add') }}" class="btn btn-primary tipsi border-0 btn-sm mr-2" title="Yeni Ürün ekle"><i class="fa fa-plus text-white mr-0"></i></a> 
							@if (request()->hasAny(['id',"product_name","product_cat","limit","orderby","is_active",'product_price','product_stok','yayinci_id','yazar_id']))
							<a  href="{{ route('crudv4.product') }}"  class="btn btn-dark tipsi border-0 btn-sm mr-2" title="Filtreyi Temizle"><i class="ri-refresh-line mr-0"></i></a>

							@endif
						</div>
						<b class="text-primary">{{ $list->total() }}</b> Adet Ürün Listelendi 
					</div>
					<div class="iq-filter-border iq-card">
						<h5 class="card-title">Durum</h5>
						<div id="numeric-menu">
							<div class="ais-NumericMenu">
								<ul class="ais-NumericMenu-list pl10">
									<li class="ais-NumericMenu-item ais-NumericMenu-item--selected">
										<div>
											<label class=" "> 
												<input type="radio" class="ais-NumericMenu-radio radiofilter" name="is_active" value="2" @if(request()->has('is_active') and request('is_active') == "2") checked @endif data-filter="is_active"> 
												<span class="ais-NumericMenu-labelText">Tüm Ürünler</span> 
											</label>
										</div>
									</li>
									<li class="ais-NumericMenu-item">
										<div>
											<label class="ais-NumericMenu-label"> 
												<input type="radio" class="ais-NumericMenu-radio radiofilter" name="is_active" @if(request()->has('is_active') and request('is_active') == "1") checked @endif value="1" data-filter="is_active"> 
												<span class="ais-NumericMenu-labelText">Aktif Ürünler</span> 
											</label>
										</div>
									</li>
									<li class="ais-NumericMenu-item">
										<div><label class="ais-NumericMenu-label"> 
											<input type="radio" class="ais-NumericMenu-radio radiofilter" name="is_active" @if(request()->has('is_active') and request('is_active') == "0") checked @endif  value="0" data-filter="is_active"> 
											<span class="ais-NumericMenu-labelText">Satışa Kapalı Ürünler</span> 
										</label>
									</div>
								</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="iq-filter-border iq-card">
					<h5 class="card-title">Filtrele </h5>

					<hr>
					<div class="form-group">
						<label  >Kategori:</label>
						<div class="input-group ">
							{!! addSelectone($main_cats,"Tüm Kategoriler") !!}
						</div>
					</div>	

					<hr>
					<div class="form-group">
						<label  >Yayınevi:</label>
						<div class="input-group ">
							<select class="form-control selectfilter form-control-sm mb-3" id="yayinci_ara" data-filter="yayinci_id">
								<option value="0">Tüm Yayıncılar</option>
							</select>

						</div>
					</div>

					<hr>
					<div class="form-group">
						<label  >Yazarlar:</label>
						<div class="input-group ">
							<select class="form-control selectfilter form-control-sm mb-3" id="yazar_ara" data-filter="yazar_id">
								<option value="0">Tüm Yazarlar</option>


							</select>
						</div>
					</div>

				</div>


			</div>
		</div>
		<div class="col-md-9">
			<div class="right-panel">
				<div class="row">

					<div class="col-md-12">
						<div class="iq-card">
							<div class="d-flex align-items-center justify-content-between pl-2 pr-3">
								<div class="d-flex col-md-6">



									<div class="form-group mt5 w350">

										<div class="input-group">

											<input type="text" class="form-control"  placeholder="Ürün Adı veya Stok Kodu ile ara...">

											<div class="input-group-append">
												<span class="input-group-text bg-primary "  ><i class="fa fa-search"></i></span>
											</div>
										</div>
									</div>





								</div>


								<div class="d-flex col-md-6">
									<div id="sort-by" class="iq-algolia-sort">



										<div class="form-row">
											<div class="form-group col-md-8 pt20">

												<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
													<option value="rank,asc">Sırala</option>

													<option value="product_name-asc" @if(request()->has('orderby') and request('orderby') == "product_name-asc") selected @endif>Ad Artan</option>
													<option value="product_name-desc" @if(request()->has('orderby') and request('orderby') == "product_name-desc") selected @endif>Ad Azalan</option>
													<option value="product_price-asc" @if(request()->has('orderby') and request('orderby') == "product_price-asc") selected @endif>Sıra Artan</option>
													<option value="product_price-desc" @if(request()->has('orderby') and request('orderby') == "product_price-desc") selected @endif>Sıra Azalan</option>
													<option value="product_stok-asc" @if(request()->has('orderby') and request('orderby') == "product_stok-asc") selected @endif>Stok Artan</option>
													<option value="product_stok-desc" @if(request()->has('orderby') and request('orderby') == "product_stok-desc") selected @endif>Stok Azalan</option>


												</select>
											</div>
											<div class="form-group col-md-4 pt20">
												<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
													<option value="25">Göster</option>
													@for ($i = 30; $i <121 ; $i+=30)
													<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

													@endfor
												</select>
											</div>


										</div>

									</div>
									<div class="d-flex">
										<a href="{{ route('crudv4.product') }}" class="btn h40 mt20 tipsi  mr-2 btn-primary" title="Blok görünüm">
											<i class="ri-grid-fill mr-0"></i>
										</a>
										<a href="{{ route('crudv4.product.list') }}" class="btn h40 mt20  btn btn- iq-bg-primary tipsi" title="Liste görünüm">
											<i class="ri-list-unordered mr-0"></i>
										</a>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div id="hits" class="iq-product-layout-grid">
							<div class="ais-Hits iq-product">
								<ul class="ais-Hits-list iq-product-list row pl0">

									@foreach ($list as $row)



									<li id="tr_{{ $row->id }}" class=" col-4">
										<div class="iq-card">
											<div class="text-center">

												<div class="d-flex align-items-center justify-content-center bg-white iq-border-radius-15">
													<img src="{{ res("product",jres($row->detail->product_image),"250x250") }}" class="rounded w150">
												</div>

												<div class="card-body">
													<div class="text-justify">
														<a href="javascript:void(0)" class="fsz12">{{ str_limit($row->product_name,45) }}</a>
														<p class="font-size-12 mb-0">{{ $row->get_category->categories_name }}  </p>
														<p class="font-size-12 mb-0">SKOD:{{ $row->product_stock_code }} </p>

													</div>
													<div class="iq-product-action my-2">
														<a href="{{ route('crudv4.product.edit',$row->id) }}" class="btn btn-primary iq-waves-effect text-uppercase btn-sm addToCart" id="5477500">
															<i class="fa fa-edit mr-0"></i>
														</a>
														<button type="button" class="btn btn-danger iq-waves-effect text-uppercase btn-sm addToWish" id="5477500">
															<i class="fa fa-trash mr-0"></i>
														</button>
														<p class="font-size-16 font-weight-bold float-right">{{ $row->product_price }}</p>



														<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
															<div class="custom-switch-inner">

																<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitch-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_active"

																{{ $row->is_active === 1 ? "checked" : "" }}>
																<label class="custom-control-label" for="customSwitch-{{$loop->iteration}}">
																	<span class="switch-icon-left"><i class="fa fa-check"></i></span>
																	<span class="switch-icon-right"><i class="fa fa-check"></i></span>
																</label>
															</div>
														</div>



													</div>
												</div>
											</div>


											<div class="form-group col-md-12 dnone konum">
												@foreach (konum() as $key => $var)

												<div class="custom-control custom-checkbox float-left col-md-6 m0">
													<input type="checkbox"  class="custom-control-input konumbtn" data-id="{{ $row->id }}" data-name="{{ $key }}" id="{{ $key.'_'.$row->id }}" {{ $row->detail->$key==1?'checked':'' }}>
													<label class="custom-control-label" for="{{ $key.'_'.$row->id }}">{{ $var }}</label>
												</div>

												@endforeach
												<div class="clearfix"></div>
												<hr>
											</div>


										</div>
									</li>
									@endforeach


								</ul>
							</div>
						</div>
					</div>
					{{ $list->withQueryString()->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
</div>


@endsection


@section('script')

<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/product/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}

	$('.textfilter').keyup(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()

		if(event.keyCode == 13){

			repUrl(filterType,val)

		}
	});



	$('.selectfilter').change(function(event) {



		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});


	$('.radiofilter').click(function(event) {

		filterType  = $(this).data('filter')
		val = $('.radiofilter:checked').val();


		repUrl(filterType,val)

	});



	$('.rank_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label,0)

		}


	});


	$('.rank_update').focusout(function() {


		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label,0)

	}); 



	$('.rank_update_click').click(function() {
		id = $(this).data("id")
		val= $("#rank_"+id).val()
		label = $(this).data('name')

		updater(id,val,label,0)


	});


	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')

		updater(id,val,label,0)


	});

	$('.konumbtn').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')


		updater(id,val,label,1)


	});



	function updater(id,value,lbl,detay = 0) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/product/update/'+id,
			data: {val: value,label:lbl,detail:detay},
			success: function(){

				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});	
	}




	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


	$( "#yazar_ara" ).select2({
		theme: 'bootstrap4',
		width: '100%',
		dropdownAutoWidth: true,
		ajax: { 
			url: '/crudv4/product/yazar_ara',
			type: "post",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});


	$( "#yayinci_ara" ).select2({
		theme: 'bootstrap4',
		width: '100%',
		dropdownAutoWidth: true,
		ajax: { 
			url: '/crudv4/product/yayinci_ara',
			type: "post",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});

	



</script>
@endsection
