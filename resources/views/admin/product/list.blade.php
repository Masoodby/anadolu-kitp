@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $list->total() }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
							<option value="rank,asc">Sırala</option>

							<option value="product_name-asc" @if(request()->has('orderby') and request('orderby') == "product_name-asc") selected @endif>Ad Artan</option>
							<option value="product_name-desc" @if(request()->has('orderby') and request('orderby') == "product_name-desc") selected @endif>Ad Azalan</option>
							<option value="product_price-asc" @if(request()->has('orderby') and request('orderby') == "product_price-asc") selected @endif>Sıra Artan</option>
							<option value="product_price-desc" @if(request()->has('orderby') and request('orderby') == "product_price-desc") selected @endif>Sıra Azalan</option>
							<option value="product_stok-asc" @if(request()->has('orderby') and request('orderby') == "product_stok-asc") selected @endif>Stok Artan</option>
							<option value="product_stok-desc" @if(request()->has('orderby') and request('orderby') == "product_stok-desc") selected @endif>Stok Azalan</option>


						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 30; $i <121 ; $i+=30)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">



			<div id="filter">
				<table class="table">
					<tr>
						<th  >
							<input type="text" class="form-control textfilter w50  float-left from-control-sm" data-filter="id"  placeholder="ID..." value= "{{ request()->has('id') ? request('id'):''}}">

							<select class="form-control selectfilter w150 float-right" data-filter="is_market" >
								<option value="">Pazaryeri Ürünleri</option>

								<option value="1-1">Trendyol Satışta</option>
								<option value="1-0">Trendyol Satışda Değil</option>
								<option value="2-2">N11 Satışta</option>
								<option value="2-0">N11 Satışta Değil</option>
								<option value="3-1">HB Satışta</option>
								<option value="3-0">HB Satışta Değil</option>

							</select>

						</th>

						<th  >
							<input type="text" class="form-control textfilter from-control-sm" data-filter="product_name"  name="product_name" id="product_name" placeholder="Ürün Adı  ile ara..." value= "{{ request()->has('product_name') ? request('product_name'):''}}">
						</th>

						<th  >
							<input type="text" class="form-control textfilter from-control-sm" data-filter="product_stock_code"  name="product_stock_code" id="product_stock_code" placeholder="Stok Kodu ile ara..." value= "{{ request()->has('product_stock_code') ? request('product_stock_code'):''}}">
						</th>


						<th  >
							<select class="form-control selectfilter" data-filter="is_active" >
								<option value="">Aktif/Pasif</option>
								<option value="1" @if(request()->has('is_active') and request('is_active') == 1) selected @endif>Aktif</option>
								<option value="0" @if(request()->has('is_active') and request('is_active') == 0) selected @endif>Pasif</option>
							</select>

							<select class="form-control selectfilter" data-filter="resim_durum" >
								<option value="">Resim Durum</option>
								<option value="1" @if(request()->has('resim_durum') and request('resim_durum') == 1) selected @endif>Var</option>
								<option value="2" @if(request()->has('resim_durum') and request('resim_durum') == 2) selected @endif>Yok</option>
							</select>
							<select class="form-control selectfilter" data-filter="is_depo" >
								<option value="">Depo Durum</option>
								<option value="1" @if(request()->has('is_depo') and request('is_depo') == 1) selected @endif>Var</option>
								<option value="0" @if(request()->has('is_depo') and request('is_depo') == 0) selected @endif>Yok</option>
							</select>
						</th>




					</tr>

					<tr>
						<td>
							<div class="form-group">

								<div class="input-group ">
									<select class="form-control selectfilter form-control-sm mb-3" id="yayinci_ara" data-filter="yayinci_id">
										<option value="0">Tüm Yayıncılar</option>
									</select>

								</div>
							</div>




						</td>
						<td>
							<div class="form-group">

								<div class="input-group ">
									<select class="form-control selectfilter form-control-sm mb-3" id="yazar_ara" data-filter="yazar_id">
										<option value="0">Tüm Yazarlar</option>


									</select>
								</div>
							</div>
						</td>

						<td>
							<div class="form-group">

								<div class="input-group ">
									{!! addSelectone($main_cats,"Tüm Kategoriler") !!}
								</div>
							</div>

						</td>


						<td >
							@if (request()->hasAny(['id',"categories_name","categories_main","limit","orderby","is_active",'product_stock_code']))
							<a href="{{ route('crudv4.product') }}" class="btn btn-dark mt10 float-right btn-sm tipsi" title="Filtreyi Sıfırla"><i class="fa fa-refresh"></i></a>
							@endif
							<a href="{{ route('crudv4.product.add') }}" class="btn btn-success mt10 float-right btn-sm tipsi" title="Yeni Ekle"><i class="fa fa-plus"></i>Yeni Ekle</a>

						</td>

					</tr>
				</table>
			</div>



			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>

							<th scope="col">Ad</th>
							<th scope="col"> </th>
							<th scope="col">Yazar/Yayın Evi</th>
							<th scope="col">Durum</th>
							<th scope="col">Konum</th>

							<th scope="col">İşlem</th>

						</tr>

					</thead>
					<tbody>
						@if(count($list)==0)
						<div class="alert alert-warning">Ürün bulunamadı..</div>
						@else
						@foreach($list as $row)
						<tr id="tr_{{ $row->id }}">

							<td scope="row">{{$loop->iteration}}</td>
							<td>
								<div class="d-flex align-items-center justify-content-center bg-white iq-border-radius-15">

									<img src="{{  ress($row->product_stock_code,$row->product_group_code,"250x250") }}" class="rounded w150" ondblclick="$('#resim_{{ $row->id }}').click();" id="image_{{ $row->id }}">


									<input type="file" class="d-none resim" name="resim" data-id="{{ $row->id }}"  id="resim_{{ $row->id }}">
								</div>
							</td>
							<td  >
								<a href="javascript:void(0)" class="fsz12">{{ str_limit($row->product_name,45) }}</a>
								<p class="font-size-12 mb-0">Kategori : {{ $row->get_category->categories_name }}  </p>
								<p class="font-size-12 mb-0">SKOD:{{ $row->product_stock_code }} </p>
								<p class="font-size-12 mb-0">Stok Adedi : {{ $row->product_stok }} </p>
								<p class="font-size-12 mb-0">Site Fiyatı : {{ $row->product_price }} TL</p>
								@php
								$markets = [1=>"Trendyol",2=>"N11",3=>'HB'];
								@endphp
								@foreach ($row->market as $item)
								@php
								$indis = $item->market_id;
								@endphp
								<p class="font-size-12 mb-0">{{ $markets[$indis] }} Fiyatı :  {{ $item->price }} TL</p>
								@endforeach

							</td>

							<td>
								{{ $row->extend->getYazar->yazar_name }} <br>
								{{ $row->extend->getYayinci->yayinci_name }}

							</td>
							<td>



								<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
									<div class="custom-switch-inner">

										<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitch-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_active"

										{{ $row->is_active === 1 ? "checked" : "" }}>
										<label class="custom-control-label" for="customSwitch-{{$loop->iteration}}">
											<span class="switch-icon-left"><i class="fa fa-check"></i></span>
											<span class="switch-icon-right"><i class="fa fa-check"></i></span>
										</label>
									</div>
								</div>

							</td>


							<td>
								<div class="form-group w200">
									@foreach (konum() as $key => $var)

									<div class="custom-control custom-checkbox m0">
										<input type="checkbox"  class="custom-control-input konumbtn" data-id="{{ $row->id }}" data-name="{{ $key }}" id="{{ $key.'_'.$row->id }}" {{ $row->detail->$key==1?'checked':'' }}>
										<label class="custom-control-label" for="{{ $key.'_'.$row->id }}">{{ $var }}</label>
									</div>

									@endforeach
									<div class="clearfix"></div>
									<hr>
								</div>
							</td>

							<td>
								<a href="{{ route('crudv4.product.edit',$row->id) }}" class="btn btn-success btn-sm tipsi mb-3" title="Düzenle"><i class="las la-edit"></i></a>
								<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>
							</td>


						</tr>

						@endforeach
						@endif

					</tbody>
				</table>



				{{ $list->withQueryString()->links() }}
			</div>



		</div>
	</div>
</div>



@endsection


@section('script')
<script>


	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/product/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			}

		});
	}

	$('.textfilter').keyup(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()

		if(event.keyCode == 13){

			repUrl(filterType,val)

		}
	});



	$('.selectfilter').change(function(event) {



		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});


	$('.radiofilter').click(function(event) {

		filterType  = $(this).data('filter')
		val = $('.radiofilter:checked').val();


		repUrl(filterType,val)

	});



	$('.rank_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label,0)

		}


	});


	$('.rank_update').focusout(function() {


		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label,0)

	});



	$('.rank_update_click').click(function() {
		id = $(this).data("id")
		val= $("#rank_"+id).val()
		label = $(this).data('name')

		updater(id,val,label,0)


	});


	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')

		updater(id,val,label,0)


	});

	$('.konumbtn').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')


		updater(id,val,label,1)


	});



	function updater(id,value,lbl,detay = 0) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/product/update/'+id,
			data: {val: value,label:lbl,detail:detay},
			success: function(){

				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});
	}




	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


	$( "#yazar_ara" ).select2({
		theme: 'bootstrap4',
		width: '100%',
		dropdownAutoWidth: true,
		ajax: {
			url: '/crudv4/product/yazar_ara',
			type: "post",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});


	$( "#yayinci_ara" ).select2({
		theme: 'bootstrap4',
		width: '100%',
		dropdownAutoWidth: true,
		ajax: {
			url: '/crudv4/product/yayinci_ara',
			type: "post",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					_token: CSRF_TOKEN,
              search: params.term // search term
          };
      },
      processResults: function (response) {
      	return {
      		results: response
      	};
      },
      cache: true
  }

});



	$('.resim').change(function() {


		id=$(this).data('id')

		UploadUrl = '/crudv4/product/updateimgList/'+id



		var file_data = document.getElementById('resim_'+id).files[0];
		var form_data = new FormData();
		form_data.append("resim", file_data)




		$.ajax({
			url: UploadUrl,

			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(msg){

				if(msg == "0"){
					sole('Hata','Resim Yüklenemedi')
				}else{
					$('#image_'+id).attr('src', msg);
				}
			}
		})



	});




</script>
@endsection
