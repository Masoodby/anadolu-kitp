@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<style>
	.raftable{
		border:1px solid #ccc;
		text-align: center;
	}

	.raftable tr th,.raftable tr td{
		border:1px solid #ccc;
		text-align: center;

		display: table-cell!important;

	}

	td.active{
		border: 1px solid black!important;
	} 

	td.active h4{
		color: black;
	} 

	td h4{
		color: #ccc;
	} 

</style>


<div class="col-sm-12 col-lg-12">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body">


			<div class="table-responsive">
				<table class="table raftable text-center">

					<tr>
						<th class="w40  text-center">#</th>


						@foreach (dizi_creat_sayi($oda->oda_start+$oda->oda_yatay,$oda->oda_start+1) as  $yatay) 




						<th class="text-center pl30 pr30 fsz20">{{ $yatay }}</th>

						<?php endforeach ?>


					</tr>




					@foreach (dizi_creat_harf($oda->oda_dikey) as   $row) 
					<tr> 
						<th class="text-center w40 fsz20">{{ $row }}</th>


						@foreach (dizi_creat_sayi($oda->oda_start+$oda->oda_yatay,$oda->oda_start+1) as  $yatay) 

						@php

						$rafName = $yatay.$row;



						if (isset($raflar[$rafName]) && $raflar[$rafName]['adet'] > 0) {


							$aktifRaf = true;

							$kitap_adet = $raflar[$rafName]['adet'];
						}else{

							$aktifRaf = false;
							$kitap_adet = 0;
						}

						@endphp

						<td class="w200   {{ $aktifRaf?" active ".rafrenk($kitap_adet):"" }}">





							<h4>
								{{ $rafName }}
							</h4>
							{{ $kitap_adet > 0?$kitap_adet.' Kitap':"Boş Raf" }}

							<br>
							<button class="btn btn-{{ $aktifRaf?"dark":"success" }} btn-sm rafislem" data-name="{{ $rafName }}" 
							data-oda="{{ $oda->id }}"  >İşlem</button>
						</td>

						@endforeach


					</tr>
					@endforeach


				</table>
			</div>


		</div>
	</div>



</div>	

<div class="modal fade" id="bigmodal" tabindex="-1" role="dialog"   aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="bigmodal_title">Modal title</h5>
				<button type="button" class="close updateclose"  >
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="bigmodal_content">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary updateclose" id="bigmodalbtnright" >Kapat</button>
				<button type="button" class="btn btn-primary" id="bigmodalbtnleft">Save changes</button>
			</div>
		</div>
	</div>
</div>

@endsection


@section('script')
<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/depo/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}


	$('.rafislem').click(function() {

		rafName = $(this).data('name');
		odaID = $(this).data("oda");



		$.ajax({
			type: 'POST',
			url: '/crudv4/raf/islem/' + odaID,
			data:{name:rafName},
			success: function (data) {
				modalyap('bigmodal','Raf İşlemleri ('+rafName+')',data);
				$('.updateclose').attr({
					"data-oda": odaID,
					'data-name': rafName
				})

			} 

		});



	});	


	$('.updateclose').click(function() {

		rafName = $(this).data('name');
		odaID = $(this).data("oda");


		$.ajax({
			type: 'POST',
			url: '/crudv4/trendyoljop/rafstockUpdate' ,
			data:{name:rafName,oda:odaID},
			success: function (data) {


				//$("#bigmodal_content").html(data);
				$("#bigmodal").modal("hide");

			} 

		});

		
		


	});



</script>


@endsection