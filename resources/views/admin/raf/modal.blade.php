 

<div class="col-md-12">


	<div class="form-group row">

		<div class="col-sm-7">

			<div class="form-group">

				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text  bg-primary"  ><i class="fa fa-barcode"></i></span>
					</div>
					<input id="isbn" type="text"  class="isbn form-control islematn" placeholder="ISBN" name="barkod">
				</div>
			</div>



		</div>	
		<div class="col-sm-3">


			<input id="adetinput" type="number" value="1" class="form-control islematn" name="adetinput">

			<input type="hidden" value="{{ $raf->id }}" class="islematn" name="rafid">
		</div>

		<div class="col-md-2">
			<button class="btn btn-success btn-xs addkitap"><i class="fa fa-check"></i></button>
		</div>
	</div>






</div>


<div id="urunler_result" class="col-md-12">


	<table class="table table-bordered">
		<tr>
			<th>#</th>
			<th>Ürün Adı</th>
			<th>ISBN</th>
			<th>Adet</th>
			<th>İşlem</th>
		</tr>


		@foreach ($books as $row)

		@if ($row->adet > 0)


		<tr id="tr_{{ $row->id }}">
			<td scope="row">{{$loop->iteration}}</td>
			<th>{{ $row->book_id==0? " Eklenmemiş Kitap":$row->getBook->product_name }}</th>
			<th>{{ $row->isbn }}</th>
			<th>

				<div class="input-group input-group-sm mb-3 w-50">

					<input type="text" class="form-control adet_update" data-name="adet" id="adet_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->adet }}">

					<div class="input-group-append">
						<span class="input-group-text cur-p adet_update_click"   data-name="adet" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
					</div>
				</div>

			</th>

			<th><button class="btn btn-danger btn-xs"  onclick="allConfirm('eraseci',{{ $row->id }})"  data-id="{{ $row->id }}" ><i class="fa fa-times"></i></button></th>
		</tr>


		@endif

		@endforeach

	</table>



</div>



<script>


	$(document).ready(function() {
		setTimeout(function() { $('#isbn').focus() }, 1000);
		$
		//$('#bigmodalbtnright').hide()
		$('#bigmodalbtnleft').hide()
	});


	$('.isbn').keypress(function (event) {

		var keycode = (event.keyCode ? event.keyCode : event.which);


		if (keycode == '13') {


			if ($(this).val().length > 5 && $(this).val().length < 15){

				$.ajax({
					type: 'POST',
					url: '/crudv4/raf/addbook',
					data:$('.islematn').serialize(),
					success: function (data) {

						$('#isbn').val('')
						$('#isbn').focus()					
						$('#urunler_result').html(data);
					} 

				});


			}else{
				sole('Hata','Hatalı Barkod')
			}

		}
	});


	$('.addkitap').click(function() {



		isbn = $('#isbn').val()
		if (isbn !="") {


			if (isbn.length > 5 && isbn.length < 15){

				$.ajax({
					type: 'POST',
					url: '/crudv4/raf/addbook',
					data:$('.islematn').serialize(),
					success: function (data) {

						$('#isbn').val('')
						$('#isbn').focus()					
						$('#urunler_result').html(data);
					} 

				});


			}else{
				sole('Hata','Hatalı Barkod')
			}




		}else{
			sole('Hata',"ISBN Boş Geçilemez")
		}

	});


	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/raf/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}


	$('.adet_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}


	});


/*
	$('.adet_update').focusout(function() {


		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label)

	}); 

	*/

	$('.adet_update_click').click(function() {
		id = $(this).data("id")
		val= $("#adet_"+id).val()
		label = $(this).data('name')

		updater(id,val,label)


	});




	function updater(id,value,lbl) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/raf/adet/'+id,
			data: {val: value,label:lbl},
			success: function(){
				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});	
	}







</script>
