@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				{{-- <a href="{{ route('crudv4.slider.add') }}" class="btn btn-primary float-right btn-lg" ><i class="fa fa-plus"></i> Yeni Ekle</a> --}}
			</div>

		</div>
		<div class="iq-card-body">

			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col" >kitap</th>
						<th scope="col">isbn kodu</th>
						<th scope="col">D&R</th>
						<th scope="col">Kitap Center</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($list as $row)
					{{-- expr --}}
					<tr id="tr_{{ $row->id }}">
						<td scope="row">{{$loop->iteration}}</td>
						<td>{{ $row->product_title }}</td>
						<td>{{ $row->product_stock_code }}</td>
						<td>
                            <a href="#" class="btn btn-success btn-sm tipsi mb-3" title="Düzenle">D&R<i class="las la-edit"></i></a>
						</td>
						<td>
                            <a href="#" class="btn btn-primary btn-sm tipsi mb-3" title="Düzenle">Kitap Center<i class="las la-edit"></i></a>
						</td>
					</tr>

					@endforeach


				</tbody>
			</table>
		</div>
        {{$list->links()}}
	</div>
</div>


@endsection


@section('script')

<script>
	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/slider/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			}

		});
	}


	$('.rank_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}


	});


	$('.rank_update').focusout(function() {


		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label)

	});



	$('.rank_update_click').click(function() {
		id = $(this).data("id")
		val= $("#rank_"+id).val()
		label = $(this).data('name')

		updater(id,val,label)


	});


	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')


		updater(id,val,label)


	});



	function updater(id,value,lbl) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/banner/update/'+id,
			data: {val: value,label:lbl},
			success: function(){
				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});
	}


</script>

@endsection
