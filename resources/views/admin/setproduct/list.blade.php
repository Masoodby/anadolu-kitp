@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.slider.add') }}" class="btn btn-primary float-right btn-lg" ><i class="fa fa-plus"></i> Yeni Ekle</a>
			</div>

		</div>
		<div class="iq-card-body">

			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col" colspan="2">Set</th>
						<th scope="col">ISBN</th>
						<th scope="col">Fiyat</th>
						<th scope="col">İşlem</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($list as $row)
					{{-- expr --}}
					<tr id="tr_{{ $row->id }}">

						<td scope="row">{{$loop->iteration}}</td>
						<td><img src="{{ res( 'slider',$row->slider_image,'100x100') }}" class="img-fluid"></td>
						<td>{{ $row->slider_name }}</td>
						<td>
							<div class="input-group input-group-sm mb-3 w-50">

								<input type="text" class="form-control rank_update" data-name="rank" id="rank_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->rank }}">

								<div class="input-group-append">
									<span class="input-group-text cur-p rank_update_click"   data-name="rank" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
								</div>
							</div>
						</td>
						<td>

							<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
								<div class="custom-switch-inner">

									<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitch-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_active"

									{{ $row->is_active === 1 ? "checked" : "" }}>
									<label class="custom-control-label" for="customSwitch-{{$loop->iteration}}">
										<span class="switch-icon-left"><i class="fa fa-check"></i></span>
										<span class="switch-icon-right"><i class="fa fa-check"></i></span>
									</label>
								</div>
							</div>


						</td>
						<td>
							<a href="{{ route('crudv4.slider.edit',$row->id) }}" class="btn btn-success btn-sm tipsi mb-3" title="Düzenle"><i class="las la-edit"></i></a>
							<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

						</td>
					</tr>

					@endforeach


				</tbody>
			</table>
		</div>
	</div>
</div>


@endsection


@section('script')

<script>
	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/slider/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}


	$('.rank_update').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}
		

	});

	
	$('.rank_update').focusout(function() {
		

		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label)

	}); 



	$('.rank_update_click').click(function() {
		id = $(this).data("id")
		val= $("#rank_"+id).val()
		label = $(this).data('name')

		updater(id,val,label)


	});


	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')


		updater(id,val,label)


	});



	function updater(id,value,lbl) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/slider/update/'+id,
			data: {val: value,label:lbl},
			success: function(){
				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});	
	}


</script>

@endsection