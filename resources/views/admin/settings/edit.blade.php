@extends('admin.layouts.master')

@section('body')

<form method="POST" action="{{  route('crudv4.settings.update',$set->id)  }}" class="row">
	@csrf

	<div class="col-lg-6 pl-5">
		<div class="iq-card">

			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }}</h4>
				</div>
			</div>


			<div class="iq-card-body">




				<div class="form-group">
					<label>Ayar Tanım </label>
					<input type="text" class="form-control" name="settings_name" value="{{ $set->settings_name }}"   >
				</div>

				<div class="form-group">
					<label>Ayar Anahtarı </label>
					<input type="text" class="form-control" name="settings_key"  value="{{ $set->settings_key }}" >
				</div>



				<div class="form-group">
					<label >Ayar Değeri</label>
					<textarea class="form-control" name="settings_val" rows="5">{{ $set->settings_val }}</textarea>
				</div>





			</div>
		</div>
	</div>


	<div class="col-md-6">

		<div class="iq-card">

			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title text-right"><a href="{{ route('crudv4.settings.index') }}" class="btn btn-info float-right">Geri</a></h4>
				</div>
			</div>


			<div class="iq-card-body">






				<div class="form-group">


					<label >Ayar Değer Türü</label>


					<div class="row">
						@foreach(formElement() as $key => $var)
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline col-3 pl-5">
							<input type="radio" id="settings_type_{{ $key }}" value="{{ $var }}" name="settings_type" class="custom-control-input bg-primary" {{ $var == $set->settings_type?"checked":"" }}>
							<label class="custom-control-label" for="settings_type_{{ $key }}"> {{ $var }} </label>
						</div>
						@endforeach

					</div>

				</div>

				<hr>


				<div class="form-group">
					<label for="exampleFormControlSelect1">Ayar Sınıfı</label>
					<select class="form-control" name="settings_group">
						@foreach($setcats as $item)
						<option value="{{ $item->id }}" {{ $item->id == $set->settings_group ? "selected":""  }}>{{ $item->setcat_name }}</option>
						@endforeach

					</select>
				</div>


				<button type="submit" class="btn btn-primary">Ayar Güncelle</button>



			</div>
		</div>

	</div>
</form>


@endsection
