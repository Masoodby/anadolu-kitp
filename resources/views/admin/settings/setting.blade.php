@extends('admin.layouts.master')

@section('body')



<div class="col-lg-3">
	<div class="iq-card">
		<div class="iq-card-body">
			<div class="">
				<div class="iq-email-list">
					<button class="btn btn-primary btn-lg btn-block mb-3 font-size-16 p-3"  ><i class="ri-settings-2-line mr-2"></i>Ayar Grupları</button>
					<div class="iq-email-ui nav flex-column nav-pills">
						@php
						$setName = "Genel Ayarlar";
						@endphp
						@foreach($setcats as $item)


						<li class="nav-link {{ $item->id == $id ? "active" : "" }}" ><a href="{{ route('crudv4.settings.index', ['id' => $item->id ]) }}"><i class="ri-file-list-2-line"></i>{{ $item->setcat_name }}<span class="badge badge-primary ml-2">

							{{ App\Models\Setting::where('settings_group',$item->id )->count() }}

						</span></a></li>

						@php
						if ($item->id == $id):
							$setName = $item->setcat_name;
						endif;

						@endphp


						@endforeach

					</div>



				</div>
			</div>
		</div>
	</div>
</div>



<div class="col-md-9">


	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $setName }} <a class="btn btn-sm btn-success ml-auto ml30" href="{{ route('crudv4.settings.create', ['id' => $id ]) }}"><i class="fa fa-plus"></i> Yeni Ekle</a></h4>

			</div>
		</div>
		<div class="iq-card-body">

			@foreach ($settings as $set)

			<div class="form-group row">
				<label class="control-label col-sm-2 align-self-center mb-0" for="{{ $set->settings_key }}">{{ $set->settings_name }}:</label>
				<div class="col-sm-8">

					@if ($set->settings_type === "input_text")

					<input type="text" class="form-control auto_save tipsi" title="{{ $set->settings_key }}" name="settings_val" data-id="{{ $set->id }}" value="{{ $set->settings_val }}">


					@elseif ($set->settings_type === "input_group_text")

					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control auto_save tipsi" title="{{ $set->settings_key }}" name="settings_val" data-id="{{ $set->id }}" value="{{ $set->settings_val }}">
						<div class="input-group-append">
							<span class="input-group-text  bg-default"  ><i class="fa fa-try"></i></span>
						</div>
					</div>

					@elseif ($set->settings_type === "textarea")

					<textarea class="form-control auto_save tipsi" title="{{ $set->settings_key }}" name="settings_val" data-id="{{ $set->id }}" rows="5">{{ $set->settings_val }}</textarea>

					@elseif ($set->settings_type === "json")

					<textarea class="form-control auto_save tipsi" title="{{ $set->settings_key }}" name="settings_val" data-id="{{ $set->id }}" rows="5">{{ $set->settings_val }}</textarea>


					@elseif ($set->settings_type === "tags")

					<input type="text" class="form-control auto_save tipsi tagci" title="{{ $set->settings_key }}" name="settings_val" data-id="{{ $set->id }}" value="{{ $set->settings_val }}">


					@elseif ($set->settings_type === "input_file")



					<div class="row pt5 pb10" style="border-bottom: 1px solid #f5f5f5;">
						<div class="col-sm-6 pull-left pt10">
							<div class="custom-file">
								<input type="file" name="dosya" class="custom-file-input setfile" lang="tr" data-id="{{ $set->id }}" id="setfile{{ $set->id }}">
								<label class="custom-file-label" for="resim" data-browse="Dosya Seçiniz">{{ $set->settings_val }}</label>
							</div>
						</div>

						<div class="col-sm-6 pull-left">

							<img src="{{ asset('uploads/content/'.$set->settings_val) }}?v=@php echo rand(9,999) @endphp" class="img-thumbnail" >
						</div>

					</div>



					@else

					<input type="text" class="form-control auto_save tipsi" title="{{ $set->settings_key }}" name="settings_val" data-id="{{ $set->id }}" value="{{ $set->settings_val }}">

					@endif

				</div>

				<div class="col-sm-2">
					<ul class="iq-social-media">
						<li><a href="{{ route('crudv4.settings.show',$set->id) }}"><i class="ri-edit-box-fill"></i></a></li>
						<li><a href="{{ route('crudv4.settings.destroy',$set->id) }}"><i class="ri-delete-bin-2-line text-danger"></i></a></li>

					</ul>
				</div>





			</div>

			@endforeach



		</div>
	</div>
</div>

@endsection


@section("script")

<script>



	$('.auto_save').focusout(function(e) {



		let	id = e.currentTarget.dataset.id
		let val = e.currentTarget.value



		$.ajax({

			type:'POST',

			url:'{{ route('crudv4.settings.ajax') }}',

			data:{ id: id,settings_val:val},

			success:function(data){

				sole("İşlem Sonucu",data.message);

			}

		});



	});



	$('.setfile').change(function() {


		id=$(this).data('id')

		UploadUrl = '/crudv4/settings/ajaxUpload/'+id



		var file_data = document.getElementById('setfile'+id).files[0];   
		var form_data = new FormData();                   
		form_data.append("dosya", file_data)              




		$.ajax({
			url: UploadUrl,

			cache: false,
			contentType: false,
			processData: false,
			data: form_data,                      
			type: 'POST',
			success: function(msg){

				location.reload()
			}
		})



	});


</script>

@endsection
