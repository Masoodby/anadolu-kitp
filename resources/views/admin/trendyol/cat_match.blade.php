@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<style>
   #trend-cat{
      width: 60%;
      height: 300px;
      overflow-x: auto;
      overflow-y: hidden; 
      float: left;
   } 

   #local-cat{
      width: 40%;


      float: right;
   }
</style>

<div class="col-sm-12">
   <div class="iq-card">
      <div class="iq-card-header d-flex justify-content-between">
         <div class="iq-header-title col-md-6">

            <h4 class="card-title">{{ $title }} </h4>
         </div>


      </div>
      <div class="iq-card-body pos-r">
         <div id="result"></div>
         <div class="maske ms1"></div>

         <div id="trend-cat"  class="horizontal-scroll scroll-demo h230">
            <div class="horz-scroll-content"  >

             <input type="hidden" id="secim" value="0">

             <div class="form-group float-left mr10 w200" id="maincat-outer">
               <select multiple=""  class="form-control h200" id="trendmain" >
                  @foreach ($maincat as $var)
                  <option value="{{ $var['id'] }}">{{ $var['name'] }}</option>
                  @endforeach


               </select>
            </div>




         </div>

      </div>



      <div id="local-cat">

         <div class="form-group mt5">

            <input type="text" class="form-control form-control-sm search-cat" placeholder="Kategori Ara..." >
         </div>

         <hr>


         <div id="brand-list" class="scrolls h300 p5">
            <ul class="list-group cat_list_hnk iq-list-style-1">
               @foreach ($cats as $item)


               <li class="mb-2 mr-0 bg-light listcat-item">
                  <div class="d-flex justify-content-between">
                     <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input catsec sendly" name="cats[{{ $item['id'] }}]"  data-name="{{ $item['categories_name']  }}"  id="cats{{ $item['id'] }}"    value="{{ $item['categories_name'] }}"  >
                        <label class="custom-control-label fsz12" for="cats{{ $item['id'] }}"><b>{{ $item['categories_name'] }}</b></label>
                     </div>

                  </div>
               </li>


               @foreach ($item['child'] as $childItem)
               <li class="mb-2 mr-0 pl10 listcat-item">
                  <div class="d-flex justify-content-between">
                     <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input catsec sendly" name="cats[{{ $childItem['id'] }}]" data-name="{{ $childItem['categories_name']  }}" id="cats{{ $childItem['id'] }}"     value="{{ $childItem['categories_name'] }}"  >
                        <label class="custom-control-label fsz11" for="cats{{ $childItem['id'] }}">{{ $childItem['categories_name'] }}</label>
                     </div>

                  </div>
               </li>


               @foreach ($childItem['child'] as $lastchild)
               <li class="mb-2 mr-0 pl20 listcat-item">
                  <div class="d-flex justify-content-between">
                     <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input catsec sendly" name="cats[{{ $lastchild['id'] }}]"  data-name="{{ $lastchild['categories_name']  }}"  id="cats{{ $lastchild['id'] }}"    value="{{ $lastchild['categories_name'] }}"  >
                        <label class="custom-control-label fsz10" for="cats{{ $lastchild['id'] }}">{{ $lastchild['categories_name'] }}</label>
                     </div>

                  </div>
               </li>
               {{-- expr --}}
               @endforeach

               @endforeach




               @endforeach


            </ul>
         </div>


         <button type="button"   class="btn btn-block mt10 btn-primary float-right save_cat">Kategori Eşleştir</button>
         <div class="clearfix"></div>
      </div>

      <div class="clearfix"></div>

   </div>

</div>
</div>




<div class="col-sm-12">
   <div class="iq-card">
      <div class="iq-card-header d-flex justify-content-between">
         <div class="iq-header-title col-md-6">

            <h4 class="card-title">Trendyol için Eşleşmiş Kategoriler</h4>
         </div>


      </div>
      <div class="iq-card-body" id="eslesenler">

         <table class="table">
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">Site Kategorisi</th>
                  <th scope="col">TrendYol Kategorisi</th>
                  <th scope="col">İşlem</th>
               </tr>
            </thead>
            <tbody>
               @php($local_dizi = [])
               @foreach ($matched as $row)


               @php($local_dizi[$row->local_id] = $row->local_id)

               <tr id="tr_{{ $row->id }}">

                  <td scope="row">{{$loop->iteration}}</td>

                  <td>{{ $row->local_name }} ({{ $row->local_id }})</td>
                  <td>{{ $row->remote_name }} <a target="_blank" href="{{ url('crudv4/trendyol/catRequired/'.$row->remote_id) }}">({{ $row->remote_id }})</a></td>

                  

                  <td>

                     <button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

                  </td>
               </tr>

               @endforeach


            </tbody>
         </table>
      </div>
   </div>
</div>



@endsection


@section('script')

<script>

   @foreach ($local_dizi as $item)
   $('#cats{{ $item }}').attr('disabled','disabled').parent('div').addClass('tipsi').attr('title', 'Eşleştirilmiş bir kategori seçilemez');
   @endforeach


   function eraseci(id) {

      $.ajax({
         type: 'GET',
         url: '/crudv4/trendyol/del/' + id,
         success: function () {
            sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
            $('#tr_'+id).remove()
         } 

      });
   }


   $(".search-cat").on('keyup', function() {
     var search = $(this).val().toLowerCase();
      //Go through each list item and hide if not match search

      $(".cat_list_hnk .listcat-item").each(function() {
       if ($(this).children('div').children('div').children('input').data('name').toLowerCase().indexOf(search) != -1) {
        $(this).show();

     }
     else {
        $(this).hide();
     }

  });


   });



   $('#trendmain').change(function() {


      maske('ms1',1);
      id = $(this).val()
      name = $(this).children('option:selected').text()
      level = 1

      levelTemizle(level)
      gen = level*30
  // $('.horz-scroll-content').width(gen+150)



  $.ajax({
   type: 'POST',
   url: '/crudv4/trendyol/loadcat/' + id,
   data: {level: level,name:name},
   success: function (data) {


    if ($( "#level"+level ).length) {
      $( "#level"+level ).html(data )}
      else{

         $( "#maincat-outer" ).after('<div class="form-group float-left mr10 w200 level1" id="level'+level+'"></div>' );
         $( "#level"+level ).html(data );
      }






      setTimeout(function(){
         maske('ms1',0);
      },1000)

   } 

});







});


   function levelTemizle(level) {

      for (var i = level+1; i < level+10; i++) {

         if ($( "#level"+i ).length) {
            $( "#level"+i ).remove()
         }

      }
   }




   $('.save_cat').click(function() {

      durum  = $('#secim').val()
      durum2  = $('.catsec').serialize()




      if (parseInt(durum) != 0 && durum2.length > 0) {



         $.ajax({
            type: 'POST',
            url: '/crudv4/trendyol/save',
            data: $(".sendly").serialize(),
            success: function(data){

               $("#eslesenler").html(data)

               levelTemizle(0)
               $( ".scroll-demo") .scrollLeft(0);
               $('.catsec').prop('checked', false)

               $('#trendmain').val(0)

            }
         });


      }else{

         if (parseInt(durum) == 0) {
            sole("Hata","Trendyol Kategorisi Seçmediniz. Son Levele Kadar Bir Kategori Seçiniz")
         }
         else if (durum2.length == 0) {
            sole("Hata","Site Kategorisi Seçmediniz.En az bir Kategori Seçiniz")
         }else{
            sole("Hata","Eşleşecek Kategorileri Seçmediniz")
         }
      }
      
   });






</script>

@endsection