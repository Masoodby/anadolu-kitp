@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12 col-lg-6">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-12">
				<h4 class="card-title">Son İşlemler 

					<a href="{{ route('crudv4.trendyoljop.stockUpdate') }}" target="_blank" class="float-right btn btn-dark">Manuel Update Başlat</a>
				</h4>
			</div>



		</div>
		<div class="iq-card-body">

			<div style="height: 500px; overflow-y: auto;">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">İstek Durumu</th>
							<th scope="col">Tarih</th>
							<th scope="col">Detaylar</th>
							<th scope="col">
								@if (count($last_queries)>0)		
								<button class="logzero float-right btn btn-danger btn-sm">Sıfırla</button>
								@endif
							</th>

						</tr>
					</thead>
					<tbody>

						@foreach ($last_queries as $var)

						<tr id="tr_{{ $var->id }}">

							<td scope="row">{{$loop->iteration}}</td>
							<td>

								<small>	{{ $var->requestID }} </small>

								@php
								$json_note = json_decode($var->json_note)
								@endphp

								<ul>
									<li>Durum : {{ isset($json_note->status)?$json_note->status:"" }}</li>
									<li>İşlem Adet : {{ isset($json_note->itemCount)?$json_note->itemCount:"" }}</li>
									<li>Başarısız İşlem Adet : {{ isset($json_note->failedItemCount)?$json_note->failedItemCount:"" }}</li>
									<li>İşlem Tarihi: {{ gmdate('d.m.Y H:i' , ($json_note->creationDate/1000)) }}</li>
								</ul>



							</td>

							<td> {{ $var->notes}} </td>
							<td> {{ date("d.m.Y h:i",strtotime($var->created_at))}} </td>
							<td>

								<button class="btn btn-success show-result" data-id="{{ $var->requestID }} "><i class="fa fa-search"></i></button>
							</td>
						</tr>

						@endforeach


					</tbody>
				</table>

			</div>
		</div>
	</div>

</div>


<div class="modal fade" id="bigmodal" tabindex="-1" role="dialog"   aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="bigmodal_title">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="bigmodal_content">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="bigmodalbtnright" data-dismiss="modal">Kapat</button>

			</div>
		</div>
	</div>
</div>



@endsection


@section('script')
<script>



	$('.show-result').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'POST',
			url: '/crudv4/trendyoljop/getresult',
			data:{id:id},
			success: function (data) {
				modalyap('bigmodal','Gelen yanıtlar',data);
			} 

		});
	});



	$('.logzero').click(function() {

		id = $(this).data('id')
		$.ajax({
			type: 'GET',
			url: '/crudv4/trendyoljop/emtpyresult',

			success: function () {
				location.reload()
			} 

		});
	});

	


</script>


@endsection