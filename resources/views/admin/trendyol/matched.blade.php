 @if(isset($matchbrand))
 @if($matchbrand))
 
 <div class="table-responsive">
 	<table class="table">
 		<thead>
 			<tr>
 				<th scope="col">#</th>
 				<th scope="col">Site Kategorisi</th>
 				<th scope="col">TrendYol Kategorisi</th>
 				<th scope="col">İşlem</th>
 			</tr>
 		</thead>
 		<tbody>
 			@foreach ($matchbrand as $row)
 			{{-- expr --}}
 			<tr id="tr_{{ $row->id }}">

 				<td scope="row">{{$loop->iteration}}</td>

 				<td>{{ $row->local_name }} ({{ $row->local_id }})</td>
 				<td>{{ $row->remote_name }} ({{ $row->remote_id }})</td>



 				<td>

 					<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

 				</td>
 			</tr>

 			@endforeach


 		</tbody>
 	</table>
 </div>


 @else
 <div class="alert alert-info">Eşleştirilmiş bir <b>Markanız</b> bulunmamaktadır.</div>

 @endif
 @endif

 

 @if(isset($matched))
 @if($matched))
 <div class="table-responsive">
 	<table class="table">
 		<thead>
 			<tr>
 				<th scope="col">#</th>
 				<th scope="col">Site Kategorisi</th>
 				<th scope="col">TrendYol Kategorisi</th>
 				<th scope="col">İşlem</th>
 			</tr>
 		</thead>
 		<tbody>
 			@php($local_dizi = [])
 			@foreach ($matched as $row)
 			@php($local_dizi[$row->local_id] = $row->local_id)

 			<tr id="tr_{{ $row->id }}">

 				<td scope="row">{{$loop->iteration}}</td>

 				<td>{{ $row->local_name }} ({{ $row->local_id }})</td>
 				<td>{{ $row->remote_name }} ({{ $row->remote_id }})</td>



 				<td>

 					<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

 				</td>
 			</tr>

 			@endforeach


 		</tbody>
 	</table>
 </div>

 
 <script>
 	@foreach ($local_dizi as $item)
 	$('#cats{{ $item }}').attr('disabled','disabled').parent('div').addClass('tipsi').attr('title', 'Eşleştirilmiş bir kategori seçilemez');
 	@endforeach

 </script>


 @else
 <div class="alert alert-info">Eşleştirilmiş bir <b>Kategoriniz</b> bulunmamaktadır.</div>


 @endif
 @endif



 
 