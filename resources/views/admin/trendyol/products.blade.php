@extends('admin.layouts.master')
@section('title', $title)
@section('body')

@php
$products = json_decode($products);
$urunler = $products->content;
$totalElements = $products->totalElements;
$totalPages = $products->totalPages;
$page = $products->page;
$curpage = $page+1;
$next = $page+2;
@endphp
<div class="col-sm-12 col-lg-12">

	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">{{ $title }} </h4>
			</div>
		</div>
		<div class="iq-card-body"  >

			<div class="col-md-12 text-right ">
				<a href="{{ route('crudv4.trendyoljop.stockUpdate') }}" target="_blank" class="btn btn-dark float-right mb10">Manuel Stok Güncelleme Başlat</a>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Ürün</th>
						<th scope="col">Barkod</th>
						<th scope="col">Fiyatı</th>
						<th scope="col">Adet</th>
						<th scope="col">Son Güncelleme</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($urunler as $var)
					@php
				//	prep($var);
					@endphp
					
					<tr>
						<td scope="row">{{$loop->iteration}}</td>
						<td>{{ $var->title }}<br>{{ $var->locked }}</td>
						<td>{{ $var->barcode }}</td>
						<td>{{ $var->salePrice }}</td>
						<td>{{ $var->quantity }}</td>
						<td>  
							{{ gmdate('d.m.Y H:i',($var->lastUpdateDate/1000)) }}

						</td>
					</tr>

					@endforeach


				</tbody>
			</table>

		</div>
	</div>

</div>	



<nav  class="col-md-6 float-right text-right">
	<ul class="pagination">


		@if ($curpage > 1)
		<li class="page-item"><a class="page-link" href="{{ route('crudv4.trendyol.products',$page) }}"><i class="fa fa-angle-left"></i></a></li>
		@endif

		@for ($i = 1; $i <= $totalPages ; $i++)

		@if ($i == $curpage + 10)
		<li class="page-item"><a class="page-link" href="{{ route('crudv4.trendyol.products',$i) }}">...</a></li>
		@endif
		@if ($i < $curpage + 10 && $i>$curpage - 10 )

		<li class="page-item"><a class="page-link" href="{{ route('crudv4.trendyol.products',$i) }}"><?php echo $i ?></a></li>
		@endif



		@if ($i >  $totalPages - 10 )

		<li class="page-item"><a class="page-link" href="{{ route('crudv4.trendyol.products',$i) }}"><?php echo $i ?></a></li>

		@endif



		@endfor


		@if ($curpage < $totalPages)
		<li class="page-item"><a class="page-link" href="{{ route('crudv4.trendyol.products',$next) }}"><i class="fa fa-angle-right"></i></a></li>
		@endif



	</ul>
</nav>





@endsection


@section('script')



@endsection