<style>
   #trend-cat{
      width: 100%;
      height: 300px;
      overflow-x: auto;
      overflow-y: hidden; 
      float: left;
   } 

   #local-cat{
      width: 100%;

      text-align: left;
      float: left;
   }
</style>

<div class="col-sm-12">
   <div class="iq-card">
      <div class="iq-card-header d-flex justify-content-between">
         <div class="iq-header-title col-md-12">

            <h4 class="card-title">{{ $urun->product_stock_code }} Trendyol Kategori Güncelle</h4>
         </div>


      </div>
      <div class="iq-card-body pos-r">
         <div id="result"></div>
         <div class="maske ms1"></div>

         <div id="trend-cat"  class="horizontal-scroll scroll-demo h230">
            <div class="horz-scroll-content"  >

             <input type="hidden" id="secim" value="0">

             <div class="form-group float-left mr10 w200" id="maincat-outer">
               <select multiple=""  class="form-control h200" id="trendmain" >
                  @foreach ($maincat as $var)
                  <option value="{{ $var['id'] }}">{{ $var['name'] }}</option>
                  @endforeach


               </select>
            </div>




         </div>

      </div>



      <div id="local-cat">


         <b>Ürün Kategorisi : {{ $urun->get_category->categories_name }}</b>



         <button type="button"   class="btn btn-block mt10 btn-primary float-right" onclick="updatecat({{ $urun->id }})"  >Kategoriyi Güncelle</button>
         <div class="clearfix"></div>
      </div>

      <div class="clearfix"></div>

   </div>

</div>
</div>

<script>
   $('#trendmain').change(function() {


      maske('ms1',1);
      id = $(this).val()
      name = $(this).children('option:selected').text()
      level = 1

      levelTemizle(level)
      gen = level*30
  // $('.horz-scroll-content').width(gen+150)



  $.ajax({
   type: 'POST',
   url: '/crudv4/trendyol/loadcat/' + id,
   data: {level: level,name:name},
   success: function (data) {


     if ($( "#level"+level ).length) {
      $( "#level"+level ).html(data )}
      else{

         $( "#maincat-outer" ).after('<div class="form-group float-left mr10 w200 level1" id="level'+level+'"></div>' );
         $( "#level"+level ).html(data );
      }






      setTimeout(function(){
         maske('ms1',0);
      },1000)

   } 

});







});


   function levelTemizle(level) {

      for (var i = level+1; i < level+10; i++) {

         if ($( "#level"+i ).length) {
            $( "#level"+i ).remove()
         }

      }
   }


   function updatecat(id) {

      if ($('#remote_cat_id').length && $('#remote_cat_id').val() > 0 ) {

        $.ajax({
         type: 'POST',
         url: '/crudv4/trendyol/changeCat/'+id,    
         data: $('.sendly').serialize(),
         success: function (data) {

            location.reload()

          //  alert(data)

         } 

      });


     }
  }


</script>