@extends('admin.layouts.master')
@section('title', $title)
@section('body')

    <div class="col-sm-12">
        <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
                <div class="iq-header-title col-md-6">

                    <h4 class="card-title">{{ $title }} </h4>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('crudv4.tslider') }}" class="btn btn-primary btn-lg float-right"><i
                            class="fa fa-angle-left"></i> Geri</a>
                </div>

            </div>
            <div class="iq-card-body">

                <form action="{{ route('crudv4.tslider.save',$tslider->id) }}" class="container-fluid row" id="sliderform" method="POST"
                    enctype="multipart/form-data">
                    {{ csrf_field() }}

                    @include('admin.layouts.errors')
                    @include('admin.layouts.alert')


                    <div class="form-group col-md-6">
                        <label>Günlük fırsatlar Başlık:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text  bg-info"><i class="fa fa-list"></i></span>
                            </div>
                            <input type="text" class="form-control req" name="title" value="{{$tslider->title}}">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>indirim yüzdesi:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text  bg-info"><i class="fa fa-percent" aria-hidden="true"></i></span>
                            </div>
                            <input class="form-control req" type="number" min="1" max="100" id="myPercent" name="percent" value="{{$tslider->discount}}" >
                        </div>
                    </div>


                    <div class="form-group col-md-12">
                        <label>Product Link:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text  bg-info">{{ config('app.url') }}</span>
                            </div>
                            <input type="text" class="form-control req" name="url" value="{{$tslider->url}}">
                        </div>
                    </div>




                        <div class="col-xl-6 col-lg-6 col-sm-12 form-group">
                            <label>Isbn</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  bg-info"><i class="fa fa-book" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control req" name="isbn" value="{{$tslider->product->product_stock_code}}">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-sm-12 form-group">
                            <label>bitiş zemanı</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  bg-info"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control req" name="time" value="{{$tslider->end_time}}">
                            </div>

                        </div>










                <button type="button" data-id="sliderform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
                </form>

            </div>
        </div>
    </div>
<script>
    document.getElementById('myForm').onsubmit = function() {
    var valInDecimals = document.getElementById('myPercent').value * 100;
}
</script>

@endsection


@section('script')



@endsection
