@extends('admin.layouts.master')
@section('title', $title)
@section('body')
{{-- @php
    dd($list);
@endphp --}}
<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.tslider.add') }}" class="btn btn-primary float-right btn-lg" ><i class="fa fa-plus"></i> Yeni Ekle</a>
			</div>

		</div>
		<div class="iq-card-body">

			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col" >kitap</th>
						<th scope="col" >ISBN kodu</th>
						<th scope="col">bitiş zemanı</th>
						<th scope="col">başlik</th>
						<th scope="col">indirim yüzdesi</th>
						<th scope="col">Durum</th>
						<th scope="col">İşlem</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($list as $row)
					{{-- expr {{  ress($row->product->product_stock_code,$row->product_group_code,"100x100") }} --}}
                     {{-- @php
                        dd($row->product->product_name);
                    @endphp --}}

					<tr id="tr_{{ $row->id }}">

						<td scope="row">{{$loop->iteration}}</td>
						<td><img src="{{  ress($row->product->product_stock_code,$row->product->product_group_code,"250x250") }}" class="img-fluid"></td>
						<td>{{ $row->product->product_name}}</td>
						<td>{{ $row->end_time }}</td>
						<td>{{ $row->title }}</td>
						<td>{{$row->discount}}%</td>
						<td>

							<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
								<div class="custom-switch-inner">

									<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitch-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="status"

									{{ $row->status === 1 ? "checked" : "" }}>
									<label class="custom-control-label" for="customSwitch-{{$loop->iteration}}">
										<span class="switch-icon-left"><i class="fa fa-check"></i></span>
										<span class="switch-icon-right"><i class="fa fa-check"></i></span>
									</label>
								</div>
							</div>


						</td>
						<td>
							<a href="{{ route('crudv4.tslider.edit',$row->id) }}" class="btn btn-success btn-sm tipsi mb-3" title="Düzenle"><i class="las la-edit"></i></a>
							<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>

						</td>
					</tr>

					@endforeach


				</tbody>
			</table>
		</div>
	</div>
</div>


@endsection


@section('script')

<script>
	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/timeslider/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			}

		});
	}





	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('status')
		updater(id,val,label)
	});



	function updater(id,value,lbl) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/timeslider/update/'+id,
			data: {val: value,label:lbl},
			success: function(){
				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});
	}


</script>

@endsection
