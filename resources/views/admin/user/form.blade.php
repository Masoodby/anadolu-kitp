@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.user.save', @$entry->id) }}" class="container-fluid row" id="userForm" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-8">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-6">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
				<div class="col-md-6">
					<a href="{{ route('crudv4.user') }}" class="btn btn-primary float-right"> <i class="fa fa-angle-left"></i> Geri</a>
				</div>
			</div>
			<div class="iq-card-body">


				<div class="row">

					<div class="form-group col-6">
						<label  >İsim:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-primary"  ><i class="fa fa-user"></i></span>
							</div>
							<input type="text" class="form-control req" name="name" value="{{ $entry->name }}">
						</div>
					</div>

					<div class="form-group col-6">
						<label  >Soy İsim:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-success"  ><i class="fa fa-user-o"></i></span>
							</div>
							<input type="text" class="form-control req" name="sname" value="{{ $entry->sname }}">
						</div>
					</div>

				</div>
				<div class="row">
					<div class="form-group col-6">
						<label  >E-Posta:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-info"  ><i class="fa fa-envelope"></i></span>
							</div>
							<input type="text" class="form-control req" name="email" value="{{ $entry->email }}">
						</div>
					</div>

					<div class="form-group col-6">
						<label  >Şifre:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-primary"  ><i class="fa fa-key"></i></span>
							</div>
							<input type="text" class="form-control" name="pass" value="">

						</div>
						<small class="text-muted">Güncellemek istemiyorsanız boş bırakın</small>
					</div>
				</div>

				@if ($id>0)

				<div class="row">
					<div class="col-sm-6 pt10">
						<label  >Durum:</label>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_active1" name="is_active" value="1" class="custom-control-input bg-primary" {{ $entry->is_active==1?"checked":"" }}>
							<label class="custom-control-label" for="is_active1"> Aktif </label>
						</div>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_active2" value="0" name="is_active" class="custom-control-input bg-danger" {{ $entry->is_active==0?"checked":"" }}>
							<label class="custom-control-label" for="is_active2"> Pasif </label>
						</div>
					</div>

					<div class="col-sm-6 pt10">
						<label  >Rol:</label>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_admin1" name="is_admin" value="1" class="custom-control-input bg-primary" {{ $entry->is_admin==1?"checked":"" }}>
							<label class="custom-control-label" for="is_admin1"> Yönetici </label>
						</div>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_admin2" value="0" name="is_admin" class="custom-control-input bg-danger" {{ $entry->is_admin==0?"checked":"" }}>
							<label class="custom-control-label" for="is_admin2"> Kullanıcı </label>
						</div>
					</div>
				</div>

				@else
				<div class="row">
					<div class="col-sm-6 pt10">
						<label  >Durum:</label>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_active1" name="is_active" value="1" class="custom-control-input bg-primary" checked>
							<label class="custom-control-label" for="is_active1"> Aktif </label>
						</div>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_active2" value="0" name="is_active" class="custom-control-input bg-danger"  >
							<label class="custom-control-label" for="is_active2"> Pasif </label>
						</div>
					</div>

					<div class="col-sm-6 pt10">
						<label  >Rol:</label>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_admin1" name="is_admin" value="1" class="custom-control-input bg-primary"  >
							<label class="custom-control-label" for="is_admin1"> Yönetici </label>
						</div>
						<div class="custom-control custom-radio custom-radio-color-checked custom-control-inline">
							<input type="radio" id="is_admin2" value="0" name="is_admin" class="custom-control-input bg-danger" checked>
							<label class="custom-control-label" for="is_admin2"> Kullanıcı </label>
						</div>
					</div>
				</div>
				@endif



				<div class="row">
					<div class="form-group col-6">
						<label  >GSM:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-success"  ><i class="fa fa-phone"></i></span>
							</div>
							<input type="text" class="form-control" name="user_gsm" value="{{ $entry->getDetail->user_gsm }}">
						</div>
					</div>

					<div class="form-group col-6">
						<label  >Alternatif Tel:</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text  bg-secondary"  ><i class="fa fa-phone"></i></span>
							</div>
							<input type="text" class="form-control" name="user_tel"   value="{{ $entry->getDetail->user_tel }}">

						</div>
						<small class="text-muted">Güncellemek istemiyorsanız boş bırakın</small>
					</div>
				</div>


				<div class="form-group">
					<label >Kullanıcı Adres <small>(Zorunlu Değil)</small></label>
					<textarea class="form-control lh23"   name="user_address">{{ $entry->getDetail->user_address }}</textarea>
					<p class="form-text"></p>
				</div>





			</div>
		</div>
		<button type="button" data-id="userForm" class="btn btn-block btn-primary sbmt_btn">{{ $id>0?"Güncelle":"Ekle" }}</button>
	</div>	



</form>


@endsection


@section('script')


@endsection