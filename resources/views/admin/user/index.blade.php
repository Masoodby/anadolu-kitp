@extends('admin.layouts.master')
@section('title', $title)
@section('body')



<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} </h4>
			</div>
			<div class="col-md-6">
				<a href="{{ route('crudv4.user.add') }}" class="btn btn-primary float-right btn-lg" ><i class="fa fa-plus"></i> Yeni Ekle</a>
			</div>
		</div>
		<div class="iq-card-body">

			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Ad Soyad</th>
						<th scope="col">E-Posta</th>
						<th scope="col">İşlem</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list as $row)
					
					<tr id="tr_{{ $row->id }}">
						<td scope="row">{{$loop->iteration}}</td>
						<td>{{ $row->full_name }}</td>
						<td>{{ $row->email }}</td>
						<td>


							<div class="flex align-items-center list-user-action">

								<a class="iq-bg-primary tipsi" title="Düzenle" href="{{ route('crudv4.user.edit',$row->id) }}" ><i class="ri-pencil-line"></i></a>
								<a  class="iq-bg-primary tipsi" title="Sil"  onclick="allConfirm('eraseci',{{ $row->id }})" href="javascript:;"><i class="ri-delete-bin-line"></i></a>

							</div>


						</td>
					</tr>
					@endforeach


				</tbody>
			</table>
		</div>
	</div>
</div>



@endsection


@section('script')
<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/user/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}

	
</script>
@endsection
