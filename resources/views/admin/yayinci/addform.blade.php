@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.yayinevi.save') }}" class="container-fluid row" id="catform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Yayıncı Adı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinci_name" value="">
					</div>
				</div>


				<div class="form-group">
					<label  >Emek Uniq ID:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-barcode"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinevi_api_id" value="">
					</div>
				</div>



				<div class="form-group">
					<div class="custom-file">
						<input type="file" name="resim" class="custom-file-input file" data-div="image_result" id="customFile" lang="tr">
						<label class="custom-file-label" for="customFile" data-browse="Resim Seç">Yayınevi Logosu</label>
					</div>

					<div id="image_result"></div>
				</div>


			</div>
		</div>

	</div>	

	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Oran Ayarları </h4>
				</div>

				<div class="col-md-4">
					<a href="{{ route('crudv4.yayinevi') }}" class="btn btn-primary float-right btn-sm"><i class="fa fa-angle-left"></i>Geri</a>
				</div>

			</div>
			<div class="iq-card-body">



				<div class="form-group">
					<label  >Alış Oranı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-success"  ><i class="fa fa-calculator"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinci_alis_oran" value="">
					</div>
				</div>



				<div class="form-group">
					<label  >Satış Oranı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-warning"  ><i class="fa fa-calculator"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinci_oran" value="">
					</div>
				</div>



				


			</div>
		</div>
		<button type="button" data-id="catform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>
	</div>



</form>


@endsection


@section('script')



@endsection