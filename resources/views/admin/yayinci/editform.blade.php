@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.yayinevi.save', @$list->id) }}" class="container-fluid row" id="yayineviform" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Yayıncı Adı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinci_name" value="{{ $list->yayinci_name }}">
					</div>
				</div>

				<div class="form-group">
					<label  >Emek Uniq ID:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-barcode"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinevi_api_id" value="{{ $list->yayinevi_api_id }}">
					</div>
				</div>





				<div class="form-group">
					<div class="custom-file">
						<input type="file" class="custom-file-input file" name="resim" data-div="image_result" id="customFile" lang="tr">
						<label class="custom-file-label" for="customFile" data-browse="Resim Seç">Yayıncı Logo</label>
					</div>

					@if($list->yayinci_logo!=null)



					<div id="image_result" class="mt10 p10 bg-dark">
						
						<img src="{{ res('yayinci',$list->yayinci_logo) }}" class="img-thumbnail w100" >

					</div>

					@endif
				</div>







			</div>
		</div>

	</div>	

	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			

			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title col-md-8">
					<h4 class="card-title">Oran Ayarları </h4>
				</div>

				<div class="col-md-4">
					<a href="{{ route('crudv4.yayinevi') }}" class="btn btn-primary float-right btn-sm"><i class="fa fa-angle-left"></i>Geri</a>
				</div>

			</div>
			<div class="iq-card-body">


				<div class="form-group">
					<label  >Alış Oranı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text bg-success"><i class="fa fa-calculator"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinci_alis_oran" value="{{ $list->yayinci_alis_oran }}">
					</div>
				</div>



				<div class="form-group">
					<label  >Satış Oranı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text bg-warning"><i class="fa fa-calculator"></i></span>
						</div>
						<input type="text" class="form-control req" name="yayinci_oran" value="{{ $list->yayinci_oran }}">
					</div>
				</div>




			</div>
		</div>
		<button type="button" data-id="yayineviform" class="btn btn-block btn-primary sbmt_btn">Güncelle</button>
	</div>



</form>


@endsection


@section('script')

@endsection