@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<div class="col-md-12 text-right pb20">
	<a href="{{ route('crudv4.yayinevi.yayinevi_islem') }}" target="_blank" class="btn btn-dark  float-right mr20" title="Tüm Oranları uygula">Depodaki Kitapların yayın evlerini Aktif Yap</a>
	<a href="{{ route('crudv4.yayinevi.prices') }}" target="_blank" class="btn btn-success  float-right mr20" title="Tüm Oranları uygula">Aktif Olan Yayın Evlerinin Oranlarını Uygula</a>
</div>

<div class="col-sm-12">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title col-md-6">

				<h4 class="card-title">{{ $title }} <small>({{ $list->total() }} adet listelendi)</small></h4>
			</div>

			<div class="col-md-6">
				<div class="form-row">
					<div class="form-group col-md-8 pt20">

						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="orderby">
							<option value="id,asc">Sırala</option>

							<option value="yayinci_name-asc" @if(request()->has('orderby') and request('orderby') == "yayinci_name-asc") selected @endif>Ad Artan</option>
							<option value="yayinci_name-desc" @if(request()->has('orderby') and request('orderby') == "yayinci_name-desc") selected @endif>Ad Azalan</option>
							<option value="id-asc" @if(request()->has('orderby') and request('orderby') == "id-asc") selected @endif>ID Artan</option>
							<option value="id-desc" @if(request()->has('orderby') and request('orderby') == "id-desc") selected @endif>ID Azalan</option>


						</select>
					</div>
					<div class="form-group col-md-4 pt20">
						<select class="form-control selectfilter form-control-sm mb-3" id="" data-filter="limit">
							<option value="25">Göster</option>
							@for ($i = 25; $i <101 ; $i+=25)
							<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

							@endfor
						</select>
					</div>
				</div>

			</div>
		</div>
		<div class="iq-card-body">







			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>

							<th scope="col">Yayıncı</th>
							<th scope="col">Alış Oran</th>
							<th scope="col">Satış Oran</th>
							<th scope="col">Durum</th>
							<th scope="col">Konum</th>

							<th scope="col">İşlem</th>

						</tr>
						<tr>
							<th scope="col"> <input type="text" class="form-control textfilter w50 from-control-sm" data-filter="id"  placeholder="ID..." value= "{{ request()->has('id') ? request('id'):''}}"></th>

							<th scope="col"> <input type="text" class="form-control textfilter from-control-sm" data-filter="yayinci_name"  name="yayinci_name" id="yayinci_name" placeholder="Yayıncı Adı..." value= "{{ request()->has('yayinci_name') ? request('yayinci_name'):''}}"></th>
							<th scope="col"></th>
							<th scope="col"> </th>
							<th scope="col">
								<select class="form-control selectfilter" data-filter="is_active" >
									<option value="">Tümü</option>
									<option value="1" @if(request()->has('is_active') and request('is_active') == 1) selected @endif>Aktif</option>
									<option value="0" @if(request()->has('is_active') and request('is_active') == 0) selected @endif>Pasif</option>
								</select>
							</th>	
							<th scope="col">
								<select class="form-control selectfilter" data-filter="is_home" >
									<option value="">Tümü</option>
									<option value="1" @if(request()->has('is_home') and request('is_home') == 1) selected @endif>Anasayfada</option>
									<option value="0" @if(request()->has('is_home') and request('is_home') == 0) selected @endif>Listede</option>
								</select>
							</th>
							<th scope="col"> 
								<a href="{{ route('crudv4.yayinevi.add') }}" class="btn btn-success btn-sm tipsi" title="Yeni Ekle"><i class="fa fa-plus"></i></a>


								@if (request()->hasAny(['id',"yayinci_name","is_home","limit","orderby","is_active"]))
								<a href="{{ route('crudv4.yayinevi') }}" class="btn btn-dark btn-sm tipsi" title="Filtreyi Sıfırla"><i class="fa fa-refresh"></i></a>
								@endif

							</th>


						</tr>
					</thead>
					<tbody>
						@if(count($list)==0)
						<div class="alert alert-warning">Yayınevi bulunamadı..</div>
						@else
						@foreach($list as $row)
						<tr id="tr_{{ $row->id }}">

							<td scope="row">{{$loop->iteration}}</td>
							<td  >{{ $row->yayinci_name }}</td>

							<td>
								<b>Site</b>
								<div class="input-group input-group-sm mb-3 w-50">

									<input type="text" class="form-control oran" data-name="yayinci_alis_oran" id="yayinci_alis_oran_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->yayinci_alis_oran }}">

									<div class="input-group-append">
										<span class="input-group-text cur-p oran_click" data-name="yayinci_alis_oran" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
									</div>
								</div>
								<b>Emek</b>
								<div class="input-group input-group-sm mb-3 w-50">

									<input type="text" class="form-control oran" data-name="emek_alis" id="emek_alis_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->emek_alis }}">

									<div class="input-group-append">
										<span class="input-group-text cur-p oran_click" data-name="emek_alis" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
									</div>
								</div>

							</td>	

							<td>
								<b>Site</b>
								<div class="input-group input-group-sm mb-3 w-50">

									<input type="text" class="form-control oran" data-name="yayinci_oran" id="yayinci_oran_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->yayinci_oran }}">

									<div class="input-group-append">
										<span class="input-group-text cur-p oran_click" data-name="yayinci_oran" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
									</div>
								</div>
								<b>Emek</b>
								<div class="input-group input-group-sm mb-3 w-50">

									<input type="text" class="form-control oran" data-name="emek_satis" id="emek_satis_{{ $row->id }}"  data-id="{{ $row->id }}"  value="{{ $row->emek_satis }}">

									<div class="input-group-append">
										<span class="input-group-text cur-p oran_click" data-name="emek_satis" data-id="{{ $row->id }}" ><i class="fa fa-check"></i></span>
									</div>
								</div>

							</td>
							<td>



								<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
									<div class="custom-switch-inner">

										<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitch-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_active"

										{{ $row->is_active === 1 ? "checked" : "" }}>
										<label class="custom-control-label" for="customSwitch-{{$loop->iteration}}">
											<span class="switch-icon-left"><i class="fa fa-check"></i></span>
											<span class="switch-icon-right"><i class="fa fa-check"></i></span>
										</label>
									</div>
								</div>

							</td>	
							<td>



								<div class="custom-control custom-switch custom-switch-icon custom-switch-color custom-control-inline">
									<div class="custom-switch-inner">

										<input type="checkbox" class="custom-control-input bg-success state_check" id="customSwitchtop-{{$loop->iteration}}" data-id="{{ $row->id }}"  data-name="is_home"

										{{ $row->is_home === 1 ? "checked" : "" }}>
										<label class="custom-control-label" for="customSwitchtop-{{$loop->iteration}}">
											<span class="switch-icon-left"><i class="fa fa-check"></i></span>
											<span class="switch-icon-right"><i class="fa fa-check"></i></span>
										</label>
									</div>
								</div>

							</td>
							<td>
								<a href="{{ route('crudv4.yayinevi.price',$row->id) }}" target="_blank"  class="btn btn-primary btn-sm tipsi mb-3" title="Oranları Uygula"><i class="la la-refresh"></i></a>
								<a href="{{ route('crudv4.yayinevi.edit',$row->id) }}" class="btn btn-success btn-sm tipsi mb-3" title="Düzenle"><i class="las la-edit"></i></a>
								<button type="button" onclick="allConfirm('eraseci',{{ $row->id }})"   class="btn btn-danger btn-sm tipsi mb-3" title="Sil"><i class="ri-delete-bin-2-fill pr-0"></i></button>
							</td>


						</tr>

						@endforeach
						@endif

					</tbody>
				</table>



				{{ $list->withQueryString()->links() }}
			</div>


			
		</div>
	</div>
</div>



@endsection


@section('script')
<script>

	function eraseci(id) {

		$.ajax({
			type: 'GET',
			url: '/crudv4/yayinevi/del/' + id,
			success: function () {
				sole('İşlem Başarılı',"Silme işlemi Başarıyla Gerçekleştirildi.")
				$('#tr_'+id).remove()
			} 

		});
	}

	$('.textfilter').keyup(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()

		if(event.keyCode == 13){

			repUrl(filterType,val)

		}
	});



	$('.selectfilter').change(function(event) {

		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});



	$('.oran').keyup(function(e) {
		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		if (e.which == '13') {

			updater(id,value,label)

		}
		

	});

	
	$('.oran').focusout(function() {
		

		value = $(this).val()
		id = $(this).data("id")
		label = $(this).data('name')

		updater(id,value,label)

	}); 



	$('.oran_click').click(function() {
		id = $(this).data("id")
		label = $(this).data('name')
		val = $("#"+label+'_'+id).val()
		

		updater(id,val,label)


	});


	$('.state_check').click(function() {
		id = $(this).data("id")
		val = $(this).prop("checked")?1:0
		label = $(this).data('name')


		updater(id,val,label)


	});



	function updater(id,value,lbl) {
		$.ajax({
			type: 'POST',
			url: '/crudv4/yayinevi/update/'+id,
			data: {val: value,label:lbl},
			success: function(){
				sole("İşlem Başarılı","Güncelleme işlemi başarılı")
			}
		});	
	}






</script>
@endsection
