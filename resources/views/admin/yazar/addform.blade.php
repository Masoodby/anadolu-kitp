@extends('admin.layouts.master')
@section('title', $title)
@section('body')

<form action="{{ route('crudv4.yazar.save') }}" class="container-fluid row" id="yazarform" method="POST"  >
	{{csrf_field()}}

	@include('admin.layouts.errors')
	@include('admin.layouts.alert')


	<div class="col-sm-12 col-lg-6">

		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">{{ $title }} </h4>
				</div>
			</div>
			<div class="iq-card-body">




				<div class="form-group">
					<label  >Yazar Adı:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-list"></i></span>
						</div>
						<input type="text" class="form-control req" name="yazar_name" value="">
					</div>
				</div>


				<div class="form-group">
					<label  >Emek Uniq ID:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text  bg-info"  ><i class="fa fa-barcode"></i></span>
						</div>
						<input type="text" class="form-control req" name="yazar_api_id" value="">
					</div>
				</div>




			</div>
		</div>

	</div>	

	<button type="button" data-id="yazarform" class="btn btn-block btn-primary sbmt_btn">Kaydet</button>

</form>


@endsection


@section('script')



@endsection