@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))


@section('content')

<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Alışveriş Sepeti</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Alışveriş Sepeti</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="cart-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="maske maske-cart"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>
				<table class="table cart-table table-responsive-xs striped-table">
					<thead>
						<tr class="table-head">
							<th scope="col">Ürün</th>
							<th scope="col"></th>
							<th scope="col">Birim Fiyatı</th>
							<th scope="col">Adet</th>
							<th scope="col">İşlem</th>
							<th scope="col">Toplam</th>
						</tr>
					</thead>
					<tbody>
						@foreach(Cart::content() as $item)
						<tr id="tr_{{ $item->rowId }}">
							<td>
								<a href="{{ $item->options->url }}"><img src="{{ res("product",jres($item->options->img),"100x100") }}" alt=""></a>
							</td>
							<td>
								<a href="{{ $item->options->url }}" class="text-left">{{ $item->name }}</a>
								<div class="mobile-cart-content row">
									<div class="col-xs-3">
										<div class="qty-box">
											<div class="input-group">
												<input type="text" name="quantity" class="form-control input-number" value="{{ $item->qty }}">
											</div>
										</div>
									</div>
									<div class="col-xs-3">
										<h2 class="td-color">{{ $item->price }} TL</h2></div>
										<div class="col-xs-3">
											<h2 class="td-color"><a href="javascript:;"  class="icon delcart" data-id="{{ $item->rowId }}"><i class="ti-close"></i></a></h2></div>
										</div>
									</td>
									<td>
										<h2>{{ $item->price }} TL</h2></td>
										<td>
											<div class="qty-box">
												<div class="input-group">
													
													<button class="btn btn-xs btn-info cart-minus" type="button" data-stok="{{ $item->options->stok }}" data-id="{{ $item->rowId }}" ><i class="fa fa-minus"></i></button>
													<input type="text" id="mcart_qty_{{ $item->rowId }}" class="form-control cart-qty int text-center cart_qty_{{ $item->rowId }}" data-stok="{{  $item->options->stok  }}" data-id="{{ $item->rowId }}" value="{{ $item->qty }}">
													<button class="btn btn-xs btn-info cart-plus" type="button" data-stok="{{  $item->options->stok  }}" data-id="{{ $item->rowId }}" ><i class="fa fa-plus"></i></button>


												</div>
											</div>
										</td>
										<td><a href="javascript:;"  class="icon delcart" data-id="{{ $item->rowId }}" ><i class="ti-close"></i></a></td>
										<td>
											<h2 class="td-color rowtotal{{ $item->rowId }}">{{ $item->subtotal }} TL</h2>
										</td>
									</tr>
									@endforeach
								</tbody>

							</table>
							<table class="table cart-table table-responsive-md">
								<tfoot>
									<tr>
										<td>Ara Toplam :</td>
										<td>
											<h2 id="cartsubtotal">{{Cart::subtotal()}} TL</h2>
										</td>
									</tr>
									<tr>
										<td>KDV :</td>
										<td>
											<h2 id="carttax">{{Cart::tax()}} TL</h2>
										</td>
									</tr>
									<tr>
										<td>Genel Toplam :</td>
										<td>
											<h2 id="grandtotal">{{Cart::total()}} TL</h2>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="row cart-buttons">
						<div class="col-6"><a href="{{ route('anasayfa') }}" class="btn btn-solid">Alışverişe Devam Et</a></div>
						<div class="col-6"><a href="{{ route('siparisi-tamamla') }}" class="btn btn-solid">Sİparişi Tamamla</a></div>
					</div>
				</div>
			</section>
			<!--section end-->



			@endsection
			@section('script')
			<script>

				$('.delcart').click(function() {

					id = $(this).data("id")

					$.ajax({
						type: 'DELETE',
						url: '/sepet/remove',
						data: {id: id},
						success: function(msg){
							location.reload()
						}
					});	


				});


				$('.cart-minus').click(function() {

					id = $(this).data('id')
					max = $(this).data('stok')

					$qty = $('#mcart_qty_'+id)

					currentVal = $qty.val()

					newVal = parseInt(currentVal)-1

					if(newVal > 0){
						$qty.val(newVal)
					}else{
						$qty.val(1)
						newVal = 1
					}


					qtyedit(id,newVal) 
					
				});

				$('.cart-plus').click(function() {

					id = $(this).data('id')
					max = $(this).data('stok')

					$qty = $('#mcart_qty_'+id)

					currentVal = $qty.val()

					newVal = parseInt(currentVal)+1

					if(max > newVal){
						$qty.val(newVal)
					}else{
						$qty.val(max)
						newVal = max
						sole('Hata',"Max adet "+max+ " olmalıdır.")
					}


					qtyedit(id,newVal) 
					
				});


				$('.cart-plus').keyup(function() {

					id = $(this).data('id')
					max = $(this).data('stok')

					currentVal = $(this).val()



					if(max > newVal){
						$qty.val(newVal)
					}else{
						$qty.val(max)
						newVal = max
						sole('Hata',"Max adet "+max+ " olmalıdır.")
					}

				});



				function qtyedit(id,qty) {

					maske('maske-cart','on')

					$.ajax({
						type: 'POST',
						url: '/sepet/edit',
						data: {id: id,qty:newVal},
						success: function(obj){
							maske('maske-cart','off')




							$('.rowtotal'+id).html(obj.rowtotal+' TL')
							$('#cartsubtotal').html(obj.subtotal+' TL')
							$('#carttax').html(obj.kdv+' TL')
							$('#grandtotal').html(obj.grandtotal+' TL')
							$('#topcartcount').html(obj.count+' ürün')

						}
					});	
				}





			</script>

			@endsection

