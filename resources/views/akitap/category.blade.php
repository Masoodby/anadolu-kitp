@extends(theme().'.layouts.master')
@section('title', $category->categories_title)
@section('description', $category->categories_desc)
@section('keywords', $category->categories_keyw)

@section('content')


<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>{{ $category->categories_name }}</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}">Anasayfa</a></li>
						@foreach ($tree as $var)
						<li class="breadcrumb-item"><a href="{{ route('kategori',$var["categories_url"]) }}">{{ $var['categories_name'] }}</a></li>
						@endforeach
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space ratio_square">
	<div class="collection-wrapper">
		<div class="container">
			<div class="row">
				@include(theme().'.layouts.theme.category.side')

				<div class="collection-content col">
					<div class="page-main-content">
						<div class="row">
							<div class="col-sm-12">
								@include(theme().'.layouts.theme.category.banner')
								<div class="collection-product-wrapper">
									@include(theme().'.layouts.theme.category.filter')
									<div class="product-wrapper-grid">
										<div class="row">

											@foreach ($products as $row)
											{{-- expr --}}

											<div class="col-xl-3 col-md-4 col-6 col-grid-box">
												@include(theme().'.layouts.theme.box.box2')
											</div>

											@endforeach

										</div>
									</div>
									<div class="product-pagination mb-2">
										<div class="theme-paggination-block">
											<div class="row">
												<div class="col-xl-6 col-md-6 col-sm-12">
													<nav aria-label="Page navigation">
														{{ $products->withQueryString()->links() }}
													</nav>
												</div>
												<div class="col-xl-6 col-md-6 col-sm-12">
													<div class="product-search-count-bottom"><h5>Toplam {{ $products->total() }} Kayıt Bulundu.</h5></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- section End -->

@endsection


@section('script')
<script>

	$(".search-spec").on('keyup', function() {
		var search = $(this).val().toLowerCase();
		

		divsi = $(this).data('grid')
		classcisi = $(this).data('clas')

		$("#"+divsi+" ."+classcisi).each(function() {
			if ($(this).data('name').toLowerCase().indexOf(search) != -1) {

				$(this).show()
			}
			else {
				$(this).hide();
			}

		});


	});


	$(function() {  
		$("#yazarlist_side").niceScroll({cursorcolor:"#3C70E6"});
		$("#yayincilist_side").niceScroll({cursorcolor:"#3C70E6"});
	});

</script>

@endsection