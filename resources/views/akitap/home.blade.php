@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')


@include(theme().'.layouts.theme.home.slider')


<!-- feature section start -->
@include(theme().'.layouts.theme.home.service')  
<!-- feature section end -->


<!-- banner section start -->
@include(theme().'.layouts.theme.home.banner1')

<!-- banner section start -->


<!-- tab section start -->
@include(theme().'.layouts.theme.home.tabproduct')

<!-- tab section start -->


<!-- Category section start -->
{{-- @include(theme().'.layouts.theme.home.cats') --}}

<!-- Category section end -->


<!-- Category banner section start -->
@include(theme().'.layouts.theme.home.banner2')
<!-- Category banner section end -->


<!-- Product slider section start -->
@include(theme().'.layouts.theme.home.product')

<!-- Product slider section end -->


<!-- banner section start -->
{{-- @include(theme().'.layouts.theme.home.banner3') --}}
<!-- banner section end -->


<!-- detail section start -->
{{-- @include(theme().'.layouts.theme.home.form') --}}
<!-- detail section end -->


<!-- logo section start -->
@include(theme().'.layouts.theme.home.partners')
<!-- logo section end -->


@endsection