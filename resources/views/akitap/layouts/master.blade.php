<!DOCTYPE html>
<html lang="tr">
<head> 
    @include(theme().'/layouts.theme.meta')
    <title>@yield('title', config('app.name'))</title>
    @include(theme().'/layouts.theme.src')

</head>
<body>
    @include(theme().'/layouts.theme.bodystart')
    @include(theme().'/layouts.theme.header')

    @yield('content')





    @include(theme().'/layouts.theme.footer')
    @include(theme().'/layouts.theme.modal.booknews')
    @include(theme().'/layouts.theme.modal.cart')
    @include(theme().'/layouts.theme.modal.wishlist')
    @include(theme().'/layouts.theme.modal.login')
    


    @include(theme().'/layouts.theme.js')
</body>

</html>
