@php
{{

	$link = route('urun',['categories_url'=>$row->get_category->categories_url,'product_url'=>$row->product_url]);
	$res = res("product",jres($row->product_image),"250x250");
	$oran = yuzde($row->product_old_price,$row->product_price);
	$name = $row->product_name;
	$price = $row->product_price;
}}
@endphp
<div class="product-box">
	<div class="img-block">
		<a href="{{ $link }}"><img src="{{ $res }}" class=" img-fluid bg-img" alt="{{ $name }}"></a>

		<div class="lable-wrapper">
			<span class="lable1 w40">{{  $oran }}</span>

		</div>


		<div class="add-btn">
			<a href="{{ $link }}" class="btn btn-outline addcart-box" tabindex="0">Sepete Ekle</a>
		</div>
	</div>
	<div class="product-info">
		<a href="{{ $link }}"><h6 class="p_name">{{ $name }}</h6></a>
		<h5>{{ $price }}</h5>
	</div>

</div> 