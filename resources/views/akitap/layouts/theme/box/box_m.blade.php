	<div class="product-box">
		<div class="img-block">
			<a href="#"><img src="{{ res("product",jres($row->product_image),"250x250") }}" class=" img-fluid bg-img" alt="{{ $row->product_name }}"></a>

			<div class="lable-wrapper">
				<span class="lable1 w40">{{ yuzde($row->product_old_price,$row->product_price) }}</span>

			</div>


			<div class="add-btn">
				<a href="#" class="btn btn-outline addcart-box" tabindex="0">Sepete Ekle</a>
			</div>
		</div>
		<div class="product-info">
			<a href="#"><h6 class="p_name">{{ $row->product_name }}</h6></a>
			<h5>{{ $row->product_price }}</h5>
		</div>

	</div>
	