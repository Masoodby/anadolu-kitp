<div>
	<div class="product">
		<div>
			<a href="#"><img src="{{ res("product",jres($row->product_image),"250x250") }}" class=" img-fluid bg-img" alt=""></a>
		</div>
		<div>
			<div class="timer">
				<p id="demo"></p>
			</div>
			<a href="#"><p>{{ $row->product_name }}</p></a>
			<h5>{{ $row->product_price }}TL <del>{{ $row->product_old_price }} TL </del></h5>
			<div class="add-btn">
				<a href="#" class="btn btn-outline" tabindex="0">Sepete Ekle</a>
			</div>
		</div>
	</div>
</div>