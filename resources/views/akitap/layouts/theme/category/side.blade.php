	<div class="col-sm-3 collection-filter">
		<!-- side-bar colleps block stat -->
		<div class="collection-filter-block">
			<!-- brand filter start -->
			<div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> Geri</span></div>

			@if (!$subcats->isEmpty())
			{{-- expr --}}

			<div class="collection-collapse-block open">
				<h3 class="collapse-block-title">Kategoriler</h3>
				<div class="collection-collapse-block-content">
					<div class="collection-brand-filter">
						<ul class="">
							@foreach ($subcats as $cat)
							<li class="cat-item">	<a href="{{ route('kategori',$cat->categories_url) }}"><i class="fa fa-angle-double-right mr-2"></i>{{ str_limit($cat->categories_name,40)  }}</a></li>
							@endforeach
						</ul>


					</div>
				</div>
			</div>

			@endif
			<!-- color filter start here -->
			<div class="collection-collapse-block open">
				<h3 class="collapse-block-title">Yazarlar</h3>
				<div class="collection-collapse-block-content">


					<div class="input-group input-group-sm pt10">

						<input type="text" class="form-control search-spec" data-grid="yazarlist_side" data-clas="yazar_row" aria-label="Yazar Ara" placeholder="Yazar Ara.." aria-describedby="inputGroup-sizing-sm">
						<div class="input-group-append">
							<span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-search"></i></span>
						</div>
					</div>
					<div class="collection-brand-filter scroll_panel " id="yazarlist_side"  >



						@foreach ($yazarlar  as $id => $name)



						<div class="custom-control custom-checkbox collection-filter-checkbox yazar_row mb0 mt4" data-name="{{ $name }}">
							<input type="checkbox"  class="custom-control-input yazar-side" name="yazarside[]" id="yazar-side-check{{ $id }}" value="{{ $id }}">
							<label class="custom-control-label text-capitalize" for="yazar-side-check{{ $id }}">{{ ucwords_tr($name) }}  </label><span class="float-right badge badge-secondary">10</span>
						</div>

						@endforeach


					</div>

					<button class="btnx  btn-sm mt5 btn-block yazar-filter-btn" type="button"  >Uygula</button>

				</div>            

				<div class="collection-collapse-block open">
					<h3 class="collapse-block-title">Yayın Evleri</h3>
					<div class="collection-collapse-block-content">


						<div class="input-group input-group-sm pt10">

							<input type="text" class="form-control search-spec" data-grid="yayincilist_side" data-clas="yayinci_row" aria-label="Yazar Ara" placeholder="Yayıncı Ara.." aria-describedby="inputGroup-sizing-sm">
							<div class="input-group-append">
								<span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-search"></i></span>
							</div>
						</div>
						<div class="collection-brand-filter scroll_panel " id="yayincilist_side"  >


							@foreach ($yayincilar as $key => $name)

							<div class="custom-control custom-checkbox collection-filter-checkbox yayinci_row mb0 mt4"  data-name="{{ $name }}">
								<input type="checkbox" class="custom-control-input yayinci-side" id="yayinci-side-check{{ $key }}" data-id="{{ $key }}">
								<label class="custom-control-label text-capitalize " for="yayinci-side-check{{ $key }}">{{ ucwords_tr($name) }} </label><span class="float-right badge badge-secondary">10</span>
							</div>

							@endforeach



						</div>

						<button class="btnx  btn-sm mt5 btn-block yazar-filter-btn" type="button"  >Uygula</button>

					</div>
				</div>
			</div>
			<!-- price filter start here -->
			<div class="collection-collapse-block border-0 open">
				<h3 class="collapse-block-title">Fiyat</h3>
				<div class="collection-collapse-block-content">
					<div class="collection-brand-filter">
						@for ($i = 0; $i <50 ; $i=$i+10)
						{{-- expr --}}

						<div class="custom-control custom-checkbox collection-filter-checkbox">
							<input type="radio" class="custom-control-input" id="fourhundredabove{{ $i }}">
							<label class="custom-control-label" for="fourhundredabove{{ $i }}">{{ $i }}TL - {{ $i+10 }} TL</label>
						</div>

						@endfor

						<div class="custom-control custom-checkbox collection-filter-checkbox">
							<input type="radio" class="custom-control-input" id="fourhundredabove{{ $i+20 }}">
							<label class="custom-control-label" for="fourhundredabove{{ $i+20 }}">  {{ $i }} TL ++</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>