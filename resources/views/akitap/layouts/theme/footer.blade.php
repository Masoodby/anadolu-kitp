    <footer>
        <div class="subscribe-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="subscribe-content">
                            <h4> <i class="fa fa-envelope-o" aria-hidden="true"></i>Bülten Kayıt</h4>
                            <p>Yeni çıkmış Ürünlerimiz için Mail Bültenimize Abone olabilirsiniz. </p>
                            <form class="form-inline subscribe-form">
                                <div class="form-group mb-0">
                                    <input type="text" class="form-control" id="exampleFormControlInput3" placeholder="E-Posta...">
                                </div>
                                <button type="submit" class="btn btn-solid">Kaydol</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="footer-section">
            <div class="container">
                <div class="row section-t-space section-b-space">
                    <div class="col-xl-4 col-lg-4 about-section">
                        <div class="footer-title footer-mobile-title">
                            <h4>{{ set("name") }}</h4>
                        </div>
                        <div class="footer-content">
                            <div class="footer-logo">
                                <img src="{{ res('content',set('footer_logo'),false)}}" alt="{{ set("name") }}">
                            </div>
                            <p>{{ set('desc') }}</p>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-4 footer-link">
                        <div>
                            <div class="footer-title">
                                <h4>Kurumsal</h4>
                            </div>
                            <div class="footer-content">
                                <ul>
                                    @foreach (staticPages(1) as $row)
                                    <li><a href="{{ route('kurumsal',$row->page_url) }}">{{ $row->page_name }}</a></li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-4 footer-link">
                        <div>
                            <div class="footer-title">
                                <h4>Yardım</h4>
                            </div>
                            <div class="footer-content">
                                <ul>
                                 @foreach (staticPages(2) as $row)
                                 <li><a href="{{ route('yardim',$row->page_url) }}">{{ $row->page_name }}</a></li>
                                 @endforeach
                             </ul>
                         </div>
                     </div>
                 </div>
                 <div class="col-xl-4 col-lg-12 ratio_square">
                    <div class="instagram">
                        <div>
                            <div class="instagram-banner">
                              <h5>Bizi Takip edin <span>#anadolukitapcom</span></h5>
                          </div>
                          <div class="container">
                            <div class="row">

                                <div class="col p-0">
                                    <a href="#">
                                        <div class="instagram-box">
                                            <div>
                                                <img src="{{ asset('dist/front/'.theme().'/images/insta/2.jpg')}}" alt="" class=" img-fluid">
                                            </div>
                                            <div class="overlay">
                                                <i class="fa fa-instagram"  aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>


                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col p-0">
                                    <a href="#">
                                        <div class="instagram-box">
                                            <div>
                                                <img src="{{ asset('dist/front/'.theme().'/images/insta/6.jpg') }}" alt="" class=" img-fluid">
                                            </div>
                                            <div class="overlay">
                                                <i class="fa fa-instagram"  aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="call-us">
                    <img src="{{ asset('dist/front/'.theme().'/images/call-us.jpg')}}" alt="" class="bg-img bg-left">
                    <div class="footer-banner-content">
                        <div class="call-text">
                            <div>
                             <h3>Müşteri Temsilcisi:</h3>
                             <span class="call-no">{{ set('tel') }} <span>(pbx)</span></span>
                         </div>
                     </div>
                     <div class="footer-social">
                        <ul>

                            @foreach (setAll(8) as $var)
                            {{-- expr --}}
                            <li>
                                <a href="{{ $var->settings_val }}" target="_blank"><i class="fa fa-{{ $var->settings_key }}" aria-hidden="true"></i></a>
                            </li>

                            @endforeach


                        </ul>
                    </div>
                    <div class="address-section">
                        <h6>Adres</h6>
                        <p>{{ set('adres') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="footer-bottom">
                <ul>
                    @foreach (nav_cat(0,10) as $var)

                    <li><a href="{{ route('kategori',$var->categories_url) }}">{{ $var->categories_name }}</a></li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<div class="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-sm-12">
                <div class="footer-end">
                    <p><i class="fa fa-copyright" aria-hidden="true"></i>  2018-{{ date('Y') }} {{ set('name') }}</p>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-12">
                <div class="payment-card-bottom">
                    <ul>
                        <li>
                            <a href="#"><img src="{{ asset('dist/front/'.theme().'/images/icon/visa.png')}}" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{ asset('dist/front/'.theme().'/images/icon/mastercard.png')}}" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{ asset('dist/front/'.theme().'/images/icon/paytr.png')}}" alt=""></a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</footer>