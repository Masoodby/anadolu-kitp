  <header class="header-1">
    <div class="mobile-fix-header">
    </div>
    <div class="container">
        <div class="row header-content">
            <div class="col-lg-3 col-6">
                <div class="left-part">
                    <p>{{ set('top_text') }}</p>
                </div>
            </div>
            <div class="col-lg-9 col-6">
                <div class="right-part">
                    <ul>
                        @guest
                        <li><a href="{{ route('panel.login') }}">Üye Girişi</a></li>
                        <li><a href="{{ route('panel.register') }}">Yeni Üye</a></li>

                        @endguest
                        <li><a href="#">İletişim</a></li>
                        @auth
                        <li><a href="{{ route('panel.login') }}">Sipariş Takibi</a></li>
                        <li><a href="#"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Çıkış Yap</a>
                          <form id="logout-form" action="{{ route('panel.logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }} </form>
                      </li>
                      @endauth





                  </ul>
              </div>
          </div>
      </div>
      <div class="row header-content">
        <div class="col-12">
            <div class="header-section">
                <div class="navbar">
                    <a href="javascript:void(0)" onclick="openNav()">
                        <div class="bar-style"><i class="fa fa-bars sidebar-bar" aria-hidden="true"></i></div>
                    </a>
                    <div id="mySidenav" class="sidenav">
                        <a href="javascript:void(0)" class="sidebar-overlay" onclick="closeNav()"></a>
                        <nav class="">
                            <div onclick="closeNav()">
                                <div class="sidebar-back text-left"><i class="fa fa-angle-left pr-2" aria-hidden="true"></i> Geri</div>
                            </div>
                            {!! nav() !!}
                        </nav>
                    </div>
                </div>
                <div class="brand-logo">
                    <a href="{{ route('anasayfa') }}"> <img src="{{ res('content',set('logo'),false)}}" class="" alt="{{ set("name") }}"></a>
                </div>
                <div class="search-bar">
                    <input class="search__input" type="text" placeholder="Arama...">
                    <div class="search-icon "></div>
                </div>
                <div class="nav-icon">
                    <ul>
                        <li class="onhover-div setting-icon">
                            <div><img src="{{ asset('dist/front/'.theme().'/images/icon/settings.png')}}" class=" img-fluid setting-img" alt="">
                                <i class="ti-settings mobile-icon"></i>
                            </div>
                            <div class="show-div setting">
                                <h6>Panelim</h6>
                                <ul>
                                    <li class="active"><a href="#">Siparişlerim</a> </li>
                                    <li><a href="#">Bilgilerim</a> </li>

                                    <li><a href="#">İstek Listem</a> </li>
                                    <li class="active"><a href="#">Adreslerim</a> </li>
                                    <li><a href="#">Hediye Puanlarım</a> </li>
                                    <li><a href="#">Çıkış</a> </li>
                                </ul>
                            </div>
                        </li>
                        <li class="onhover-div search-3">
                            <div onclick="openSearch()">
                                <i class="ti-search mobile-icon-search" ></i>
                                <img src="{{ asset('dist/front/'.theme().'/images/icon/l-1/search.png')}}" class=" img-fluid search-img" alt="">
                            </div>
                            <div id="search-overlay" class="search-overlay">
                                <div>
                                    <span class="closebtn" onclick="closeSearch()" title="Kapat">×</span>
                                    <div class="overlay-content">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <form>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Arama...">
                                                        </div>
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="onhover-div wishlist-icon" onclick="openWishlist()">
                            <img src="{{ asset('dist/front/'.theme().'/images/icon/l-1/wishlist.png')}}" alt=""  class="wishlist-img">
                            <i class="ti-heart mobile-icon"></i>
                            <div class="wishlist icon-detail">
                                <h6 class="up-cls"><span id="topwishlistcount">0 Ürün</span></h6>
                                <h6><a href="#">Favorilerim</a></h6>
                            </div>
                        </li>
                        <li class="onhover-div user-icon"   onclick="openAccount()"  >
                            <img src="{{ asset('dist/front/'.theme().'/images/icon/l-1/user.png')}}" alt="" class="user-img">
                            <i class="ti-user mobile-icon"></i>
                            <div class="wishlist icon-detail">
                                <h6 class="up-cls"><span>Üye İşlemleri</span></h6>
                                @guest('web')
                                <h6><a href="javascript:;">Giriş Yap</a></h6>
                                @endguest

                                @auth('web')
                                <h6><a href="javascript:;">Panel</a></h6>
                                @endauth

                            </div>
                        </li>
                        <li class="onhover-div cart-icon" onclick="openCart()">
                            <img src="{{ asset('dist/front/'.theme().'/images/icon/l-1/shopping-cart.png')}}" alt="" class="cart-image">
                            <i class="ti-shopping-cart mobile-icon"></i>
                            <div class="cart  icon-detail">
                                <h6 class="up-cls"><span id="topcartcount">{{Cart::count()}} Ürün</span></h6>
                                <h6><a href="#">Sepetim</a></h6>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-class">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav id="main-nav">
                    <div class="toggle-nav">
                        <i class="ti-menu-alt"></i>
                    </div>
                    <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                        <li>
                            <div class="mobile-back text-right">
                                Geri<i class="fa fa-angle-right pl-2" aria-hidden="true"></i>
                            </div>
                        </li>
                        <li class="icon-cls"><a href="{{ route('anasayfa') }}"><i class="fa fa-home home-icon" aria-hidden="true"></i></a></li>



                        <li class="mega"><a href="#">Tüm Kitaplar</a>
                            <ul class="mega-menu feature-menu full-mega-menu">
                                <li>
                                    <div class="container">
                                        <div class="row">

                                            @foreach (nav_cat(0) as $var)


                                            <div class="col-xl-2 mega-box mb10">
                                                <div class="link-section">
                                                    <div class="demo">
                                                        <div class="menu-title mt10 ">
                                                            <h6>{{ $var->categories_name }}</h6>
                                                        </div>
                                                        <div class="menu-content">

                                                            <ul>
                                                                @foreach (nav_cat($var->id,6) as $sub)
                                                                <li><a href="{{ route('kategori',$sub->categories_url) }}" class="fsz12">{{ str_limit($sub->categories_name,30) }}</a></li>
                                                                @endforeach

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li><a href="{{ route('kategori','cocuk-kitaplari') }}">Çocuk Kitapları</a>  </li>
                        <li><a href="{{ route('en-cok-okunan-kitaplar') }}">En Çok Okunanlar</a>  </li>
                        <li><a href="{{ route('yeni-kitaplar') }}">Yeni Çıkanlar</a>  </li>
                        <li><a href="{{ route('yayin-evleri') }}">Yayın Evleri</a>  </li>
                        <li><a href="{{ route('yazarlar') }}">Yazarlar</a>  </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</header>