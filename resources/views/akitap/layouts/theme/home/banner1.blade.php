<section class="section-b-space banner">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="banner-content">
					<h6>Depodan Satış..</h6>
					<h2>Tüm Ürünlerde <span>Aynı Gün Kargo</span></h2>
					<h4>Tedarik süresi derdi yok.</h4>
					<div class="banner-btn">
						<h6>Etiket fiyatından  <span>%50'ye varan</span> indirimler</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>