<section class="ratio_45">
	<div class="container">
		<div class="row partition-3">
			<div class="col-md-4">
				<a href="#">
					<div class="collection-banner p-left">
						<div class="img-part">
							<img src="{{ res('banner','1.jpg',false)}}" class=" img-fluid bg-img" alt="">
						</div>
						<div class="contain-banner banner-3">
							<div>
								<div class="banner-deal">
									<h6>İbni Saad</h6>
								</div>
								<h3>Tabakatı</h3>
								<h6>11 Cilt | İncele</h6>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="#">
					<div class="collection-banner p-left">
						<div class="img-part">
							<img src="{{ res('banner','2.jpg',false)}}" class=" img-fluid bg-img" alt="">
						</div>
						<div class="contain-banner banner-3">
							<div>
								<div class="banner-deal">
									<h6>Kanes Yayınları</h6>
								</div>
								<h3>Hirai Zerdüş</h3>
								<h6>Topla Yüreğini <br>Gidelim Buradan</h6>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="#">
					<div class="collection-banner p-left">
						<div class="img-part">
							<img src="{{ res('banner','3.jpg',false)}}"class=" img-fluid bg-img" alt="">
						</div>
						<div class="contain-banner banner-3">
							<div>
								<div class="banner-deal">
									<h6>Tahlil Yayınları</h6>
								</div>
								<h3>% 30 İndirim</h3>
								<h6>Tüm Kitalar</h6>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>