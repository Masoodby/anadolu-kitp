<section class="section-b-space banner-sec landscape-layout ">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-6 pr-0">
				<a href="#">
					<div class="left-banner">
						<h2>5% off</h2>
						<img src="../assets/images/banner/5.png" class=" img-fluid" alt="">
					</div>
				</a>
			</div>
			<div class="col-lg-7 col-12 pl-0 order-class">
				<div class="center-banner">
					<div class="center">
						<a href="#"><img src="../assets/images/banner/4.jpg" alt="" class=" img-fluid"></a>
					</div>
					<div class="contain-left">
						<div>
							<h4>today</h4>
							<h4><span>money savers</span></h4>
						</div>
					</div>
					<div class="contain-right">
						<div>
							<a href="#"><h6>Richard McClintock</h6></a>
							<h5>$963.00</h5>
							<h6><span>sale ending</span></h6>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-6">
				<a href="#">
					<div class="right-banner">
						<div>
							<h5>summer sale</h5>
							<h2>sale</h2>
							<h6>save 30% off</h6>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>