<section class="category no-arrow">
	<div class="container">
		<div class="category-6">
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/fashion.png" alt="">
					<div class="category-content">
						<h6>20% off</h6>
						<h5>fashion</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/mobile.png" alt="">
					<div class="category-content">
						<h6>-10% off</h6>
						<h5>mobile</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/furniture.png" alt="">
					<div class="category-content">
						<h6>30% off</h6>
						<h5>furniture</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/light.png" alt="">
					<div class="category-content">
						<h6>sale</h6>
						<h5>light</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/bag.png" alt="">
					<div class="category-content">
						<h6>-5% off</h6>
						<h5>bag</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/razer.png" alt="">
					<div class="category-content">
						<h6>$99</h6>
						<h5>razer</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
			<div>
				<div class="category-block">
					<img src="../assets/images/category/layout-1/razer.png" alt="">
					<div class="category-content">
						<h6>$99</h6>
						<h5>razer</h5>
						<a href="#" class="btn btn-solid btn-solid-sm">view more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>