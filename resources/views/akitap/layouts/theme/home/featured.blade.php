<section class="upper-part">
	<div class="container">
		<div class="row partition">
			<div class="col-xl-4 col-lg-6 col-md-12">
				<div class="blog-box blog-pattern">
					<div class="blog-white feature-sec">
						<h2 class="title">our featured offers</h2>
						<div class="feature-block">
							<div class="media">
								<a href="#"><img class="mr-3  img-fluid" src="{{ asset('dist/front/'.theme()) }}/images/featured/featured1.jpg"  alt="Generic placeholder image"></a>
								<div class="media-body">
									<a href="#"><h5>save 30% off</h5></a>
								</div>
							</div>
							<div class="media">
								<a href="#"><img class="mr-3  img-fluid" src="{{ asset('dist/front/'.theme()) }}/images/featured/featured2.jpg" alt="Generic placeholder image"></a>
								<div class="media-body">
									<a href="#"><h5>only $99</h5></a>
								</div>
							</div>
						</div>
						<div class="feature-block">
							<div class="media">
								<a href="#"><img class="mr-3  img-fluid" src="{{ asset('dist/front/'.theme()) }}/images/featured/featured3.jpg"  alt="Generic placeholder image"></a>
								<div class="media-body">
									<a href="#"><h5>save 20%</h5></a>
								</div>
							</div>
							<div class="media">
								<a href="#"><img class="mr-3  img-fluid" src="{{ asset('dist/front/'.theme()) }}/images/featured/featured4.jpg" alt="Generic placeholder image"></a>
								<div class="media-body">
									<a href="#"><h5>2 day shipping</h5></a>
								</div>
							</div>
						</div>
						<div class="feature-block">
							<div class="media">
								<a href="#"><img class="mr-3  img-fluid" src="{{ asset('dist/front/'.theme()) }}/images/featured/featured5.jpg"  alt="Generic placeholder image"></a>
								<div class="media-body">
									<a href="#"><h5>sale 60% off</h5></a>
								</div>
							</div>
							<div class="media">
								<a href="#"><img class="mr-3  img-fluid" src="{{ asset('dist/front/'.theme()) }}/images/featured/featured6.jpg" alt="Generic placeholder image"></a>
								<div class="media-body">
									<a href="#"><h5>20% off toys</h5></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-md-12 order-lg-1 order-xl-0">
				<div class="blog-box blog-pattern">
					<div class="blog-white review-sec">
						<h2 class="title">customer says</h2>
						<div class="slide-1">
							<div>
								<div class="review-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/testimonial/1.jpg"  alt="Generic placeholder image">
										<div class="icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
										<div class="media-body">
											<h6>mark jecno</h6>
											<p>Long established fact that a reader will be distracted by the readable.Long established fact. </p>
										</div>
									</div>
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/testimonial/2.jpg"  alt="Generic placeholder image">
										<div class="icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
										<div class="media-body">
											<h6>lara oprt</h6>
											<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for text.</p>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="review-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/testimonial/1.jpg"  alt="Generic placeholder image">
										<div class="icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
										<div class="media-body">
											<h6>mark jecno</h6>
											<p>Long established fact that a reader will be distracted by the readable.Long established fact. </p>
										</div>
									</div>
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/testimonial/2.jpg"  alt="Generic placeholder image">
										<div class="icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
										<div class="media-body">
											<h6>lara oprt</h6>
											<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for text.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-12">
				<div class="blog-box blog-pattern">
					<div class="blog-white product-sec">
						<h2 class="title">you may like</h2>
						<div class="slide-1">
							<div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/1.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>memory card</h5>
												<p>965 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/2.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>watch</h5>
												<p>265 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/3.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>printer</h5>
												<p>653 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/4.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>books</h5>
												<p>4965 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/1.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>memory card</h5>
												<p>965 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/2.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>watch</h5>
												<p>265 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/3.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>printer</h5>
												<p>653 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
								<div class="product-block">
									<div class="media">
										<img class="mr-3" src="{{ asset('dist/front/'.theme()) }}/images/may-like/4.jpg"  alt="Generic placeholder image">
										<div class="media-body">
											<div>
												<h5>books</h5>
												<p>4965 products</p>
											</div>
											<a href="#" class="btn btn-solid btn-solid-sm">shop now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>