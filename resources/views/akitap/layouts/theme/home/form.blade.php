<section class="">
	<div class="container">
		<div class="row partition-3">
			<div class="col-xl-4 col-md-6">
				<div class="blog-box blog-pattern">
					<div class="blog-white contact">
						<h5>need help? contact us</h5>
						<h3>info@bigboost.com</h3>
						<div class="contact-form">
							<h5><span>contact us</span></h5>
							<form class="theme-form">
								<div class="form-group">
									<input type="email" class="form-control" id="exampleInputEmail1"  placeholder="your name">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" id="exampleInputEmail2" placeholder="number">
								</div>
								<div class="form-group">
									<textarea class="form-control" id="exampleFormControlTextarea1" placeholder="message" rows="3"></textarea>
								</div>
								<button type="submit" class="btn btn-solid btn-block">send a enquary</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-md-12 order-cls">
				<div class="blog-box blog-pattern blog">
					<div class="blog-white">
						<h2 class="title">success story</h2>
						<div class="slide-1">
							<div>
								<div class="media">
									<a href="#"><img src="../assets/images/blog/multi-category/4.jpg" class=" img-fluid mr-3" alt=""></a>
									<div class="media-body blog-info blog-vertical">
										<div>
											<a href="#"><h5>25 july 2018</h5></a>
											<h6>by: admin, 0 comment</h6>
											<p>Sometimes on purpose ected humour. dummy text.</p>
										</div>
									</div>
								</div>
								<div class="media">
									<a href="#"><img src="../assets/images/blog/multi-category/5.jpg" class=" img-fluid mr-3" alt=""></a>
									<div class="media-body blog-info blog-vertical">
										<div>
											<a href="#"><h5>25 july 2018</h5></a>
											<h6>by: admin, 0 comment</h6>
											<p>Sometimes on purpose ected humour. dummy text.</p>
										</div>
									</div>
								</div>
								<div class="media">
									<a href="#"><img src="../assets/images/blog/multi-category/6.jpg" class=" img-fluid mr-3" alt=""></a>
									<div class="media-body blog-info blog-vertical">
										<div>
											<a href="#"><h5>25 july 2018</h5></a>
											<h6>by: admin, 0 comment</h6>
											<p>Sometimes on purpose ected humour. dummy text.</p>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="media">
									<a href="#"><img src="../assets/images/blog/multi-category/1.jpg" class=" img-fluid mr-3" alt=""></a>
									<div class="media-body blog-info blog-vertical">
										<div>
											<a href="#"><h5>25 july 2018</h5></a>
											<h6>by: admin, 0 comment</h6>
											<p>Sometimes on purpose ected humour. dummy text.</p>
										</div>
									</div>
								</div>
								<div class="media">
									<a href="#"><img src="../assets/images/blog/multi-category/2.jpg" class=" img-fluid mr-3" alt=""></a>
									<div class="media-body blog-info blog-vertical">
										<div>
											<a href="#"><h5>25 july 2018</h5></a>
											<h6>by: admin, 0 comment</h6>
											<p>Sometimes on purpose ected humour. dummy text.</p>
										</div>
									</div>
								</div>
								<div class="media">
									<a href="#"><img src="../assets/images/blog/multi-category/3.jpg" class=" img-fluid mr-3" alt=""></a>
									<div class="media-body blog-info blog-vertical">
										<div>
											<a href="#"><h5>25 july 2018</h5></a>
											<h6>by: admin, 0 comment</h6>
											<p>Sometimes on purpose ected humour. dummy text.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-md-6">
				<div class="blog-box blog-pattern">
					<div class="blog-white app-white">
						<h5>download the <span>bigboost app</span></h5>
						<div class="app-buttons">
							<a href="#"><img src="../assets/images/app/app-storw.png" class=" img-fluid" alt=""></a>
							<a href="#"><img src="../assets/images/app/play-store.png" class=" img-fluid" alt=""></a>
						</div>
						<img src="../assets/images/app/1.png" class=" img-fluid mobile1" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>