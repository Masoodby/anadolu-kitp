<section class="section-b-space slider-section">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5 ratio_asos">
				<div class="deal-box">
					<div >
						<h2 class="title">HAFTANIN EN'LERİ</h2>
						<div class="slide-1 border-0">
							
							@foreach (getProduct("product_konum_today",5) as $row)
							@include(theme().'/layouts.theme.box.box_week')
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-2 side-banner side-banner1 pr-0 d-none d-xl-block">
				<a href="#"><img src="{{ res('banner','side-banner.jpg',false)}}" alt="" class=" img-fluid "></a>
			</div>
			<div class="col-xl-6 col-lg-7 pl-0 padding-cls ratio_square">
				<div class="tab-head">
					<h2 class="title">Ayın Enleri</h2>
				</div>
				<div class="slider-3">
					<div>
						@foreach (getProduct("product_konum_cat",18) as $row)
						@include(theme().'/layouts.theme.box.box_m')

						@if ($loop->iteration%2==0)
					</div>
					<div>
						@endif
						@endforeach

					</div>



				</div>
			</div>
		</div>
	</div>
</section>