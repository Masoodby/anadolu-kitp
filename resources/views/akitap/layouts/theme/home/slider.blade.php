<div class="slider-bg slider-bg-2 ">
	<div class="container">
		<div class="row">
			<div class="col-12 slider-part">
				<div class="slide-1 home-slider">
					@foreach ($slider as $row)
					{{-- expr --}}
					<div>
						<div class="home text-left p-left">
							<img src="{{ res('slider' ,$row->slider_image,'1400x508')}}" class="bg-img " alt="">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="slider-contain">
											<div>
												<h5>{{ $row->slider_name }}</h5>
												@php($text =  json_decode($row->slider_desc))

												<h1>{!! @$text->sub1 !!}</h1>
												<h4>{{ @$text->sub2 }}</h4>
												<a href="#" class="btn btn-solid">{{ @$text->sub3 }}</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					@endforeach


				</div>
			</div>
		</div>
	</div>
</div>