<section class="section-b-space tab-layout1 ratio_square">
	<div class="theme-tab">
		<div class="drop-shadow">
			<div class="left-shadow">
				<img src="{{ asset('dist/front/'.theme().'/images/left.png')}}" alt="" class=" img-fluid">
			</div>
			<div class="right-shadow">
				<img src="{{ asset('dist/front/'.theme().'/images/right.png')}}" alt="" class=" img-fluid">
			</div>
		</div>
		<ul class="tabs">
			@php($dizi = ["product_konum_cok"=>'EN ÇOK OKUNANLAR',"product_konum_new"=>'YENİ KİTAPLAR',"product_konum_ind"=>'İNDİRİMLİ KİTAPLAR','cocuk'=>"Çocuk Kitapları"])

			@foreach ($dizi as $key => $row)			
			<li class="{{ $loop->iteration == 1?'current':'' }}">
				<a href="tab-{{ $loop->iteration }}">{{ $row }}</a>
			</li>
			@endforeach



		</ul>
		<div class="tab-content-cls">

			@foreach ($dizi as $key => $element)
			{{-- expr --}}

			@if ($key !='cocuk')
			{{-- expr --}}

			<div id="tab-{{ $loop->iteration }}" class="tab-content {{ $loop->iteration == 1?'active default':'' }} " >
				<div class="container">
					<div class="row border-row1">

						@foreach (getProduct($key,18) as $row)

						<div class="col-lg-2 col-sm-4 col-6 p-0">
							@include(theme().'/layouts.theme.box.box1')
						</div>

						@endforeach

					</div>
				</div>
			</div>

			@else



			<div id="tab-4" class="tab-content" >
				<div class="container">
					<div class="row border-row1">

						@foreach (getProductAll(6,18) as $row)

						<div class="col-lg-2 col-sm-4 col-6 p-0">
							@include(theme().'/layouts.theme.box.box1')
						</div>

						@endforeach

					</div>
				</div>
			</div>


			@endif

			@endforeach

		</div>
	</div>
</section>