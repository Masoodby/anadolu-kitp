 <div class="tap-top top-cls">
 	<div>
 		<i class="fa fa-angle-double-up"></i>
 	</div>
 </div>

 <!-- latest jquery-->
 <script src="{{ asset('dist/front/'.theme().'/js/jquery-3.3.1.min.js')}}" ></script>

 <!-- menu js-->
 <script src="{{ asset('dist/front/'.theme().'/js/menu.js')}}"></script>

 <!-- popper js-->
 <script src="{{ asset('dist/front/'.theme().'/js/popper.min.js')}}" ></script>

 <!-- slick js-->
 <script  src="{{ asset('dist/front/'.theme().'/js/slick.js')}}"></script>

 <!-- Bootstrap js-->
 <script src="{{ asset('dist/front/'.theme().'/js/bootstrap.js')}}" ></script>
 

 <!-- Bootstrap Notification js-->
 <script src="{{ asset('dist/front/'.theme().'/js/bootstrap-notify.min.js')}}"></script>
 <script src="{{ asset('dist/front/'.theme().'/js/jquery.jscrollpane.min.js')}}"></script>

 <!-- Theme js-->
 <script src="{{ asset('dist/front/'.theme().'/js/script.js')}}" ></script>

 <!-- modal js-->
 <script src="{{ asset('dist/front/'.theme().'/js/modal.js')}}" ></script>



 <script src="{{ asset('dist/panel/js/notify.min.js')}}"></script>
 <script src="{{ asset('dist/panel/js/bootbox.min.js')}}"></script>

 <script src="{{ asset('dist/panel/js/jquery.toast.min.js')}}"></script>

 <script src="{{ asset('dist/front/'.theme().'/js/app.js')}}" ></script>

 @yield('script')