    <!-- Add to cart bar -->
    <div id="cart_side" class="add_to_cart right">
        <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>


        <div class="cart-inner">
            <div class="cart_top">
                <h3>Sepetim</h3>
                <div class="close-cart">
                    <a href="javascript:void(0)" onclick="closeCart()">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            @if(count(Cart::content())>0)
            <div class="cart_media" id="sidecart">
               <ul class="cart_product" >

                 @foreach(Cart::content() as $item)


                 <li>
                    <div class="media">

                        <a href="{{ $item->options->url }}"><img src="{{ res("product",jres($item->options->img),"100x100") }}" class="mr-3"></a>
                        <div class="media-body">
                            <a href="{{ $item->options->url }}"><h4 class="fsz12">{{ $item->name }}</h4></a>
                            <h4><span>{{ $item->qty }} x {{ $item->price }} TL</span></h4>
                        </div>
                    </div>

                </li>

                @endforeach
            </ul>
            <ul class="cart_total">
                <li>
                    <div class="total">
                        <h5>Ara Toplam : <span>{{Cart::subtotal()}} TL</span></h5>
                    </div>
                </li>
                <li>
                    <div class="buttons">
                        <a href="{{ route('sepet') }}" class="btn btn-solid btn-block btn-solid-sm view-cart">Sepete Git</a>
                        <a href="{{ route('siparisi-tamamla') }}" class="btn btn-solid btn-solid-sm btn-block checkout">Siparişi Tamamla</a>
                    </div>
                </li>
            </ul>
        </div>
        @else
        <div class="cart_media" id="sidecart">
            <h3>Sepetiniz Boş.</h3>
        </div>
        @endif
    </div>


</div>
<!-- Add to cart bar end-->

<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="exampleModalLabel">
                    Sepete Eklendi
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="cartmodalbody">
                <p>Sepet Boş</p>
            </div>
            <div class="modal-footer border-top-0 d-flex justify-content-between">
                <a href="{{ route('anasayfa') }}" class="btn btn-secondary"  >Alışverişe Devam Et</a>
                <a href="{{ route('sepet') }}" class="btn btn-primary">Sepeti Düzenle</a>
                <a href="{{ route('siparisi-tamamla') }}" class="btn btn-success">Siparişi Tamamla</a>
            </div>
        </div>
    </div>
</div>

