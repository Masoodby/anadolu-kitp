 <table class="table table-image table-hover">
 	<thead>
 		<tr>
 			<th scope="col"></th>
 			<th scope="col">Ürün</th>
 			<th scope="col">Fiyat</th>
 			<th scope="col">Adet</th>
 			<th scope="col">Toplam</th>

 		</tr>
 	</thead>
 	<tbody>
 		<tr>
 			<td class="w-25">
 				<img src="{{ res("product",jres($item->options->img),"100x100") }}" class="img-fluid img-thumbnail"  >
 			</td>
 			<td>{{ $item->name }}</td>
 			<td>{{ $item->price }} TL</td>
 			<td class="qty">{{ $item->qty }}</td>
 			<td>{{ $item->subtotal }} TL</td>

 		</tr>
 	</tbody>
 </table> 
 <div class="d-flex justify-content-end">
 	<h5>Sepet Toplamı: <span class="price text-success">{{Cart::total()}} TL</span></h5>
 </div>