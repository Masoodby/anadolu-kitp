
<!-- My account bar -->
<div id="myAccount" class="add_to_cart right">
    <a href="javascript:void(0)" class="overlay" onclick="closeAccount()"></a>
    @guest('web')


    <div class="cart-inner">
        <div class="cart_top">
            <h3>Giriş Formu</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeAccount()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <form class="theme-form" role="form" method="POST" id="sideloginform" action="{{ route('panel.login' )}}">
            {{ csrf_field()}}
            <div class="form-group">
                <label for="email">E-Posta Adresi</label>
                <input type="text" class="form-control req" name="email" placeholder="Email"  >
            </div>
            <div class="form-group">
                <label for="review">Şifre</label>
                <input type="password" class="form-control req" name="password" placeholder="Şifre Giriniz"  >
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" checked> Beni hatırla
                        </label>
                    </div>
                </div>
            </div>

            <button type="button" class="btn btn-solid btn-solid-sm btn-block sbmt_btn" data-id="sideloginform">Giriş Yap</button>

            <h5 class="forget-class text-center"><a href="#" class="d-block">Şifre Kurtarma</a></h5>
            <h5 class="forget-class text-center"><a href="{{ route('panel.register') }}" class="d-block">Yeni Üye Kayıt</a></h5>
        </form>
    </div>

    @endguest

    @auth('web')

    <div class="cart-inner">
        <div class="cart_top">
            <h3>Panele Git</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeAccount()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>


        <div class="col-md-9 mx-auto text-center">

            <a href="{{ route('panelim') }}" class="btn btn-solid bg-info btn-solid-sm btn-block ">Panelim</a>
            <hr>



            <a href="#" class="btn btn-solid bg-danger logoutbtnx btn-solid-sm btn-block" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Çıkış Yap </a>

            <form id="logout-form" action="{{ route('panel.logout') }}" method="POST" style="display: none;">{{ csrf_field() }} </form>

        </div>



    </div>

    @endauth
</div>
    <!-- Add to wishlist bar end-->