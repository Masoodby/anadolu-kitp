 <ul class="cart_product" >

     @foreach(Cart::content() as $item)


     <li>
        <div class="media">

            <a href="{{ $item->options->url }}"><img src="{{ res("product",jres($item->options->img),"100x100") }}" class="mr-3"></a>
            <div class="media-body">
                <a href="{{ $item->options->url }}"><h4 class="fsz12">{{ $item->name }}</h4></a>
                <h4><span>{{ $item->qty }} x {{ $item->price }} TL</span></h4>
            </div>
        </div>

    </li>

    @endforeach
</ul>
<ul class="cart_total">
    <li>
        <div class="total">
            <h5>Ara Toplam : <span>{{Cart::subtotal()}} TL</span></h5>
        </div>
    </li>
    <li>
        <div class="buttons">
            <a href="{{ route('sepet') }}" class="btn btn-solid btn-block btn-solid-sm view-cart">Sepete Git</a>
            <a href="{{ route('siparisi-tamamla') }}" class="btn btn-solid btn-solid-sm btn-block checkout">Siparişi Tamamla</a>
        </div>
    </li>
</ul>

<script>
    jQuery(document).ready(function($) {
        $('#topcartcount').html('{{Cart::count()}} Ürün' )
    });
</script>