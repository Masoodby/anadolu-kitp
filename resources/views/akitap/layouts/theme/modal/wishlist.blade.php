    <!-- Add to wishlist bar -->
    <div id="wishlist_side" class="add_to_cart right">
        <a href="javascript:void(0)" class="overlay" onclick="closeWishlist()"></a>
        <div class="cart-inner">
            <div class="cart_top">
                <h3>Favori Listem</h3>
                <div class="close-cart">
                    <a href="javascript:void(0)" onclick="closeWishlist()">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="cart_media">
                <ul class="cart_product">
                    @foreach ([] as $element)

                    <li>
                        <div class="media">
                            <a href="#"><img alt="" class="mr-3" src="../assets/images/product/3.jpg"></a>
                            <div class="media-body">
                                <a href="#"><h4>item name</h4></a>
                                <h4>
                                    <span>sm</span>
                                    <span>, blue</span>
                                </h4>
                                <h4><span>$ 299.00</span></h4>
                            </div>
                        </div>
                        <div class="close-circle">
                            <a href="#">
                                <i class="ti-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                    </li>

                    @endforeach

                </ul>
                <ul class="cart_total">
                    <li>
                        <div class="total">
                            <h5>Ara Toplam : <span>00.00</span></h5>
                        </div>
                    </li>
                    <li>
                        <div class="buttons">
                            <a href="#" class="btn btn-solid btn-block btn-solid-sm view-cart">Favori Listem</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Add to wishlist bar end-->