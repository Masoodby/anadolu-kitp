				<div class="col-lg-4">
					<div class="product-slick">
						@foreach (json_decode($product->detail->product_image) as $key => $var)
						<div><img src="{{ res("product",$var,"500x500") }}" alt="" class="img-fluid"></div>
						@endforeach


					</div>
					@if ($key > 0)
					{{-- expr --}}

					<div class="row">
						<div class="col-12 p-0">
							<div class="slider-nav">
								@foreach (json_decode($product->detail->product_image) as $var)
								<div><img src="{{ res("product",$var,"100x100") }}" alt="" class="img-fluid"></div>
								@endforeach
							</div>
						</div>
					</div>

					@endif
				</div>