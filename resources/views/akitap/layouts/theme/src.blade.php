  

<link rel="icon" href="{{ res('content',set('favicon'),false)}}" type="image/x-icon"/>
<link rel="shortcut icon" href="{{ res('content',set('favicon'),false)}}" type="image/x-icon"/>

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin-ext" rel="stylesheet">

<!-- Icons -->
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/fontawesome.css')}}">

<!-- Icons -->
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/animate.css')}}">

<!--Slick slider css-->
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/slick-theme.css')}}">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/bootstrap.css')}}">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{ asset('dist/panel/css/jquery.toast.min.css')}}">
<!-- Theme css -->
<link rel="stylesheet" type="text/css" id="color" href="{{ asset('dist/front/'.theme().'/css/color1.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/mp.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/extend.css')}}">


<meta name="csrf-token" content="{{ csrf_token() }}">

@yield('style')