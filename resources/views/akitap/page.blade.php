@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')


<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>{{ $page->page_name }}</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item " aria-current="page">{{ $title }}</li>
						<li class="breadcrumb-item active" aria-current="page">{{ $page->page_name }}</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="account-sidebar"><a class="popup-btn">{{ $title }}</a></div>
				<div class="dashboard-left">
					<div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> back</span></div>
					<div class="block-content">
						<ul>
							@foreach ($pages as $row)
							<li class="{{ $row->id == $page->id?'active':'' }}"><a href="{{ route(str_slug($title),$row->page_url) }}">{{ $row->page_name }}</a></li>
							@endforeach


						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="dashboard-right">
					<div class="dashboard">
						<div class="page-title">
							<h2>{{ $page->page_name }}</h2>
						</div>
						<div class="welcome-msg">
							{!! $page->page_content !!}
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- section end -->



@endsection