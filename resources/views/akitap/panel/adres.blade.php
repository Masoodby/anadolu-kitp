@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')




<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Panelim</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="account-sidebar"><a class="popup-btn">İşlemlerim</a></div>
				<div class="dashboard-left">
					<div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> geri</span></div>
					<div class="block-content">
						@include(theme().'/panel.side')
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="dashboard-right">
					<div class="dashboard">
						<div class="page-title">
							<h2>{{ $title }} <button class="btn btn-smx btn-info float-right add_adres" data-return="adreslerim" type="button">Yeni Ekle</button></h2>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="panel panel-default bg-f4 p10">
									<div class="panel-heading">
										<h3 class="panel-title">Default <small class="fsz10">(Varsayılan)</small></h3>
									</div>
									<div class="panel-body">
										<address>
											<p><strong>{{ auth()->user()->name }}</strong></p>
											<p>{{ $user_detail->user_address }}</p>
											<p>{{ $user_detail->user_tel }}</p>
										</address>
										<button  class="btn btn-sm btn--primary edit_adres"><i class="fa fa-edit pr5"></i> Düzenle</button>
										<button  class="btn btn-sm btn-danger del_adres"><i class="fa fa-times pr5"></i> Sil</button>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- section end -->



@endsection