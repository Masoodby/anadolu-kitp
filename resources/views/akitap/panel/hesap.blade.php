@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')




<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Panelim</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="account-sidebar"><a class="popup-btn">İşlemlerim</a></div>
				<div class="dashboard-left">
					<div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> geri</span></div>
					<div class="block-content">
						@include(theme().'/panel.side')
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="dashboard-right">
					<div class="dashboard">
						<div class="page-title">
							<h2>{{ $title }}</h2></div>
							
							<div class="account-details-form">


								<form action="{{ route('panelim.hesap') }}" id="" method="post" accept-charset="utf-8">

									{{ csrf_field() }}						
									<div class="row">
										<div class="col-md-6 col-12  mb30">
											<label for="">İsim</label>
											<input name="name" class="form-control req" placeholder="Ad" type="text" value="{{ auth()->user()->name }}">
										</div>   

										<div class="col-md-6 col-12  mb30">
											<label for="">Soyisim</label>
											<input name="sname" class="form-control req" placeholder="Soyad" type="text" value="{{ auth()->user()->sname }}">
										</div>

										<div class="col-md-6 col-12  mb30">
											<label for="">E-Posta</label>
											<input name="email" class="form-control req" placeholder="E-Posta Adresi" type="email" value="{{ auth()->user()->email }}">
										</div>
										<div class="col-md-6 col-12  mb30">
											<label for="">GSM</label>
											<input name="user_tel" class="form-control req int" placeholder="GSM" type="text" value="{{ $user_detail->user_tel }}">
										</div>
										<div class="col-md-12 col-12  mb30">

											<h4>Şifre Oluştur</h4>
										</div>

										<div class="col-md-6 col-12  mb30">
											<label for="">Eski şifre</label>
											<input name="pass" placeholder="Eski şifre" class="form-control" type="password">
										</div> 

										<div class="col-md-6 col-12  mb30">
											<label for="">Yeni Şifre</label>
											<input name="pass" placeholder="Yeni Şifre " class="form-control" type="password">
										</div> 

										<div class="col-lg-6 col-12  mb30">

											<div class="custom-control custom-checkbox">
												<input type="checkbox" name="bulten" class="custom-control-input" id="userbulten">
												<label class="custom-control-label" for="userbulten">Bülten aboneliğini onaylıyorum</label>
											</div>
										</div>

										<div class="col-12">
											<button class="btn btn-solid " type="button">Güncelle</button>
										</div>
									</div>
								</form>           
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- section end -->



	@endsection