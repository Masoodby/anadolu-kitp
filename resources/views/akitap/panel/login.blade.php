@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')


<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Üye Girişi</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Üye Girişi</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="login-page section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3 class="title">Giriş</h3>
				<div class="theme-card">

					<form class="theme-form" role="form" method="POST" action="{{ route('panel.login' )}}">
						{{ csrf_field()}}
						<div class="form-group">
							<label for="email">E-Posta Adresi</label>
							<input type="text" class="form-control req" name="email" placeholder="E-Posta Adresi" >
						</div>
						<div class="form-group">
							<label for="review">Şifre</label>
							<input type="password" class="form-control req" name="password" placeholder="Şifre" >
						</div>
						<button type="submit" class="btn btn-solid btn-solid-sm" >Giriş Yap</button>

					</form>
				</div>
			</div>
			<div class="col-lg-6 right-login">
				<h3 class="title">Yeni Üye</h3>
				<div class="theme-card authentication-right">
					<h6 class="title-font">Üye Kayıt</h6>
					<p>Eğer bir üyeliğiniz mevcut değilse veya sitemizde ilk defa alışveriş yapıyorsanız hemen üye olabilirsiniz.</p><a href="{{ route('panel.register') }}" class="btn btn-solid">Şimdi Kaydol</a><a href="{{ route('siparisi-tamamla') }}" class="btn btn-solid ml-5">Üye Olmadan Devam Et</a></div>
				</div>
			</div>
		</div>
	</section>
	<!--Section ends-->


	@endsection