@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')




<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Panelim</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="account-sidebar"><a class="popup-btn">İşlemlerim</a></div>
				<div class="dashboard-left">
					<div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> geri</span></div>
					<div class="block-content">
						@include(theme().'/panel.side')
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="dashboard-right">
					<div class="dashboard">
						<div class="page-title">
							<h2>{{ $title }}</h2></div>
							<div class="table-responsive text-center">
								<table class="table table-bordered">
									<thead class="thead-light">
										<tr>
											<th>No</th>
											<th>Sipariş No</th>
											<th>Tarih</th>
											<th>Durum</th>
											<th>Total</th>
											<th>İşlem</th>
										</tr>

									</thead>


									<tr>
										<th>#</th>
										<th>#</th>
										<th>#</th>
										<th>#</th>
										<th>#</th>
										<th>#</th>
									</tr>


									<tbody>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- section end -->



	@endsection