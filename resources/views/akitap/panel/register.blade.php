@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')


<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Yeni Üye Kayıt</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Yeni Üye Kayıt</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->

<section class="register-page section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="title pt-0">Üye Kayıt Formu  <a href="{{ route('panel.login') }}" class="float-right fsz12">[Üyeliğim Var]</a></h3>
				<div class="theme-card">
					@include(theme().'.layouts.theme.errors')
					<form class="form-horizontal" role="form" method="POST" id="user-register-form" action="{{ route('panel.register' )}}">
						{{ csrf_field()}}
						<div class="maske maske-cart"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>
						<div class="form-row">
							<div class="col-md-6">
								<label class="lh30 mt10 pl10" for="name">Ad</label>
								<input type="text" class="form-control req" id="name"  name="name" value="{{old('name')}}"  placeholder="İsim">
							</div>
							<div class="col-md-6">
								<label class="lh30 mt10 pl10" for="sname">Soyad</label>
								<input type="text" class="form-control req" id="sname"name="sname" value="{{old('sname')}}" placeholder="Soy isim">
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-6">
								<label class="lh30 mt10 pl10" for="email">E-Posta Adresi</label>
								<input   class="form-control req" placeholder="E-Posta Adresi" type="email" name="email" value="{{old('email')}}" >
							</div>
							<div class="col-md-6">
								<label class="lh30 mt10 pl10" for="gsm">GSM</label>
								<input   class="form-control req int" placeholder="Telefon No Giriniz" type="text" name="gsm" value="{{old('gsm')}}" >
							</div>
						</div>  

						<div class="form-row">
							<div class="col-md-6">
								<label class="lh30 mt10 pl10" for="password">Şifre Oluştur</label>
								<input name="password" placeholder="Şifre Belirleyiniz" class="form-control req" type="password" value="">
							</div>
							<div class="col-md-6 pt40 pl20">
								<div class="custom-control custom-checkbox" id="accepting">
									<input type="checkbox" class="custom-control-input" id="acceptuser" checked >
									<label  class="custom-control-label pt5" for="acceptuser">Üyelik Sözleşmesini Kabul Ediyorum</label>
								</div>  

								<div class="custom-control custom-checkbox">
									<input type="checkbox" name="bulten" class="custom-control-input" id="userbulten"  >
									<label   class="custom-control-label pt5" for="userbulten">Bülten aboneliğini onaylıyorum</label>
								</div>
							</div>
						</div>


						<button class="btn btn-solid  float-right sbmt_btn" data-id="user-register-form" type="button">Kaydet</button>

					</form>




				</div>




			</div>
		</div>
	</div>
</div>
</section>
@endsection