		<ul>
			<li class="{{ $active =="index"?"active":"" }}"><a href="{{ route('panelim') }}">Panelim</a></li>
			<li class="{{ $active =="orders"?"active":"" }}"><a href="{{ route('panelim.siparisler') }}">Tüm Siparişler</a></li>
			<li class="{{ $active =="adres"?"active":"" }}"><a href="{{ route('panelim.adreslerim') }}">Adreslerim</a></li>
			<li class="{{ $active =="info"?"active":"" }}"><a href="{{ route('panelim.bilgilerim') }}">Destek</a></li>
			<li class="{{ $active =="hesap"?"active":"" }}"><a href="{{ route('panelim.hesap') }}">Hesap Ayarları</a></li>
			
			<li class="last">

				<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Çıkış Yap </a>
				<form id="logout-form" action="{{ route('panel.logout') }}" method="POST" style="display: none;">{{ csrf_field() }} </form>
			</li>


		</ul>