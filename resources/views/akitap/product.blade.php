@extends(theme().'.layouts.master')
@section('title', $product->product_title)
@section('description', $product->product_desc)
@section('keywords', $product->product_keyw)

@section('content')


@php
{{

	$link = route('urun',['categories_url'=>$category->categories_url,'product_url'=>$product->product_url]);

	$oran = yuzde($product->detail->product_old_price,$product->product_price);
	$id = $product->id;
	$name = $product->product_name;
	$price = $product->product_price;
	$oldprice = $product->detail->product_old_price;
}}
@endphp

<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2 class="text-capitalize">{{ ucwords_tr($name) }}</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}">Anasayfa</a></li>
						@foreach ($tree as $var)
						<li class="breadcrumb-item"><a href="{{ route('kategori',$var["categories_url"]) }}">{{ $var['categories_name'] }}</a></li>
						@endforeach

						<li class="breadcrumb-item active text-capitalize" aria-current="page">{{ ucwords_tr($name)  }}</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->




<!-- section start -->
<section>
	<div class="collection-wrapper">
		<div class="container">
			<div class="row">
				@include(theme().'.layouts.theme.product.img')
				<div class="col-lg-8 rtl-text">
					<div class="row">
						<div class="col-md-8 col-sm-12">
							<div class="product-right">
								<h2 class="text-capitalize">{{ ucwords_tr($name) }}</h2>
								<h4><del>{{ $oldprice }}TL</del><span>{{ $oran }} indirim</span></h4>
								<h3 >{{ $price }}TL</h3>
								@if (strlen($product->product_sub_name) > 3)

								<h4 class="text-capitalize">{{ ucwords_tr($product->product_sub_name) }}</h4>

								@endif
								<div class="product-description">

									<div class="border-product">
										<h6 class="product-title">{{ @$product->extend->getYayinci->yayinci_name }}</h6>
										<p>	Yazar : <b>{{ @$product->extend->getYazar->yazar_name }}</b></p>
										<p> ISBN : <b>{{ $product->product_stock_code }}</b></p>
									</div>
									<hr>

									<h6 class="product-title">Adet</h6>
									<div class="qty-box">
										<div class="input-group"><span class="input-group-prepend"><button type="button" class="btn quantity-left-minus" data-type="minus" data-field=""><i class="ti-angle-left"></i></button> </span>
											<input type="text" name="quantity" data-max="{{ $product->product_stok }}" class="form-control input-number qty" value="1"> <span class="input-group-prepend"><button type="button" class="btn quantity-right-plus" data-type="plus" data-field=""><i class="ti-angle-right"></i></button></span></div>
										</div>

									</div>
									<div class="product-buttons mt15"><button type="button" class="btn btn-solid addcart mr20" data-id="{{ $id }}">Sepete Ekle</button> <button data-id="{{ $id }}" type="button" class="btn btn-solid bg-dark  buynow">Hızlı Satın Al</button></div>

									<div class="border-product">
										<h6 class="product-title">Ürünü Paylaş</h6>
										<div class="product-icon">
											<ul class="product-social">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-instagram"></i></a></li>
												<li><a href="#"><i class="fa fa-rss"></i></a></li>
											</ul>
											<form class="d-inline-block">
												<button type="button" class="wishlist-btn addfavori" data-id="{{ $id }}"><i class="fa fa-heart"></i><span class="title-font">Favorilerime Ekle</span></button>
											</form>
										</div>
									</div>

								</div>
							</div>
							<div class="col-md-4 hidden-md-down">

								@if ($product->extend->extend_json !='')
								{{-- expr --}}
								
								<table class="table table-sm  d-none d-lg-block d-md-block">
									<thead>
										<tr>
											<th class="table-dark" colspan="2"><i class="fa fa-book mr10"></i>Kitap Detay</th>
										</tr>
									</thead>
									<tbody>

										@foreach (json_decode($product->extend->extend_json) as $key => $item)


										<tr>

											@if ($key == "cevirmen")


											<td>{{ str_replace(':', '', $item->attr) }}</td>
											<td><b>:</b> 
												@foreach ($item->value as $var)
												{{ $var }}
												@endforeach

											</td>

											@else

											<td>{{ str_replace(':', '', $item->attr) }}</td>
											<td><b>:</b>  {{ $key == "isbn"? $product->product_stock_code :$item->value }} </td>
											@endif

										</tr>


										@endforeach


									</tbody>
								</table>

								@endif
							</div>
						</div>



					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Section ends -->


	<!-- product-tab starts -->
	<section class="tab-product m-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
						<li class="nav-item"><a class="nav-link active" id="top-home-tab" data-toggle="tab" href="#top-home" role="tab" aria-selected="true">Detay</a>
							<div class="material-border"></div>
						</li>
						<li class="nav-item"><a class="nav-link" id="profile-top-tab" data-toggle="tab" href="#top-profile" role="tab" aria-selected="false">Ödeme Seçenekleri</a>
							<div class="material-border"></div>
						</li>
						<li class="nav-item"><a class="nav-link" id="contact-top-tab" data-toggle="tab" href="#top-contact" role="tab" aria-selected="false">Yorumlar</a>
							<div class="material-border"></div>
						</li>

					</ul>
					<div class="tab-content nav-material" id="top-tabContent">
						<div class="tab-pane fade show active" id="top-home" role="tabpanel" aria-labelledby="top-home-tab">
							{!! $product->product_content !!}
						</div>
						<div class="tab-pane fade" id="top-profile" role="tabpanel" aria-labelledby="profile-top-tab">
							<div class="single-product-tables">
								<table>
									<tbody>
										<tr>
											<td>Havale</td>
											<td><i class="fa fa-check"></i></td>
										</tr>
										<tr>
											<td>Kredi Kartı</td>
											<td><i class="fa fa-check"></i></td>
										</tr>
										<tr>
											<td>Kredi Kartı Taksit</td>
											<td><i class="fa fa-check"></i></td>
										</tr>
									</tbody>
								</table>

							</div>
						</div>

						<div class="tab-pane fade" id="top-contact" role="tabpanel" aria-labelledby="contact-top-tab">
							<p>Henüz yorum eklenmemiş</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- product-tab ends -->


	<!-- product section start -->
	<section class="section-b-space ratio_square product-related">
		<div class="container">
			<div class="row">
				<div class="col-12 product-related">
					<h2 class="title pt-0">İlgili Kitaplar</h2></div>
				</div>
				<div class="slide-6">
					@foreach ($related as $row)


					<div class="">
						@include(theme().'.layouts.theme.box.box3')
					</div>
					@endforeach
				</div>
			</div>
		</section>
		<!-- product section end -->

		@endsection


		@section('script')
		<script>

			$('.addcart').click(function() {

				id = $(this).data("id")
				qty = $('.qty').val()				

				$.ajax({
					type: 'POST',
					url: '/sepet/add',
					data: {id: id,qty:qty},
					success: function(msg){
						$('#cartmodalbody').html(msg)
						$('#cartModal').modal()

						loadsidecart()
					}
				});	


			});

			$('.addfavori').click(function() {

				id = $(this).data("id")


				$.ajax({
					type: 'POST',
					url: '/favori/add',
					data: {id: id},
					success: function(msg){
						sole('Sonuç',msg)
					}
				});	


			});



		</script>

		@endsection