@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-title">
					<h2>Siparişi Tamamla</h2>
				</div>
			</div>
			<div class="col-12">
				<nav aria-label="breadcrumb" class="theme-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a></li>
						<li class="breadcrumb-item active" aria-current="page">Siparişi Tamamla</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space">
	<div class="container">
		<div class="checkout-page">
			<div class="checkout-form">
				<form>
					<div class="row">
						<div class="col-lg-6 col-sm-12 col-xs-12">
							<div class="checkout-title">
								<h3>Teslimat Bilgileri</h3></div>
								<div class="row check-out">
									<div class="form-group col-md-6 col-sm-6 col-xs-12">
										<div class="field-label">Ad</div>
										<input type="text" name="order_name" value="" class="req" placeholder="">
									</div>
									<div class="form-group col-md-6 col-sm-6 col-xs-12">
										<div class="field-label">Soyad</div>
										<input type="text" name="order_sname" value="" class="req" placeholder="">
									</div>
									<div class="form-group col-md-6 col-sm-6 col-xs-12">
										<div class="field-label">Telefon</div>
										<input type="text" name="order_tel" value="" class="req int" placeholder="">
									</div>
									<div class="form-group col-md-6 col-sm-6 col-xs-12">
										<div class="field-label">E-Posta</div>
										<input type="text" name="field-name" value="" class="req isMail" placeholder="">
									</div>
									<div class="form-group col-md-6 col-sm-6 col-xs-12">
										<div class="field-label">İl</div>
										<select class="req">
											<option>India</option>
											<option>South Africa</option>
											<option>United State</option>
											<option>Australia</option>
										</select>
									</div>
									<div class="form-group col-md-6 col-sm-6 col-xs-12">
										<div class="field-label">İlçe</div>
										<select class="req">
											<option>India</option>
											<option>South Africa</option>
											<option>United State</option>
											<option>Australia</option>
										</select>
									</div>
									<div class="form-group col-md-12 col-sm-12 col-xs-12">
										<div class="field-label">Adres</div>
										<textarea type="text" name="field-name" value="" class="req" placeholder="Mahalle / Cadde / Sokak / Daire No"></textarea>
									</div>

									<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<input type="checkbox" name="shipping-option" id="account-option"> &ensp;
										<label for="account-option">Bu Bilgilerle beni üye olarak <b>kaydet Şifremi</b> E-Postama gönder</label>
									</div>
									<div class="form-group col-md-12 col-sm-12 col-xs-12">
										<div class="field-label">Sipariş Notu</div>
										<textarea type="text" name="field-name" value="" placeholder="Teslimatta veya Paketlemede belirtmek istedikleriniz varsa buraya yazabilirsiniz."></textarea> 
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-12 col-xs-12">
								<div class="checkout-details">
									<div class="order-box">
										<div class="title-box">
											<div>Ürünler <span>Toplam</span></div>
										</div>
										@foreach(Cart::content() as $item)

										<ul class="qty">
											<li>{{ $item->name }} × {{ $item->qty }} <span>{{ $item->price * $item->qty }} TL</span></li>
										</ul>
										@endforeach

										<ul class="sub-total">
											<li>Ara Toplam <span class="count">{{Cart::subtotal()}} TL</span></li>
											<li>Kargo Ücreti <span class="count">10 TL</span></li>
										</ul>
										<ul class="total">
											<li>Genel Toplam <span class="count">{{Cart::total()}} TL</span></li>
										</ul>
									</div>
									<div class="payment-box">
										<div class="upper-box">
											<div class="payment-options">
												<ul>
													<li>
														<div class="radio-option">
															<input type="radio" name="payment-group" id="payment-1" checked="checked">
															<label for="payment-1">Kredi Kartı/Banka Kartı<span class="small-text"></span></label>
														</div>
													</li>
													<li>
														<div class="radio-option">
															<input type="radio" name="payment-group" id="payment-2">
															<label for="payment-2">Banka Havale<span class="small-text"></span></label>
														</div>
													</li>
													
												</ul>
											</div>
										</div>
									</div>
									<div>
										<article>Adınıza düzenlenmiş mesafeli satış sözleşmesi ve E-Faturanız E-Posta adresinize gönderilecektir.</article>
									</div>
									<div class="term-block" id="accepting">
										<input type="checkbox" id="accept_terms2" checked="">
										<label for="accept_terms2">Satış sözleşmesini ve Üyelik Sözleşmesini Kabul ediyorum</label>
									</div>
									<div>
										<div class="text-right"><a href="#" class="btn-solid btn">Siparişi Tamamla</a></div>
									</div>

								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- section end -->

	@endsection