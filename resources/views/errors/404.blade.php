@extends(theme().'.layouts.master')

@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')
<div class="container mt30 mb30">
	<div class="jumbotron text-center">
		<h1>404</h1>
		<h2>Aradığınız sayfa bulunamadı!</h2>
		<a href="{{url('/')}}" class="btn btn-primary">Anasayfa'ya Dön</a>
	</div>
</div>

@endsection