@php
{{

	$link = route('urun',['categories_url'=>$row->get_category->categories_url,'product_url'=>$row->product_url]);
	//$res = res("product",jres($row->detail->product_image),"250x250");
	$res = ress($row->product_stock_code,$row->product_group_code,"250x250");
	$oran = yuzde($row->product_old_price,$row->product_price);
	$name = $row->product_name;
	$price = price($row->product_price);
}}
@endphp
<div class="product-card product-card--hidden-actions ">
	
	<div class="product-card__badges-list">
		@if ($row->detail->product_konum_new == 1)
		<div class="product-card__badge product-card__badge--new">Yeni</div>
		@endif
		
	</div>
	<div class="product-card__image product-image">
		<a href="{{ $link }}" class="product-image__body">
			<img class="product-image__img" src="{{ $res }}" alt="{{ $name }}">
		</a>
	</div>
	<div class="product-card__info">
		<div class="product-card__name">
			<a href="{{ $link }}">{{ $name }}</a>
		</div>

		<small ><i class="fa fa-circle site-color1"></i> <a href="{{ route('yayin-evleri.kitaplar',$row->extend->getYayinci->yayinci_url) }}" class="text-muted">{{ $row->extend->getYayinci->yayinci_name }}</a> <i class="fa fa-i-cursor site-color1"></i> <a href="{{ route('yazarlar.kitaplar',$row->extend->getYazar->yazar_url) }}" class="site-color2">{{ $row->extend->getYazar->yazar_name }}</a> </small>

		<ul class="product-card__features-list">
			<li>Yayınevi: <a href="{{ route('yayin-evleri.kitaplar',$row->extend->getYayinci->yayinci_url) }}">{{ $row->extend->getYayinci->yayinci_name }}</a></li>
			<li>Yazar : <a href="{{ route('yazarlar.kitaplar',$row->extend->getYazar->yazar_url) }}">{{ $row->extend->getYazar->yazar_name }}</a></li>
			<li>Kategori : <a href="{{ route('kategori',$row->get_category->categories_url) }}">{{ $row->get_category->categories_name }}</a></li>
			<li>Stok Durumu: Mevcut</li>
			<li>Hızlı Kargo: Evet</li>
		</ul>
	</div>
	<div class="product-card__actions">
		<div class="product-card__availability">
			Stok Durumu: <span class="text-success">Mevcut</span>
		</div>
		<div class="product-card__prices">
			{{ $price }}
		</div>
		<div class="product-card__buttons">
			<button class="btn btn-primary product-card__addtocart add-to-cart"  max="{{ $row->product_stok }}" data-stok="{{ $row->product_stok }}" data-id="{{ $row->id }}" type="button">Sepete Ekle</button>
			<button class="btn btn-secondary product-card__addtocart product-card__addtocart--list  add-to-cart"  max="{{ $row->product_stok }}" data-stok="{{ $row->product_stok }}" data-id="{{ $row->id }}" " type="button">Sepete Ekle</button>
			<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist add-to-wishlist"  data-id="{{ $row->id }}" type="button">
				<svg width="16px" height="16px">
					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#wishlist-16"></use>
				</svg>
				<span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
			</button>
			<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare add-to-compare"  data-id="{{ $row->id }}" type="button">
				<svg width="16px" height="16px">
					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#compare-16"></use>
				</svg>
				<span class="fake-svg-icon fake-svg-icon--compare-16"></span>
			</button>
		</div>
	</div>
</div>
