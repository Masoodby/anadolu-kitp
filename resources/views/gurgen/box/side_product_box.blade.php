 
@php
{{

	$link = route('urun',['categories_url'=>$row->get_category->categories_url,'product_url'=>$row->product_url]);
	//$res = res("product",jres($row->detail->product_image),"100x100");
	$res = ress($row->product_stock_code,$row->product_group_code,"250x250");
	$oran = yuzde($row->detail->product_old_price,$row->product_price);
	$name = $row->product_name;
	$price = price($row->product_price);


	
}}
@endphp
<div class="widget-products__item">
	<div class="widget-products__image">
		<div class="product-image">
			<a href="{{ $link }}" class="product-image__body">
				<img class="product-image__img" src="{{ $res }}" alt="">
			</a>
		</div>
	</div>
	<div class="widget-products__info">
		<div class="widget-products__name">
			<a href="{{ $link }}">{{ $name }}</a>
		</div>
		<div class="widget-products__prices">
			{{ $price }}
		</div>
	</div>
</div>

