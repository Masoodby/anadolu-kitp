  @php
  {{

  	$link = route('kitap-setleri', $row->combo_url);
  	//$res = res("product",jres($row->product_image),"250x250");
      $res =  res( 'combo',$row->combo_image,'250x250') ;
    // $oran = yuzde($row->product_old_price,$row->product_price);
    $name = $row->combo_name;
    $price = price($row->combo_price);

    // $disprice =price($row->product_old_price < $row->product_price?$row->product_price*1.25:$row->product_old_price);

    // {{ $product->detail->product_old_price < $product->product_price?$product->product_price*1.25:$product->detail->product_old_price}}
  }}

  @endphp

  <div class="product-card product-card--hidden-actions ">

  	<div class="product-card__badges-list">
  		{{-- @if ($row->product_konum_new == 1)
  		<div class="product-card__badge product-card__badge--new">Yeni</div>
  		@endif --}}

  	</div>
  	<div class="product-card__image product-image">
  		<a href="{{ $link }}" class="product-image__body">
  			<img class="product-image__img" src="{{ $res }}" alt="{{ $name }}">
  		</a>
  	</div>
  	<div class="product-card__info">
  		<div class="product-card__name">
  			<a href="{{ $link }}">{{ $name }}</a>
  		</div>

  		<small class="site-color2"><i class="fa fa-angle-right site-color1"></i> {{ $row->combo_desc }}   </small>

  		<ul class="product-card__features-list">

  			<li>Garanti: 24 Ay</li>
  			<li>Ürün Grubu: {{ $row->categories_name }}</li>
  			<li>Stok Durumu: Mevcut</li>
  			<li>Hızlı Kargo: Evet</li>
  		</ul>
  	</div>
  	<div class="product-card__actions">
  		<div class="product-card__availability">
  			Stok Durumu: <span class="text-success">Mevcut</span>
  		</div>
  		<div class="product-card__availability">
            Ürün sayısı : <span class="text-success">{{$row->products->count()}}</span>
  		</div>
  		<div class="product-card__prices">
            {{ $price }}
        </div>
  		<div class="product-card__buttons">
  			<button class="btn btn-primary product-card__addtocart add-to-cart"  max="{{ $row->id }}" data-stok="{{ $row->id}}" data-id="{{ $row->id }}" type="button">Sepete Ekle</button>
  			<button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Sepete Ekle</button>
  			<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist add-to-wishlist"  data-id="{{ $row->id }}" type="button">
  				<svg width="16px" height="16px">
  					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#wishlist-16"></use>
  				</svg>
  				<span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
  			</button>
  			<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare add-to-compare"  data-id="{{ $row->id }}" type="button">
  				<svg width="16px" height="16px">
  					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#compare-16"></use>
  				</svg>
  				<span class="fake-svg-icon fake-svg-icon--compare-16"></span>
  			</button>
  		</div>
  	</div>
  </div>
