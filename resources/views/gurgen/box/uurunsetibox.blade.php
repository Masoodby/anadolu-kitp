@php
{{

	$link = route('urun',['urun-seti',$row->combo_name]);
	//$res = res("product",jres($row->detail->product_image),"250x250");
	$res =  res( 'combo',$row->combo_image,'250x250') ;

	$name = $row->combo_name;
	$price = price($row->combo_price);
}}
@endphp
<div class="product-card product-card--hidden-actions ">

	<div class="product-card__badges-list">
		{{-- @if ($row->detail->product_konum_new == 1)
		<div class="product-card__badge product-card__badge--new">Yeni</div>
		@endif --}}

	</div>
	<div class="product-card__image product-image">
		<a href="{{ $link }}" class="product-image__body">
			<img class="product-image__img" src="{{ $res }}" alt="{{ $name }}">
		</a>
	</div>
	<div class="product-card__info">
        @foreach ($row->products as $item)


		<div class="product-card__name">
			<a href="{{ $link }}">{{ $name }}</a>
		</div>

		<small ><i class="fa fa-circle site-color1"></i> <a href="{{ route('yayin-evleri.kitaplar',$item->extend->getYayinci->yayinci_url) }}" class="text-muted">{{ $item->extend->getYayinci->yayinci_name }}</a> <i class="fa fa-i-cursor site-color1"></i> <a href="{{ route('yazarlar.kitaplar',$item->extend->getYazar->yazar_url) }}" class="site-color2">{{ $item->extend->getYazar->yazar_name }}</a> </small>

		<ul class="product-card__features-list">
			<li>Yayınevi: <a href="{{ route('yayin-evleri.kitaplar',$item->extend->getYayinci->yayinci_url) }}">{{ $item->extend->getYayinci->yayinci_name }}</a></li>
			<li>Yazar : <a href="{{ route('yazarlar.kitaplar',$item->extend->getYazar->yazar_url) }}">{{ $item->extend->getYazar->yazar_name }}</a></li>
			<li>Kategori : <a href="{{ route('kategori',$item->get_category->categories_url) }}">{{ $item->get_category->categories_name }}</a></li>
			<li>Stok Durumu: Mevcut</li>
			<li>Hızlı Kargo: Evet</li>
		</ul>
        @endforeach
	</div>
	<div class="product-card__actions">
		<div class="product-card__availability">
			Stok Durumu: <span class="text-success">Mevcut</span>
		</div>
		<div class="product-card__prices">
			{{ $price }}
		</div>
		<div class="product-card__buttons">
			<button class="btn btn-primary product-card__addtocart add-to-cart"  max="{{ $row->product_stok }}" data-stok="{{ $row->product_stok }}" data-id="{{ $row->id }}" type="button">Sepete Ekle</button>
			<button class="btn btn-secondary product-card__addtocart product-card__addtocart--list  add-to-cart"  max="{{ $row->product_stok }}" data-stok="{{ $row->product_stok }}" data-id="{{ $row->id }}" " type="button">Sepete Ekle</button>
			<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist add-to-wishlist"  data-id="{{ $row->id }}" type="button">
				<svg width="16px" height="16px">
					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#wishlist-16"></use>
				</svg>
				<span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
			</button>
			<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare add-to-compare"  data-id="{{ $row->id }}" type="button">
				<svg width="16px" height="16px">
					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#compare-16"></use>
				</svg>
				<span class="fake-svg-icon fake-svg-icon--compare-16"></span>
			</button>
		</div>
	</div>
</div>
