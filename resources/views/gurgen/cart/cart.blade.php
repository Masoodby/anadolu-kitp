@extends(theme().'.layouts.cart')
@section('title', "Alış Veriş Sepeti")
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">								
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>

						<li class="breadcrumb-item active" aria-current="page">Alışveriş Sepeti</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="cart block">
		<div class="container">
			<div class="maske maske-cart"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>
			<table class="cart__table cart-table">
				<thead class="cart-table__head">
					<tr class="cart-table__row">
						<th class="cart-table__column cart-table__column--product" colspan="2">Ürün</th>

						<th class="cart-table__column cart-table__column--price">Birim Fiyatı</th>
						<th class="cart-table__column cart-table__column--quantity">Adet</th>
						<th class="cart-table__column cart-table__column--total">Toplam</th>
						<th class="cart-table__column cart-table__column--remove"></th>
					</tr>
				</thead>
				<tbody class="cart-table__body">
					@foreach(Cart::content() as $item)
					<tr class="cart-table__row" id="tr_{{ $item->rowId }}">
						<td class="cart-table__column cart-table__column--image">
							<div class="product-image">
								<a href="{{ $item->options->url }}" class="product-image__body">
									<img class="product-image__img" src="{{ $item->options->image }}">
								</a>
							</div>
						</td>
						<td class="cart-table__column cart-table__column--product">
							<a href="{{ $item->options->url }}" class="cart-table__product-name">{{ $item->name }}</a>
							<ul class="cart-table__options">
								<li>Yazar: {{ $item->options->yazar }}</li>
								<li> <small class="site-color2">{{ $item->options->yayinci }}</small></li>

							</ul>
						</td>
						<td class="cart-table__column cart-table__column--price" data-title="Birim Fiyatı">{{ price($item->price) }}</td>
						<td class="cart-table__column cart-table__column--quantity" data-title="Adet">
							<div class="input-number">

								<input class="form-control input-number__input cart-qty" data-id="{{ $item->rowId }}" data-stok="{{  $item->options->stok  }}" type="number" min="1" max="{{ $item->options->stok }}" value="{{ $item->qty }}">

								<div class="input-number__add cart-plus" max="{{ $item->options->stok }}" ></div>
								<div class="input-number__sub cart-minus" ></div>
							</div>
						</td>
						<td class="cart-table__column cart-table__column--total rowtotal{{ $item->rowId }}" data-title="Toplam">{{ price($item->subtotal)  }}</td>
						<td class="cart-table__column cart-table__column--remove">
							<button type="button" class="btn btn-light btn-sm btn-svg-icon" onclick="delcart('{{ $item->rowId }}')" >
								<svg width="12px" height="12px">
									<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#cross-12"></use>
								</svg>
							</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div class="cart__actions">
				<form class="cart__coupon-form">
					<label for="input-coupon-code" class="sr-only">Kupon Kodu</label>
					<input type="text" class="form-control" id="input-coupon-code" placeholder="Kupon kodu">
					<button type="submit" class="btn btn-primary">Kodu Uygula</button>
				</form>
				<div class="cart__buttons">
					<a href="{{ route('anasayfa') }}" class="btn btn-secondary">Alışverişe Devam Et</a>
					<a href="{{ route('siparisi-tamamla') }}" class="btn btn-primary cart__update-button">Siparişi Tamamla</a>
				</div>
			</div>
			<div class="row justify-content-end pt-5">
				<div class="col-12 col-md-7 col-lg-6 col-xl-5">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Sepet Özeti</h3>
							<table class="cart__totals">
								<thead class="cart__totals-header">
									<tr>
										<th>Ara Toplam</th>
										<td><span id="cartsubtotal">{{  price(Cart::total()) }}</span><br><small class="text-muted">Kitap için KDV 0.00 (Sıfır)'dır.</small></td>
									</tr>
								</thead>
								<tbody class="cart__totals-body">

									<tr>
										<th>Kargo</th>
										<td id="cartkargo">
											{{ set('kargo_esik')>=Cart::total() ? price(set('kargo_cost')):"Ücretsiz Kargo" }}

										</td>
									</tr>

								</tbody>
								<tfoot class="cart__totals-footer">
									<tr>
										<th>Genel Toplam</th>
										<td id="grandtotal">{{ price((Cart::total()) + (set('kargo_esik')>=Cart::total() ? set('kargo_cost'):0)) }}</td>
									</tr>
								</tfoot>
							</table>
							<a class="btn btn-primary btn-xl btn-block cart__checkout-button" href="{{ route('siparisi-tamamla') }}">Siparişi Tamamla</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection



@section('script')
<script>





	$('.cart-plus').click(function() {

		sole('Hata',"En Fazla "+$(this).attr('max')+" adet alınabilir.")
	});

	$('.cart-qty').on( "change",function() {

		id = $(this).data('id')
		qty = $(this).val()
		max = parseInt($(this).attr('max'))

		if (qty <= max) {
			qtyedit(id,qty)
		}else{
			sole('Hata',"En Fazla "+max+" adet alınabilir.")
			$(this).val($(this).attr('max'))
		}



	});


	function qtyedit(id,qty) {

		maske('maske-cart','on')

		$.ajax({
			type: 'POST',
			url: '/sepet/edit',
			data: {id: id,qty:qty},
			success: function(obj){
				maske('maske-cart','off')


				$('.rowtotal'+id).html(obj.rowtotal)
				$('#cartsubtotal').html(obj.subtotal)
				$('#cartkargo').html(obj.kargo)
				$('#grandtotal').html(obj.grandtotal)


			}
		});	
	}





</script>
@endsection