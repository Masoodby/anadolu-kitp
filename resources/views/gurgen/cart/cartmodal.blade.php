 <table class="table table-image table-hover">
 	<thead>
 		<tr>
 			<th scope="col"></th>
 			<th scope="col">Ürün</th>
 			<th scope="col">Fiyat</th>
 			<th scope="col">Adet</th>
 			<th scope="col">Toplam</th>

 		</tr>
 	</thead>
 	<tbody>
 		<tr>
 			<td class="w-25">
 				<img src="{{  $item->options->image  }}" class="img-fluid img-thumbnail"  >
 			</td>
 			<td>{{ $item->name }}</td>
 			<td>{{ $item->price }} TL</td>
 			<td class="qty">{{ $item->qty }}</td>
 			<td>{{ $item->subtotal }} TL</td>

 		</tr>
 	</tbody>
 </table> 
 <div class="d-flex justify-content-end">
 	<h5>Sepet Toplamı: <span class="price text-success">{{Cart::total()}} TL</span></h5>
 </div>


 <script>
 	$('#top-cart-count').removeClass('d-none').text('{{ Cart::count() }}')
 </script>

 <hr>
 <div class="justify-content-end d-none d-md-block d-sm-block d-lg-block">
 	<a href="{{ route('sepet') }}" class="btn btn-primary btn-lg float-left ">Sepeti Düzenle</a>
 	<a href="{{ route('siparisi-tamamla') }}" class="btn btn-success float-right ml10 btn-lg ">Siparişi Tamamla</a>
 	<button type="button" class="btn btn-dark btn-lg float-right closemodal ">Alışverişe Devam Et</button>

 </div> 

 <div class="d-block d-sm-none">
 	<a href="{{ route('sepet') }}" class="btn btn-primary btn-lg  btn-block">Sepeti Düzenle</a>
 	<button class="btn btn-dark btn-lg btn-block closemodal">Alışverişe Devam Et</button>
 	<a href="{{ route('siparisi-tamamla') }}" class="btn btn-success btn-lg  btn-block">Siparişi Tamamla</a>
 </div>

 <script>
 	$('.closemodal').click(function() {
 		
 		$('#globalmodal').modal('hide')
 	});
 </script>