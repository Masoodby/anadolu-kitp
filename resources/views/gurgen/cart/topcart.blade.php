   @if ($count > 0)

   <div class="dropcart__products-list">

       @foreach($items as $item)

       <div class="dropcart__product">
        <div class="product-image dropcart__product-image">
            <a href="{{ $item->options->url }}" class="product-image__body">
                <img class="product-image__img" src="{{ $item->options->image }}" alt="">
            </a>
        </div>
        <div class="dropcart__product-info">
            <div class="dropcart__product-name"><a href="{{ $item->options->url }}">{{ $item->name }}</a></div>

            <div class="dropcart__product-meta">
                <span class="dropcart__product-quantity">{{ $item->qty }}</span> ×
                <span class="dropcart__product-price">{{ $item->price }} TL</span>
                <span class="dropcart__product-price"> = {{ $item->price*$item->qty }} TL</span>
            </div>
        </div>
        <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon" onclick="delcart('{{ $item->rowId }}')"  >
            <svg width="10px" height="10px">
                <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#cross-10"></use>
            </svg>
        </button>
    </div>

    @endforeach



</div>
<div class="dropcart__totals">
    <table>
        <tr>
            <th>Ara Toplam</th>
            <td>{{ price($total) }}</td>
        </tr>

        <tr>
            <th>Kargo</th>
            <td>{{set('kargo_esik')>=$total ? price(set('kargo_cost')):"Ücretsiz Kargo" }}</td>
        </tr>
        <tr>
            <th>Toplam</th>
            <td>{{ set('kargo_esik')>=$total ? price(set('kargo_cost')+$total):price($total) }} TL</td>
        </tr>
    </table>
</div>
<div class="dropcart__buttons d-block">
    <a class="btn btn-secondary btn-block" href="{{ route('sepet') }}">Sepeti Düzenle</a>
    <a class="btn btn-primary btn-block" href="{{ route('siparisi-tamamla') }}">Siparişi Tamamla</a>
</div>

<script>
    $('#top-cart-count').removeClass('d-none').text('{{ $count }}');
</script>

@endif
