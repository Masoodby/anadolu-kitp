@extends(theme().'.layouts.master')
@section('title', $category->categories_title)
@section('description', $category->categories_desc)
@section('keywords', $category->categories_keyw)

@section('content')
<!-- site__body -->
<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						@foreach ($tree as $var)
						<li class="breadcrumb-item">
							<a href="{{ route('kategori',$var["categories_url"]) }}">{{ $var['categories_name'] }}</a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						@endforeach
						
					</ol>
				</nav>
			</div>
			
		</div>
	</div>
	<div class="container">
		<div class="shop-layout shop-layout--sidebar--start">
			@include(theme().'.category.sidebar')
			<div class="shop-layout__content">
				<div class="block">
					<div class="products-view">
						@include(theme().'.category.filter')

						<div class="products-view__list products-list" data-layout="grid-3-sidebar" data-with-features="false" data-mobile-grid-columns="2">
							<div class="products-list__body">
								@foreach ($products as $row)

								
								<div class="products-list__item">
									@include(theme().'.box.box')
								</div>

								
								@endforeach
							</div>
						</div>
						<div class="products-view__pagination">
							
							{{ $products->withQueryString()->links() }}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- site__body / end -->


@endsection

@section('script')
<script>
	
	$('.selectfilter').change(function(event) {



		filterType  = $(this).data('filter')
		val  = $(this).val()


		repUrl(filterType,val)

	});


	$('.brand_filter').change(function() {

		var val = [];
		$('.brand_filter:checked').each(function(i){
			val[i] = $(this).val();
		});


		repUrl("brand", val.join("-"))

	});

	$('.unsetfilter').click(function() {

		par = $(this).data('par')
		filter = $(this).data('filter')


		if (par =="brand") {
			$('#brand'+filter).prop('checked', false)

			var val = [];
			$('.brand_filter:checked').each(function(i){
				val[i] = $(this).val();
			});


			repUrl("brand", val.join("-"))
		}


		if (par =="price") {

			var originalURL = window.location.href
			var alteredURL = removeParam("price", originalURL);

			location.href=alteredURL

		}
		
	});


	$('#filter_price').click(function() {

		minprice = $('#minprice').text().trim()
		maxprice = $('#maxprice').text().trim()

		val = minprice+'-'+maxprice

		repUrl("price", val)
		
	});

</script>
@endsection