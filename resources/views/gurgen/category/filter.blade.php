<div class="products-view__options">
	<div class="view-options view-options--offcanvas--mobile">
		<div class="view-options__filters-button">
			<button type="button" class="filters-button">
				<svg class="filters-button__icon" width="16px" height="16px">
					<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#filters-16"></use>
				</svg>
				<span class="filters-button__title">Filtrele</span>

			</button>
		</div>
		<div class="view-options__layout">
			<div class="layout-switcher">
				<div class="layout-switcher__list">
					<button data-layout="grid-3-sidebar" data-with-features="false" title="Grid" type="button" class="layout-switcher__button  layout-switcher__button--active ">
						<svg width="16px" height="16px">
							<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#layout-grid-16x16"></use>
						</svg>
					</button>
					<button data-layout="grid-3-sidebar" data-with-features="true" title="Grid With Features" type="button" class="layout-switcher__button ">
						<svg width="16px" height="16px">
							<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#layout-grid-with-details-16x16"></use>
						</svg>
					</button>
					<button data-layout="list" data-with-features="false" title="List" type="button" class="layout-switcher__button ">
						<svg width="16px" height="16px">
							<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#layout-list-16x16"></use>
						</svg>
					</button>
				</div>
			</div>
		</div>
		<div class="view-options__legend">{{ $products->total() }} adet Listelendi</div>
		<div class="view-options__divider"></div>
		<div class="view-options__control">
			<label for="">Sırala</label>
			<div>

				<select class="form-control selectfilter form-control-sm" id="" data-filter="orderby">
					<option value="id-desc">Varsayılan</option>

					<option value="product_name-asc" @if(request()->has('orderby') and request('orderby') == "product_name-asc") selected @endif>Ad Artan</option>
					<option value="product_name-desc" @if(request()->has('orderby') and request('orderby') == "product_name-desc") selected @endif>Ad Azalan</option>
					<option value="product_price-asc" @if(request()->has('orderby') and request('orderby') == "product_price-asc") selected @endif>Fiyat Artan</option>
					<option value="product_price-desc" @if(request()->has('orderby') and request('orderby') == "product_price-desc") selected @endif>Fiyat Azalan</option>
					<option value="product_stok-asc" @if(request()->has('orderby') and request('orderby') == "product_stok-asc") selected @endif>Stok Artan</option>
					<option value="product_stok-desc" @if(request()->has('orderby') and request('orderby') == "product_stok-desc") selected @endif>Stok Azalan</option>


				</select>



			</div>
		</div>
		<div class="view-options__control">
			<label for="">Göster</label>
			<div>
				<select class="form-control selectfilter form-control-sm"  data-filter="limit">
					<option value="30">30</option>
					@for ($i = 30; $i <121 ; $i+=30)
					<option value="{{ $i }}"  @if(request()->has('limit') and request('limit') == $i) selected @endif>{{ $i }}</option>

					@endfor
				</select>

			</div>
		</div>
	</div>
	@if (request()->hasAny(["brand","price"]))

	@if ( request('brand') !="")

	<div class="current-filter row p0 mt10 pl15 h30 ">
		@foreach (explode("-",request('brand')) as $var)

		<div class="form-group float-left mr5">
			<button data-filter="{{ $var }}"  data-par="brand" class="btn btn-secondary unsetfilter btn-sm pos-r pr30">{{ $filter['brand'][$var]['name'] }} <span class="rounded-pill bg-dark w20 h20 p0 text-center lh20 pos-a text-white right3 top5"><i class="fa fa-times"></i></span></button>
		</div>

		@endforeach

	</div>
	@endif


	@if ( request('price') !="")

	<div class="current-filter row p0 mt10 pl15 h30 ">


		<div class="form-group float-left mr5">
			<button data-filter=""  data-par="price" class="btn btn-secondary unsetfilter btn-sm pos-r pr30">
				{{ isset($filter['current_price'])?$filter['current_price'][0]:0 }} TL -
				{{ isset($filter['current_price'])?$filter['current_price'][1]:0 }} TL
				<span class="rounded-pill bg-dark w20 h20 p0 text-center lh20 pos-a text-white right3 top5"><i class="fa fa-times"></i></span></button>



			</div>


		</div>
		@endif



		@endif
	</div>