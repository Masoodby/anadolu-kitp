<div class="widget-products__item">
	<div class="widget-products__image">
		<div class="product-image">
			<a href="product.html" class="product-image__body">
				<img class="product-image__img" src="{{ asset('dist/front/'.theme()) }}/images/products/product-2.jpg" alt="">
			</a>
		</div>
	</div>
	<div class="widget-products__info">
		<div class="widget-products__name">
			<a href="product.html">Undefined Tool IRadix DPS3000SY 2700 Watts</a>
		</div>
		<div class="widget-products__prices">
			$1,019.00
		</div>
	</div>
</div>