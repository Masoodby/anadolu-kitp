<div class="shop-layout__sidebar">
	<div class="block block-sidebar block-sidebar--offcanvas--mobile">
		<div class="block-sidebar__backdrop"></div>
		<div class="block-sidebar__body">
			<div class="block-sidebar__header">
				<div class="block-sidebar__title">Filtrele</div>
				<button class="block-sidebar__close" type="button">
					<svg width="20px" height="20px">
						<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#cross-20"></use>
					</svg>
				</button>
			</div>
			<div class="block-sidebar__item">
				<div class="widget-filters widget widget-filters--offcanvas--mobile" data-collapse data-collapse-opened-class="filter--opened">
					<h4 class="widget-filters__title widget__title">Filtrele</h4>
					<div class="widget-filters__list">
						<div class="widget-filters__item">
							<div class="filter filter--opened" data-collapse-item>
								<button type="button" class="filter__title" data-collapse-trigger>
									Kategoriler
									<svg class="filter__arrow" width="12px" height="7px">
										<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
									</svg>
								</button>
								<div class="filter__body" data-collapse-content>
									<div class="filter__container">
										<div class="filter-categories">
											<ul class="filter-categories__list vertical-scroll pos-r" style="height: 500px; overflow: auto;">


												@foreach ($levelcat as $element)

												<li class="filter-categories__item filter-categories__item--child">
													<a href="{{ route('kategori',$element->categories_url) }}">{{ str_limit($element->categories_name,40) }}</a>
													<div class="filter-categories__counter">15</div>
												</li>


												@if ($element->id == $category->id)
												@foreach ($subcats as $cat)
												<li class="filter-categories__item filter-categories__item--current">
													<a href="{{ route('kategori',$cat->categories_url) }}">{{ str_limit($cat->categories_name,40)  }}</a>
													<div class="filter-categories__counter">21</div>
												</li>
												@endforeach
												@endif

												@endforeach





											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="widget-filters__item">
							<div class="filter filter--opened" data-collapse-item>
								<button type="button" class="filter__title" data-collapse-trigger>
									Fiyat Aralığı
									<svg class="filter__arrow" width="12px" height="7px">
										<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
									</svg>
								</button>
								<div class="filter__body" data-collapse-content>
									<div class="filter__container">
										<div class="filter-price" data-min="1" data-max="{{ floatval(max($filter['price'])) }}" 
										data-from="{{ isset($filter['current_price'])?$filter['current_price'][0]:floatval(min($filter['price'])) }}" 
										data-to="{{ isset($filter['current_price'])?$filter['current_price'][1]:floatval(max($filter['price'])) }}">
										<div class="filter-price__slider"></div>
										<div class="filter-price__title">Fiyat: <span class="filter-price__min-value" id="minprice"></span> TL – <span class="filter-price__max-value" id="maxprice"></span> TL</div>
									</div>
								</div>
								<hr>
								<div class="text-center">
									<button class="btn btn-primary btn-sm" id="filter_price">Filtrele</button>
									<button class="btn btn-secondary btn-sm unsetfilter" data-filter="0" data-par="price" >Sıfırla</button>

								</div>
							</div>
						</div>
					</div>
					<div class="widget-filters__item">
						<div class="filter filter--opened" data-collapse-item>
							<button type="button" class="filter__title" data-collapse-trigger>
								Yayınevleri
								<svg class="filter__arrow" width="12px" height="7px">
									<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
								</svg>
							</button>
							<div class="filter__body" data-collapse-content>
								<div class="filter__container">
									<div class="filter-list">
										<div class="filter-list__list vertical-scroll pos-r" style="height: 500px; overflow: auto;">


											@foreach ($filter['yayinci'] as $key => $var)

											<label class="filter-list__item ">
												<span class="filter-list__input input-check">
													<span class="input-check__body">
														<input class="input-check__input yayinevi_filter" type="checkbox" id="yayinevi{{ $var['slug'] }}"  value="{{ $var['slug'] }}" @if(request()->has('yayinevi') and in_array_string(request('yayinevi'),$var['slug']) == TRUE) checked @endif   >
														<span class="input-check__box"></span>
														<svg class="input-check__icon" width="9px" height="7px">
															<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
														</svg>
													</span>
												</span>
												<span class="filter-list__title">
													{{ $var['name']}}
												</span>
												<span class="filter-list__counter">{{ $filter['yayinci_adet'][$key] }}</span>
											</label>

											@endforeach





										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	

					<div class="widget-filters__item">
						<div class="filter filter--opened" data-collapse-item>
							<button type="button" class="filter__title" data-collapse-trigger>
								Yazarlar
								<svg class="filter__arrow" width="12px" height="7px">
									<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
								</svg>
							</button>
							<div class="filter__body" data-collapse-content>
								<div class="filter__container">
									<div class="filter-list">
										<div class="filter-list__list vertical-scroll pos-r" style="height: 500px; overflow: auto;">


											@foreach ($filter['yazar'] as $key => $var)

											<label class="filter-list__item ">
												<span class="filter-list__input input-check">
													<span class="input-check__body">
														<input class="input-check__input yayinevi_filter" type="checkbox" id="yazar{{ $var['slug'] }}"  value="{{ $var['slug'] }}" @if(request()->has('yazar') and in_array_string(request('yazar'),$var['slug']) == TRUE) checked @endif   >
														<span class="input-check__box"></span>
														<svg class="input-check__icon" width="9px" height="7px">
															<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
														</svg>
													</span>
												</span>
												<span class="filter-list__title">
													{{ $var['name']}}
												</span>
												<span class="filter-list__counter">{{ $filter['yazar_adet'][$key] }}</span>
											</label>

											@endforeach





										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

			</div>
		</div>
		@if (isset($filter['discount']) && count($filter['discount']) > 0)

		<div class="block-sidebar__item d-none d-lg-block">
			<div class="widget-products widget">
				<h4 class="widget__title fsz14">Kategorideki İndirimliler</h4>
				<div class="widget-products__list">

					@foreach ($filter['discount'] as $row)

					@include(theme().'/box.side_product_box')

					@endforeach


				</div>
			</div>
		</div>

		@endif

	</div>
</div>
</div>