@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						<li class="breadcrumb-item">
							<a href="">{{ $title }}</a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
					</ol>
				</nav>
			</div>
			
		</div>
	</div>
	<div class="container">
		
		<div class="col-12">
			<div class="block">
				<div class="products-view">
					@include(theme().'.category.filter')

					<div class="products-view__list products-list" data-layout="grid-4-full" data-with-features="false" data-mobile-grid-columns="2">
						<div class="products-list__body">
							
							@foreach ($products as $row)
							<div class="products-list__item">
								@include(theme().'.box.box')
							</div>
							@endforeach
							
							
						</div>
					</div>
					<div class="products-view__pagination">
						
						{{ $products->withQueryString()->links() }}

					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>


@endsection