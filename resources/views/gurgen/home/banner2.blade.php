  <div class="block block-banner">
    <div class="container">
        <div class="row">
            @foreach ($banners1 as $banner)
            <div class="col-lg-4 col-md-12">
                <a href="" class="block-banner__body">
                    <div class="block-banner__image block-banner__image--desktop" style="background-image: url('{{ res( 'banner',$banner->image) }}')"></div>
                    <div class="block-banner__image block-banner__image--mobile" style="background-image: url('{{ res( 'banner',$banner->image) }}')"></div>
                    <div class="block-banner__title d-none">{{$banner->title}} <br class="block-banner__mobile-br"> Anadolu Kitap</div>
                    <div class="block-banner__text d-none">{{$banner->text}}</div>
                    <div class="block-banner__button d-none">
                        <span class="btn btn-sm btn-primary">{{$banner->button}}</span>
                    </div>
                </a>
            </div>
            @endforeach
        </div>


    </div>
</div>
