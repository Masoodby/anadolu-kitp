<div class="block block-brands">
    <div class="container">
        <div class="block-brands__slider">
            <div class="owl-carousel">

                @foreach ($yayincilar as $var)

                <div class="block-brands__item">
                    <a href="{{ route('yayin-evleri.kitaplar',$var->yayinci_url) }}"><img src="{{ res("yayinci",$var->yayinci_logo,"100x100") }}" alt="{{ $var->yayinci_name }}"></a>
                </div>

                @endforeach


            </div>
        </div>
    </div>
</div>