<div class="block block-features block-features--layout--classic">
    <div class="container">
        <div class="block-features__list">
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ asset('dist/front/'.theme().'/images/sprite.svg#fi-free-delivery-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">Ücretsiz Kargo</div>
                    <div class="block-features__subtitle">{{ set('kargo_esik') }} TL ve Üzeri Siparişlerde</div>
                </div>
            </div>
            <div class="block-features__divider"></div>
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">

                        <use xlink:href="{{ asset('dist/front/'.theme().'/images/sprite.svg#fi-payment-security-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">3D Secure ile</div>
                    <div class="block-features__subtitle">Tüm Kartlara Taksit</div>
                </div>
            </div>
            <div class="block-features__divider"></div>
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ asset('dist/front/'.theme().'/images/sprite.svg#fi-24-hours-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">Müşteri Temsilcisi</div>
                    <div class="block-features__subtitle">Hafta içi : 9:00 - 18:00 </div>
                </div>
            </div>

        </div>
    </div>
</div>