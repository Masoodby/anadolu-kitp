@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<!-- .block-slideshow -->
	@include(theme().'.home.slider')

	<!-- .block-slideshow / end -->
	<!-- .block-features -->
	@include(theme().'.home.features')

	<!-- .block-features / end -->
	<!-- .block-products-carousel -->
	@include(theme().'.home.carousel')

	<!-- .block-products-carousel / end -->
	<!-- .block-banner -->
	@include(theme().'.home.banner')
	<!-- .block-banner / end -->
	<!-- .block-products -->
	@include(theme().'.home.bestseller')
	@include(theme().'.home.urunseti')

	<!-- .block-products / end -->
    <!-- .block-banner -->
	@include(theme().'.home.banner2')
	<!-- .block-banner / end -->
	<!-- .block-categories -->
	@include(theme().'.home.popular')

	<!-- .block-categories / end -->
    <!-- .block-banner -->
	@include(theme().'.home.banner3')
	<!-- .block-banner / end -->
	<!-- .block-products-carousel -->
	@include(theme().'.home.new')

	<!-- .block-products-carousel / end -->
    <!-- .block-banner -->
	@include(theme().'.home.banner4')
	<!-- .block-banner / end -->
	<!-- .block-posts -->
	@include(theme().'.home.latesnews')


	<!-- .block-posts / end -->
	<!-- .block-brands -->
	@include(theme().'.home.blockbrand')

	<!-- .block-brands / end -->
	<!-- .block-product-columns -->


	<!-- .block-product-columns / end -->
</div>


@endsection
