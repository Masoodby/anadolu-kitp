 <div class="block block-posts" data-layout="list" data-mobile-columns="1">
    <div class="container">
        <div class="block-header">
            <h3 class="block-header__title">Kitap İncelemeleri</h3>
            <div class="block-header__divider"></div>
            <div class="block-header__arrows-list">
                <button class="block-header__arrow block-header__arrow--left" type="button">
                    <svg width="7px" height="11px">
                        <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-left-7x11"></use>
                    </svg>
                </button>
                <button class="block-header__arrow block-header__arrow--right" type="button">
                    <svg width="7px" height="11px">
                        <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-7x11"></use>
                    </svg>
                </button>
            </div>
        </div>
        <div class="block-posts__slider">
            <div class="owl-carousel">

                @foreach ($blogs as $var)

                <div class="post-card  ">
                    <div class="post-card__image">
                        <a href="{{ route('blog',$var->page_url) }}">
                            <img src="{{ res("blog",$var->page_img,"240x160") }}" alt="">
                        </a>
                    </div>
                    <div class="post-card__info">
                        <div class="post-card__category">
                            <a href="{{ route('blog',$var->page_url) }}"> {{ $var->page_keyw }}</a>
                        </div>
                        <div class="post-card__name">
                            <a href="{{ route('blog',$var->page_url) }}">{{ $var->page_name }}</a>
                        </div>
                        <div class="post-card__date">{{ date("d.m.Y H:i",strtotime($var->created_at)) }}</div>
                        <div class="post-card__content">
                            {!! kisalt(strip_tags($var->page_content),100) !!}
                        </div>
                        <div class="post-card__read-more">
                            <a href="{{ route('blog',$var->page_url) }}" class="btn btn-secondary btn-sm">Devamı</a>
                        </div>
                    </div>
                </div>

                @endforeach


            </div>
        </div>
    </div>
</div>