 <div class="block block--highlighted block-categories block-categories--layout--classic">
    <div class="container">
        <div class="block-header">
            <h3 class="block-header__title">En Çok Okunan Kategoriler</h3>
            <div class="block-header__divider"></div>
        </div>
        <div class="block-categories__list">
            @php
            $cats = DB::table('categories')->where(["is_active"=>1,"is_top"=>1,"categories_main"=>0])->orderBy("rank","asc")->limit(6)->get();
            @endphp
            @foreach ($cats as $var)


            <div class="block-categories__item category-card category-card--layout--classic">
                <div class="category-card__body">

                    <div class="category-card__content">
                        <div class="category-card__name">
                            <a href="{{ route('kategori',$var->categories_url) }}">{{ $var->categories_name }}</a>
                        </div>


                        @php
                        $subcats = DB::table('categories')->where(["is_active"=>1,"is_top"=>1,"categories_main"=>$var->id])->orderBy("rank","asc")->limit(6)->get();
                        @endphp

                        <ul class="category-card__links">
                            @foreach ($subcats as $item)

                            <li><a href="{{ route('kategori',$item->categories_url) }}">{{ $item->categories_name }}</a></li>

                            @endforeach

                        </ul>
                        <div class="category-card__all">
                            <a href="{{ route('kategori',$var->categories_url) }}">Tümü</a>
                        </div>

                    </div>
                </div>
            </div>

            @endforeach


        </div>
    </div>
</div>