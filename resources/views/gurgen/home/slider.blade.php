<?php
use Illuminate\Support\Carbon; ?>
<div class="block-slideshow block-slideshow--layout--with-departments block">
    <div class="container">
        <div class="row col-container " style="margin-top: 15px">
            <div class="col-lg-3 d-none d-lg-block">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                    </ol>
                    {{-- <div class="carousel-inner">
                        @if(count($tslider))
                        @foreach ($tslider as $key => $item)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <a href="{{ url($item->url) }}">
                                    <img src="{{ ress($item->product->product_stock_code, $item->product->product_group_code) }}"
                                         class="d-block w-100" alt="" height="345px">
                                </a>
                                <div class="countdown mt-5 carousel-caption d-none d-md-block">
                                    <p class="">{{$item->title}}</p>
                                    <ul class="d-flex p-0">
                                        <li class="background-dark-blue"><span id="hours{{$item->id}}"></span><span class="string-countdown font-light">saat</span></li>
                                        <li class="background-dark-blue"><span id="minutes{{$item->id}}"></span><span class="string-countdown font-light">dakkika</span></li>
                                        <li class="background-dark-blue"><span id="seconds{{$item->id}}"></span><span class="string-countdown font-light">saniye</span></li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                      @else

                     @endif
                    </div> --}}
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-12 col-lg-8">
                @php
                    $i = 0;
                @endphp
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($slider as $key => $row)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <a href="{{ url($row->slider_link) }}">
                                    <img src="{{ res('slider', $row->slider_image) }}" class="d-block w-100" alt="" height="345px">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>




<style>
    .countdown ul li {
        list-style: none;
        margin-right: 6px;
        background: #0b2c6e;
        height: 54px;
        width: 55px;
        border-radius: 6px;
        text-align: center;
        padding: 8px 0;
        font-size: 20px;
        color: #fff;
        line-height: 1.2rem;
        box-shadow: 0px 1px 2px #566e8c;
    }

    .countdown .string-countdown {
        background: #0b2c6e !important;
        font-size: 12px !important;
        display: block;
    }

</style>

@section('script')
    @parent
    {{-- <script>
@if(count($tslider))
    @foreach($tslider as $item)
        $(document).ready(function() {
            //Countdown

                const second = 1000,
                    minute = second * 60,
                    hour = minute * 60,
                    day = hour * 24;
                let countDown = new Date('{{ $item->end_time }}').getTime(),
                    x = setInterval(function() {
                        let now = new Date().getTime(),
                            distance = countDown - now;

                        // document.getElementById('days').innerHTML = Math.floor(distance / (day)),
                        document.getElementById('hours{{ $item->id }}').innerHTML = Math.floor((distance % (day)) / (hour)),
                            document.getElementById('minutes{{ $item->id }}').innerHTML = Math.floor((distance % (hour)) / (
                                minute)),
                            document.getElementById('seconds{{ $item->id}}').innerHTML = Math.floor((distance % (minute)) /
                                second);
                    }, second)




        });
    @endforeach
@endif
    </script> --}}
@endsection



