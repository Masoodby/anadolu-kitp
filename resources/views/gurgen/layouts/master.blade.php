<!DOCTYPE html>
<html lang="tr" dir="ltr">

<head>
    @include(theme().'/layouts.theme.meta')
    @yield('meta')
    <title>@yield('title', config('app.name'))</title>



    @include(theme().'/layouts.theme.src')
<style>
    html {
        --scrollbarBG: #CFD8DC;
        --thumbBG: #3C70E5;
    }
    body::-webkit-scrollbar {
        width: 11px;
    }
    body {
        scrollbar-width: thin;
        scrollbar-color: var(--thumbBG) var(--scrollbarBG);
    }
    body::-webkit-scrollbar-track {
        background: var(--scrollbarBG);
    }
    body::-webkit-scrollbar-thumb {
        background-color: var(--thumbBG) ;
        border-radius: 6px;
        border: 3px solid var(--scrollbarBG);
    }
    #style-7::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
        border-radius: 10px;
    }

    #style-7::-webkit-scrollbar
    {
        width: 10px;
        background-color: #F5F5F5;
    }

    #style-7::-webkit-scrollbar-thumb
    {
        border-radius: 10px;
        background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122,153,217)),
        color-stop(0.72, rgb(73,125,189)),
        color-stop(0.86, rgb(28,58,148)));
    }
</style>


</head>

<body>
 @include('facebook-pixel::body')
 @yield("bodyust")
 <!-- site -->
 <div class="site">
    <!-- mobile site__header -->

    @include(theme().'/layouts.theme.header-mobile')

    <!-- mobile site__header / end -->
    <!-- desktop site__header -->
    @include(theme().'/layouts.theme.header')
    @include(theme().'/layouts.theme.alert')


    <!-- desktop site__header / end -->
    <!-- site__body -->


    @yield('content')
    <!-- site__body / end -->
    <!-- site__footer -->
    @include(theme().'/layouts.theme.footer')


    <!-- site__footer / end -->
</div>
<!-- site / end -->
<!-- quickview-modal -->
@include(theme().'/layouts.theme.mobilemenu')

<!-- photoswipe / end -->
<!-- js -->


@include(theme().'/layouts.theme.js')
 <script>
     $(document).ready(function () {
         if (!$.browser.webkit) {
             $('.wrapper').html('<p>Sorry! Non webkit users. :(</p>');
         }
     });
 </script>
@yield('bodyalt')
{!! set('body_end') !!}
</body>

</html>
