<!DOCTYPE html>
<html lang="tr" dir="ltr">

<head>
  @include(theme().'/layouts.theme.meta')

  <title>@yield('title', config('app.name'))</title>

  @include(theme().'/layouts.theme.src-cart')

</head>

<body>
 @include('facebook-pixel::body')
 <!-- site -->
 <div class="site">
  <!-- mobile site__header -->

  @include(theme().'/layouts.theme.header-cart')


  

  @yield('content')
  <!-- site__body / end -->
  <!-- site__footer -->
  @include(theme().'/layouts.theme.footer-cart')


  <!-- site__footer / end -->
</div>
<!-- site / end -->

@include(theme().'/layouts.theme.modal.global')

<!-- photoswipe / end -->
<!-- js -->


@include(theme().'/layouts.theme.js-cart')
{!! set('body_end') !!}
</body>

</html>