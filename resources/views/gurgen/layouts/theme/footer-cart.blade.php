<footer class="site__footer">
    <div class="site-footer">
        <div class="container">

            <div class="site-footer__bottom">
                <div class="site-footer__copyright">
                    <!-- copyright -->
                    <p><i class="fa fa-copyright" aria-hidden="true"></i>  {{ set('name') }}</p>
                    <!-- copyright / end -->
                </div>

            </div>
        </div>
        
    </div>
</footer>
