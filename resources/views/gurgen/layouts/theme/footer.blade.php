<footer class="site__footer">
    <div class="site-footer">
        <div class="container">
            <div class="site-footer__widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="site-footer__widget footer-contacts">
                            <h5 class="footer-contacts__title">İletişim</h5>
                            
                            <ul class="footer-contacts__contacts">
                                <li><i class="footer-contacts__icon fas fa-globe-americas"></i> {{ set('adres') }}</li>
                                <li><i class="footer-contacts__icon far fa-envelope"></i> {{ set('mail') }}</li>
                                <li><i class="footer-contacts__icon fas fa-mobile-alt"></i> {{ set('gsm') }}</li>
                                <li><i class="footer-contacts__icon far fa-clock"></i> {{ set('worktime') }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Kurumsal</h5>
                            <ul class="footer-links__list">
                                @foreach (staticPages(1) as $val)
                                <li class="footer-links__item"><a href="{{ route('kurumsal',$val->page_url) }}" class="footer-links__link">{{ $val->page_name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Yardım</h5>
                            <ul class="footer-links__list">
                                @foreach (staticPages(2) as $val)

                                <li class="footer-links__item"><a href="{{ route('yardim',$val->page_url) }}" class="footer-links__link">{{ $val->page_name }}</a></li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4">
                        <div class="site-footer__widget footer-newsletter">
                            <h5 class="footer-newsletter__title">Bülten</h5>
                            
                            <form action="" class="footer-newsletter__form">
                                <label class="sr-only" for="footer-newsletter-address">E-Posta Adresi..</label>
                                <input type="text" class="footer-newsletter__form-input form-control" id="footer-newsletter-address" placeholder="E-Posta Adresi..">
                                <button class="footer-newsletter__form-button btn btn-primary">Üyelik</button>
                            </form>
                            <div class="footer-newsletter__text footer-newsletter__text--social">
                               Sosyal medya hesaplarımızdan bizi takip edin
                           </div>
                           <!-- social-links -->
                           <div class="social-links footer-newsletter__social-links social-links--shape--circle">
                            <ul class="social-links__list">
                                @foreach (setAll(8) as $var)

                                <li class="social-links__item">
                                    <a class="social-links__link social-links__link--type--{{ $var->settings_key }}" target="_blank" href="{{ $var->settings_val }}" no-follow>
                                        <i class="fab fa-{{ $var->settings_key }}"></i>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <hr>
                        <a href="https://etbis.eticaret.gov.tr/sitedogrulama/5812382737516452" target="_blank">
                            <img class="img-thumbnail w100" src="{{ asset('uploads/content/etbis.jpg')}}" alt="">
                        </a>

                        <!-- social-links / end -->
                    </div>
                </div>
            </div>
        </div>
        <div class="site-footer__bottom">
            <div class="site-footer__copyright">
                <!-- copyright -->
                <p><i class="fa fa-copyright" aria-hidden="true"></i>  {{ set('name') }} </p>
                <!-- copyright / end -->
            </div>

        </div>
    </div>
    <div class="totop">
        <div class="totop__body">
            <div class="totop__start"></div>
            <div class="totop__container container"></div>
            <div class="totop__end">
                <button type="button" class="totop__button">
                    <svg width="13px" height="8px">
                        <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-up-13x8"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
</div>
</footer>
@include(theme().'/layouts.theme.modal.global')
@guest()
@include(theme().'.panel.forget')
@endguest