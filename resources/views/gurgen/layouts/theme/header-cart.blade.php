 <header class="site__header">
    <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
    <div class="mobile-header mobile-header--sticky" data-sticky-mode="pullToShow">
        <div class="mobile-header__panel">
            <div class="container">
                <div class="mobile-header__body">
                    <a class="mobile-header__menu-button tipsi site-color1" title="Anasayfa" href="{{ route('anasayfa') }}">
                      <i class="fa fa-home"></i>
                  </a>
                  <a class="mobile-header__logo" href="{{ route('anasayfa') }}">
                    <!-- mobile-logo -->
                    <img src="{{ res('content',set('mlogo'),false)}}" alt="{{ set("name") }}">
                    <!-- mobile-logo / end -->
                </a>


            </div>
        </div>
    </div>
</div>
</header>