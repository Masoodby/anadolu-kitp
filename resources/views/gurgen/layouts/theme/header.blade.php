<header class="site__header d-lg-block d-none">
    <div class="site-header">
        <!-- .topbar -->
        @include(theme().'/layouts.theme.header.topbar')

        <!-- .topbar / end -->
        @include(theme().'/layouts.theme.header.center')

        <div class="site-header__nav-panel">
            <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
            <div class="nav-panel nav-panel--sticky" data-sticky-mode="pullToShow">
                <div class="nav-panel__container container">
                    <div class="nav-panel__row">
                     
                        <!-- .departments -->

                        @include(theme().'/layouts.theme.header.departments')
                        <!-- .departments / end -->
                        
                        <!-- .nav-links -->
                        @include(theme().'/layouts.theme.header.navbar')

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>