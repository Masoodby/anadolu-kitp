 <div class="site-header__middle container">
    <div class="site-header__logo">
        <a href="{{ route('anasayfa') }}">
            <!-- logo -->
            <img src="{{ res('content',set('logo'),false)}}" alt="{{ set("name") }}">
            <!-- logo / end -->
        </a>
    </div>
    <div class="site-header__search">
        <div class="search search--location--header ">
            <div class="search__body">
                <form class="search__form" action="">

                    <input class="search__input" name="search" placeholder="Arama.." aria-label="Site search" type="text" autocomplete="off">
                    <button class="search__button search__button--type--submit" type="submit">
                        <svg width="20px" height="20px">
                            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#search-20"></use>
                        </svg>
                    </button>
                    <div class="search__border"></div>

                    <div id="hidden-result" class="pos-a top50 d-none left1 pre-scrollable w-100 p10 border border-info bg-white"></div>
                </form>
                <div class="search__suggestions suggestions suggestions--location--header"></div>
            </div>
        </div>
    </div>
    <div class="site-header__phone">
        <div class="site-header__phone-title">Müşteri Temsilcisi</div>
        <div class="site-header__phone-number">{{ set('gsm') }}</div>
    </div>
</div>

@section("script")
<script>
    $('.search__input').keyup(function(event) {

        val  = $(this).val()


        if (   val.length > 2) {

            $.ajax({
                url: '/ajaxSearch',
                type: 'POST',

                data: {q: val},
            })
            .done(function(data) {
                $('#hidden-result').html(data).removeClass('d-none');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            

        }
    });



/*
    if(event.keyCode == 13){

               params = encodeURIComponent(val);

               location.href="/arama?q="+params

           }

           */

       </script>
       @endsection