<div class="nav-panel__departments">
    {{--     <div class="departments <?php echo isset($slider)?"departments--open departments--fixed ":""; ?>" data-departments-fixed-by="<?php echo isset($slider)?".block-slideshow":""; ?>" >--}}
    {{--     mby--}}
         <div class="departments " data-departments-fixed-by="" >
            <!-- class="departments  departments--open departments--fixed " data-departments-fixed-by=".block-slideshow"-->
            <div class="departments__body">
                <div class="departments__links-wrapper scrollbar" id="style-7">
                    <div class="departments__submenus-container force-overflow"></div>
                    <ul class="departments__links">


                        @foreach (nav_cat() as $item)

                        @if ($item['is_active'] == 1)



                        @if ($item['child'])




                        <li class="departments__item">
                            <a class="departments__item-link" href="{{ route('kategori',$item['categories_url']) }}">
                                {{ $item['categories_name'] }}
                                <svg class="departments__item-arrow" width="6px" height="9px">
                                    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </a>
                            <div class="departments__submenu departments__submenu--type--menu">
                                <!-- .menu -->
                                <div class="menu menu--layout--classic ">
                                    <div class="menu__submenus-container"></div>
                                    <ul class="menu__list">
                                        @foreach ($item['child'] as $childItem)

                                        @if ($childItem['is_active'] == 1)
                                        @if ($childItem['child'])


                                        <li class="menu__item">
                                            <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                            <div class="menu__item-submenu-offset"></div>
                                            <a class="menu__item-link fsz12" href="{{ route('kategori',$childItem['categories_url']) }}">
                                                {{ str_limit($childItem['categories_name'],30) }}

                                                <svg class="departments__item-arrow" width="6px" height="9px">
                                                    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                </svg>

                                            </a>


                                            <div class="menu__submenu">
                                                <!-- .menu -->
                                                <div class="menu menu--layout--classic ">
                                                    <div class="menu__submenus-container"></div>
                                                    <ul class="menu__list">

                                                        @foreach ($childItem['child'] as $level_tree)

                                                        @if ($level_tree['is_active'] == 1)

                                                        <li class="menu__item">
                                                            <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                            <div class="menu__item-submenu-offset"></div>
                                                            <a class="menu__item-link" href="{{ route('kategori',$level_tree['categories_url']) }}">
                                                               {{ str_limit($level_tree['categories_name'],30) }}
                                                           </a>
                                                       </li>

                                                       @endif <!-- end is active -->

                                                       @endforeach

                                                   </ul>
                                               </div>
                                               <!-- .menu / end -->
                                           </div>



                                       </li>

                                       @else


                                       <li class="menu__item">
                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                        <div class="menu__item-submenu-offset"></div>
                                        <a class="menu__item-link fsz12" href="{{ route('kategori',$childItem['categories_url']) }}">
                                            {{ str_limit($childItem['categories_name'],30) }}

                                        </a>


                                    </li>


                                    @endif <!-- end is sub -->
                                    @endif <!-- end is active -->

                                    @endforeach

                                </ul>
                            </div>
                            <!-- .menu / end -->
                        </div>
                    </li>


                    @else


                    <li class="departments__item">
                        <a class="departments__item-link" href="{{ route('kategori',$item['categories_url']) }}">
                            {{ str_limit($item['categories_name'],30) }}

                        </a>

                    </li>



                    @endif <!-- end is sub -->

                    @endif <!-- end is active -->

                    @endforeach




                </ul>
            </div>
        </div>
        <button class="departments__button">
            <svg class="departments__button-icon" width="18px" height="14px">
                <use xlink:href="{{ asset('dist/front/'.theme().'/images/sprite.svg#menu-18x14') }}"></use>
            </svg>
            Kategoriler

        </button>
    </div>
    </div>
