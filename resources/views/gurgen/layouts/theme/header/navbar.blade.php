 <div class="nav-panel__nav-links nav-links">
    <ul class="nav-links__list">
        <li class="nav-links__item  nav-links__item--has-submenu ">
            <a class="nav-links__item-link" href="{{ route('yeni-kitaplar') }}">
                <div class="nav-links__item-body">
                    <i class="fa fa-book"></i>  Yeni Kitaplar

                </div>
            </a>

        </li>
        <li class="nav-links__item  nav-links__item--has-submenu ">
            <a class="nav-links__item-link" href="{{ route('en-cok-okunan-kitaplar') }}">
                <div class="nav-links__item-body">
                 <i class="fa fa-star" aria-hidden="true"></i>  En Çok Okunanlar
             </div>
         </a>

     </li>
     <li class="nav-links__item  nav-links__item--has-submenu ">
        <a class="nav-links__item-link" href="{{ route('yayin-evleri') }}">
            <div class="nav-links__item-body">
                <i class="fa fa-th" aria-hidden="true"></i>  YayınEvleri

            </div>
        </a>

    </li>       <li class="nav-links__item  nav-links__item--has-submenu ">
        <a class="nav-links__item-link" href="{{ route('yazarlar') }}">
            <div class="nav-links__item-body">
                <i class="fas fa-user-edit" aria-hidden="true"></i>  Yazarlar

            </div>
        </a>

    </li>

</ul>
</div>
<!-- .nav-links / end -->
<div class="nav-panel__indicators">
    <div class="indicator">
        <a href="" class="indicator__button">
            <span class="indicator__area">
                <svg width="20px" height="20px">
                    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#heart-20"></use>
                </svg>

                <span class="indicator__value d-none" id="top-wish-count">0</span>


            </span>
        </a>
    </div>
    <div class="indicator indicator--trigger--click">
        <a href="" class="indicator__button">
            <span class="indicator__area">
                <svg width="20px" height="20px">
                    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#cart-20"></use>
                </svg>

                <span class="indicator__value d-none" id="top-cart-count">0</span>


            </span>
        </a>
        <div class="indicator__dropdown">
            <!-- .dropcart -->
            <div class="dropcart dropcart--style--dropdown">
                <div class="dropcart__body" id="topcart-content">
                    <p class="p30">Sepet Boş</p>
                </div>
            </div>
            <!-- .dropcart / end -->
        </div>
    </div>
    <div class="indicator indicator--trigger--click">
        <a href="" class="indicator__button">
            <span class="indicator__area">
                <svg width="20px" height="20px">
                    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#person-20"></use>
                </svg>
            </span>
        </a>
        <div class="indicator__dropdown">
            <div class="account-menu">
             @guest


             <form class="account-menu__form">
                <div class="account-menu__form-title">Üye Girişi Yap</div>
                <div class="alert alert-warning d-none" id="top-notify"></div>

                <div class="form-group">
                    <label for="header-signin-email" class="sr-only">E-Posta Adresi</label>
                    <input name="email" type="email" class="form-control form-control-sm top_login" placeholder="E-Posta Adresi">
                </div>
                <div class="form-group">
                    <label for="header-signin-password" class="sr-only">Şifre</label>
                    <div class="account-menu__form-forgot">
                        <input name="password" type="password" class="form-control form-control-sm top_login" placeholder="Şifre">
                        <a href="javascript:;"  data-toggle="modal" data-target="#resetpasswordModal"  class="account-menu__form-forgot-link bg-dark text-white"><i class="fa fa-question"></i></a>
                    </div>
                </div>
                <div class="form-group account-menu__form-button">
                    <button type="button" class="btn btn-primary top-login-btn btn-sm">Giriş Yap</button>
                </div>
                <div class="account-menu__form-link"><a href="{{ route('panel.register') }}">Yeni Üyelik</a></div>
            </form>


            <div class="account-menu__divider"></div>
            @endguest


            @auth

            <a href="" class="account-menu__user">
                <div class="account-menu__user-avatar">
                    <img src="{{ userImage([90,90],auth()->user()->name,auth()->user()->sname) }}" alt="">
                </div>
                <div class="account-menu__user-info">
                    <div class="account-menu__user-name">{{ auth()->user()->full_name }}</div>
                    <div class="account-menu__user-email">{{ auth()->user()->email }}</div>
                </div>
            </a>
            <div class="account-menu__divider"></div>
            <ul class="account-menu__links">
                <li>  <a href="{{ route('panelim') }}">Panelim</a></li>
                <li>  <a href="{{ route('panelim.siparisler') }}">Tüm Siparişler</a></li>
                <li><a href="{{ route('panelim.adreslerim') }}">Adreslerim</a></li>
                <li><a href="{{ route('panelim.destek') }}">Destek</a></li>
                <li><a href="{{ route('panelim.editprofil') }}">Hesap Ayarı</a></li>





            </ul>
            <div class="account-menu__divider"></div>
            <ul class="account-menu__links">
                <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Çıkış</a></li>
            </ul>

            @endauth
        </div>
    </div>
</div>
</div>
