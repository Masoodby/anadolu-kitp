   <div class="site-header__topbar topbar">
    <div class="topbar__container container">
        <div class="topbar__row">            
            <div class="topbar__spring"></div>
            <div class="topbar__item">
                <div class="topbar-dropdown">
                    <button class="topbar-dropdown__btn" type="button">
                        Üye İşlemleri
                        <svg width="7px" height="5px">
                            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-7x5"></use>
                        </svg>
                    </button>
                    <div class="topbar-dropdown__body">
                        <!-- .menu -->
                        <div class="menu menu--layout--topbar ">
                            <div class="menu__submenus-container"></div>
                            <ul class="menu__list">

                                @guest
                                <li class="menu__item">
                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                    <div class="menu__item-submenu-offset"></div>
                                    <a class="menu__item-link" href="{{ route('panel.login' )}}">
                                        <i class="fa fa-user color-site w30"></i>Giriş Yap
                                    </a>
                                </li>
                                <li class="menu__item">
                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                    <div class="menu__item-submenu-offset"></div>
                                    <a class="menu__item-link" href="{{ route('panel.register') }}">
                                        <i class="fa fa-user-plus color-site w30"></i>Yeni Üye
                                    </a>
                                </li>


                                @endguest


                                @auth

                                <li class="menu__item">
                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                    <div class="menu__item-submenu-offset"></div>
                                    <a class="menu__item-link" href="{{ route('panelim' )}}">
                                        <i class="fa fa-server color-green w30"></i>Panelim
                                    </a>
                                </li>
                                <li class="menu__item">
                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                    <div class="menu__item-submenu-offset"></div>
                                    <a class="menu__item-link" href="#"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-times color-red w30"></i>Çıkış Yap
                                    </a>

                                    <form id="logout-form" action="{{ route('panel.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }} </form>
                                </li>


                                @endauth

                            </ul>



                        </div>
                        <!-- .menu / end -->
                    </div>
                </div>
            </div>
            <div class="topbar__item">
                <div class="topbar-dropdown">
                    <button class="topbar-dropdown__btn" type="button">
                        Kurumsal
                        <svg width="7px" height="5px">
                            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-7x5"></use>
                        </svg>
                    </button>
                    <div class="topbar-dropdown__body">
                        <!-- .menu -->
                        <div class="menu menu--layout--topbar ">
                            <div class="menu__submenus-container"></div>
                            <ul class="menu__list">

                                @foreach (staticPages(1) as $var)
                                {{-- expr --}}

                                <li class="menu__item">
                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                    <div class="menu__item-submenu-offset"></div>
                                    <a class="menu__item-link fsz12" href="{{ route('kurumsal',$var->page_url) }}">
                                        {{ $var->page_name }}
                                    </a>
                                </li>
                                @endforeach                                  
                            </ul>
                        </div>
                        <!-- .menu / end -->
                    </div>
                </div>
            </div>
            <div class="topbar__item">
                <div class="topbar-dropdown">
                    <button class="topbar-dropdown__btn" type="button">
                        Yardım
                        <svg width="7px" height="5px">
                            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-7x5"></use>
                        </svg>
                    </button>
                    <div class="topbar-dropdown__body">
                        <!-- .menu -->
                        <div class="menu menu--layout--topbar  menu--with-icons ">
                            <div class="menu__submenus-container"></div>
                            <ul class="menu__list">
                                @foreach (staticPages(2) as $var)

                                <li class="menu__item">
                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                    <div class="menu__item-submenu-offset"></div>
                                    <a class="menu__item-link fsz12 pl15" href="{{ route('yardim',$var->page_url) }}">
                                        {{ $var->page_name }}
                                    </a>
                                </li>

                                @endforeach


                            </ul>
                        </div>
                        <!-- .menu / end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>