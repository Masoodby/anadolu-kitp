    <script src="{{ asset('dist/front/'.theme().'/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/nouislider/nouislider.min.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/photoswipe/photoswipe.min.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/photoswipe/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/select2/js/select2.min.js') }}"></script>

    <script src="{{ asset('dist/front/default/js/perfect-scrollbar.min.js')}}"></script>
    <script src="{{ asset('dist/front/default/js/scrollable-custom.js')}}"></script>
    <script src="{{ asset('dist/front/default/js/jquery.maskedinput.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="{{ asset('dist/front/'.theme().'/js/number.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/js/main.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/js/header.js') }}"></script>
    <script src="{{ asset('dist/front/'.theme().'/vendor/svg4everybody/svg4everybody.min.js') }}"></script>
    <script src="{{ asset('dist/front/default/js/jquery.toast.min.js')}}"></script>
    <script src="{{ asset('dist/front/default/js/app.js') }}"></script>
    <script src="{{ asset('dist/front/default/js/cart.js') }}"></script>
    <script src="{{ asset('dist/front/default/js/combocart.js') }}"></script>
    <script>
        svg4everybody();
    </script>
    @yield('script')
