<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="@yield('description', config('app.name'))">
<meta name="keywords" content="@yield('keywords', config('app.name'))">
<meta name="author" content="@yield('author', "Eticarix")">