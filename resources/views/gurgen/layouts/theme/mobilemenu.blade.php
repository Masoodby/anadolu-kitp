 <div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content"></div>
    </div>
</div>
<!-- quickview-modal / end -->
<!-- mobilemenu -->
<div class="mobilemenu">
    <div class="mobilemenu__backdrop"></div>
    <div class="mobilemenu__body">
        @guest

        <div class="btn-group" role="group"  >
            <a href="{{ route('panel.login') }}" class="btn btn-info"><i class="fa fa-user"></i> Üye Girişi</a>
            <a href="{{ route('panel.register') }}" class="btn btn-warning"><i class="fa fa-user-plus"></i> Yeni Üye</a>
        </div>

        @endguest


        @auth
        <div class="btn-group" role="group"  >
            <a href="{{ route('panelim') }}" class="btn btn-info"><i class="fa fa-user-o"></i> Panelim</a>
            <button  onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-warning"><i class="fa fa-times"></i> Çıkış</button>
        </div>
        @endauth

        <div class="mobilemenu__header">
            <div class="mobilemenu__title">Ürün Gruplarımız</div>
            <button type="button" class="mobilemenu__close">
                <svg width="20px" height="20px">
                    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#cross-20"></use>
                </svg>
            </button>
        </div>
        <div class="mobilemenu__content">
            <ul class="mobile-links mobile-links--level--0" data-collapse data-collapse-opened-class="mobile-links__item--open">

               @foreach (nav_cat() as $item)

               @if ($item['child'])
               <li class="mobile-links__item" data-collapse-item>
                <div class="mobile-links__item-title">
                    <a href="#" class="mobile-links__item-link">{{ str_limit($item['categories_name'],30) }}</a>
                    <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                        <svg class="mobile-links__item-arrow" width="12px" height="7px">
                            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
                        </svg>
                    </button>
                </div>
                <div class="mobile-links__item-sub-links" data-collapse-content>
                    <ul class="mobile-links mobile-links--level--1">


                        @foreach ($item['child'] as $childItem)

                        @if ($childItem['child'])
                        <li class="mobile-links__item" data-collapse-item>
                            <div class="mobile-links__item-title">
                                <a href="#" class="mobile-links__item-link">{{ str_limit($childItem['categories_name'],30) }}</a>
                                <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                    <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                        <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
                                    </svg>
                                </button>
                            </div>

                            <div class="mobile-links__item-sub-links" data-collapse-content>
                                <ul class="mobile-links mobile-links--level--2">

                                 @foreach ($childItem['child'] as $level_tree)
                                 <li class="mobile-links__item" data-collapse-item>
                                    <div class="mobile-links__item-title">
                                        <a href="{{ route('kategori',$level_tree['categories_url']) }}" class="mobile-links__item-link">{{ str_limit($level_tree['categories_name'],30) }}</a>
                                    </div>
                                </li>
                                @endforeach

                            </ul>
                        </div>




                    </li>

                    @else
                    <li class="mobile-links__item" data-collapse-item>
                        <div class="mobile-links__item-title">
                            <a href="{{ route('kategori',$childItem['categories_url']) }}" class="mobile-links__item-link">{{ str_limit($childItem['categories_name'],30) }}</a>
                        </div>
                    </li>
                    @endif
                    @endforeach



                </ul>
            </div>
        </li>


        @else


        <li class="mobile-links__item">
          <div class="mobile-links__item-title">
            <a class="mobile-links__item-link" href="{{ route('kategori',$item['categories_url']) }}">
                {{ str_limit($item['categories_name'],30) }}

            </a>
        </div>

    </li>



    @endif



    @endforeach


    <li class="mobile-links__item bg-light">
      <div class="mobile-links__item-title">
        <a class="mobile-links__item-link" href="{{ route('yeni-kitaplar') }}">
          <b>Yeni Kitaplar</b>

      </a>
  </div>

</li>

<li class="mobile-links__item bg-light">
  <div class="mobile-links__item-title">
    <a class="mobile-links__item-link" href="{{ route('en-cok-okunan-kitaplar') }}">
       <b>En Çok Okunanlar</b>

   </a>
</div>
</li>
<li class="mobile-links__item bg-light">
  <div class="mobile-links__item-title">
    <a class="mobile-links__item-link" href="{{ route('yayin-evleri') }}">
       <b>YayınEvleri</b>

   </a>
</div>
</li>

<li class="mobile-links__item bg-light">
  <div class="mobile-links__item-title">
    <a class="mobile-links__item-link" href="{{ route('yazarlar') }}">
     <b>Yazarlar</b>

 </a>
</div>
</li>


<li class="mobile-links__item">
  <div class="mobile-links__item-title">
    <a class="mobile-links__item-link" href="#"> <b> Kurumsal</b> </a>

    <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
        <svg class="mobile-links__item-arrow" width="12px" height="7px">
            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
        </svg>
    </button>
</div>

<div class="mobile-links__item-sub-links" data-collapse-content>
    <ul class="mobile-links mobile-links--level--1">
        @foreach (staticPages(1) as $var)
        <li class="mobile-links__item" data-collapse-item>
            <div class="mobile-links__item-title">

                <a class="mobile-links__item-link" href="{{ route('kurumsal',$var->page_url) }}">
                    {{ $var->page_name }}
                </a>
            </div>
        </li>
        @endforeach
    </ul>
</div>



</li>


<li class="mobile-links__item">
  <div class="mobile-links__item-title">
    <a class="mobile-links__item-link" href="#"> <b> Yardım</b> </a>

    <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
        <svg class="mobile-links__item-arrow" width="12px" height="7px">
            <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-down-12x7"></use>
        </svg>
    </button>
</div>

<div class="mobile-links__item-sub-links" data-collapse-content>
    <ul class="mobile-links mobile-links--level--1">
        @foreach (staticPages(2) as $var)
        <li class="mobile-links__item" data-collapse-item>
            <div class="mobile-links__item-title">

                <a class="mobile-links__item-link" href="{{ route('kurumsal',$var->page_url) }}">
                    {{ $var->page_name }}
                </a>
            </div>
        </li>
        @endforeach
    </ul>
</div>



</li>



</ul>
</div>
</div>
</div>
<!-- mobilemenu / end -->
<!-- photoswipe -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Kapat (Esc)"></button>
                <!--<button class="pswp__button pswp__button&#45;&#45;share" title="Share"></button>-->
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>