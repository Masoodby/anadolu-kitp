
<link rel="icon" href="{{ res('content',set('favicon'),false)}}" type="image/x-icon"/>
<link rel="shortcut icon" href="{{ res('content',set('favicon'),false)}}" type="image/x-icon"/>
<!-- fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i">
<!-- css -->
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/vendor/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/vendor/owl-carousel/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/vendor/photoswipe/photoswipe.css') }}">
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/vendor/photoswipe/default-skin/default-skin.css') }}">
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('dist/front/default/css/jquery.toast.min.css')}}">
<link rel="stylesheet" href="{{ asset('dist/front/default/css/scrollable.css')}}">
<!-- font - fontawesome -->
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/vendor/fontawesome/css/all.min.css') }}">
<!-- font - stroyka -->
<link rel="stylesheet" href="{{ asset('dist/front/'.theme().'/fonts/stroyka/stroyka.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/front/'.theme().'/css/mp.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">


{!! set('f_verification_code') !!}
{!! set('g_analytics_code') !!}
{!! set('g_verification_code') !!}


<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '138374054887990');
		fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=138374054887990&ev=PageView&noscript=1"
		/>
	</noscript>

	@yield("headclose")