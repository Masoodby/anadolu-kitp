@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }};"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">								
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						<li class="breadcrumb-item">
							<a href="">{{  $title }} </a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						<li class="breadcrumb-item active" aria-current="page">{{  $page->page_name }} </li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-3 d-flex">
					<div class="account-nav flex-grow-1">
						<h4 class="account-nav__title">{{ $title }} </h4>
						<ul>
							@foreach ($pages as $var)

							<li class="account-nav__item  {{ $var->id == $page->id?"account-nav__item--active":"" }}  " ><a href="{{ route(str_slug($title),$var->page_url) }}">{{  $var->page_name }} </a></li> 

							@endforeach


						</ul>
					</div>
				</div>
				<div class="col-12 col-lg-9 mt-4 mt-lg-0">
					<div class="card">
						<div class="card-header">
							<h1>{{  $page->page_name }} </h1>
						</div>
						<div class="card-divider"></div>
						<div class="card-body">
							<div class="row no-gutters">
								{!! $page->page_content !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection