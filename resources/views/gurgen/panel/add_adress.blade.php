	<form action="#" method="post" id="adres-form">

		<div class="row">

			<div class="col-12 col-lg-12 col-xl-12">


				<div class="form-group">
					<label for="adres_title">Adres Tanım</label>
					<input type="text" class="form-control req" name="adres_title" id="adres_title" placeholder="Örnek: Evim, İş yeri, vb." >
				</div>

				<div class="form-group">
					<label for="adres_name">İsim</label>
					<input type="text" class="form-control req" name="adres_name" id="adres_name" value="{{ $user->name }}">
				</div>

				<div class="form-group">
					<label for="adres_name">Soyisim</label>
					<input type="text" class="form-control req" name="adres_sname" id="adres_sname" value="{{ $user->sname }}">
				</div>

				<div class="form-group">
					<label for="checkout-gsm">GSM  </label>
					<input type="text" class="form-control gsm req" id="checkout-gsm" name="adres_gsm" value="{{ $user->getDetail->user_gsm }}">
				</div>


				<div class="form-row">
					<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Teslimat Bilgileri</b>
					<div class="form-group col-md-6">

						<select id="city" name="adres_il" class="form-control form-control-select2 req">
							<option value="">İl Seçiniz</option>
							@foreach($cities as $city)
							<option value="{{ $city->city}}">{{ $city->city }}</option>
							@endforeach
						</select>
					</div>	

					<div class="form-group col-md-6" id="county_outer">

						<select id="county" name="adres_ilce" class="form-control form-control-select2 req">
							<option value="">---------</option>

						</select>
					</div>
				</div>
				<div class="form-group">
					<label >Açık Adres</label>
					<textarea name="adres_detay" class="form-control req" rows="4" placeholder="Mahalle/Cadde/Sokak/Bina/Daire"></textarea>
				</div>

				<div class="form-group">
					<div class="form-check">
						<span class="form-check-input input-check">
							<span class="input-check__body">
								<input class="input-check__input" type="checkbox" id="fatura-info-onay" name="fatura-info-onay">
								<span class="input-check__box"></span>
								<svg class="input-check__icon" width="9px" height="7px">
									<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
								</svg>
							</span>
						</span>
						<label class="form-check-label" for="fatura-info-onay">Fatura Adresimide Ekle</label>
					</div>
				</div>



				<div class="fatura-panel d-none">
					<div class="form-row mb20">

						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<label class="btn btn-secondary active">
								<input type="radio" name="fatura_tur" id="payment_type_1" checked value="0"> Bireysel
							</label>
							<label class="btn btn-secondary">
								<input type="radio" name="fatura_tur" id="payment_type_2" value="1"> Kurumsal
							</label>

						</div>
						<hr>
					</div>


					<div class="form-row kurumsal d-none">
						<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Bilgileri</b>
						<div class="form-group col-md-12">
							<label for="checkout-first-name">Kurumsal Ünvan</label>
							<input type="text" class="form-control" name="fatura_unvan"   placeholder="Faturada kullanılacak ünvan...">
						</div>
						<div class="form-group col-md-6">
							<label for="checkout-last-name">Vergi Dairesi</label>
							<input type="text" class="form-control" name="fatura_vd" placeholder="Vergi Dairesi">
						</div>	

						<div class="form-group col-md-6">
							<label for="checkout-last-name">Vergi No</label>
							<input type="text" class="form-control int" name="fatura_vno"   placeholder="Vergi No">
						</div>
					</div>



					<div class="form-row bireysel">
						<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Bilgileri</b>

						<div class="form-group col-md-12">
							<label for="checkout-last-name">TCK No <small class="text-muted">(Zorunlu Değildir)</small></label>
							<input type="number" maxlength="11" class="form-control" name="fatura_tck" >
							<small class="p10 site-color2">Fatura Ünvanı  İsim ve Soyisim olarak kullanılacaktır.</small>
						</div>
					</div>



					<div class="form-group">
						<div class="form-check">
							<span class="form-check-input input-check">
								<span class="input-check__body">
									<input class="input-check__input" type="radio" name="equalask" value="0" id="adres_equal" checked>
									<span class="input-check__box"></span>
									<svg class="input-check__icon" width="9px" height="7px">
										<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
									</svg>
								</span>
							</span>
							<label class="form-check-label" for="adres_equal">Teslimat adresimla Fatura Adresim Aynı</label>
						</div>

						<div class="form-check">
							<span class="form-check-input input-check">
								<span class="input-check__body">
									<input class="input-check__input" type="radio" name="equalask" value="1" id="adres_differtn">
									<span class="input-check__box"></span>
									<svg class="input-check__icon" width="9px" height="7px">
										<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
									</svg>
								</span>
							</span>
							<label class="form-check-label" for="adres_differtn">Yeni Fatura Adresi Girmek İstiyorum</label>
						</div>
					</div>


					<div class="fatura-adres d-none">
						<div class="form-row">
							<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Adresi</b>
							<div class="form-group col-md-6">

								<select id="city_fat" name="fat_adres_il" class="form-control form-control-select2">
									<option value="">İl Seçiniz</option>
									@foreach($cities as $city)
									<option value="{{ $city->city}}">{{ $city->city }}</option>
									@endforeach
								</select>
							</div>	

							<div class="form-group col-md-6" id="county_outer">

								<select id="county_fat" name="fat_adres_ilce" class="form-control form-control-select2">
									<option value="">---------</option>

								</select>
							</div>
						</div>
						<div class="form-group">
							<label >Açık Adres</label>
							<textarea name="fat_adres_detay" class="form-control" rows="4" placeholder="Mahalle/Cadde/Sokak/Bina/Daire"></textarea>
						</div>
					</div>



				</div>


			</div>

			<button type="button" class="btn btn-primary add-adress-btn btn-xl btn-block">Adres Ekle</button>
		</div>


	</form>

	<script>

		$('#city').change(function(){
			if($(this).val() != ''){
				var select = $(this).attr("id");
				var value = $(this).val();

				$.ajax({
					url:"{{ route('get-counties') }}",
					method:"POST",
					data:{select:select, value:value},
					success:function(result){
						$('#county').html(result);
					}
				})
			}
		});

		$(function () {
			$('.form-control-select2').select2({width: ''});
		});


		$('#city_fat').change(function(){
			if($(this).val() != ''){
				var select = "city";
				var value = $(this).val();

				$.ajax({
					url:"{{ route('get-counties') }}",
					method:"POST",
					data:{select:select, value:value},
					success:function(result){
						$('#county_fat').html(result);
					}
				})
			}
		});



		$("#fatura-info-onay").click(function() {

			if ($(this).prop('checked')) {
				$('.fatura-panel').removeClass('d-none')
			}else{
				$('.fatura-panel').addClass('d-none')
			}

		});



		$('input[name="fatura_tur"]').click(function() {

			durum = $(this).val()

			if (durum == 1) {
				$(".bireysel").addClass('d-none')
				$(".kurumsal").removeClass('d-none')
			}else{
				$(".bireysel").removeClass('d-none')
				$(".kurumsal").addClass('d-none')
			}
			
		});

		$('input[name="equalask"]').click(function() {

			durum = $(this).val()

			if (durum == 1) {

				$(".fatura-adres").removeClass('d-none')
			}else{

				$(".fatura-adres").addClass('d-none')
			}
			
		});

		$(".add-adress-btn").click(function() {


			$('#adres-form .req').each(function(index, element) {

				var degert = $(this).val();

				if(degert == '' || degert == -1 ){

					$(this).css('border-color','red');

					$(this).focus();

					die();
				} else {

					$(this).css('border-color','green');

				}
			});

			

			$('#county_outer').css('border', '0px solid white').removeClass('p10');


			$(this).attr("disabled","disabled");

			$(this).css("opacity","0.6");

			$(this).html("Bekleyiniz");


			$.ajax({
				url:"{{ route('panelim.adres-ekle') }}",
				method:"POST",
				data:$('#adres-form').serialize(),
				success:function(){
					location.reload()
				}
			})



		});

		genx = $('body').width()
		if (genx>900) {
			$(".gsm").mask("(599) 999-9999");
		}


	</script>