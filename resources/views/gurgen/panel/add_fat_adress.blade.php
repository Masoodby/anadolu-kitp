	<form action="#" method="post" id="fat-adres-form">

		<div class="row">

			<div class="col-12 col-lg-12 col-xl-12">


				<div class="form-group">
					<label for="adres_title">Fatura Bilgi Tanımı</label>
					<input type="text" class="form-control req" name="adres_title" id="adres_title"   >
				</div>

				<div class="form-row mb20">

					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<label class="btn btn-secondary active">
							<input type="radio" name="fatura_tur" id="payment_type_1" checked value="0"> Bireysel
						</label>
						<label class="btn btn-secondary">
							<input type="radio" name="fatura_tur" id="payment_type_2" value="1"> Kurumsal
						</label>

					</div>
					<hr>
				</div>


				<div class="form-row kurumsal d-none">
					<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Bilgileri</b>
					<div class="form-group col-md-12">
						<label for="checkout-first-name">Kurumsal Ünvan</label>
						<input type="text" class="form-control kurumsal_input" name="fatura_unvan"   placeholder="Faturada kullanılacak ünvan...">
					</div>
					<div class="form-group col-md-6">
						<label for="checkout-last-name">Vergi Dairesi</label>
						<input type="text" class="form-control kurumsal_input" name="fatura_vd" placeholder="Vergi Dairesi">
					</div>	

					<div class="form-group col-md-6">
						<label for="checkout-last-name">Vergi No</label>
						<input type="text" class="form-control kurumsal_input int" name="fatura_vno"   placeholder="Vergi No">
					</div>
				</div>



				<div class="form-row bireysel">
					<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Bilgileri</b>


					<div class="form-group  col-md-12">
						<label for="adres_name">İsim</label>
						<input type="text" class="form-control bireysel_input" name="adres_name" id="adres_name" value="{{ $user->name }}">
					</div>

					<div class="form-group  col-md-12">
						<label for="adres_name">Soyisim</label>
						<input type="text" class="form-control bireysel_input" name="adres_sname" id="adres_sname" value="{{ $user->sname }}">
					</div>

					<div class="form-group col-md-12">
						<label for="checkout-last-name">TCK No</label>
						<input type="number" maxlength="11" class="form-control bireysel_input" name="fatura_tck" >
						<small class="p10 site-color2">Fatura Ünvanı  İsim ve Soyisim olarak kullanılacaktır.</small>
					</div>
				</div>

				<div class="form-row">

					<div class="form-group col-md-6">

						<select id="city" name="adres_il" class="form-control form-control-select2 req">
							<option value="">İl Seçiniz</option>
							@foreach($cities as $city)
							<option value="{{ $city->city}}">{{ $city->city }}</option>
							@endforeach
						</select>
					</div>	

					<div class="form-group col-md-6" id="county_outer">

						<select id="county" name="adres_ilce" class="form-control form-control-select2 req">
							<option value="">---------</option>

						</select>
					</div>
				</div>
				<div class="form-group">
					<label >Fatura Adresi</label>
					<textarea name="adres_detay" class="form-control req" rows="4" placeholder="Mahalle/Cadde/Sokak/Bina/Daire"></textarea>
				</div>

				<div class="form-group">
					<div class="form-check">
						<span class="form-check-input input-check">
							<span class="input-check__body">
								<input class="input-check__input" type="checkbox" id="fatura-e" name="efatura">
								<span class="input-check__box"></span>
								<svg class="input-check__icon" width="9px" height="7px">
									<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
								</svg>
							</span>
						</span>
						<label class="form-check-label" for="fatura-e">E-Fatura Mükellefiyim</label>
					</div>
				</div>



			</div>

			<button type="button" class="btn btn-primary fat-add-adress-btn btn-xl btn-block">Fatura Adresi Ekle</button>
		</div>


	</form>

	<script>

		$('#city').change(function(){
			if($(this).val() != ''){
				var select = $(this).attr("id");
				var value = $(this).val();

				$.ajax({
					url:"{{ route('get-counties') }}",
					method:"POST",
					data:{select:select, value:value},
					success:function(result){
						$('#county').html(result);
					}
				})
			}
		});


		$(function () {
			$('.form-control-select2').select2({width: ''});
		});





		$('input[name="fatura_tur"]').click(function() {

			durum = $(this).val()

			if (durum == 1) {
				$(".bireysel").addClass('d-none')
				$(".kurumsal").removeClass('d-none')
				$(".bireysel_input").removeClass('req')
				$(".kurumsal_input").addClass('req')
			}else{
				$(".bireysel").removeClass('d-none')
				$(".kurumsal").addClass('d-none')
				$(".bireysel_input").addClass('d-none')
				$(".kurumsal_input").removeClass('req')
			}
			
		});



		$(".fat-add-adress-btn").click(function() {


			$('#fat-adres-form .req').each(function(index, element) {

				var degert = $(this).val();

				if(degert == '' || degert == -1 ){

					$(this).css('border-color','red');

					$(this).focus();

					die();
				} else {

					$(this).css('border-color','green');

				}
			});



			if ($('#county').val() == "") {
				sole('Hata',"İlçe Seçmediniz")
				$('#county_outer').css('border', '1px solid red').addClass('p10');

			}else{

				$('#county_outer').css('border', '0px solid white').removeClass('p10');


				$(this).attr("disabled","disabled");

				$(this).css("opacity","0.6");

				$(this).html("Bekleyiniz");


				$.ajax({
					url:"{{ route('panelim.fat-adres-ekle') }}",
					method:"POST",
					data:$('#fat-adres-form').serialize(),
					success:function(){
						location.reload()
					}
				})

			}

		});

		genx = $('body').width()
		if (genx>900) {
			$(".gsm").mask("(599) 999-9999");
		}

	</script>