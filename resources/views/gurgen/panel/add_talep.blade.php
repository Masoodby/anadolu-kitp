	<form action="#" method="post" id="talep-form">

		<div class="row">

			<div class="col-12 col-lg-12 col-xl-12">


				<div class="form-group">
					<label for="subject">Konu</label>

					<select name="subject" class="form-control req" >
						<option value="">Bir Konu Seçiniz</option>
						<option value="Genel Bilgi">Genel Bilgi</option>
						<option value="Siparişim Hakkında">Siparişim Hakkında</option>
						<option value="Teslimat Hakkında">Teslimat Hakkında</option>
						<option value="iade veya iptal Talebi">İade veya iptal Talebi</option>
						<option value="Ürün Destek">Ürün Destek</option>
						<option value="Kargo Zayiatı">Kargo Zayiatı</option>
					</select>
				</div>


				<div class="form-group">
					<label for="subject">İlişkili Sipariş</label>

					<select name="subject" class="form-control" >
						<option value="0">Bir Sipariş Seçiniz</option>
						@foreach ($lastorders as $var)
						<option value="0">#{{ $var->id }} - {{ $var->baskets->basket_products[0]->product->product_name }}</option>
						@endforeach

					</select>
				</div>




				<div class="form-group">
					<label>Mesaj</label>
					<textarea name="message" class="form-control req" rows="4" placeholder="Mesaj yazınız..."></textarea>
				</div>



			</div>

			<button type="button" class="btn btn-primary  add-talep-btn btn-xl btn-block">Talep Ekle</button>
		</div>


	</form>

	<script>

		$(".add-talep-btn").click(function() {


			$('#talep-form .req').each(function(index, element) {

				var degert = $(this).val();

				if(degert == '' || degert == -1 ){

					$(this).css('border-color','red');

					$(this).focus();

					die();
				} else {

					$(this).css('border-color','green');

				}
			});

			

			$(this).attr("disabled","disabled");

			$(this).css("opacity","0.6");

			$(this).html("Bekleyiniz");


			$.ajax({
				url:"{{ route('panelim.talep-ekle') }}",
				method:"POST",
				data:$('#talep-form').serialize(),
				success:function(){
					location.reload()
				}
			})



		});
	</script>