@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				@include(theme().'.panel.side')

				<div class="col-12 col-lg-9 mt-4 mt-lg-0">


					<div class="addresses-list">
						<a href="javascript:;" class="addresses-list__item addresses-list__item--new add_delivery">
							<div class="addresses-list__plus"></div>
							<div class="btn btn-secondary btn-sm">Teslimat Adresi Ekle</div>
						</a>

						@foreach ($adresler as $row)


						@if ($row->adres_tur == 0)

						<div class="addresses-list__divider"></div>


						<div class="addresses-list__item card address-card">
							@if ($row->adres_current == 1)

							<div class="address-card__badge">Varsayılan</div>

							@endif

							<div class="address-card__body">
								<div class="address-card__name">{{ $row->adres_title }}</div>
								<div class="address-card__row">
									{{ $row->adres_name." " .$row->adres_sname }}<br>
									<small>
										{{ $row->adres_detay }}<br>
										{{ $row->adres_ilce }} - {{ $row->adres_il }}
									</small>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">İrtibat no</div>
									<div class="address-card__row-content">{{ $row->adres_gsm }}</div>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">E-Posta Adresi</div>
									<div class="address-card__row-content">{{  auth()->user()->email }}</div>
								</div>
								<div class="address-card__footer">
									<button type="button" class="btn btn-sm btn-secondary edit-adres" data-id="{{ $row->id }}">Düzenle</button>
									<button type="button" class="btn btn-sm btn-danger tipsi" onclick="allConfirm('eraseci',{{ $row->id }})"  title="Adresi Kaldır" data-id="{{ $row->id }}"><i class="fa fa-times"></i></button>

								</div>
							</div>
						</div>

						@endif

						@endforeach






						<div class="addresses-list__divider"></div>
					</div>

					<hr>


					<div class="addresses-list">
						<a href="javascript:;" class="addresses-list__item addresses-list__item--new add_invoice">
							<div class="addresses-list__plus"></div>
							<div class="btn btn-secondary btn-sm">Fatura Adresi Ekle</div>
						</a>




						@foreach ($adresler as $invoice)


						@if ($invoice->adres_tur == 1)

						<div class="addresses-list__divider"></div>


						<div class="addresses-list__item card address-card">
							@if ($row->adres_current == 1)

							<div class="address-card__badge">Varsayılan</div>

							@endif
							<div class="address-card__body">
								<div class="address-card__name">{{ $invoice->adres_title }}</div>
								<div class="address-card__row">

									@if ($invoice->fatura_tur == 0)

									Unvan :  {{ $invoice->full_name }}<br>
									<small>

										{{ $invoice->adres_detay }} 
										{{ $invoice->adres_ilce }} - {{ $invoice->adres_il }}</small>
										@else												

										Unvan : 	{{ $invoice->fatura_unvan }}<br>
										<small>
											{{ $invoice->adres_detay }} <br>
											{{ $invoice->adres_ilce }} - {{ $invoice->adres_il }}
										</small>
										@endif
									</div>
									@if ($invoice->fatura_tur == 1 )

									<div class="address-card__row">
										<div class="address-card__row-title">Fatura Bilgileri</div>
										<div class="address-card__row-content">	<small>V.D.</small> : <b>{{ $invoice->fatura_vd }} </b> - <small>V.No</small> : <b>{{ $invoice->fatura_vno }}</b></div>
									</div>
									@else
									<div class="address-card__row">
										<div class="address-card__row-title">Fatura Bilgileri</div>
										<div class="address-card__row-content">	 <b>TCK : {{ $invoice->fatura_tck }} </b>  </div>
									</div>
									@endif
									<div class="address-card__row">
										<div class="address-card__row-title">E-Posta Adresi</div>
										<div class="address-card__row-content">{{  auth()->user()->email }}</div>
									</div>
									<div class="address-card__footer">
										<button type="button" class="btn btn-sm btn-secondary edit-invoice" data-id="{{ $row->id }}">Düzenle</button>
										<button type="button" class="btn btn-sm btn-danger tipsi" onclick="allConfirm('eraseci',{{ $row->id }})"  title="Adresi Kaldır" data-id="{{ $row->id }}"><i class="fa fa-times"></i></button>
									</div>
								</div>
							</div>


							@endif

							@endforeach



						</div>






					</div>
				</div>
			</div>
		</div>
	</div>


	@endsection


	@section('script')
	<script src="{{ asset('dist/panel/js/bootbox.min.js')}}"></script>
	<script>


		function eraseci(id) {

			$.ajax({
				type: 'GET',
				url: '/panelim/erase-adres/' + id,
				success: function () {
					location.reload()
				} 

			});
		}





		$('.add_delivery').click(function() {

			$.ajax({
				url:"{{ route('panelim.adres-form') }}",
				method:"GET",				
				success:function(result){
					modalyap('globalmodal',"Teslimat Adresi Ekle",result)
				}
			})

		});




		$('.add_invoice').click(function() {

			$.ajax({
				url:"{{ route('panelim.fat-adres-form') }}",
				method:"GET",				
				success:function(result){
					modalyap('globalmodal',"Fatura Adresi Ekle",result)
				}
			})

		});




		$('.edit-adres').click(function() {

			id = $(this).data('id')

			$.ajax({
				url:"{{ route('panelim.adres-form') }}/",
				method:"GET",
				data:{id:id},				
				success:function(result){
					modalyap('globalmodal',"Adres Güncelle",result)
				}
			})

		});


		$('.edit-invoice').click(function() {

			id = $(this).data('id')

			$.ajax({
				url:"{{ route('panelim.fat-adres-form') }}/",
				method:"GET",
				data:{id:id},				
				success:function(result){
					modalyap('globalmodal',"Adres Güncelle",result)
				}
			})

		});



		


	</script>

	@endsection