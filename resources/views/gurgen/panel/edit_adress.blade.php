	<form action="#" method="post" id="edit-adres-form">

		<div class="row">

			<div class="col-12 col-lg-12 col-xl-12">


				<div class="form-group">
					<label for="adres_title">Adres Tanım</label>
					<input type="text" class="form-control req" name="adres_title" id="adres_title" value="{{ $adres->adres_title }}" >
				</div>

				<div class="form-group">
					<label for="adres_name">İsim</label>
					<input type="text" class="form-control req" name="adres_name" id="adres_name" value="{{ $adres->adres_name }}">
				</div>

				<div class="form-group">
					<label for="adres_name">Soyisim</label>
					<input type="text" class="form-control req" name="adres_sname" id="adres_sname" value="{{ $adres->adres_sname }}">
				</div>

				<div class="form-group">
					<label for="checkout-gsm">GSM  </label>
					<input type="text" class="form-control gsm req" id="checkout-gsm" name="adres_gsm" value="{{ $adres->adres_gsm }}">
				</div>


				<div class="form-row">
					<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Teslimat Bilgileri</b>
					<div class="form-group col-md-6">

						<select id="city" name="adres_il" class="form-control form-control-select2 req">
							<option value="">İl Seçiniz</option>
							@foreach($cities as $city)
							<option value="{{ $city->city}}" {{ $city->city == $adres->adres_il ?"selected":""  }}>{{ $city->city }}</option>
							@endforeach
						</select>
					</div>	

					<div class="form-group col-md-6" id="county_outer">

						<select id="county" name="adres_ilce" class="form-control form-control-select2 req">
							@foreach ($ilceler as $ilce)
							<option value="{{ $ilce->county }}" {{ $ilce->county == $adres->adres_ilce ?"selected":""  }}>{{ $ilce->county }}</option>
							@endforeach
							

						</select>
					</div>
				</div>
				<div class="form-group">
					<label >Açık Adres</label>
					<textarea name="adres_detay" class="form-control req" rows="4" >{{ $adres->adres_detay }}</textarea>
				</div>

				<div class="form-group">
					<div class="form-check">
						<span class="form-check-input input-check">
							<span class="input-check__body">
								<input class="input-check__input" type="checkbox" id="fatura-info-onay" name="fatura-info-onay" checked>
								<span class="input-check__box"></span>
								<svg class="input-check__icon" width="9px" height="7px">
									<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
								</svg>
							</span>
						</span>
						<label class="form-check-label" for="fatura-info-onay">Varsayılan Adresim Olsun</label>
					</div>
				</div>

			</div>

			<button type="button" class="btn btn-primary edit-adress-btn btn-xl btn-block" data-id="{{ $adres->id }}">Adres Güncelle</button>
		</div>


	</form>

	<script>

		$('#city').change(function(){
			if($(this).val() != ''){
				var select = $(this).attr("id");
				var value = $(this).val();

				$.ajax({
					url:"{{ route('get-counties') }}",
					method:"POST",
					data:{select:select, value:value},
					success:function(result){
						$('#county').html(result);
					}
				})
			}
		});


		$(function () {
			$('.form-control-select2').select2({width: ''});
		});




		$(".edit-adress-btn").click(function() {

			id = $(this).data("id"); 


			$('#edit-adres-form .req').each(function(index, element) {

				var degert = $(this).val();

				if(degert == '' || degert == -1 ){

					$(this).css('border-color','red');

					$(this).focus();

					die();
				} else {

					$(this).css('border-color','green');

				}
			});

			


			$(this).attr("disabled","disabled");

			$(this).css("opacity","0.6");

			$(this).html("Bekleyiniz");
			


			$.ajax({
				url:"{{ route('panelim.adres-edit') }}/"+id,
				method:"POST",
				data:$('#edit-adres-form').serialize(),
				success:function(){
					location.reload()
				}
			})



		});

		genx = $('body').width()
		if (genx>900) {
			$(".gsm").mask("(599) 999-9999");
		}


	</script>