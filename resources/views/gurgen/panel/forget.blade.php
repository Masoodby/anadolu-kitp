 <div class="modal fade " id="resetpasswordModal" tabindex="-1" role="dialog" aria-labelledby="resetpasswordModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resetpasswordModalTitle">Şifre Kurtarma</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="resetpasswordmodal-load">
        <form>
          <div class="form-row">
            <div id="hataRec" class="alert d-none"></div>
            <div class="form-group col-md-12">
              <label for="reset-email">E-Posta Adresi</label>
              <input type="email" class="form-control" id="reset-email-modal" placeholder="E-Posta Adresi">
            </div>

          </div>
          
          <hr>
          <button type="button" class="btn btn-primary recoveryPassBtn" id="reset-email-modal-btn" onclick="recoveryPass()">Şifre Talebi</button>
        </form>
      </div>

    </div>
  </div>
</div>
