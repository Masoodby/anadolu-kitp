@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
    <div class="container">
      <div class="row">
       @include(theme().'.panel.side')

       <div class="col-12 col-lg-9 mt-4 mt-lg-0">
        <div class="card">
         <div class="card-header">
           <h5>{{ $title }}</h5>
         </div>
         <div class="card-divider"></div>
         <div class="card-body">
          <div class="row no-gutters">
           <div class="col-12 col-lg-12 col-xl-12">
            <form action="#" id="editform" method="post" accept-charset="utf-8">

             <div id="result_1" class="d-none alert"></div>

             <div class="form-row">
              <div class="form-group col-md-6">
               <label for="">İsim</label>
               <input name="name" class="form-control req" placeholder="Ad" type="text" value="{{ auth()->user()->name }}">
             </div>
             <div class="form-group col-md-6">                                   
               <label for="">Soyisim</label>
               <input name="sname" class="form-control req" placeholder="Soyad" type="text" value="{{ auth()->user()->sname }}">
             </div>
           </div>

           <div class="form-row">
             <div class="form-group col-md-6">
              <label for="">E-Posta</label>
              <input name="email" class="form-control req" placeholder="E-Posta Adresi" type="email" value="{{ auth()->user()->email }}">
            </div>
            <div class="form-group col-md-6">
             <label for="">GSM</label>
             <input name="user_tel" class="form-control req int" placeholder="GSM" type="text" value="{{ $user_detail->user_tel }}">
           </div>
         </div>

         <p class="text-muted fsz20">Şifre Oluştur</p>

         <div class="form-row">
          <div class="form-group col-md-6">
           <label for="">Eski şifre</label>
           <input name="oldpass" placeholder="Eski şifre" class="form-control" type="password">

         </div>
         <div class="form-group col-md-6">
          <label for="">Yeni Şifre</label>
          <input name="pass" placeholder="Yeni Şifre " class="form-control" type="password">
        </div>
      </div>
      <div class="custom-control custom-checkbox">
       <input type="checkbox" name="bulten" class="custom-control-input" id="userbulten">
       <label class="custom-control-label" for="userbulten">Bülten aboneliğini onaylıyorum</label>
     </div>
     <div class="form-group mt-3 mb-0">
      <button class="btn btn-primary editinfobtn" type="button">Güncelle</button>
    </div>
  </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


@endsection

@section("script")
<script>

  $('.editinfobtn').click(function() {

    id = $(this).data('id')

    $.ajax({

      url:"/panelim/editProfilSave",

      data:$('#editform').serialize(),

      method:"PUT",       
      success:function(data){


        if(data.statu =="success"){
          $('#result_1').removeClass('d-none').addClass('alert-success').html(data.message)

        }else{
          $('#result_1').removeClass('d-none').addClass('alert-danger').html(data.message)
        }
      }
    })

  });

</script>
@endsection
