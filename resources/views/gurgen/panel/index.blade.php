@extends(theme().'.layouts.master')
@section('title', "Hoşgeldin ". $user->name)
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				@include(theme().'.panel.side')

				<div class="col-12 col-lg-9 mt-4 mt-lg-0">
					<div class="dashboard">
						<div class="dashboard__profile card profile-card">
							<div class="card-body profile-card__body">
								<div class="profile-card__avatar">
									<img src="{{ userImage([90,90],auth()->user()->name,auth()->user()->sname) }}" alt="">
								</div>
								<div class="profile-card__name">{{ $user->full_name }}</div>
								<div class="profile-card__email">{{ $user->email }}</div>
								<div class="profile-card__edit">
									<a href="{{ route('panelim.editprofil') }}" class="btn btn-secondary btn-sm">Bilgilerimi Güncelle</a>
								</div>
							</div>
						</div>

						@if ($adres)

						<div class="dashboard__address card address-card address-card--featured">
							<div class="address-card__badge">Varsayılan Adresim</div>
							<div class="address-card__body">
								<div class="address-card__name">{{ $adres->full_name }}</div>
								<div class="address-card__row">
									<small>
										{{ $adres->adres_detay }}<br>
										{{ $adres->adres_ilce }} - {{ $adres->adres_il }}
									</small>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">İrtibat no</div>
									<div class="address-card__row-content">{{ $adres->adres_gsm }}</div>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">E-Posta Adresi</div>
									<div class="address-card__row-content">{{  auth()->user()->email }}</div>
								</div>
								<div class="address-card__footer">
									<a href="javascript:;" class="btn btn-sm btn-secondary edit-adres" data-id="{{ $adres->id }}">[Adresi Düzenle]</a>
								</div>
							</div>
						</div>

						@else
						<div class="dashboard__address card address-card address-card--featured">
							<div class="address-card__badge">Varsayılan Adres</div>
							<div class="address-card__body text-center pt70">
								<i class="fa fa-truck fsz50 site-color1"></i><hr>
								Varsayılan Adresiniz Bulunmamaktadır. Şimdi Adres Eklemek İstermisiniz.


								<div class="address-card__footer">
									<a href="javascript:;" class="btn btn-sm btn-secondary add_delivery" >Adres Ekle</a>
								</div>
							</div>
						</div>

						@endif

						<div class="dashboard__orders card">
							<div class="card-header">
								<h5>Son Siparişlerim</h5>
							</div>
							<div class="card-divider"></div>
							<div class="card-table">
								<div class="table-responsive-sm">
									<table>
										<thead>
											<tr>
												<th>Sipariş ID</th>
												<th>Sipariş Tarihi</th>
												<th>Sipariş Durumu</th>
												<th>Genel Total</th>
											</tr>
										</thead>
										<tbody>

											@foreach ($lastorders as $var)

											<tr class="order-detay cur-p" data-id="{{ $var->id }}">
												<td><a href="javascript:;" >#{{ $var->id }}</a></td>
												<td>{{ date('d.m.Y H:i',strtotime($var->created_at)) }}</td>
												<td>{{ order_durum($var->order_state) }}</td>
												<td>{{ price($var->grand_total)}}</td>
											</tr>

											@endforeach


										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection



@section('script')
<script>
	$('.order-detay').click(function() {

		id = $(this).data('id')
		$.ajax({
			url:"/panelim/siparis/"+id,
			method:"GET",				
			success:function(result){
				modalyap('globalmodal',"Sipariş No",result)
			}
		})
		
	});	


	$('.edit-adres').click(function() {

		id = $(this).data('id')

		$.ajax({
			url:"{{ route('panelim.adres-form') }}/",
			method:"GET",
			data:{id:id},				
			success:function(result){
				modalyap('globalmodal',"Adres Güncelle",result)
			}
		})

	});



</script>
@endsection

