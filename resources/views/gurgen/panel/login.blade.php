@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')
<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Üye Girişi</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">


		<div class="container">
			<div class="row">
				<div class="col-md-6 d-flex flex-column">
					<div class="card flex-grow-1 mb-md-0">
						<div class="card-body">

							@if (request('back')!==null)
							<a href="{{ route('hizli-satin-al') }}" class="btn btn-info btn-block mt-4 mb-4 d-block d-sm-none"><i class="fa fa-angle-double-right right-effect"></i> Üye Olmadan Devam Et</a>
							@endif



							<h3 class="card-title">Giriş</h3>
							<form role="form" method="POST" action="{{ route('panel.login' )}}" id="loginform">
								{{ csrf_field()}}

								@if(session()->has('mesaj'))

								<div class="alert alert-warning"> {{session('mesaj')}}</div>

								@endif

								<div class="form-group">
									<label for="email">E-Posta Adresi</label>
									<input type="email" class="form-control req" name="email"placeholder="E-Posta Adresi">
								</div>
								<div class="form-group pos-r">
									<label>Şifre</label>
									<input type="password" class="form-control req" name="password" placeholder="Şifre">

									<a href="javascript:;"   data-toggle="modal" data-target="#resetpasswordModal" class=" pos-a right5 border-dark bg-dark text-white w30 h30 lh30 p0 rounded-pill text-center top33"><i class="fa fa-question"></i></a>
									<input type="hidden" name="back" value="{{  request('back')!==null?request('back'):"panelim" }}">
								</div>
								
								<div class="form-group">
									<div class="form-check">
										<span class="form-check-input input-check">
											<span class="input-check__body">
												<input class="input-check__input" type="checkbox" name="remember" id="login-remember">
												<span class="input-check__box"></span>
												<svg class="input-check__icon" width="9px" height="7px">
													<use xlink:href="{{ asset('dist/front/'.theme().'/images/sprite.svg#check-9x7')}}"></use>
												</svg>
											</span>
										</span>
										<label class="form-check-label" for="login-remember">Beni Hatırla</label>
									</div>
								</div>

								<button type="button" class="btn submit_btn btn-primary mt-4" data-id="loginform">Giriş Yap</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6 d-flex flex-column mt-4 mt-md-0">
					<div class="card flex-grow-1 mb-0">
						<div class="card-body">
							<h3 class="card-title">Yeni Üye</h3>
							<form role="form" method="POST" action="{{ route('panel.register' )}}" id="registerform">
								{{ csrf_field()}}
								@include(theme().'/layouts.theme.errors')
								<div class="row">
									<div class="form-group col-md-6 col-xs-12">
										<label>Ad</label>
										<input type="text" class="form-control req" name="name" placeholder="İsim">
									</div>
									<div class="form-group col-md-6 col-xs-12">
										<label>Soyad</label>
										<input type="text" class="form-control req" name="sname" placeholder="Soyisim">
									</div>
								</div>
								<div class="form-group">
									<label>E-Posta Adresi</label>
									<input type="email" class="form-control req" name="email" placeholder="E-Posta Adresi">
								</div>
								<div class="row">
									<div class="form-group col-md-6 col-xs-12">
										<label>GSM</label>
										<input type="text" class="form-control req gsm" name="gsm" placeholder="Telefon">
									</div>


									<div class="form-group col-md-6 col-xs-12">
										<label>Şifre</label>
										<input type="password" class="form-control req" name="password" placeholder="Şifre">
									</div>

								</div>

								<input type="hidden" name="back" value="{{  request('back')!==null?request('back'):"panelim" }}">
								<button type="button" class="btn submit_btn btn-primary mt-4" data-id="registerform">Yeni Üye Kayıt</button>



								@if (request('back')!==null)
								<a href="{{ route('hizli-satin-al') }}" class="btn btn-info mt-4 d-none d-sm-block float-right"><i class="fa fa-angle-double-right right-effect"></i> Üye Olmadan Devam Et</a>
								@endif
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>



	</div>
</div>

@endsection