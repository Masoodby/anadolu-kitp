 <div class="col-12 col-lg-12 mt-4 mt-lg-0">
    <div class="card">
        <div class="order-header">

            <h5 class="order-header__title">Sipariş ID  #{{ $order->id }}</h5>
            <div class="order-header__subtitle">
                Sipariş Tarihi <mark class="order-header__date">{{ date('d.m.Y H:i',strtotime($order->created_at)) }}</mark>
                Sipariş Durumu <mark class="order-header__status">{{ order_durum($order->order_state) }}</mark>.
            </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-table">
            <div class="table-responsive-sm">
                <table>
                    <thead>
                        <tr>
                            <th>Ürünler</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody class="card-table__body card-table__body--merge-rows">

                        @foreach ($order->baskets->basket_products as $var)

                        <tr>
                            <td>{{ $var->product->product_name." x ".$var->qty }}</td>
                            <td>{{ price($var->total_price*$var->qty) }}</td>
                        </tr>

                        @endforeach


                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Ara Toplam</th>
                            <td>{{ price($order->order_total) }}</td>
                        </tr>  
                        <tr>
                            <th>Kargo</th>
                            <td>{{ price($order->cargo_cost) }}</td>
                        </tr>  
                        <tr>
                            <th>Genel Toplam</th>
                            <td>{{ price($order->grand_total) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row mt-3 no-gutters mx-n2">
        <div class="col-sm-6 col-12 px-2">
            <div class="card address-card address-card--featured">
                <div class="address-card__body">
                    <div class="address-card__badge address-card__badge--muted">Teslimat Adresi</div>
                    <div class="address-card__name">{{ $order->address->full_name }}</div>
                    <div class="address-card__row">
                       {{ $order->address->adres_detay }}<br>
                       {{ $order->address->adres_ilce }} - {{ $order->address->adres_il }}

                   </div>
                   <div class="address-card__row">
                    <div class="address-card__row-title">İrtibat Tel</div>
                    <div class="address-card__row-content">{{ $order->address->adres_gsm }}</div>
                </div>
                <div class="address-card__row">
                    <div class="address-card__row-title">E-Posta Adresi</div>
                    <div class="address-card__row-content">{{ $user->email }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-12 px-2 mt-sm-0 mt-3">
        <div class="card address-card address-card--featured">
            <div class="address-card__body">
                <div class="address-card__badge address-card__badge--muted">Fatura Adresi</div>
                <div class="address-card__name">{{ $order->fatadres->fatura_tur == 0 ? $order->fatadres->adres_name.' '.$order->fatadres->adres_sname : $order->fatadres->fatura_unvan }}</div>
                <div class="address-card__row">
                    {{ $order->fatadres->adres_detay }}<br>
                    {{ $order->fatadres->adres_ilce }} - {{ $order->fatadres->adres_il }} 
                </div>
                <div class="address-card__row">
                    <div class="address-card__row-title">Fatura Bilgileri</div>
                    @if ( $order->fatadres->fatura_tur == 0 )

                    <div class="address-card__row-content"><small>TCK No : {{ strlen($order->fatadres->fatura_tck)==11?$order->fatadres->fatura_tck:"11111111111" }} </small></div>
                    @else
                    <div class="address-card__row-content"><small>V.D. {{ $order->fatadres->fatura_vd }} V.No: {{ $order->fatadres->fatura_vno }} </small></div>
                    @endif
                </div>

                <div class="address-card__row">
                    <div class="address-card__row-title">E-Fatura Mükellefi</div>
                    <div class="address-card__row-content">{{ $order->fatadres->efatura == 0?"Hayır":"Evet" }}</div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>