@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				@include(theme().'.panel.side')

				<div class="col-12 col-lg-9 mt-4 mt-lg-0">
					<div class="card">
						<div class="card-header">
							<h5>Tüm Siparişlerim</h5>
						</div>
						<div class="card-divider"></div>
						<div class="card-table">
							<div class="table-responsive-sm">
								<table>
									<thead>
										<tr>
											<th>Sipariş ID</th>
											<th>Sipariş Tarihi</th>
											<th>Sipariş Durumu</th>
											<th>Genel Total</th>
										</tr>
									</thead>
									<tbody>

										@foreach ($orders as $var)

										<tr class="order-detay cur-p" data-id="{{ $var->id }}">
											<td><a href="javascript:;" >#{{ $var->id }}</a></td>
											<td>{{ date('d.m.Y H:i',strtotime($var->created_at)) }}</td>
											<td>{{ order_durum($var->order_state) }}</td>
											<td>{{ price($var->grand_total)}}</td>
										</tr>

										@endforeach


									</tbody>
								</table>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

@section('script')
<script>
	$('.order-detay').click(function() {

		id = $(this).data('id')
		$.ajax({
			url:"/panelim/siparis/"+id,
			method:"GET",				
			success:function(result){
				modalyap('globalmodal',"Sipariş No",result)
			}
		})
		
	});	
</script>
@endsection