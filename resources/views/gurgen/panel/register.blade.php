@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')
<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Üye İşlemleri<svg class="breadcrumb-arrow" width="6px" height="9px">
							<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
						</svg></li>

						<li class="breadcrumb-item active" aria-current="page">Yeni Üye Kayıt</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="title pt-0">Üye Kayıt Formu  <a href="{{ route('panel.login' )}}" class="float-right fsz12">[Üyeliğim Var]</a></h3>
					<div class="theme-card">
						@include(theme().'.layouts.theme.errors')
						<form role="form" method="POST" action="{{ route('panel.register' )}}" id="registerform">
							{{ csrf_field()}}
							@include(theme().'/layouts.theme.errors')
							<div class="row">
								<div class="form-group col-md-6 col-xs-12">
									<label>Ad</label>
									<input type="text" class="form-control req" name="name" placeholder="İsim">
								</div>
								<div class="form-group col-md-6 col-xs-12">
									<label>Soyad</label>
									<input type="text" class="form-control req" name="sname" placeholder="Soyisim">
								</div>
							</div>
							<div class="form-group">
								<label>E-Posta Adresi</label>
								<input type="email" class="form-control req" name="email" placeholder="E-Posta Adresi">
							</div>
							<div class="row">
								<div class="form-group col-md-6 col-xs-12">
									<label>GSM</label>
									<input type="text" class="form-control req gsm" name="gsm" placeholder="Telefon">
								</div>


								<div class="form-group col-md-6 col-xs-12">
									<label>Şifre</label>
									<input type="password" class="form-control req" name="password" placeholder="Şifre">
								</div>

							</div>

							<input type="hidden" name="back" value="{{  request('back')!==null?request('back'):"panelim" }}">
							<button type="button" class="btn submit_btn btn-primary mt-4" data-id="registerform">Yeni Üye Kayıt</button>



							@if (request('back')!==null)
							<a href="{{ route('hizli-satin-al') }}" class="btn btn-info mt-4 d-none d-sm-block float-right"><i class="fa fa-angle-double-right right-effect"></i> Üye Olmadan Devam Et</a>
							@endif
						</form>

					</div>




				</div>
			</div>
		</div>
	</div>
</div>

@endsection