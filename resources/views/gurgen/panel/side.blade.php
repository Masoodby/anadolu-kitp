    <div class="col-12 col-lg-3 d-flex">
        <div class="account-nav flex-grow-1">
            <h4 class="account-nav__title">Panel</h4>
            <ul>
                <li class="account-nav__item {{ $active =="index"?"account-nav__item--active":"" }}">
                    <a href="{{ route('panelim') }}">Panelim</a>
                </li>
                <li class="account-nav__item {{ $active =="orders"?"account-nav__item--active":"" }}">
                    <a href="{{ route('panelim.siparisler') }}">Tüm Siparişler</a>
                </li>
                <li class="account-nav__item {{ $active =="adres"?"account-nav__item--active":"" }}">
                    <a href="{{ route('panelim.adreslerim') }}">Adreslerim</a>
                </li>
                <li class="account-nav__item {{ $active =="info"?"account-nav__item--active":"" }}">
                    <a href="{{ route('panelim.destek') }}">Destek</a>
                </li>
                <li class="account-nav__item {{ $active =="hesap"?"account-nav__item--active":"" }}">
                    <a href="{{ route('panelim.editprofil') }}">Hesap Ayarı</a>
                </li>
                
                <li class="account-nav__item ">
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Çıkış Yap </a>
                    <form id="logout-form" action="{{ route('panel.logout') }}" method="POST" style="display: none;">{{ csrf_field() }} </form>
                </li>
            </ul>
        </div>
    </div>