@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				@include(theme().'.panel.side')

				<div class="col-12 col-lg-9 mt-4 mt-lg-0">
					<div class="card">
						<div class="card-header">
							<h5 class="pt-2 float-left">{{ $title }}</h5>
							<a  href="javascript:;"><span class="float-right btn btn-primary add_talep">Talep Oluştur</span></a>

						</div>

						<div class="card-divider"> </div>

						@if (isset($taleplerim))
						


						<div class="card-table">
							<div class="table-responsive-sm">
								<table>
									<thead>
										<tr>
											<th>Tarihi</th>
											<th>Konusu</th>
											<th>İçeriği</th>
											<th>Durumu</th>
											<th>Detay</th>
										</tr>
									</thead>
									<tbody>

										@foreach ($taleplerim as $var)

										<tr>
											<td>{{ $var->id }}</td>
											<td>{{ $var->subject}}</td>
											<td>{{ $var->message}}</td>
											<td>{{ $var->message_state }}</td>
											<td><a href="{{ route('panelim.talep-detay',$var->id) }}"><button type="button" class="btn btn-sm btn-success">Detay</button></a></td>
										</tr>

										@endforeach


									</tbody>
								</table>
							</div>

						</div>
						@else
						{{-- expr --}}

						<div class="card-body">
							<div class="row no-gutters">
								<p>Henüz Destek Talebiniz yok.</p>
							</div>
							<a  href="javascript:;"><span class=" btn btn-primary add_talep">Talep Oluştur</span></a>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection


@section('script')

<script>
	
	$('.add_talep').click(function() {

		$.ajax({
			url:"{{ route('panelim.talep-form') }}",
			method:"GET",				
			success:function(result){
				modalyap('globalmodal',"Talep Ekle",result)
			}
		})

	});
</script>


@endsection
