@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						
						<li class="breadcrumb-item active" aria-current="page">Panelim</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row">
				@include(theme().'.panel.side')

				<div class="col-12 col-lg-9 mt-4 mt-lg-0">
					<div class="card">
						<div class="card-header">
							<h5 class="pt-2 float-left">{{ $title->subject }}</h5>

						</div>

						<div class="card-divider"> </div>
						<div class="chat">
							<div class="bubble me">Hellfhdfdgfcghdgfhdfgdgfdfdgo there!</div>
							<div class="bubble you">Hi. I'm an expandeable chat box with box shadow. How are you? I expand horizontally and vertically, as you can see here.</div>
							<div class="bubble me">Awesome.</div>



						</div>
						<form action="#" method="post" action="">
							{{ csrf_field() }}  

							<div class="row mt-3">

								<div class="col-12 col-lg-12 col-xl-12">



									<div class="form-group">
										<textarea name="message" class="form-control req" rows="3" placeholder="Cevap yazınız..."></textarea>
									</div>

									<button type="button" class="btn btn-primary  btn-xl btn-block">Gönder</button>

								</div>

							</div>


						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

