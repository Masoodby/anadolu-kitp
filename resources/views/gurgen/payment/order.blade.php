@extends(theme().'.layouts.order')
@section('title', "Siparişi Tamamla")
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header mt20">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">								
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>

						<li class="breadcrumb-item active" aria-current="page">Siparişi Tamamla</li>
						<li class="breadcrumb-item d-block d-sm-none"><a href="{{ url()->previous() }}" class="btn btn-primary btn-sm  ml90 float-right pos-r top-10 ">Geri</a> </li>
					</ol>
					<a href="{{ url()->previous() }}" class="btn btn-primary btn-sm d-none d-sm-block ml90 float-right pos-r top-20 ">Geri</a>
				</nav>
			</div>

		</div>
	</div>

	<div class="checkout block">
		<div class="container">
			<form action="{{route('save')}}" method="post" id="order-form">
				{{csrf_field()}}
				<div class="row">

					<div class="col-12 col-lg-6 col-xl-7">
						<div class="card mb-lg-0">
							<div class="card-body">
								<h3 class="card-title">Sipariş Formu</h3>


								<div class="addresses-list">


									@if ($delivery)

									
									<div class="addresses-list__item card address-card">
										<input type="hidden" name="order_adres_id" id="order_adres_id" value="{{ $delivery->id }}">
										<div class="address-card__badge">Teslimat Adresi</div>
										<div class="address-card__body">
											<div class="address-card__name">{{ $delivery->adres_title }}</div>
											<div class="address-card__row">
												{{ $delivery->adres_name." " .$delivery->adres_sname }}<br>
												<small>
													{{ $delivery->adres_detay }}<br>
													{{ $delivery->adres_ilce }} - {{ $delivery->adres_il }}
												</small>
											</div>
											<div class="address-card__row">
												<div class="address-card__row-title">İrtibat no</div>
												<div class="address-card__row-content">{{ $delivery->adres_gsm }}</div>
											</div>
											<div class="address-card__row">
												<div class="address-card__row-title">E-Posta Adresi</div>
												<div class="address-card__row-content">{{  auth()->user()->email }}</div>
											</div>
											<div class="address-card__footer">


												<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
													<button type="button" class="btn btn-secondary add_delivery">Yeni Ekle</button>

													@if ($all_adress)
													<div class="btn-group" role="group">
														<button id="btnGroupDrop1" type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															Değiştir
														</button>
														<div class="dropdown-menu dropdown-menu-right">
															@foreach ($all_adress as $var)
															@if ($var->adres_tur == 0)	
															<a class="dropdown-item adres_sec" data-id="{{ $var->id }}" data-tur="0" href="javascript:;"><b>{{ $var->adres_title }}</b><br><small>	{{ $delivery->adres_detay }}<br>{{ $delivery->adres_ilce }} - {{ $delivery->adres_il }}  </small></a>
															@endif

															@endforeach

														</div>
													</div>

													@endif
												</div>







											</div>
										</div>
									</div>


									@else
									<a href="javascript:;" class="addresses-list__item addresses-list__item--new add_delivery">
										<div class="addresses-list__plus"></div>
										<div class="btn btn-secondary btn-sm">Teslimat Adresi Ekle</div>
									</a>
									@endif




									<div class="addresses-list__divider"></div>
									@if ($invoice)


									<div class="addresses-list__item card address-card">
										<input type="hidden" name="order_fat_adres_id" id="order_fat_adres_id" value="{{ $invoice->id }}">
										<div class="address-card__badge">Fatura Adresi</div>
										<div class="address-card__body">
											<div class="address-card__name">{{ $invoice->adres_title }}</div>
											<div class="address-card__row">

												@if ($invoice->fatura_tur == 0)

												{{ $invoice->adres_yetkili }}<br>
												<small>
													V.No: {{ $invoice->fatura_tck }}<br>
													{{ $invoice->adres_detay }} 
													{{ $invoice->adres_ilce }} - {{ $invoice->adres_il }}</small>
													@else												

													{{ $invoice->fatura_unvan }}<br>
													<small>
														{{ $invoice->adres_detay }} <br>
														{{ $invoice->adres_ilce }} - {{ $invoice->adres_il }}
													</small>
													@endif
												</div>

												@if ($invoice->fatura_tur == 1 )

												<div class="address-card__row">
													<div class="address-card__row-title">Fatura Bilgileri</div>
													<div class="address-card__row-content">	<small>V.D.</small> : <b>{{ $invoice->fatura_vd }} </b> - <small>V.No</small> : <b>{{ $invoice->fatura_vno }}</b></div>
												</div>
												@else
												<div class="address-card__row">
													<div class="address-card__row-title">Fatura Bilgileri</div>
													<div class="address-card__row-content">	 <b>TCK : {{ $invoice->fatura_tck }} </b>  </div>
												</div>
												@endif




												<div class="address-card__row">
													<div class="address-card__row-title">E-Posta Adresi</div>
													<div class="address-card__row-content">{{  auth()->user()->email }}</div>
												</div>
												<div class="address-card__footer">


													<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
														<button type="button" class="btn btn-secondary add_invoice">Yeni Ekle</button>

														@if ($all_adress)
														<div class="btn-group" role="group">
															<button id="btnGroupDrop1" type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																Değiştir
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																@foreach ($all_adress as $var)
																@if ($var->adres_tur == 1)	
																<a class="dropdown-item adres_sec" data-id="{{ $var->id }}" data-tur="1" href="javascript:;"><b>{{ $var->adres_title }}</b><br><small>	{{ $delivery->adres_detay }}<br>{{ $delivery->adres_ilce }} - {{ $delivery->adres_il }}  </small></a>
																@endif

																@endforeach

															</div>
														</div>

														@endif
													</div>



												</div>
											</div>
										</div>

										@else
										<a href="javascript:;" class="addresses-list__item addresses-list__item--new add_invoice">
											<div class="addresses-list__plus"></div>
											<div class="btn btn-secondary btn-sm">Fatura Adresi Ekle</div>
										</a>
										@endif
										<div class="addresses-list__divider"></div>
									</div>


								</div>
								<div class="card-divider"></div>
								<div class="card-body">

									<div class="form-group">
										<label >Sipariş notu <span class="text-muted">(Zorunlu Değil)</span></label>
										<textarea name="order_nots" class="form-control" rows="4"></textarea>
									</div>



								</div>	

								<div class="card-divider"></div>
								<div class="card-body">

									<div class="form-group">
										<label >Mesafeli Satış Sözleşmesi</label>

										<div class=" vertical-scroll pos-r" style="height: 300px; overflow: auto; font-size: 10px">
											{!! satisSozlesme(staticPage(2)->page_content) !!}
										</div>
									</div>



								</div>
							</div>
						</div>
						<div class="col-12 col-lg-6 col-xl-5 mt-4 mt-lg-0">
							<div class="card mb-0">
								<div class="card-body">
									<h3 class="card-title">Sipariş özeti</h3>
									<table class="checkout__totals">
										<thead class="checkout__totals-header">
											<tr>
												<th>Ürün</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody class="checkout__totals-products">
											@foreach(Cart::content() as $item)

											<tr>
												<td class="d-flex">

													<a href="{{ $item->options->url }}" class="float-left pos-r">
														<img class="img-thumbnail w50" src="{{ $item->options->image }}" >
														<span class="badge badge-info rounded right-5 top-5 pos-a">{{ $item->qty }}</span>
													</a>

													<a href="{{ $item->options->url }}" class="float-left pl10 fsz12 text-dark">
														{{ $item->name }}<br>
														{{ price($item->price) }} x {{ $item->qty }}

													</a></td>
													<td>{{ price($item->subtotal)  }}</td>
												</tr>

												@endforeach


											</tbody>
											<tbody class="checkout__totals-subtotals">
												<tr>
													<th>Ara Toplam</th>
													<td>{{  price(Cart::total()) }}</td>
												</tr>
												<tr>
													<th>Kargo</th>
													<td id="cartkargo">
														{{ set('kargo_esik')>=Cart::total() ? price(set('kargo_cost')):"Ücretsiz Kargo" }}

													</td>
												</tr>

											</tbody>
											<tfoot class="checkout__totals-footer">
												<tr>
													<th>Genel Toplam</th>
													<td id="grandtotal">{{ price((Cart::total()) + (set('kargo_esik')>=Cart::total() ? set('kargo_cost'):0)) }}</td>
												</tr>
											</tfoot>
										</table>
										<div class="payment-methods">
											<ul class="payment-methods__list">


												@foreach (pay_metod() as $key => $var)



												<li class="payment-methods__item {{ $key ==1?"payment-methods__item--active":"" }}">
													<label class="payment-methods__item-header">
														<span class="payment-methods__item-radio input-radio">
															<span class="input-radio__body">
																<input class="input-radio__input payment_type" value="{{ $key }}" name="payment_type" type="radio" {{ $key ==1?"checked":"" }}>
																<span class="input-radio__circle"></span>
															</span>
														</span>
														<span class="payment-methods__item-title">{{ $var }}</span>
													</label>
													<div class="payment-methods__item-container">
														<div class="payment-methods__item-description text-muted">
															{{ pay_detail($key) }}
														</div>
													</div>
												</li>

												@endforeach




											</ul>
										</div>

										<div class="bg-light p10 lh18 fsz12 text-left mb10"> <i class="fa fa-envelope fsz35 site-color2 mr10 float-left"></i> Adınıza düzenlenmiş mesafeli satış sözleşmesi ve E-Faturanız E-Posta adresinize gönderilecektir.</div>
										<div class="checkout__agree form-group">
											<div class="form-check">
												<span class="form-check-input input-check">
													<span class="input-check__body">
														<input class="input-check__input" type="checkbox" id="checkout-terms" checked>
														<span class="input-check__box"></span>
														<svg class="input-check__icon" width="9px" height="7px">
															<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
														</svg>
													</span>
												</span>
												<label class="form-check-label" for="checkout-terms">


													<a class="open-terms site-color2" data-id="2" href="javascript:;">Mesafeli Satış Sözleşmesini</a> Kabul ediyorum
												</label>
											</div>
										</div>
										<button type="button" class="btn btn-primary order-complete btn-xl btn-block">Siparişi Tamamla</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>


		@endsection


		@section('script')
		<script>


			$('.add_delivery').click(function() {

				$.ajax({
					url:"{{ route('panelim.adres-form') }}",
					method:"GET",				
					success:function(result){
						modalyap('globalmodal',"Teslimat Adresi Ekle",result)
					}
				})

			});




			$('.add_invoice').click(function() {

				$.ajax({
					url:"{{ route('panelim.fat-adres-form') }}",
					method:"GET",				
					success:function(result){
						modalyap('globalmodal',"Fatura Adresi Ekle",result)
					}
				})

			});





			$('.adres_sec').click(function() {

				tur = $(this).data('tur')
				id = $(this).data('id')

				$.ajax({
					url:"{{ route('panelim.change-address') }}",
					method:"POST",
					data:{tur:tur,id:id},			
					success:function(result){
						location.reload()
					}
				})

			});




			



			$('.open-terms').click(function() {

				pid = $(this).data('id')
				texts = $(this).text().replace("ni", "");


				$.ajax({
					type: 'POST',
					url: '/page/'+pid,
					success: function(data){

						modalyap('globalmodal',texts,data)


					}
				});	



			});


			$('input[name="fatura_tur"]').click(function() {

				durum = $(this).val()

				if (durum == 1) {
					$(".bireysel").addClass('d-none')
					$(".kurumsal").removeClass('d-none')
				}else{
					$(".bireysel").removeClass('d-none')
					$(".kurumsal").addClass('d-none')
				}

			});



			$(".order-complete").click(function() {



				if (!$('#checkout-terms').prop('checked')) {
					sole('Hata',"Sözleşmeleri Onaylamadınız")
					$('.checkout__agree').css('border', '1px solid red').addClass('p10');

				}else{

					$('.checkout__agree').css('border', '0px solid white').removeClass('p10');


					if ($('#order_adres_id').length && $('#order_fat_adres_id').length ) {




						$(this).attr("disabled","disabled");

						$(this).css("opacity","0.6");

						$(this).html("Bekleyiniz");


						setTimeout(function(){ $('#order-form').submit(); }, 1000);

					}else{
						sole('Hata',"Adres Seçmelisiniz")
					}
				}

			});



		</script>
		@endsection