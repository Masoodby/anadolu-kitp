@extends(theme().'.layouts.order')
@section('title', "Siparişi Tamamla")
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header mt20">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">								
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>

						<li class="breadcrumb-item active" aria-current="page">Siparişi Tamamla</li>
						<li class="breadcrumb-item d-block d-sm-none"><a href="{{ url()->previous() }}" class="btn btn-primary btn-sm  ml90 float-right pos-r top-10 ">Geri</a> </li>
					</ol>
					<a href="{{ url()->previous() }}" class="btn btn-primary btn-sm d-none d-sm-block ml90 float-right pos-r top-20 ">Geri</a>
				</nav>
			</div>

		</div>
	</div>

	<div class="checkout block">
		<div class="container">
			
			
			<div class="row">

				<div class="col-12 col-lg-12 col-xl-12">
					<div class="card mb-lg-0">
						<div class="card-body">
							<h3 class="card-title">3D Ödeme</h3>

							<script src="https://www.paytr.com/js/iframeResizer.min.js"></script>
							<iframe src="https://www.paytr.com/odeme/guvenli/<?php echo $token;?>" id="paytriframe" frameborder="0" scrolling="no" style="width: 100%;"></iframe>
							<script>iFrameResize({},'#paytriframe');</script>

						</div>
					</div>

				</div>
				
			</div>
		</div>
	</div>


	@endsection


	@section('script')
	<script>






	</script>
	@endsection