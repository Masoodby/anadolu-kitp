@extends(theme().'.layouts.order')
@section('title', "Siparişi Tamamla")
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header mt20">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">								
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>

						<li class="breadcrumb-item active" aria-current="page">Hızlı Satın Al</li>
						<li class="breadcrumb-item d-block d-sm-none"><a href="{{ url()->previous() }}" class="btn btn-primary btn-sm  ml90 float-right pos-r top-10 ">Geri</a> </li>
					</ol>
					<a href="{{ url()->previous() }}" class="btn btn-primary btn-sm d-none d-sm-block ml90 float-right pos-r top-20 ">Geri</a>
				</nav>
			</div>

		</div>
	</div>

	<div class="checkout block">
		<div class="container">
			<form action="{{route('save-order')}}" method="post" id="order-form">
				{{csrf_field()}}
				<div class="row">

					<div class="col-12 col-lg-6 col-xl-7">
						<div class="card mb-lg-0">
							<div class="card-body">
								<h3 class="card-title">Hızlı Sipariş Formu</h3>

								<div class="form-row mb20">

									<div class="btn-group btn-group-toggle" data-toggle="buttons">
										<label class="btn btn-secondary active">
											<input type="radio" name="fatura_tur" id="payment_type_1" checked value="0"> Bireysel
										</label>
										<label class="btn btn-secondary">
											<input type="radio" name="fatura_tur" id="payment_type_2" value="1"> Kurumsal
										</label>

									</div>
									<hr>
								</div>



								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="checkout-first-name">İsim</label>
										<input type="text" class="form-control req" name="order_name" id="checkout-first-name" placeholder="İsim" value="{{ olt("order_name") }}">
									</div>
									<div class="form-group col-md-6">
										<label for="checkout-last-name">Soyisim</label>
										<input type="text" class="form-control req" name="order_sname" id="checkout-last-name" placeholder="Soyisim" value="{{ olt("order_sname") }}">
									</div>
								</div>
								<div class="form-group">
									<label for="checkout-mail">E-Posta Adresi  </label>
									<input type="email" class="form-control ismail req" id="checkout-mail" name="email" placeholder="E-Posta Adresi"  value="{{ olt("email") }}">
								</div>	

								<div class="form-group">
									<label for="checkout-gsm">GSM  </label>
									<input type="text" class="form-control gsm req" id="checkout-gsm" name="adres_gsm" placeholder="Telefon no"  value="{{ olt("adres_gsm") }}">
								</div>


								<div class="form-row kurumsal d-none">
									<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Bilgileri</b>
									<div class="form-group col-md-12">
										<label for="checkout-first-name">Kurumsal Ünvan</label>
										<input type="text" class="form-control" name="fatura_unvan"   placeholder="Faturada kullanılacak ünvan..."  value="{{ olt("fatura_unvan") }}">
									</div>
									<div class="form-group col-md-6">
										<label for="checkout-last-name">Vergi Dairesi</label>
										<input type="text" class="form-control" name="fatura_vd" placeholder="Vergi Dairesi"  value="{{ olt("fatura_vd") }}">
									</div>	

									<div class="form-group col-md-6">
										<label for="checkout-last-name">Vergi No</label>
										<input type="text" class="form-control int" name="fatura_vno"   placeholder="Vergi No"  value="{{ olt("fatura_vno") }}">
									</div>
								</div>



								<div class="form-row bireysel">
									<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Fatura Bilgileri</b>

									<div class="form-group col-md-12">
										<label for="checkout-last-name">TCK No <small class="text-muted">(Zorunlu Değildir)</small></label>
										<input type="number" maxlength="11" class="form-control" name="fatura_tck" value="{{ olt("fatura_tck") }}">
										<small class="p10 site-color2">Fatura Ünvanı  İsim ve Soyisim olarak kullanılacaktır.</small>
									</div>
								</div>




								<div class="form-row">
									<b class="col-12 bg-light p10 pl20 mb10"><i class="fa fa-info-circle site-color1 mr5"></i>Teslimat Bilgileri</b>
									<div class="form-group col-md-6">

										<select id="city" name="adres_il" class="form-control form-control-select2 req">
											<option value="">İl Seçiniz</option>
											@foreach($cities as $city)
											<option value="{{ $city->city}}" {{ olt("adres_il") == $city->city ?"selected":"" }} >{{ $city->city }}</option>
											@endforeach
										</select>
									</div>	

									<div class="form-group col-md-6">

										<select id="county" name="adres_ilce" class="form-control form-control-select2 req">
											@if (olt("adres_ilce") !="")
											<option value="{{ olt("adres_ilce") }}">{{ olt("adres_ilce") }}</option>
											@endif
											

										</select>
									</div>
								</div>
								<div class="form-group">
									<label >Açık Adres</label>
									<textarea name="adres_detay" class="form-control req" rows="4" placeholder="Mahalle/Cadde/Sokak/Bina/Daire">{{ olt("adres_detay") }}</textarea>
								</div>





							</div>
							<div class="card-divider"></div>
							<div class="card-body">

								<div class="form-group">
									<label >Sipariş notu <span class="text-muted">(Zorunlu Değil)</span></label>
									<textarea name="order_nots" class="form-control" rows="4">{{ olt("order_nots") }}</textarea>
								</div>



							</div>
						</div>
					</div>
					<div class="col-12 col-lg-6 col-xl-5 mt-4 mt-lg-0">
						<div class="card mb-0">
							<div class="card-body">
								<h3 class="card-title">Sipariş özeti</h3>
								<table class="checkout__totals">
									<thead class="checkout__totals-header">
										<tr>
											<th>Ürün</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody class="checkout__totals-products">
										@foreach(Cart::content() as $item)

										<tr>
											<td class="d-flex">

												<a href="{{ $item->options->url }}" class="float-left pos-r">
													<img class="img-thumbnail w50" src="{{ $item->options->image }}" >
													<span class="badge badge-info rounded right-5 top-5 pos-a">{{ $item->qty }}</span>
												</a>

												<a href="{{ $item->options->url }}" class="float-left pl10 fsz12 text-dark">
													{{ $item->name }}<br>
													{{ price($item->price) }} x {{ $item->qty }}

												</a></td>
												<td>{{ price($item->subtotal)  }}</td>
											</tr>

											@endforeach


										</tbody>
										<tbody class="checkout__totals-subtotals">
											<tr>
												<th>Ara Toplam</th>
												<td>{{  price(Cart::total()) }}</td>
											</tr>
											<tr>
												<th>Kargo</th>
												<td id="cartkargo">
													{{ set('kargo_esik')>=Cart::total() ? price(set('kargo_cost')):"Ücretsiz Kargo" }}

												</td>
											</tr>

										</tbody>
										<tfoot class="checkout__totals-footer">
											<tr>
												<th>Genel Toplam</th>
												<td id="grandtotal">{{ price((Cart::total()) + (set('kargo_esik')>=Cart::total() ? set('kargo_cost'):0)) }}</td>
											</tr>
										</tfoot>
									</table>
									<div class="payment-methods">
										<ul class="payment-methods__list">


											@foreach (pay_metod() as $key => $var)



											<li class="payment-methods__item {{ $key ==1?"payment-methods__item--active":"" }}">
												<label class="payment-methods__item-header">
													<span class="payment-methods__item-radio input-radio">
														<span class="input-radio__body">
															<input class="input-radio__input payment_type" value="{{ $key }}" name="payment_type" type="radio" {{ $key ==1?"checked":"" }}>
															<span class="input-radio__circle"></span>
														</span>
													</span>
													<span class="payment-methods__item-title">{{ $var }}</span>
												</label>
												<div class="payment-methods__item-container">
													<div class="payment-methods__item-description text-muted">
														{{ pay_detail($key) }}
													</div>
												</div>
											</li>

											@endforeach




										</ul>
									</div>

									<div class="bg-light p10 lh18 fsz12 text-left mb10"> <i class="fa fa-envelope fsz35 site-color2 mr10 float-left"></i> Adınıza düzenlenmiş mesafeli satış sözleşmesi ve E-Faturanız E-Posta adresinize gönderilecektir.</div>
									<div class="checkout__agree form-group">
										<div class="form-check">
											<span class="form-check-input input-check">
												<span class="input-check__body">
													<input class="input-check__input" type="checkbox" id="checkout-terms" checked>
													<span class="input-check__box"></span>
													<svg class="input-check__icon" width="9px" height="7px">
														<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-9x7"></use>
													</svg>
												</span>
											</span>
											<label class="form-check-label" for="checkout-terms">


												<a class="open-terms site-color2" data-id="2" href="javascript:;">Mesafeli Satış Sözleşmesini</a> ve
												<a class="open-terms site-color2" data-id="4" href="javascript:;">Üyelik Sözleşmesini</a> Kabul ediyorum
											</label>
										</div>
									</div>
									<button type="button" class="btn btn-primary order-complete btn-xl btn-block">Siparişi Tamamla</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>


	@endsection


	@section('script')
	<script>
		$('#city').change(function(){
			if($(this).val() != ''){
				var select = $(this).attr("id");
				var value = $(this).val();

				$.ajax({
					url:"{{ route('get-counties') }}",
					method:"POST",
					data:{select:select, value:value},
					success:function(result){
						$('#county').html(result);
					}
				})
			}
		});




		$('.open-terms').click(function() {

			pid = $(this).data('id')
			texts = $(this).text().replace("ni", "");


			$.ajax({
				type: 'POST',
				url: '/page/'+pid,
				success: function(data){

					modalyap('globalmodal',texts,data)


				}
			});	



		});


		$('input[name="fatura_tur"]').click(function() {

			durum = $(this).val()

			if (durum == 1) {
				$(".bireysel").addClass('d-none')
				$(".kurumsal").removeClass('d-none')
			}else{
				$(".bireysel").removeClass('d-none')
				$(".kurumsal").addClass('d-none')
			}
			
		});



		$(".order-complete").click(function() {



			if (!$('#checkout-terms').prop('checked')) {
				sole('Hata',"Sözleşmeleri Onaylamadınız")
				$('.checkout__agree').css('border', '1px solid red').addClass('p10');

			}else{

				$('.checkout__agree').css('border', '0px solid white').removeClass('p10');


				$('#order-form .req').each(function(index, element) {

					var degert = $(this).val();

					if(degert == '' || degert == -1 ){

						$(this).css('border-color','red');

						$(this).focus();

						die();
					} else {

						$(this).css('border-color','green');

					}
				});


				$(this).attr("disabled","disabled");

				$(this).css("opacity","0.6");

				$(this).html("Bekleyiniz");




				setTimeout(function(){ $('#order-form').submit(); }, 1000);
			}

		});



	</script>
	@endsection