@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="block order-success">
		<div class="container">
			<div class="order-success__body">
				<div class="order-success__header">
					<svg class="order-success__icon" width="100" height="100">
						<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#check-100"></use>
					</svg>
					<h1 class="order-success__title">Siparişiniz Başarıyla Kaydedildi</h1>
					<div class="order-success__subtitle">Bizi Tercih Ettiğiniz için Teşekkür Ederiz.</div>
					<div class="order-success__actions">
						<a href="{{ route('panelim') }}" class="btn btn-xs btn-secondary">Panelime Git</a>
					</div>
				</div>
				<div class="order-success__meta">
					<ul class="order-success__meta-list">
						<li class="order-success__meta-item">
							<span class="order-success__meta-title">Sipariş ID:</span>
							<span class="order-success__meta-value">#{{ $order->id }}</span>
						</li>
						<li class="order-success__meta-item">
							<span class="order-success__meta-title">Sipariş Tarihi:</span>
							<span class="order-success__meta-value">{{ date("d.m.Y H:i",strtotime($order->created_at)) }}</span>
						</li>
						<li class="order-success__meta-item">
							<span class="order-success__meta-title">Sipariş Tutarı:</span>
							<span class="order-success__meta-value">{{ price($order->grand_total) }}</span>
						</li>
						<li class="order-success__meta-item">
							<span class="order-success__meta-title">Ödeme Metodu:</span>
							<span class="order-success__meta-value">{{ pay_metod($order->payment_type) }}</span>
						</li>
					</ul>
				</div>
				<div class="card">
					<div class="order-list">
						<table>
							<thead class="order-list__header">
								<tr>
									<th class="order-list__column-label" colspan="2">Sipariş Ürünleri</th>
									<th class="order-list__column-quantity">Adet</th>
									<th class="order-list__column-total">Total</th>
								</tr>
							</thead>
							<tbody class="order-list__products">
                                @if (count($order->baskets->basket_products))
                                @foreach ($order->baskets->basket_products as $var)


								@php
								$json = json_decode($var->cartjson);

								@endphp
								<tr>
									<td class="order-list__column-image">
										<div class="product-image">
											<a href="{{ $json->url }}" class="product-image__body">
												<img class="product-image__img" src="{{ $json->image }}" alt="">
											</a>
										</div>
									</td>
									<td class="order-list__column-product">
										<a href="{{ $json->url }}">{{ $var->product->product_name }}</a>

									</td>
									<td class="order-list__column-quantity" data-title="Qty:">{{ $var->qty }}</td>
									<td class="order-list__column-total">{{ price($var->total_price*$var->qty) }}</td>
								</tr>

								@endforeach
                                @endif
                                @if (count($order->baskets->basket_combos))
                                @foreach ($order->baskets->basket_combos as $var)


								@php
								$json = json_decode($var->cartjson);

								@endphp
								<tr>
									<td class="order-list__column-image">
										<div class="product-image">
											<a href="{{ $json->url }}" class="product-image__body">
												<img class="product-image__img" src="{{ res( 'combo',$var->combo_image,'100x100')}}" alt="">
											</a>
										</div>
									</td>
									<td class="order-list__column-product">
										<a href="{{ $json->url }}">{{ $var->combo_name }}</a>

									</td>
									<td class="order-list__column-quantity" data-title="Qty:">{{ $var->qty }}</td>
									<td class="order-list__column-total">{{ price($var->total_price*$var->qty) }}</td>
								</tr>

								@endforeach

                                @endif



							</tbody>
							<tbody class="order-list__subtotals">
								<tr>
									<th class="order-list__column-label" colspan="3">Ara Toplam</th>
									<td class="order-list__column-total">{{ price($order->order_total) }}</td>
								</tr>
								<tr>
									<th class="order-list__column-label" colspan="3">Kargo</th>
									<td class="order-list__column-total">{{ $order->cargo_cost==0?"Ücretsiz Kargo":price($order->cargo_cost) }}</td>
								</tr>

							</tbody>
							<tfoot class="order-list__footer">
								<tr>
									<th class="order-list__column-label" colspan="3">Genel toplam</th>
									<td class="order-list__column-total">{{ price($order->grand_total) }}</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div class="row mt-3 no-gutters mx-n2">


					<div class="col-sm-6 col-12 px-2">
						<div class="card address-card">
							<div class="address-card__body">
								<div class="address-card__badge address-card__badge--muted">Teslimat Adresi</div>
								<div class="address-card__name">{{ $order->address->adres_name.' '.$order->address->adres_sname }}</div>
								<div class="address-card__row">
									<small>
										{{ $order->address->adres_detay }}<br>
										{{ $order->address->adres_ilce }} - {{ $order->address->adres_il }}
									</small>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">GSM</div>
									<div class="address-card__row-content">{{ $order->address->adres_gsm }}</div>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">E-Posta Adresi</div>
									<div class="address-card__row-content">{{ auth()->user()->email }}</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-12 px-2 mt-sm-0 mt-3">
						<div class="card address-card">
							<div class="address-card__body">
								<div class="address-card__badge address-card__badge--muted">Fatura Adresi</div>
								<div class="address-card__name">{{ $order->fatadres->fatura_tur == 0 ? $order->fatadres->adres_name.' '.$order->fatadres->adres_sname : $order->fatadres->fatura_unvan }}</div>
								<div class="address-card__row">
									<small>
										{{ $order->fatadres->adres_detay }}<br>
										{{ $order->fatadres->adres_ilce }} - {{ $order->fatadres->adres_il }}
									</small>
								</div>
								<div class="address-card__row">
									<div class="address-card__row-title">Fatura Bilgileri</div>
									@if ( $order->fatadres->fatura_tur == 0 )

									<div class="address-card__row-content"><small>TCK No : {{ strlen($order->fatadres->fatura_tck)==11?$order->fatadres->fatura_tck:"11111111111" }} </small></div>
									@else
									<div class="address-card__row-content"><small>V.D. {{ $order->fatadres->fatura_vd }} V.No: {{ $order->fatadres->fatura_vno }} </small></div>
									@endif

								</div>

							</div>
						</div>
					</div>




				</div>
			</div>
		</div>
	</div>
</div>


@endsection
