@extends(theme().'.layouts.master')
@section('title', $combo->combo_name)
@section('description', $combo->combo_desc)
@section('keywords')

@section('meta')
    <meta property="og:url" content="{{ url()->full() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $combo->combo_name }}" />
    <meta property="og:description" content="{{ $combo->combo_desc }}" />
    <meta property="og:image" content="{{ res('combo', $combo->combo_image, '100x100') }}" />
@endsection


@section('headclose')
    <script>
        fbq('track', 'PageView');

        fbq('track', 'ViewContent', {
            content_name: '{{ $combo->combo_name }}',
            content_category: 'Ürün seti',
            content_ids: ['{{ $combo->id }}'],
            content_type: 'product',
            value: {{ $combo->combo_price }},
            currency: 'USD'
        });
    </script>



@endsection


@section('bodyust')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/tr_TR/sdk.js#xfbml=1&autoLogAppEvents=1&version=v9.0&appId={{ set('fb_app_id') }}"
        nonce="N1bk1gCS"></script>
@endsection

@section('content')
    {{-- mby start --}}
    <?php


    $now= date('H');
    $min=60 - date('i');
    if ($now >= 14) {
        $hour=(24 - $now) + 14;

         } else {
            $hour=14 - $now;
         }
   ?>
        {{-- mby end --}}
         <!-- site__body -->
        <div class="site__body">
            <div class="page-header">
                <div class="page-header__container container">
                    <div class="page-header__breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use
                                            xlink:href="{{ asset('dist/front/' . theme()) }}/images/sprite.svg#arrow-rounded-right-6x9">
                                        </use>
                                    </svg>
                                </li>

                                {{-- @foreach ($tree as $var)
            <li class="breadcrumb-item">
              <a href="{{ route('kategori',$var["categories_url"]) }}">{{ $var['categories_name'] }}</a>
              <svg class="breadcrumb-arrow" width="6px" height="9px">
                <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
              </svg>
            </li>
            @endforeach --}}
                                <li class="breadcrumb-item active" aria-current="page">{{ $combo->combo_name }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="container">
                    <div class="product product--layout--columnar" data-layout="columnar">
                        <div class="product__content">
                            <!-- .product__gallery -->
                            @include(theme().'.product.comboimg')

                            <!-- .product__info -->
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach ($combo->products as $key =>  $product)
                                    <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                        @include(theme().'.product.info')<br>
                                      </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                  <span class="carousel-control-prev-icon" style="color: black" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>


                            <!-- .product__sidebar -->
                            <div class="product__sidebar">
                                <div class="product__availability">
                                    {{-- mby start --}}
                                    @if ($now >= 14)
                                    <span> <strong><i class="fas fa-truck"></i> {{$hour}} saat {{$min}} dakika</strong>   içinde sipariş verirseniz yarın kargoda</span>
                                    @else
                                    <img src="{{ res('content','today-delivery.png') }}" class="img-fluid">
                                    <span><i class="fas fa-truck"></i> <strong>{{$hour}} saat {{$min}} dakika</strong>  içinde sipariş verirseniz bugün kargoda</span>
                                    @endif
                                    {{-- mby end --}}

                                </div>
                                <div class="product__prices">
                                    <small style="font-size: 12px">
                                        @php
                                        $i=0;
                                        foreach ($combo->products as $kitap)

                                              $i += $kitap->product_price;

                                            $i= round($i,2)

                                        @endphp

                                        <table>
                                            <thead>
                                                <tr>
                                                <td>Kıtaplar</td>
                                                <td>Fiyat</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($combo->products as $product )
                                                <tr>
                                                    <td>{{$product->product_name}}</td>
                                                    <td><span style="color: rgb(255, 31, 31)"><strong>{{$product->product_price}}</strong></span></td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td><strong>toplan</strong></td>
                                                    <td><del class="fsz14 color-grey "><span style="color: rgb(255, 31, 31)"><strong>{{$i}}</strong></span></del></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </small>
                                       <span class="fsz16 ml10 p5 site-color1 bg-light"> İndirimli</span> <br>
                                      {{ $combo->combo_price }} TL
                                </div>
                                <!-- .product__options -->
                                @include(theme().'.product.comboaction')
                                <!-- .product__options / end -->
                            </div>
                            <!-- .product__end -->
                            <div class="product__footer">
                                <div class="product__tags tags">
                                    <div class="tags__list">
                                        @foreach ($combo->products as $product)
                                        <a href="{{ route('yazarlar.kitaplar',$product->extend->getYazar->yazar_url) }}">{{ $product->extend->getYazar->yazar_name }}</a>
                                        <a href="{{ route('kategori',$product->get_category->categories_url) }}">{{ $product->get_category->categories_name }}</a>
                                        @endforeach


                                    </div>
                                </div>
                                <div class="product__share-links share-links">
                                    <ul class="share-links__list">

                                        <div class="fb-like" data-href="{{ url()->full() }}" data-width=""
                                            data-layout="button" data-action="like" data-size="small" data-share="true">
                                        </div>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                     @include(theme().'.product.combodetail')
                </div>
            </div>
            <!-- .block-products-carousel -->
            {{-- @include(theme().'.product.related') --}}
            <!-- .block-products-carousel / end -->
        </div>
        <!-- site__body / end -->

    @endsection
