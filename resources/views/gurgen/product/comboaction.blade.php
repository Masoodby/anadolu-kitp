       <form class="product__options">
            @if ($currentCount > 0)
        <div class="alert alert-warning site-color2">
          <i class="fa fa-shopping-cart "></i>    Sepetinizde bu üründen <b>{{ $currentCount  }}</b> adet var
        </div>
        @endif

           <div class="form-group product__option">
               <label class="product__option-label" for="combo-quantity">Adet</label>
               <div class="product__actions">
                   <div class="product__actions-item">
                       <div class="input-number product__quantity">
                           <input id="product-quantity" class="input-number__input form-control form-control-lg"
                               type="number" min="1" value="1">
                           <div class="input-number__add"></div>
                           <div class="input-number__sub"></div>
                       </div>
                   </div>

                   <div class="product__actions-item product__actions-item--addtocart">
                    <button type="button" class="btn btn-primary btn-lg comboadd-to-cart" max="1" data-stok="1" data-id="{{ $combo->id }}"> Sepete Ekle</button>
                   </div>



               </div>
           </div>
           - <div class="form-group product__option">
               <div class="product__actions-item">
                   <button type="button" class="btn btn-success btn-lg comboquick-buy tipsi center-block"
                       title="Hızlı Satın Al" data-id="{{ $combo->id }}"><i class="fa fa-shopping-cart"></i></button>
                   <button type="button" class="btn btn-secondary btn-svg-icon btn-lg" data-toggle="tooltip"
                       title="Favori Listeme Ekle">
                       <svg width="16px" height="16px">
                           <use xlink:href="{{ asset('dist/front/' . theme()) }}/images/sprite.svg#wishlist-16"></use>
                       </svg>
                   </button>
                   <button type="button" class="btn btn-secondary btn-svg-icon btn-lg" data-toggle="tooltip"
                       title="Karşılaştırma Listeme Ekle">
                       <svg width="16px" height="16px">
                           <use xlink:href="{{ asset('dist/front/' . theme()) }}/images/sprite.svg#compare-16"></use>
                       </svg>
                   </button>
               </div>

           </div>



       </form>
