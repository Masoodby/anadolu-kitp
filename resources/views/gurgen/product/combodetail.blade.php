<div class="product-tabs  product-tabs--sticky">
	<div class="product-tabs__list">
		<div class="product-tabs__list-body">
			<div class="product-tabs__list-container container">
				<a href="#tab-description" class="product-tabs__item product-tabs__item--active">Ürün Açıklaması</a>
				<a href="#tab-specification" class="product-tabs__item">Ödeme Seçenekleri</a>
				<a href="#tab-reviews" class="product-tabs__item">Yorumlar</a>
			</div>
		</div>
	</div>
	<div class="product-tabs__content">
		<div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
			<div class="typography">
				<h3>{{ $combo->combo_name }}</h3>
        {!! $combo->combo_desc !!}
      </div>
    </div>
    <div class="product-tabs__pane" id="tab-specification">
     <div class="spec">

      <div class="spec__section">

       <div class="spec__row">
        <div class="spec__name">Havale ile Ödeme</div>
        <div class="spec__value"><i class="fa fa-check"></i></div>
      </div>
      <div class="spec__row">
        <div class="spec__name">Kredi Kartı ile Ödeme</div>
        <div class="spec__value"><i class="fa fa-check"></i></div>
      </div>
      <div class="spec__row">
        <div class="spec__name">Kapıda Ödeme</div>
        <div class="spec__value"><i class="fa fa-times"></i></div>
      </div>
      <div class="spec__row">
        <div class="spec__name">Kapıda Kredi Kartı ile Ödeme</div>
        <div class="spec__value"><i class="fa fa-times"></i></div>
      </div>


    </div>
    <div class="spec__section">
      <h4 class="spec__section-title">Taksit Seçenekleri</h4>


    </div>

  </div>
</div>
<div class="product-tabs__pane" id="tab-reviews">
 <div class="reviews-view">
  <div class="fb-comments" data-href="{{ url()->full() }}" data-width="100%" data-numposts="5"></div>

</div>
</div>
</div>
</div>
