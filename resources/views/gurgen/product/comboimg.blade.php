@php
{{
    $res =  res( 'combo',$combo->combo_image,'250x250') ;
}}

@endphp

<div class="product-gallery">
    <div class="product-gallery__featured">
        <button class="product-gallery__zoom">
            <svg width="24px" height="24px">
                <use xlink:href="images/sprite.svg#zoom-in-24"></use>
            </svg>
        </button>
        <div class="owl-carousel owl-loaded owl-drag" id="product-image">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2655px;">
                    <div class="owl-item active" style="width: 531px;">
                        <div class="product-image product-image--location--gallery">
                            <a href="{{res( 'combo',$combo->combo_image)}}" data-width="700" data-height="700"
                                class="product-image__body" target="_blank">
                                <img class="product-image__img" src="{{res( 'combo',$combo->combo_image)}}" alt="">
                            </a>
                        </div>
                    </div>
                    @foreach ($combo->products as $row)
                    <div class="owl-item" style="width: 531px;">
                        <div class="product-image product-image--location--gallery">
                            <a href="{{ress($row->product_stock_code,$row->product_group_code,"250x250")}}" data-width="700" data-height="700"
                                class="product-image__body" target="_blank">
                                <img class="product-image__img" src="{{ress($row->product_stock_code,$row->product_group_code,"250x250")}}" alt="">
                            </a>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
            <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
    <div class="product-gallery__carousel">
        <div class="owl-carousel owl-loaded owl-drag" id="product-carousel">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 545px;">
                    <div class="owl-item active" style="width: 99px; margin-right: 10px;">
                        <a href="{{res( 'combo',$combo->combo_image,'250x250')}}"
                            class="product-image product-gallery__carousel-item product-gallery__carousel-item--active">
                            <div class="product-image__body">
                                <img class="product-image__img product-gallery__carousel-image"
                                    src="{{res( 'combo',$combo->combo_image,'250x250')}}" alt="">
                            </div>
                        </a>
                    </div>
                    @foreach ($combo->products as $row)
                    <div class="owl-item active" style="width: 99px; margin-right: 10px;">
                        <a href="{{ress($row->product_stock_code,$row->product_group_code,"250x250")}}" class="product-image product-gallery__carousel-item">
                            <div class="product-image__body">
                                <img class="product-image__img product-gallery__carousel-image"
                                    src="{{ress($row->product_stock_code,$row->product_group_code,"250x250")}}" alt="">
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</div>
