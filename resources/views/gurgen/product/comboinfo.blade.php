


<div class="product__info">
    <div class="product__wishlist-compare">
     <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Wishlist">
      <svg width="16px" height="16px">
       <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#wishlist-16"></use>
     </svg>
   </button>
   <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Compare">
    <svg width="16px" height="16px">
     <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#compare-16"></use>
   </svg>
 </button>
</div>
<h1 class="product__name">{{ $product->product_name }}</h1>


<div class="product__rating">
 <div class="product__rating-stars">
  <div class="rating">
   <div class="rating__body">


     @for ($i = 0; $i < 5; $i++)
     @if ($i<$product->product_rate +1)

     <svg class="rating__star rating__star--active" width="13px" height="12px">
       <g class="rating__fill">
        <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#star-normal"></use>
      </g>
      <g class="rating__stroke">
        <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#star-normal-stroke"></use>
      </g>
    </svg>
    <div class="rating__star rating__star--only-edge rating__star--active">
     <div class="rating__fill">
      <div class="fake-svg-icon"></div>
    </div>
    <div class="rating__stroke">
      <div class="fake-svg-icon"></div>
    </div>
  </div>
  @else
  <svg class="rating__star" width="13px" height="12px">
   <g class="rating__fill">
    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#star-normal"></use>
  </g>
  <g class="rating__stroke">
    <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#star-normal-stroke"></use>
  </g>
</svg>
<div class="rating__star rating__star--only-edge">
 <div class="rating__fill">
  <div class="fake-svg-icon"></div>
</div>
<div class="rating__stroke">
  <div class="fake-svg-icon"></div>
</div>
</div>
@endif




@endfor


</div>
</div>
</div>
<div class="product__rating-legend">
<a href="">{{ $product->product_rate +1 }} Değerlendirme</a><span>/</span><a href="">Ürünü Değerlendir</a>
</div>
</div>
@if ($product->product_jenerik != "NULL")

<div class="product__description">
{{ $product->product_jenerik }}
</div>

@endif
<ul class="product__meta mb10">
<li class="product__meta-availability">
 @if ( $product->product_stok == 0 )
 <span class="text-warning"> 3 iş günü içerisinde tedarik </span>
 @else
 <span class="text-success fsz16"> Aynı Gün Kargo </span>
 @endif
</li>
<li>Yazar: <a href="{{ route('yazarlar.kitaplar',$product->extend->getYazar->yazar_name) }}">{{ $product->extend->getYazar->yazar_name }}</a></li><br>
<li>Yayınevi: <a href="{{ route('yayin-evleri.kitaplar',$product->extend->getYayinci->yayinci_url) }}">{{ $product->extend->getYayinci->yayinci_name }}</a></li><br>
<li>Stok Kodu: {{ $product->product_stock_code }}</li><br>
</ul>

<hr>

@if (!empty($product->extend->extend_json))

<ul class="product__features">
@foreach (json_decode($product->extend->extend_json) as $key => $var)
@if ($key !="cevirmen")
<li><b>{{ $var->attr }}</b>: {{ $var->value }}</li>

@endif
<br>
@endforeach


</ul>
@endif



</div>

<!-- .product__info / end -->
