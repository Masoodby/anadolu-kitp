      <div class="product__gallery">
          <div class="product-gallery">
              <div class="product-gallery__featured">
                  <img class="img-thumbnail rounded rounded-top float-left"
                      src="{{ ress($product->product_stock_code, $product->product_group_code) }}"
                      alt="{{ $product->product_name }}">
              </div>
          </div>
      </div>

      <!-- .product__gallery / end -->
