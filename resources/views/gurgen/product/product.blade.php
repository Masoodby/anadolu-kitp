@extends(theme().'.layouts.master')
@section('title', $product->product_title.' '.$product->product_stock_code )
@section('description', $product->product_desc )
@section('keywords', $product->product_keyw )

@section('meta')
<meta property="og:url"           content="{{   url()->full() }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="{{ $product->product_title }}" />
<meta property="og:description"   content="{{ $product->product_desc }}" />
<meta property="og:image"         content="{{ res("product", jres($product->detail->product_image)) }}" />
@endsection


@section('headclose')
<script>
  fbq('track', 'PageView');

  fbq('track', 'ViewContent', {
    content_name: '{{ $product->product_name }}',
    content_category: '{{ $product->get_category->categories_name }}',
    content_ids: ['{{ $product->id }}'],
    content_type: 'product',
    value: {{ $product->product_price }},
    currency: 'USD'
  });


</script>



@endsection


@section('bodyust')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/tr_TR/sdk.js#xfbml=1&autoLogAppEvents=1&version=v9.0&appId={{ set('fb_app_id') }}" nonce="N1bk1gCS"></script>
@endsection

@section('content')
{{-- mby start --}}
<?php
// dd($firset);
if ($firset) {
    $indi=$firset->discount / 100 ;
$indrim = $product->product_price * $indi;
$newprice=$product->product_price - $indrim;
$newprice=round($newprice,2);
}



$now= date('H');
$min=60 - date('i');
if ($now < 14) {
    $hour= 14 - $now;
} else {
    $hour= (24 - $now) + 14;
}

//  dd($hour);
?>
{{-- mby end --}}

<!-- site__body -->
<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>

            @foreach ($tree as $var)
            <li class="breadcrumb-item">
              <a href="{{ route('kategori',$var["categories_url"]) }}">{{ $var['categories_name'] }}</a>
              <svg class="breadcrumb-arrow" width="6px" height="9px">
                <use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
              </svg>
            </li>
            @endforeach
            <li class="breadcrumb-item active" aria-current="page">{{ $product->product_name }}</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
  <div class="block">
    <div class="container">
     <div class="product product--layout--columnar" data-layout="columnar">
      <div class="product__content">
       <!-- .product__gallery -->
       @include(theme().'.product.img')

       <!-- .product__info -->
       @include(theme().'.product.info')
       <!-- .product__sidebar -->
       <div class="product__sidebar">
         <div class="product__availability">

          @if ( $product->is_depo == 0 && $product->is_sale == 1 )
          <div class="text-dark alert text-left fsz18 bg-light rounded-top rounded d-block"><i class="fa fa-clock fsz35 text-primary float-left pr8 mr5 border-right  border-dark"></i> 3 iş günü içerisinde tedarik </div>
            {{-- mby start --}}

            {{-- mby end --}}
          @else
          @if ($now > 14)
          <span><i class="fas fa-truck"></i> <strong>{{$hour}} saat {{$min}} dakika</strong>   içinde sipariş verirseniz yarın kargoda</span>
          @else
          <span><i class="fas fa-truck"></i> <strong>{{$hour}} saat {{$min}} dakika</strong>  içinde sipariş verirseniz bugün kargoda</span>
          @endif

          <img src="{{ res('content','today-delivery.png') }}" class="img-fluid">
          @endif
        </div>
        <div class="product__prices">

            @if ($firset)
            <del class="fsz14 color-grey ">
                {{ $product->detail->product_old_price < $product->product_price?$product->product_price*1.25:$product->detail->product_old_price}} TL
              </del>


              <span class="fsz16 ml10 p5 site-color1 bg-light">{{ yuzde($product->detail->product_old_price,$product->product_price) }} İndirim</span> <br>
              <del class="fsz14 color-grey ">
              {{ $product->product_price }} TL
              </del><span class="fsz16 ml10 p5  bg-light" style="color: red">Bugüne Özel</span><br>

              {{$newprice}} TL
            @else
            <del class="fsz14 color-grey ">
                {{ $product->detail->product_old_price < $product->product_price?$product->product_price*1.25:$product->detail->product_old_price}} TL
              </del>


              <span class="fsz16 ml10 p5 site-color1 bg-light">{{ yuzde($product->detail->product_old_price,$product->product_price) }} İndirim</span> <br>
              {{ $product->product_price }} TL

            @endif










        </div>
        <!-- .product__options -->
        @include(theme().'.product.action')
        <!-- .product__options / end -->
      </div>
      <!-- .product__end -->
      <div class="product__footer">
       <div class="product__tags tags">
        <div class="tags__list">
          <a href="{{ route('yazarlar.kitaplar',$product->extend->getYazar->yazar_url) }}">{{ $product->extend->getYazar->yazar_name }}</a>
          <a href="{{ route('kategori',$product->get_category->categories_url) }}">{{ $product->get_category->categories_name }}</a>

        </div>
      </div>
      <div class="product__share-links share-links">
        <ul class="share-links__list">

         <div class="fb-like" data-href="{{ url()->full() }}" data-width="" data-layout="button" data-action="like" data-size="small" data-share="true"></div>

       </ul>
     </div>
   </div>
 </div>
</div>
@include(theme().'.product.detail')
</div>
</div>
<!-- .block-products-carousel -->
@include(theme().'.product.related')
<!-- .block-products-carousel / end -->
</div>
<!-- site__body / end -->

@endsection
