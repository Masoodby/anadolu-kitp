<div class="block block-products-carousel" data-layout="grid-5" data-mobile-grid-columns="2">
	<div class="container">
		<div class="block-header">
			<h3 class="block-header__title">Benzer Ürünler</h3>
			<div class="block-header__divider"></div>
			<div class="block-header__arrows-list">
				<button class="block-header__arrow block-header__arrow--left" type="button">
					<svg width="7px" height="11px">
						<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-left-7x11"></use>
					</svg>
				</button>
				<button class="block-header__arrow block-header__arrow--right" type="button">
					<svg width="7px" height="11px">
						<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-7x11"></use>
					</svg>
				</button>
			</div>
		</div>
		<div class="block-products-carousel__slider">
			<div class="block-products-carousel__preloader"></div>
			<div class="owl-carousel">

				@foreach ($related as $row)
				
				<div class="block-products-carousel__column">
					<div class="block-products-carousel__cell">
						@include(theme().'.box.box')
					</div>
				</div>

				@endforeach

			</div>
		</div>
	</div>
</div>