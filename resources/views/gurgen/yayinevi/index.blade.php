@extends(theme().'.layouts.master')
@section('title', set('title'))
@section('description', set('desc'))
@section('keywords', set('keyw'))

@section('content')

<div class="site__body">
	<div class="page-header">
		<div class="page-header__container container">
			<div class="page-header__breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('anasayfa') }}"><i class="fa fa-home"></i></a>
							<svg class="breadcrumb-arrow" width="6px" height="9px">
								<use xlink:href="{{ asset('dist/front/'.theme()) }}/images/sprite.svg#arrow-rounded-right-6x9"></use>
							</svg>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Yayıncılar</li>
					</ol>
				</nav>
			</div>

		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="card">
				<div class="card-header">
					<h5>Yayınevleri</h5>
				</div>
				<div class="card-divider"></div>
				<div class="card-body">
					<div class="row">


						<div class="col-lg-12 pb10 pos-r">
							<div class="header-search-block pos-r">
								<input type="text" placeholder="Yayınevi adı giriniz" class="form-control"  id="yayinci_ara" autocomplete="off">
								<i class="fa pos-a top12 right20 fsz20 fa-search"></i>

							</div>

							<div id="yayinci_result"></div>            
						</div>

						<div class="col-lg-12">
							@php
							$hafr = ['A','B','C','Ç','D','E','F','G','H','I','i','J','K','L','M','N','O','Ö','P','Q','R','S','Ş','T','U','Ü','V','W','X','Y','Z'];
							@endphp

							@foreach ($hafr as $ch) 
							<a href="{{ route('yayin-evleri',rawurlencode($ch)) }}" class="badge fsz15 w30 m3 badge-dark color-white d-inline-block">{{ $ch }}</a>
							@endforeach
						</div>

						<div class="col-lg-12">
							<hr>
							<div class="row">
								@foreach ($list as $var) 
								<div class="col-md-3 col-xs-6">
									<a href="{{ route('yayin-evleri.kitaplar',$var->yayinci_url) }}" class="badge fsz12 d-flex  m3 badge-light site-color2 d-inline-block">{{ $var->yayinci_name }}</a>
								</div>
								@endforeach

							</div>
						</div>






					</div>
				</div>
			</div>



		</div>
	</div>
</div>


@endsection