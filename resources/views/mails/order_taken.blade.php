<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<title></title>
	<!--[if !mso]> <!-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<![endif]--> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
		#outlook a { padding: 0; }
		.ReadMsgBody { width: 100%; }
		.ExternalClass { width: 100%; }
		.ExternalClass * { line-height:100%; }
		body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
		table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
		img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
		p { display: block; margin: 13px 0; }
	</style>
	<!--[if !mso]><!-->
	<style type="text/css">
		@media only screen and (max-width:480px) {
			@-ms-viewport { width:320px; }
			@viewport { width:320px; }
		}
	</style>
	<!--<![endif]-->
<!--[if mso]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]-->
<!--[if lte mso 11]>
<style type="text/css">
  .outlook-group-fix {
    width:100% !important;
  }
</style>
<![endif]-->

<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
<style type="text/css">

	@import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);

</style>
<!--<![endif]--><style type="text/css">
	.ks-logo {
		font-size: 18px;
		text-decoration: none;
		color: #3a529b;
		font-weight: bold;
	}

	.ks-link {
		color: #22a7f0;
		text-decoration: none;
	}

	p {
		font-size: 14px;
		color: #333;
	}
</style><style type="text/css">
	@media only screen and (min-width:480px) {
		.mj-column-per-100 { width:100%!important; }
	}
</style>
</head>
<body style="background: #f5f6fa;">

  <div class="mj-container" style="background-color:#f5f6fa;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:20px;padding-top:30px;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="vertical-align:undefined;width:600px;">
      <![endif]--><div style="line-height:60px;text-align:center;"><img src="https://www.anadolukitap.com/uploads/content/logo.png" height="60" width="219"  alt="ANADOLU KİTAP"></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
    <![endif]-->
      <!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;background:#fff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#fff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="vertical-align:top;width:600px;">
          <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="left"><div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p>
          	Merhaba <b>{{ $order->order_name }}</b>,<br>
          	AnadoluKitap.com Siparişiniz alındı. 

          </p></div></td></tr>

          <tr><td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="left"><div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;">
            
          	<hr>

          </div></td></tr>

          <tr><td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="left"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="left" border="0"><tbody><tr><td style="border:none;border-radius:2px;color:#fff;cursor:auto;padding:12px 30px;" align="center" valign="middle" bgcolor="#3a529b"><a href="https://www.anadolukitap.com/panel/login/" style="text-decoration:none;background:#3a529b;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;font-weight:500;line-height:120%;text-transform:none;margin:0px;" target="_blank">Anadolu Kitap</a></td></tr></tbody></table></td></tr><tr><td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="left"><div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p>

          </p>
          <p>
          	Paneliniz veya siparişleriniz ile alakalı tüm isteklerinizi <a href="mailto:bilgi@anadolukitap.com">bilgi@anadolukitap.com</a> adresine mail atarak veya panelinizdeki destek formunu kullanarak iletebilirsiniz.
                        </p></div></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
    <![endif]-->
      <!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="vertical-align:undefined;width:300px;">
      <![endif]--><div style="cursor:auto;color:#858585;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:12px;line-height:22px;text-align:center;">{{ set('desc') }} <br> Anadolu Kitap :: Anadolu Kadar  <a href="#" class="ks-link">Farklı</a></div><!--[if mso | IE]>
      </td><td style="vertical-align:undefined;width:300px;">
      <![endif]--><div style="cursor:auto;color:#858585;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:12px;line-height:22px;text-align:center;">{{ set('desc') }} | {{ set('tel') }}</div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
    <![endif]--></div>
  </body>
  </html>