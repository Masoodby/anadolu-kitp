<?php

Route::group(['prefix'=>'crudv4', 'namespace'=>'Admin'], function(){
	Route::redirect('/', '/crudv4/home');
	Route::match(['get','post'],'/login','UserController@login')->name('crudv4.login');

	Route::get('/logout','UserController@logout')->name('crudv4.logout');


	Route::group(['middleware'=>'admin'], function ()
	{
		Route::get('/home','HomeController@index')->name('crudv4.home');

		Route::get('/resim','HomeController@resim')->name('crudv4.resim');

		Route::group(['prefix'=>'user'], function ()
		{
			Route::get('/', 'UserController@index')->name('crudv4.user');
			Route::get('/admin', 'UserController@index')->name('crudv4.user.admin');
			Route::get('/member', 'UserController@member')->name('crudv4.user.member');
			Route::get('/add', 'UserController@form')->name('crudv4.user.add');
			Route::get('/edit/{id}', 'UserController@form')->name('crudv4.user.edit');
			Route::post('/save/{id?}', 'UserController@save')->name('crudv4.user.save');
			Route::get('/del/{id}', 'UserController@del')->name('crudv4.user.del');
		});

		Route::group(['prefix'=>'category'], function ()
		{
			Route::match(['get','post'],'/', 'CategoryController@index')->name('crudv4.category');
			Route::get('/add', 'CategoryController@form')->name('crudv4.category.add');
			Route::get('/edit/{id}', 'CategoryController@form')->name('crudv4.category.edit');
			Route::post('/save/{id?}', 'CategoryController@save')->name('crudv4.category.save');
			Route::post('/update/{id?}', 'CategoryController@update')->name('crudv4.category.update');
			Route::get('/del/{id}', 'CategoryController@del')->name('crudv4.category.del');
			Route::get('/sortable', 'CategoryController@sortable')->name('crudv4.category.sortable');
			Route::post('/save-menu', 'CategoryController@save_menu')->name('crudv4.category.save-menu');
			Route::post('/savesort', 'CategoryController@savesort')->name('crudv4.category.savesort');
			Route::get('/optcat', 'CategoryController@optcat')->name('crudv4.category.optcat');
			Route::get('/oldnew', 'CategoryController@oldnew')->name('crudv4.category.oldnew');
			Route::post('/esles', 'CategoryController@esles')->name('crudv4.category.esles');
			Route::get('/unesles/{id?}/{nid?}', 'CategoryController@unesles')->name('crudv4.category.unesles');
		});

		Route::group(['prefix'=>'product'], function ()
		{
			Route::match(['get','post'],'/', 'ProductController@liste')->name('crudv4.product');
			Route::get('/listeci', 'ProductController@listeci')->name('crudv4.product.listeci');
			Route::match(['get','post'],'/list', 'ProductController@index')->name('crudv4.product.list');
			Route::get('/add', 'ProductController@form')->name('crudv4.product.add');
			Route::get('/edit/{id}', 'ProductController@form')->name('crudv4.product.edit');
			Route::post('/save/{id?}', 'ProductController@save')->name('crudv4.product.save');
			Route::post('/update/{id?}', 'ProductController@update')->name('crudv4.product.update');
			Route::match(['get','post'],'/updateimg/{id?}', 'ProductController@updateimg')->name('crudv4.product.updateimg');
			Route::match(['get','post'],'/updateimgList/{id?}', 'ProductController@updateimgList')->name('crudv4.product.updateimgList');
			Route::get('/del/{id}', 'ProductController@del')->name('crudv4.product.del');
			Route::post('/eraseimage/{id}', 'ProductController@eraseimage')->name('crudv4.product.eraseimage');
			Route::post('/yazar_ara', 'ProductController@yazar_ara')->name('crudv4.product.yazar_ara');
			Route::post('/yayinci_ara', 'ProductController@yayinci_ara')->name('crudv4.product.yayinci_ara');
			Route::get('/getResim', 'ProductController@getResim')->name('crudv4.product.getResim');
			Route::get('/gecici', 'ProductController@gecici')->name('crudv4.product.gecici');
			Route::get('/jpgCreate', 'ProductController@jpgCreate')->name('crudv4.product.jpgCreate');
		});

		Route::group(['prefix'=>'order'], function ()
		{
			Route::match(['get','post'],'/', 'OrderController@index')->name('crudv4.order');
			Route::get('/add', 'OrderController@form')->name('crudv4.order.add');
			Route::get('/detail/{id}', 'OrderController@detail')->name('crudv4.order.detail');
			Route::post('/orderUpdate/{id}', 'OrderController@orderUpdate')->name('crudv4.order.orderUpdate');
			Route::post('/save/{id?}', 'OrderController@save')->name('crudv4.order.save');
			Route::get('/del/{id}', 'OrderController@del')->name('crudv4.order.del');
			Route::get('/cargoEtiket/{id}', 'OrderController@cargoEtiket')->name('crudv4.order.cargoEtiket');
		});


		Route::group(['prefix'=>'combo'], function ()
		{
			Route::get('/', 'ComboController@index')->name('crudv4.combo');
			Route::get('/add', 'ComboController@form')->name('crudv4.combo.add');
			Route::get('/edit/{id}', 'ComboController@form')->name('crudv4.combo.edit');
			Route::post('/save/{id?}', 'ComboController@save')->name('crudv4.combo.save');
			Route::post('/update/{id?}', 'ComboController@update')->name('crudv4.combo.update');
			Route::get('/del/{id}', 'ComboController@del')->name('crudv4.combo.del');
            Route::post('/urunara','ComboController@urunara')->name('combo.urunara');
		});
		Route::group(['prefix'=>'slider'], function ()
		{
			Route::get('/', 'SliderController@index')->name('crudv4.slider');
			Route::get('/add', 'SliderController@form')->name('crudv4.slider.add');
			Route::get('/edit/{id}', 'SliderController@form')->name('crudv4.slider.edit');
			Route::post('/save/{id?}', 'SliderController@save')->name('crudv4.slider.save');
			Route::post('/update/{id?}', 'SliderController@update')->name('crudv4.slider.update');
			Route::get('/del/{id}', 'SliderController@del')->name('crudv4.slider.del');
		});
		Route::group(['prefix'=>'timeslider'], function ()
		{
			Route::get('/', 'TimeSliderController@index')->name('crudv4.tslider');
			Route::get('/add', 'TimeSliderController@form')->name('crudv4.tslider.add');
			Route::get('/edit/{id}', 'TimeSliderController@form')->name('crudv4.tslider.edit');
			Route::post('/save/{id?}', 'TimeSliderController@save')->name('crudv4.tslider.save');
			Route::post('/update/{id?}', 'TimeSliderController@update')->name('crudv4.tslider.update');
			Route::get('/del/{id}', 'TimeSliderController@del')->name('crudv4.tslider.del');
		});

		Route::group(['prefix'=>'banner'], function ()
		{
			Route::get('/', 'BannerController@index')->name('crudv4.banner');
			Route::get('/add', 'BannerController@form')->name('crudv4.banner.add');
			Route::get('/edit/{id}', 'BannerController@form')->name('crudv4.banner.edit');
			Route::post('/save/{id?}', 'BannerController@save')->name('crudv4.banner.save');
			Route::post('/update/{id}', 'BannerController@update')->name('crudv4.banner.update');
			Route::get('/del/{id}', 'BannerController@del')->name('crudv4.banner.del');
		});

		Route::group(['prefix'=>'pages'], function ()
		{
			Route::get('/', 'PageController@index')->name('crudv4.pages');
			Route::get('/add', 'PageController@form')->name('crudv4.pages.add');
			Route::get('/edit/{id}', 'PageController@form')->name('crudv4.pages.edit');
			Route::post('/save/{id?}', 'PageController@save')->name('crudv4.pages.save');
			Route::post('/update/{id?}', 'PageController@update')->name('crudv4.pages.update');
			Route::get('/del/{id}', 'PageController@del')->name('crudv4.pages.del');
		});


		Route::group(['prefix'=>'blogs'], function ()
		{
			Route::get('/', 'BlogController@index')->name('crudv4.blogs');
			Route::get('/add', 'BlogController@form')->name('crudv4.blogs.add');
			Route::get('/edit/{id}', 'BlogController@form')->name('crudv4.blogs.edit');
			Route::post('/save/{id?}', 'BlogController@save')->name('crudv4.blogs.save');
			Route::post('/update/{id?}', 'BlogController@update')->name('crudv4.blogs.update');
			Route::get('/del/{id}', 'BlogController@del')->name('crudv4.blogs.del');
		});




		Route::group(['prefix'=>'settings'], function ()
		{
			Route::get('/index/{id?}', 'SettingController@index')->name('crudv4.settings.index');
			Route::get('/create', 'SettingController@create')->name('crudv4.settings.create');
			Route::post('/store', 'SettingController@store')->name('crudv4.settings.store');
			Route::post('/ajax', 'SettingController@ajax')->name('crudv4.settings.ajax');
			Route::post('/ajaxUpload/{id?}', 'SettingController@ajaxUpload')->name('crudv4.settings.ajaxUpload');
			Route::get('/show/{id?}', 'SettingController@show')->name('crudv4.settings.show');
			Route::post('/update/{id?}', 'SettingController@update')->name('crudv4.settings.update');
			Route::get('/destroy/{id?}', 'SettingController@destroy')->name('crudv4.settings.destroy');
		});



		Route::group(['prefix'=>'yayinevi'], function ()
		{
			Route::match(['get','post'],'/', 'YayinciController@index')->name('crudv4.yayinevi');
			Route::get('/add', 'YayinciController@form')->name('crudv4.yayinevi.add');
			Route::get('/edit/{id}', 'YayinciController@form')->name('crudv4.yayinevi.edit');
			Route::post('/save/{id?}', 'YayinciController@save')->name('crudv4.yayinevi.save');
			Route::post('/update/{id?}', 'YayinciController@update')->name('crudv4.yayinevi.update');
			Route::get('/del/{id}', 'YayinciController@del')->name('crudv4.yayinevi.del');
			Route::get('/prices', 'YayinciController@prices')->name('crudv4.yayinevi.prices');
			Route::get('/price/{id}', 'YayinciController@price')->name('crudv4.yayinevi.price');
			Route::get('/yayinevi_islem', 'BookController@yayinevi_islem')->name('crudv4.yayinevi.yayinevi_islem');
			Route::get('/emekYayincilar', 'YayinciController@emekYayincilar')->name('crudv4.yayinevi.emekYayincilar');
		});



		Route::group(['prefix'=>'yazar'], function ()
		{
			Route::match(['get','post'],'/', 'YazarController@index')->name('crudv4.yazar');
			Route::get('/add', 'YazarController@form')->name('crudv4.yazar.add');
			Route::get('/edit/{id}', 'YazarController@form')->name('crudv4.yazar.edit');
			Route::post('/save/{id?}', 'YazarController@save')->name('crudv4.yazar.save');
			Route::post('/update/{id?}', 'YazarController@update')->name('crudv4.yazar.update');
			Route::get('/del/{id}', 'YazarController@del')->name('crudv4.yazar.del');
		});

		Route::group(['prefix'=>'depo'], function ()
		{
			Route::get('/add', 'DepoController@form')->name('crudv4.depo.add');
			Route::get('/edit/{id}', 'DepoController@form')->name('crudv4.depo.edit');
			Route::post('/save/{id?}', 'DepoController@save')->name('crudv4.depo.save');

			Route::get('/del/{id}', 'DepoController@del')->name('crudv4.depo.del');

		});



		Route::group(['prefix'=>'oda'], function ()
		{
			Route::get('/add', 'OdaController@form')->name('crudv4.oda.add');
			Route::get('/edit/{id}', 'OdaController@form')->name('crudv4.oda.edit');
			Route::post('/save/{id?}', 'OdaController@save')->name('crudv4.oda.save');

			Route::get('/del/{id}', 'OdaController@del')->name('crudv4.oda.del');
		});




		Route::group(['prefix'=>'raf'], function ()
		{
			Route::get('/list/{id?}', 'RafController@index')->name('crudv4.raf.list');

			Route::post('/islem/{id?}', 'RafController@islem')->name('crudv4.raf.islem');
			Route::post('/addbook', 'RafController@addbook')->name('crudv4.raf.addbook');
			Route::post('/adet/{id}', 'RafController@adet')->name('crudv4.raf.adet');

			Route::get('/del/{id}', 'RafController@del')->name('crudv4.raf.del');
		});



		Route::group(['prefix'=>'marketplaces'], function ()
		{

			Route::get('/add', 'MarketPlacesController@form')->name('crudv4.marketplaces.add');
			Route::get('/edit/{id}', 'MarketPlacesController@form')->name('crudv4.marketplaces.edit');
			Route::post('/save/{id?}', 'MarketPlacesController@save')->name('crudv4.marketplaces.save');
			Route::get('/del/{id}', 'MarketPlacesController@del')->name('crudv4.marketplaces.del');
			Route::get('/marketproduct/{pid?}', 'MarketPlacesController@marketproduct')->name('crudv4.marketplaces.marketproduct');
			Route::get('/marketproduct/{pid?}', 'MarketPlacesController@usePrice')->name('crudv4.marketplaces.usePrice');
		});




		Route::group(['prefix'=>'depolog'], function ()
		{
			Route::get('/analitik', 'BookController@index')->name('crudv4.depolog.analitik');
			Route::get('/depoLogs', 'BookController@depoLogs')->name('crudv4.depolog.depoLogs');
			Route::get('/stok', 'BookController@stokguncelle')->name('crudv4.depolog.stok');
			Route::get('/depoTosite', 'BookController@depoTosite')->name('crudv4.depolog.depoTosite');
			Route::get('/depolist', 'BookController@depolist')->name('crudv4.depolog.depolist');
			Route::get('/depoexit', 'BookController@depoexit')->name('crudv4.depolog.depoexit');
			Route::post('/exitdepo', 'BookController@exitdepo')->name('crudv4.depolog.exitdepo');
			Route::get('/exitdeporun/{id}', 'BookController@exitdeporun')->name('crudv4.depolog.exitdeporun');


		});




		Route::group(['prefix'=>'emek'], function ()
		{
			Route::get('/addBook/{id?}', 'EmekController@addBook')->name('crudv4.emek.addBook');
			Route::get('/oranUpdate', 'EmekController@oranUpdate')->name('crudv4.emek.oranUpdate');
			Route::get('/fileFolder', 'EmekController@fileFolder')->name('crudv4.emek.fileFolder');


		});


		Route::group(['prefix'=>'dnr'], function ()
		{
			Route::get('/addYayinci/{id?}', 'DnrBotController@addYayinci')->name('crudv4.dnr.addYayinci');
			Route::get('/kcenter/{isbn?}', 'DnrBotController@kcenter')->name('crudv4.dnr.kcenter');
			Route::get('/resim', 'DnrBotController@resim')->name('crudv4.dnr.resim');
			Route::get('/getProduct', 'DnrBotController@getProduct')->name('crudv4.dnr.getProduct');
			Route::get('/getProduct_page/{yid?}/{page?}/{last?}', 'DnrBotController@getProduct_page')->name('crudv4.dnr.getProduct_page');



		});

		Route::group(['prefix'=>'resimsiz'], function ()
		{
			Route::get('/list', 'ResimAraController@index')->name('crudv4.resimara');
			Route::get('/kcenter/{isbn?}', 'DnrBotController@kcenter')->name('crudv4.dnr.kcenter');
			Route::get('/resim', 'DnrBotController@resim')->name('crudv4.dnr.resim');
			Route::get('/getProduct', 'DnrBotController@getProduct')->name('crudv4.dnr.getProduct');
			Route::get('/getProduct_page/{yid?}/{page?}/{last?}', 'DnrBotController@getProduct_page')->name('crudv4.dnr.getProduct_page');



		});




		Route::group(['prefix'=>'trendyoljop'], function ()
		{
			Route::get('/stockUpdate', 'TrendyolJopController@stockUpdate')->name('crudv4.trendyoljop.stockUpdate');
			Route::post('/rafstockUpdate', 'TrendyolJopController@rafstockUpdate')->name('crudv4.trendyoljop.rafstockUpdate');
			Route::post('/stockUpdateById/{id}', 'TrendyolJopController@stockUpdateById')->name('crudv4.trendyoljop.stockUpdateById');
			Route::get('/stockUpdateId/{id}', 'TrendyolJopController@stockUpdateId')->name('crudv4.trendyoljop.stockUpdateId');
			Route::post('/getresult', 'TrendyolJopController@getresult')->name('crudv4.trendyoljop.getresult');
			Route::get('/emtpyresult', 'TrendyolJopController@emtpyresult')->name('crudv4.trendyoljop.emtpyresult');
			Route::get('/sync', 'TrendyolJopController@sync')->name('crudv4.trendyoljop.sync');
			Route::get('/update', 'TrendyolJopController@update')->name('crudv4.trendyoljop.update');
			Route::get('/readcsv', 'TrendyolJopController@readcsv')->name('crudv4.trendyoljop.readcsv');
			Route::get('/saveProduct/{page?}', 'TrendyolJopController@saveProduct')->name('crudv4.trendyoljop.saveProduct');
			Route::get('/updateByID/{id?}', 'TrendyolJopController@updateByID')->name('crudv4.trendyoljop.updateByID');
			Route::get('/zeroRep', 'TrendyolJopController@zeroRep')->name('crudv4.trendyoljop.zeroRep');




		});



		Route::prefix('trendyol')->group(function () {
			Route::get('/', 'TrendyolController@index')->name('crudv4.trendyol');
			Route::get('/catmatch', 'TrendyolController@catmatch')->name('crudv4.trendyol.catmatch');
			Route::get('/brand', 'TrendyolController@brand')->name('crudv4.trendyol.brand');
			Route::match(['get','post'],'/loadcat/{main}', 'TrendyolController@loadCat')->name('crudv4.trendyol.loadcat');
			Route::get('/del/{id}', 'TrendyolController@del')->name('crudv4.trendyol.del');
			Route::match(['get','post'],'/save', 'TrendyolController@save')->name('crudv4.trendyol.save');
			Route::match(['get','post'],'/getmatched/{id}', 'TrendyolController@getmatched')->name('crudv4.trendyol.getmatched');

			Route::post('/localbrand', 'TrendyolController@localbrand')->name('crudv4.trendyol.localbrand');
			Route::post('/remotebrand', 'TrendyolController@remotebrand')->name('crudv4.trendyol.remotebrand');
			Route::post('/save_brand', 'TrendyolController@save_brand')->name('crudv4.trendyol.save_brand');
			Route::get('/delbrand/{id}', 'TrendyolController@delbrand')->name('crudv4.trendyol.delbrand');
			Route::get('/products/{page?}', 'TrendyolController@products')->name('crudv4.trendyol.products');
			Route::get('/orders/{status?}', 'TrendyolController@orders')->name('crudv4.trendyol.orders');
			Route::get('/getOrder/{id?}', 'TrendyolController@getOrder')->name('crudv4.trendyol.getOrder');
			Route::get('/picking/{id?}', 'TrendyolController@Picking')->name('crudv4.trendyol.picking');
			Route::get('/cargoEtiket/{id?}', 'TrendyolController@cargoEtiket')->name('crudv4.trendyol.cargoEtiket');
			Route::get('/cargoEtiket2/{id?}', 'TrendyolController@cargoEtiket2')->name('crudv4.trendyol.cargoEtiket2');
			Route::post('/exitdepo/{id?}', 'TrendyolController@exitdepo')->name('crudv4.trendyol.exitdepo');

			Route::get('/catRequired/{id?}', 'TrendyolController@catRequired')->name('crudv4.trendyol.catRequired');
			Route::get('/adremotewriter', 'TrendyolController@adremotewriter')->name('crudv4.trendyol.adremotewriter');
			Route::match(['get','post'],'/changeCat/{id?}', 'TrendyolController@changeCat')->name('crudv4.trendyol.changeCat');

		});




		Route::group(['prefix'=>'n11'], function ()
		{
			Route::get('/', 'N11Controller@index')->name('crudv4.n11');
			Route::get('/catmatch', 'N11Controller@catmatch')->name('crudv4.n11.catmatch');
			Route::match(['get','post'],'/loadcat/{main}', 'N11Controller@loadCat')->name('crudv4.n11.loadcat');
			Route::get('/brand', 'N11Controller@brand')->name('crudv4.n11.brand');

			Route::get('/del/{id}', 'N11Controller@del')->name('crudv4.n11.del');
			Route::match(['get','post'],'/save', 'N11Controller@save')->name('crudv4.n11.save');
			Route::match(['get','post'],'/getmatched/{id}', 'N11Controller@getmatched')->name('crudv4.n11.getmatched');

			Route::post('/localbrand', 'N11Controller@localbrand')->name('crudv4.n11.localbrand');
			Route::post('/remotebrand', 'N11Controller@remotebrand')->name('crudv4.n11.remotebrand');
			Route::post('/save_brand', 'N11Controller@save_brand')->name('crudv4.n11.save_brand');
			Route::get('/delbrand/{id}', 'N11Controller@delbrand')->name('crudv4.n11.delbrand');
			Route::get('/products/{page?}', 'N11Controller@products')->name('crudv4.n11.products');
			Route::get('/orders/{status?}', 'N11Controller@orders')->name('crudv4.n11.orders');
			Route::get('/getOrder/{id?}', 'N11Controller@getOrder')->name('crudv4.n11.getOrder');
			Route::get('/picking/{id?}', 'N11Controller@Picking')->name('crudv4.n11.picking');
			Route::get('/cargoEtiket/{id?}', 'N11Controller@cargoEtiket')->name('crudv4.n11.cargoEtiket');
			Route::post('/exitdepo/{id?}', 'N11Controller@exitdepo')->name('crudv4.n11.exitdepo');

			Route::get('/catRequired/{id?}', 'N11Controller@catRequired')->name('crudv4.n11.catRequired');


		});



		Route::group(['prefix'=>'n11jop'], function ()
		{

			Route::get('/saveProduct', 'N11JopController@saveProduct')->name('crudv4.n11jop.saveProduct');
			Route::get('/delProductAll/{page?}', 'N11JopController@delProductAll')->name('crudv4.n11jop.delProductAll');
			Route::get('/saveOrder', 'N11JopController@saveOrder')->name('crudv4.n11jop.saveOrder');
			Route::get('/stok_update/{indis?}', 'N11JopController@stok_update')->name('crudv4.n11jop.stok_update');
			Route::get('/price_update/{indis?}', 'N11JopController@price_update')->name('crudv4.n11jop.price_update');


		});



		Route::group(['prefix'=>'hb'], function ()
		{

			Route::get('/', 'HbController@index')->name('crudv4.hb');
			Route::get('/stokUpdate', 'HbController@stokUpdate')->name('crudv4.hb.stokUpdate');
			Route::get('/addProduct', 'HbController@addProduct')->name('crudv4.hb.addProduct');

			Route::get('/catmatch', 'HbController@catmatch')->name('crudv4.hb.catmatch');
			Route::get('/brand', 'HbController@brand')->name('crudv4.hb.brand');
			Route::match(['get','post'],'/loadcat/{main}', 'HbController@loadCat')->name('crudv4.hb.loadcat');
			Route::get('/del/{id}', 'HbController@del')->name('crudv4.hb.del');
			Route::match(['get','post'],'/saveMatchCat', 'HbController@saveMatchCat')->name('crudv4.hb.saveMatchCat');
			Route::match(['get','post'],'/getmatched/{id}', 'HbController@getmatched')->name('crudv4.hb.getmatched');

			Route::post('/localbrand', 'HbController@localbrand')->name('crudv4.hb.localbrand');
			Route::post('/remotebrand', 'HbController@remotebrand')->name('crudv4.hb.remotebrand');
			Route::post('/save_brand', 'HbController@save_brand')->name('crudv4.hb.save_brand');
			Route::get('/cokluEtiket', 'HbController@cokluEtiket')->name('crudv4.hb.cokluEtiket');
			Route::get('/products/{page?}', 'HbController@products')->name('crudv4.hb.products');
			Route::get('/saveProductLocal', 'HbController@saveProductLocal')->name('crudv4.hb.saveProductLocal');
			Route::get('/orders', 'HbController@orders')->name('crudv4.hb.orders');
			Route::get('/toCargo/{page?}', 'HbController@toCargo')->name('crudv4.hb.toCargo');
			Route::get('/getOrder/{id?}', 'HbController@getOrder')->name('crudv4.hb.getOrder');
			Route::get('/paketdetay/{id?}', 'HbController@paketdetay')->name('crudv4.hb.paketdetay');
			Route::get('/picking/{id?}', 'HbController@Picking')->name('crudv4.hb.picking');
			Route::get('/cargoEtiket/{id?}', 'HbController@cargoEtiket')->name('crudv4.hb.cargoEtiket');
			Route::get('/cargoEtiket2/{id?}', 'HbController@cargoEtiket2')->name('crudv4.hb.cargoEtiket2');
			Route::post('/exitdepo/{id?}', 'HbController@exitdepo')->name('crudv4.hb.exitdepo');
			Route::post('/getresult', 'HbController@getresult')->name('crudv4.hb.getresult');

			Route::get('/catRequired/{id?}', 'HbController@catRequired')->name('crudv4.hb.catRequired');
			Route::get('/excel', 'HbController@excel')->name('crudv4.hb.excel');
			Route::match(['get','post'],'/changeCat/{id?}', 'HbController@changeCat')->name('crudv4.hb.changeCat');



		});





	});
});
