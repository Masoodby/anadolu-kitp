<?php

Route::get('/','HomeController@index')->name('anasayfa');
Route::get('/sitemap.xml','XmlFeedController@sitemap')->name('sitemap.xml');
Route::get('/googleFeed','XmlFeedController@googleFeed')->name('googleFeed');

//Route::view('/urun','urun');

Route::get('/kategori/{cat_url}','CategoryController@index')->name('kategori');
Route::get('/kurumsal/{page_url}','PageController@index')->name('kurumsal');
Route::get('/yardim/{page_url}','PageController@help')->name('yardim');
Route::get('/blog/{page_url}','PageController@blog')->name('blog');
Route::post('/page/{id}','PageController@page')->name('page');
Route::get('en-cok-okunan-kitaplar','CategoryController@bestbook')->name('en-cok-okunan-kitaplar');
Route::get('yeni-kitaplar','CategoryController@newbook')->name('yeni-kitaplar');
Route::get('yayin-evleri/{harf?}','YayineviController@index')->name('yayin-evleri');
Route::get('yayin-evleri/kitaplar/{page_url}','YayineviController@books')->name('yayin-evleri.kitaplar');
Route::get('yazarlar/{harf?}','YazarController@index')->name('yazarlar');
Route::get('yazarlar/kitaplar/{page_url}','YazarController@books')->name('yazarlar.kitaplar');

Route::get('/urun/{categories_url}/{product_url}','ProductController@index')->name('urun');
Route::get('/kitap-setleri/{slug}','ProductController@combo')->name('kitap-setleri');

Route::get('/arama','ProductController@search')->name('arama');
Route::post('/ajaxSearch','ProductController@ajaxSearch')->name('ajaxSearch');


//Route::get('/sepet','BasketController@index')->name('sepet'); //->middleware('auth');
Route::group(['prefix'=>'sepet'], function(){
	Route::get('/','BasketController@index')->name('sepet');
	Route::post('/add','BasketController@add')->name('sepet.add');
	Route::post('/addcombo','BasketController@addcombo')->name('sepet.addcombo');
	Route::delete('/remove','BasketController@remove')->name('sepet.remove');
	Route::delete('/removeAll','BasketController@removeAll')->name('sepet.removeAll');
	Route::post('/edit','BasketController@edit')->name('sepet.edit');
	Route::get('/loadtopcart','BasketController@loadtopcart')->name('sepet.loadtopcart');

});




Route::group(['prefix'=>'favori'], function(){
	Route::get('/','WishlistController@index')->name('favori');
	Route::post('/add','WishlistController@add')->name('favori.add');
	Route::delete('/remove','WishlistController@remove')->name('favori.remove');
	Route::delete('/removeAll','WishlistController@removeAll')->name('favori.removeAll');
	Route::post('/edit','WishlistController@edit')->name('favori.edit');
	Route::post('/countwishlist','WishlistController@countwishlist')->name('favori.countwishlist');

});

Route::get('/siparisi-tamamla','PaymentController@index')->name('siparisi-tamamla');
Route::get('/hizli-satin-al','PaymentController@quickOrder')->name('hizli-satin-al');

Route::post('/save-order','PaymentController@SaveOrder')->name('save-order');
Route::post('/save','PaymentController@save')->name('save');
Route::post('/paymentcallback','PaymentController@PaytrCallBack')->name('paymentcallback');
Route::get('/payment3d','PaymentController@PaytrForm')->name('payment3d');
Route::get('/siparis-basarili','PaymentController@success')->name('siparis-basarili');
Route::get('/siparis-basarisiz','PaymentController@error')->name('siparis-basarisiz');
Route::post('/get-counties','PaymentController@getCounties')->name('get-counties');

Route::get('/mng', 'MngController@siparis')->name('tracking');

Route::group(['middleware'=>'auth'], function ()
{

	Route::get('/siparisler','OrderController@index')->name('siparisler');
	Route::get('/siparisler/{id}','OrderController@show')->name('siparis');
});




Route::group(['middleware'=>'auth'], function ()
{

	Route::group(['prefix'=>'panelim'], function()
	{
		Route::get('/','AccountController@index')->name('panelim');
		Route::get('/siparisler','AccountController@orders')->name('panelim.siparisler');
		Route::get('/siparis/{id?}','AccountController@order')->name('panelim.siparis');
		Route::get('/adreslerim','AccountController@adres')->name('panelim.adreslerim');
		Route::get('/destek','AccountController@support')->name('panelim.destek');
		Route::post('/adres-ekle','AccountController@addresAdd')->name('panelim.adres-ekle');
		Route::post('/fat-adres-ekle','AccountController@fataddresAdd')->name('panelim.fat-adres-ekle');


		Route::get('/adres-form/{id?}','AccountController@adresForm')->name('panelim.adres-form');
		Route::get('/fat-adres-form/{id?}','AccountController@fatadresForm')->name('panelim.fat-adres-form');

		Route::post('/adres-edit/{id?}','AccountController@adresEdit')->name('panelim.adres-edit');
		Route::post('/fat-adres-edit/{id?}','AccountController@fatadresEdit')->name('panelim.fat-adres-edit');



		Route::get('/editprofil','AccountController@editProfil')->name('panelim.editprofil');

		Route::put('/editProfilSave','AccountController@editProfilSave')->name('panelim.editProfilSave');

		Route::post('/change-address','AccountController@changeAdress')->name('panelim.change-address');
		Route::get('/erase-adres/{id?}','AccountController@delAdres')->name('panelim.erase-adres');




		Route::get('/talep-form/{id?}','AccountController@talepForm')->name('panelim.talep-form');
		Route::post('/talep-ekle','AccountController@talepAdd')->name('panelim.talep-ekle');

		Route::get('/talep-detay/{id?}','AccountController@talepDetay')->name('panelim.talep-detay');


	});
});




///////////////////////user-panel///////////////////////
Route::group(['prefix'=>'panel'], function()
{


	Route::get('/login/{back?}','PanelController@loginform')->name('panel.login');
	Route::post('/login','PanelController@login');

	Route::get('/register','PanelController@register_form')->name('panel.register');
	Route::post('/register','PanelController@register')->name('panel.registerx');
	Route::post('/loginAjax','PanelController@loginAjax');

	Route::post('recoveryPass','PanelController@recoveryPass')->name('recoveryPass');

	Route::post('/logout','PanelController@logout')->name('panel.logout');

});



