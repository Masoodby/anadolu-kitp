<?php

use Illuminate\Support\Facades\Route;




require __DIR__.'/admin_routes.php';

require __DIR__.'/frontend_routes.php';

Route::get('/clear', function() {
	Artisan::call('cache:clear');
	Artisan::call('view:clear');
	Artisan::call('config:clear');
	//Artisan::call('route:clear');
	return "Cache is cleared";
});



Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'admin']], function () {
	\UniSharp\LaravelFilemanager\Lfm::routes();
});

